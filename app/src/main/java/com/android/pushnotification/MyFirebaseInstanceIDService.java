package com.android.pushnotification;

import android.util.Log;

import com.android.helper.UserPref;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Afycn on 3/29/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("refrence token id", "Refreshed token: " + refreshedToken);
        UserPref.getuser_instance(getApplicationContext()).setUserToken(refreshedToken);

        //   String token = instanceID.getToken(context.getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

        //send token to app server
    }
}
