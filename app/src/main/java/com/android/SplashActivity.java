package com.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.helper.UserPref;
import com.android.stafftraderapp.R;


public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 2000;
    private ImageView splash_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (UserPref.getuser_instance(this).getAutoLogin()) {
            Intent i = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        } else {
            setSplash();
        }
    }
    private void setSplash() {
        splash_image = (ImageView) findViewById(R.id.splash_image);
        final Animation animTranslate = AnimationUtils.loadAnimation(this,
                R.anim.translate);
        splash_image.startAnimation(animTranslate);
        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                splash_image.clearAnimation();
                splash_image.clearAnimation();
                Intent i = new Intent(SplashActivity.this, ChooseCredential.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
