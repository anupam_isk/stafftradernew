package com.android;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.GsonResponse.CategoryResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.LoginUserResponse;
import com.android.ServerCommunication.CallService;
import com.android.SinchCommunication.BaseActivity;
import com.android.SinchCommunication.SinchService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.helper.Validation;
import com.android.stafftraderapp.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.sinch.android.rtc.SinchError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ChooseCredential extends BaseActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ActivityCompat.OnRequestPermissionsResultCallback {
    private TextView want_to_recruite, need_a_job, need_to_hire, login, signin, signup, forgot_password;
    private LinearLayout signin_layout, signup_layout;
    private String emei="";
    private static final int PERMISSION_REQUEST_CODE = 1;
    private ProgressDialog mSpinner;
    private EditText emailID, password;
    private static final long INTERVAL = 1000 * 10;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final String RECURITER = "recuriter";
    private static final String CANDIDATE = "candidate";
    private static final String MANAGER = "manager";
    private static final String LOGIN = "login";
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private int REQUEST_LOCATION = 1;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    public static CharSequence[] items = null;
    Location mLastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aftersplash);

        buildGoogleApiClient();
        initializeUI();
        setListener();
        //  getCategoryData();
        setLayoutVisibility();
    }

    private void setLayoutVisibility() {
        signin_layout.setVisibility(View.VISIBLE);
        signup_layout.setVisibility(View.GONE);
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            Log.e("api client is null", "api client is null");
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }

    private void getCategoryData() {
        CallService.getInstance().passCategoryinformation(api_listener, Constants.CATEGORY_URL, false, "3", ChooseCredential.this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        signin_layout.setVisibility(View.GONE);
        signup_layout.setVisibility(View.VISIBLE);
        Log.e("on restart", "on restart");
    }

  /*  @Override
    public void onStarted() {
        Log.e("started in staff sinch", "on started in staff sinch");
        openHomeActivity();
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        if (mSpinner != null) {
            mSpinner.dismiss();
        }
    }

    @Override
    protected void onServiceConnected() {
        Log.e("login connected", "login connected");
        getSinchServiceInterface().setStartListener(this);
    }
*/
    @Override
    protected void onPause() {
     /*   if (mSpinner != null) {
            mSpinner.dismiss();
        }*/
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            Log.e("api client is not  null", "api client is not  null");
            mGoogleApiClient.connect();
        }
        Log.e("on resume", "on resume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("on start", "on start");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_LOCATION);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        } else {
            Log.e("in else check ","in else check");
            new asyn().execute("");
            emei = getEmeiNo();
            getUserPop();
        }
    }

    private void getUserPop() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates LS_state = locationSettingsResult.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        getLocation();
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        Log.e("success", "success");

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ChooseCredential.this, REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                emei = getEmeiNo();
                new asyn().execute("");
                // We can now safely use the API we requested access to
                getUserPop();
            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();
                        // All required changes were successfully made
                        Log.e("success 2", "success 2");//FINALLY YOUR OWN METHOD TO GET YOUR USER LOCATION HERE
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to

                        break;
                    default:
                        break;
                }
                break;
        }
    }


    private void initializeUI() {
        want_to_recruite = (TextView) findViewById(R.id.want_to_recruite);
        need_a_job = (TextView) findViewById(R.id.need_a_job);
        need_to_hire = (TextView) findViewById(R.id.need_to_hire);
        login = (TextView) findViewById(R.id.login);
        signin = (TextView) findViewById(R.id.signin);
        signup = (TextView) findViewById(R.id.signup);
        signin_layout = (LinearLayout) findViewById(R.id.signin_layout);
        signup_layout = (LinearLayout) findViewById(R.id.signup_layout);
        emailID = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    sendLoginInformation();
                    return true;
                }
                return false;
            }
        });
    }

    private void setListener() {
        want_to_recruite.setOnClickListener(this);
        need_a_job.setOnClickListener(this);
        need_to_hire.setOnClickListener(this);
        login.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
        signin.setOnClickListener(this);
        signup.setOnClickListener(this);
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    private void sendLoginInformation() {
        if (NetworkCheck.getInstance(this).isNetworkAvailable()) {
            if (validateLogin()) {
                HashMap<String, String> params = new HashMap<>();
                params.put("e", emailID.getText().toString());
                params.put("p", password.getText().toString());
                params.put("token_id", UserPref.getuser_instance(this).getUserToken());
                //  params.put("token", "AFcWxV21C7fd0v3bYYYRCpSSRl31AcsAW04eK1GU5HZCj8LnCCFBd");
                params.put("imei", emei);
                params.put("device_type", "2");
                CallService.getInstance().passInfromation(api_listener, Constants.LOGIN_URL, params, true, "1", ChooseCredential.this);
            }
        } else {
            UtilityPermission.showToast(this, Constants.NETWORK_STRING);
        }
    }

    private boolean validateLogin() {
        if (Validation.isValidEmail(emailID.getText().toString()) && emailID.getText().toString().length() > 0) {
            if (password.getText().toString().length() > 0) {
                return true;
            } else {
                password.setError("Please Enter Password");
            }
        } else {
            emailID.setError("Please Enter Valid Email Id");
        }
        return true;
    }

    private void showSpinner() {
        mSpinner = new ProgressDialog(this);
        mSpinner.setTitle("Logging in");
        mSpinner.setMessage("Please wait...");
        mSpinner.show();
    }

    public class asyn extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String firebase_token = FirebaseInstanceId.getInstance().getToken();
            //  String token = instanceID.getToken(getApplicationContext().getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.e("firebase token", firebase_token);
            UserPref.getuser_instance(ChooseCredential.this).setUserToken(firebase_token);
            // Log.v("dsffffffffffff", token.intern());

            return null;
        }
    }

    @Override
    public void onClick(View v) {
        if (v == want_to_recruite) {
            goToNext(this, RECURITER);
        } else if (v == need_a_job) {
            goToNext(this, CANDIDATE);
        } else if (v == need_to_hire) {
            goToNext(this, MANAGER);
        } else if (v == login) {
            sendLoginInformation();
        } else if (v == forgot_password) {
            showPopup();
        } else if (v == signin) {
            signin_layout.setVisibility(View.VISIBLE);
            signup_layout.setVisibility(View.GONE);
            signin_layout.animate().alpha(0.0f);
            signin_layout.animate().alpha(1.0f);
            signin.setBackgroundColor(getResources().getColor(R.color.button_color));
            signin.setTextColor(getResources().getColor(android.R.color.white));
            signup.setBackgroundColor(getResources().getColor(android.R.color.white));
            signup.setTextColor(getResources().getColor(android.R.color.black));
        } else if (v == signup) {
            signin_layout.setVisibility(View.GONE);
            signup_layout.setVisibility(View.VISIBLE);
            signup_layout.animate().alpha(0.0f);
            signup_layout.animate().alpha(1.0f);
            signup.setBackgroundColor(getResources().getColor(R.color.button_color));
            signup.setTextColor(getResources().getColor(android.R.color.white));
            signin.setBackgroundColor(getResources().getColor(android.R.color.white));
            signin.setTextColor(getResources().getColor(android.R.color.black));
        }

    }

    private void goToNext(ChooseCredential chooseCredential, String type) {
        Intent i = null;
        if (type.equals(RECURITER)) {
            i = new Intent(chooseCredential, CommonRegistrationActivity.class);
            i.putExtra("role_id", Constants.RECRUITER_ROLEID);
        } else if (type.equals(CANDIDATE)) {
            i = new Intent(chooseCredential, CommonRegistrationActivity.class);
            i.putExtra("role_id", Constants.CANDIDATE_ROLEID);
        } else if (type.equals(MANAGER)) {
            i = new Intent(chooseCredential, CommonRegistrationActivity.class);
            i.putExtra("role_id", Constants.HIRINGMANAGER_ROLEID);
        }

        startActivity(i);


    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e("connected", "connected");
        getLocation();
    }

    private void getLocation() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            getCompleteAddressString(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            UserPref.getuser_instance(this).setLattitude(String.valueOf(mLastLocation.getLatitude()));
            UserPref.getuser_instance(this).setLongitude(String.valueOf(mLastLocation.getLongitude()));
            Log.e("lattiude", mLastLocation.getLatitude() + "");
            Log.e("longitude", mLastLocation.getLongitude() + "");
        } else {
            Log.e("mLastLocation is null", "null");
        }
    }

    private void getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                UserPref.getuser_instance(this).setStreet(returnedAddress.getAddressLine(0));
                Log.e("street", UserPref.getuser_instance(this).getStreet());
                UserPref.getuser_instance(this).setCity(returnedAddress.getLocality());
                UserPref.getuser_instance(this).setState(returnedAddress.getAdminArea());
                UserPref.getuser_instance(this).setCountry(returnedAddress.getCountryName());
               /* StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();*/

            }
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
            Log.e("My Current]address", "Canont get Address!");
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        buildGoogleApiClient();
        Log.e("call login", "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private String getEmeiNo() {
        TelephonyManager telephonyManager = (TelephonyManager) ChooseCredential.this
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    private void openHomeActivity() {
        Intent i = new Intent(ChooseCredential.this, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    private void showPopup() {
        LayoutInflater factory = LayoutInflater.from(this);
        final View hire_view = factory.inflate(R.layout.forgotpassword, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(hire_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        TextView back_to_login = (TextView) hire_view.findViewById(R.id.back_to_login);
        final EditText email = (EditText) hire_view.findViewById(R.id.email);
        Button button = (Button) hire_view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPasswordToTheirEmail(email.getText().toString());
            }
        });
        back_to_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    private void sendPasswordToTheirEmail(String s) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", s);
        params.put("user_id", UserPref.getuser_instance(this).getUserId());
        CallService.getInstance().passInfromation(api_listener, Constants.FORGET_PASSWORD, params, true, "2", ChooseCredential.this);
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (request_id.equals("3")) {
                if (response != null) {
                    CategoryResponse category_response = new Gson().fromJson(response, CategoryResponse.class);
                    if (category_response.isStatus()) {
                        ArrayList<CategoryResponse.CategoryType> list = category_response.getIndustry();
                        items = new CharSequence[list.size()];
                        for (int i = 0; i < list.size(); i++) {
                            items[i] = list.get(i).getIndustry_name();
                        }
                    } else {
                        UtilityPermission.showToast(ChooseCredential.this, "There is some error on server side");
                    }
                }
            }
            if (request_id.equals("1")) {
                if (response != null) {
                    Log.e("response ", response);
                    LoginUserResponse user_response = new Gson().fromJson(response, LoginUserResponse.class);
                    if (user_response.isStatus())
                    {
                        LoginUserResponse.UserData user_data = user_response.getData();
                        Log.e("status is true", user_response.isStatus() + "");
                        Log.e("user first name ", user_data.getFirst_name() + user_data.getId());
                        Log.e("user phone", user_data.getPhone());
                        Log.e("user1 image ", user_data.getImage());
                        if (user_data.getJob_title() != null)
                            Log.e("user job title", user_data.getJob_title());
                        UserPref.getuser_instance(ChooseCredential.this).setUserId(user_data.getId());
                        UserPref.getuser_instance(ChooseCredential.this).setUserFirstName(user_data.getFirst_name());
                        UserPref.getuser_instance(ChooseCredential.this).setUserLastName(user_data.getLast_name());
                        UserPref.getuser_instance(ChooseCredential.this).setUserEmail(user_data.getEmail());
                        UserPref.getuser_instance(ChooseCredential.this).setUserPhone(user_data.getPhone());
                        UserPref.getuser_instance(ChooseCredential.this).setRoleId(user_data.getRole_id());
                        UserPref.getuser_instance(ChooseCredential.this).setuserImage(user_data.getImage());
                        if (user_data.getJob_title() != null)
                            UserPref.getuser_instance(ChooseCredential.this).setuserjobtitle(user_data.getJob_title());
                        UserPref.getuser_instance(ChooseCredential.this).setAutoLogin(true);
                        Log.e("user image ", UserPref.getuser_instance(ChooseCredential.this).getUserImage());
                        Log.e("user pref data ", UserPref.getuser_instance(ChooseCredential.this).getUserFirstName());
                        Log.e("user pref job title ", UserPref.getuser_instance(ChooseCredential.this).getUserjobtitle());
                        openHomeActivity();
                        /*if (!getSinchServiceInterface().isStarted()) {
                            getSinchServiceInterface().startClient(UserPref.getuser_instance(ChooseCredential.this).getUserEmail());
                            showSpinner();
                        }*/
                    } else {
                        AlertManger.getAlert_instance().showAlert("Either Email id or Password is invalid.", ChooseCredential.this);
                    }
                } else {
                    Log.e("response is null", "respo");
                }
            } else if (request_id.equals("2")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        AlertManger.getAlert_instance().showAlert("Password has been sent to your Email.Please check Email.", ChooseCredential.this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    };
}
