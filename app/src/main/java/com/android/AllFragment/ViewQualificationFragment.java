package com.android.AllFragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.VideoView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewQualificationFragment extends Fragment {
    private EditText brief_bio, qualification, top_skills;
    private Button view_resume, view_video, update;
    private TextView select_resume, text_resume;
    private TableRow table_row, table_row_second;
    private String candidate_id = null;
    private File file1;
    private final int REQUEST_CODE_DOC = 3;
    private String file_nam = "", role_id="4";
    Context context;
    private onFragmentListener listener;

    public ViewQualificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    private void showkeyboard(EditText edit_text) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edit_text, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle b = getArguments();
        if (b != null) {
            if (b.containsKey("candidate_id")) {
                setVisibility(table_row, View.GONE);
                setVisibility(table_row_second, View.VISIBLE);
                setVisibility(update, View.VISIBLE);
                brief_bio.setFocusable(true);
                showkeyboard(brief_bio);
                qualification.setFocusable(true);
                top_skills.setFocusable(true);
                candidate_id = b.getString("candidate_id");
            } else {
                role_id = b.getString("role_id");
                if (role_id.equals("4"))
                    setVisibility(table_row, View.VISIBLE);
                else if (role_id.equals("3")) {
                    setVisibility(table_row, View.GONE);
                } else
                    setVisibility(table_row, View.GONE);
                setVisibility(table_row_second, View.GONE);
                setVisibility(update, View.GONE);
                brief_bio.setText(b.getString("bio"));
                qualification.setText(b.getString("qualification"));
                top_skills.setText(b.getString("skill"));
            }
        }
    }

    private void setVisibility(View table_row, int gone) {
        table_row.setVisibility(gone);
        table_row.setVisibility(gone);
        table_row.setVisibility(gone);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_DOC) {
            if (resultCode == Activity.RESULT_OK) {
                final String state = Environment.getExternalStorageState();
                Log.e("result ok", resultCode + "" + requestCode);
                String selectedPath = UtilityPermission.getPath(getActivity(), data.getData());
//                Log.e("selected path",selectedPath);
                File file = null;
                if (selectedPath != null) {
                    file = new File(selectedPath);
                } else {
                    Uri selectedFileURI = data.getData();
                    file = new File(selectedFileURI.getPath().toString());
                }

                // that file object send to server like image upload
                Log.e("", "File : " + file.getName() + " length " + file.length() + " PATH" + file.getPath());
                if (file.getPath().toString().contains(":")) {
                    StringTokenizer tokens = new StringTokenizer(file.getPath().toString(), ":");
                    String first = tokens.nextToken();
                    Log.e("", "first: " + first);
                    String file_1 = tokens.nextToken();
                    Log.e("", "second" + file_1);
                    file_nam = file_1;
                    String sd1path = "";
                    if (new File("/storage/extSdCard/").exists()) {
                        sd1path = "/storage/extSdCard/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("Sd Cardext Path", sd1path);
                    }
                    if (new File("/storage/sdcard1/").exists()) {
                        sd1path = "/storage/sdcard1/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("Sd Card1 Path", sd1path);
                    }
                    if (new File("/storage/usbcard1/").exists()) {
                        sd1path = "/storage/usbcard1/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("USB Path", sd1path);
                    }
                 /*   if(new File("/storage/sdcard0/").exists())
                    {
                        sd1path="/storage/sdcard0/";
                        file1 = new File(sd1path+
                                file_nam);
                        Log.e("Sd Card0 Path",sd1path);
                    }*/
                    if (new File("/storage/sdcard1/").exists()) {
                        sd1path = "/storage/sdcard1/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("Sd Card0 Path", sd1path);
                    }

                  /*  file1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                            file_nam);*/
                    //file1 = new File("/mnt/external_sd/"+file_nam);
                    Log.e("File Path : ", file1.getPath() + ", File size : " + file1.length() + " KB");
                } else {
                    // file_nam = file.getName().toString();
                    // file_nam= "Download/pdfurl_guide.pdf";
                    file1 = file;
                    file_nam = file1.getName();
                }
                try {
                    //  file = new File(selectedPath);
                    long length = file1.length();
                    length = length / 1024;
                    Log.e("File Path : ", file.getPath() + ", File size : " + length + " KB");
                    if (length > 2048) {
                        AlertManger.getAlert_instance().showAlert("You cant select more than 2 mb Resume", getActivity());
                    } else {
                        text_resume.setText(file_nam);
                    }
                } catch (Exception e) {
                    Log.e("File not found : ", e.getMessage() + e);
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_view_qualification, container, false);
        initializeUI(v);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    public static void showEmailPopup(String url, Context context) {
        LayoutInflater factory = LayoutInflater.from(context);
        final View deleteDialogView = factory.inflate(R.layout.show_video, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageButton image_button = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        final VideoView video_view = (VideoView) deleteDialogView.findViewById(R.id.video_player_view);
        Uri uri = Uri.parse(url);
        video_view.setVideoURI(uri);
        video_view.start();
        image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    private void initializeUI(View v) {
        brief_bio = (EditText) v.findViewById(R.id.brief_bio);
        qualification = (EditText) v.findViewById(R.id.qualification);
        table_row = (TableRow) v.findViewById(R.id.table_row);
        table_row_second = (TableRow) v.findViewById(R.id.table_row_second);
        top_skills = (EditText) v.findViewById(R.id.top_skills);
        update = (Button) v.findViewById(R.id.update);
        view_resume = (Button) v.findViewById(R.id.view_resume);
        select_resume = (TextView) v.findViewById(R.id.select_resume);
        text_resume = (TextView) v.findViewById(R.id.text_resume);
        view_video = (Button) v.findViewById(R.id.view_video);
        setListener();

    }

    private void setListener() {
        view_resume.setOnClickListener(click_listener);
        view_video.setOnClickListener(click_listener);
        update.setOnClickListener(click_listener);
        select_resume.setOnClickListener(click_listener);
    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == view_resume) {
                showResume("http://docs.google.com/viewer?url=http://online.verypdf.com/examples/pdfeditor.pdf", context);

            } else if (v == view_video) {
                showEmailPopup("http://iskdemo.com/stafftrader/public/upload/video/2016/11/22/58343e2a9a9df.3gp", context);
            } else if (v == select_resume) {
                getDocument();
            } else {
                sendToServer();
            }

        }
    };

    public static void showResume(String url, Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    private void getDocument() {
        Intent intent = new Intent();
        Log.e("version", String.valueOf(Build.VERSION.SDK_INT));
        if (Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.KITKAT) {

            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        } else
            intent.setAction(Intent.ACTION_GET_CONTENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/*");
        startActivityForResult(Intent.createChooser(intent, "Select a file "), REQUEST_CODE_DOC);
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            if (response != null) {
                try {
                    JSONObject json_string = new JSONObject(response);
                    if (json_string.getBoolean("status")) {
                        UtilityPermission.showToast(getActivity(), "Candidate Bio has been saved succesfully");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private void sendToServer() {
        HashMap<String, String> hmap = new HashMap<>();
        hmap.put("bio", brief_bio.getText().toString());
        hmap.put("qualification", qualification.getText().toString());
        hmap.put("skills", top_skills.getText().toString());
        hmap.put("candidate_id", candidate_id);
        CallService.getInstance().passDataToMultiPart(api_listener, Constants.UPLOAD_CANDIDATE_BIO_URL, hmap, true, file1, "1", getActivity(), "doc");
    }

}
