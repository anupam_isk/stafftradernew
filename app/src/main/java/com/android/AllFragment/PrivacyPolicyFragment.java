package com.android.AllFragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.android.FragmentInterface.onFragmentListener;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Anupam tyagi.
 */
public class PrivacyPolicyFragment extends Fragment {
    private onFragmentListener listener;
    private String constants_url = null;

    public PrivacyPolicyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().get("value").equals("policy"))
                constants_url = Constants.PRIVACY_POLICY_URL;
            else if (getArguments().get("value").equals("agree"))
                constants_url = Constants.TERMS_AND_CONDITIONS;
            else if (getArguments().get("value").equals("contact"))
                constants_url = Constants.CONTACT_US;
            else if (getArguments().get("value").equals("about"))
                constants_url = Constants.ABOUT_US;
            else
                constants_url = Constants.TERMS_AND_CONDITIONS;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        WebView story_data = (WebView) v.findViewById(R.id.privacy_policy_webview);
        startWebView(story_data, constants_url);
        return v;
    }

    private void startWebView(WebView story_data, String constants_url) {
        story_data.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        // Javascript inabled on webview
        story_data.getSettings().setJavaScriptEnabled(true);
        // Other webview options
        /*
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(true);
        */
        //Load url in webview
        story_data.loadUrl(constants_url);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }
}
