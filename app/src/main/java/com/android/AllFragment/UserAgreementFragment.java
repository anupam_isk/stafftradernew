package com.android.AllFragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.ChatActivity;
import com.android.FragmentInterface.AnotherListener;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.SinchCommunication.BaseFragment;
import com.android.SinchCommunication.SinchService;
import com.android.ViewProfileActivity;
import com.android.adapter.UserNotificationAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam Tyagi.
 */
public class UserAgreementFragment extends BaseFragment implements SearchView.OnQueryTextListener {
    private onFragmentListener listener;
    private UserNotificationAdapter adapter;
    private RecyclerView notification_list;
    private String check_value = null;
    private TableRow table_row;
    private Button button1, button2;
    private ArrayList<AllCandidateResponse.Candidate> candidates_list, managers_list, recruiters_list, common_list;

    public UserAgreementFragment() {
        // Required empty public constructor
    }

    /*  @Override
      public void onStop() {
          super.onStop();
          if (getSinchServiceInterface() != null) {
              getSinchServiceInterface().stopClient();
          }
      }*/
    @Override
    protected void onServiceConnected() {
       /* if (!getSinchServiceInterface().isStarted())
            getSinchServiceInterface().startClient(UserPref.getuser_instance(getActivity()).getUserEmail());
        String name = getSinchServiceInterface().getUserName();
        Log.e("on connected ", name + "on userfragment");*/
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null)
            check_value = getArguments().getString("check_value");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_agreement, container, false);
        intitializeUI(v);
        return v;
    }

    private void intitializeUI(View v) {
        notification_list = (RecyclerView) v.findViewById(R.id.recycler_view);
        table_row = (TableRow) v.findViewById(R.id.table_row);
        button1 = (Button) v.findViewById(R.id.button1);
        button2 = (Button) v.findViewById(R.id.button2);
        button1.setOnClickListener(click_listener);
        button2.setOnClickListener(click_listener);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        notification_list.setLayoutManager(mLayoutManager);
        notification_list.setItemAnimator(new DefaultItemAnimator());
        notification_list.addItemDecoration(new SimpleDividerDecoration(getActivity()));
    }

    private void getDataFromServer(String webservice_name) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        CallService.getInstance().passInfromation(api_listener, webservice_name, params, true, "1", getActivity());
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_recruiter_registration, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    private void anotherActivity(Context context, int position) {
        Intent i = new Intent(context, ViewProfileActivity.class);
        Bundle b = new Bundle();
        b.putString("open_job", "open_job");
        b.putString("job_title", candidates_list.get(position).getFirst_name());
        b.putString("brief", "brief static data");
        b.putString("skill", "skill static data");
        b.putString("duration", "static  duration");
        b.putString("budget", "static  budget");
        b.putString("desired_location", "static desired location");
        i.putExtra("bundle", b);
        context.startActivity(i);
    }

    AnotherListener another_listener = new AnotherListener() {
        @Override
        public void onClickItem(int position) {
            if (check_value.equals("chat")) {
                Fragment fragment = new ViewQualificationFragment();
                Bundle b = new Bundle();
                b.putString("bio", "bio");
                b.putString("skill", "skill");
                b.putString("role_id", common_list.get(position).getRole_id());
                b.putString("qualification", "qualification");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) getActivity()).getSupportFragmentManager(), fragment);
            } else {
                Fragment fragment = new SearchCandidateFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        if (check_value.equals("chat")) {
            table_row.setVisibility(View.VISIBLE);
            if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
                button1.setText("Candidates");
                button2.setText("Recruiters");
            } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                button1.setText("Managers");
                button2.setText("Candidates");
            } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                button1.setText("Managers");
                button2.setText("Recruiters");
            }
        } else
            table_row.setVisibility(View.GONE);
        if (check_value.equals("chat")) {
            getDataFromServer(Constants.CHAT_USER_LIST);
        } else
            getDataFromServer(Constants.STATUS_REPORT_ON_HIRE_URL);
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == button1) {
                button1.setBackgroundColor(getActivity().getResources().getColor(R.color.list_background_color));
                button2.setBackgroundColor(getActivity().getResources().getColor(R.color.button_color));
                button2.setTextColor(getResources().getColor(android.R.color.white));
                button1.setTextColor(getResources().getColor(android.R.color.black));
                if (button1.getText().toString().equalsIgnoreCase("managers")) {
                    common_list = managers_list;
                    adapter = new UserNotificationAdapter(managers_list, getActivity(), another_listener, getSinchServiceInterface());
                    notification_list.setAdapter(adapter);
                } else if (button1.getText().toString().equalsIgnoreCase("recruiters")) {
                    common_list = recruiters_list;
                    adapter = new UserNotificationAdapter(recruiters_list, getActivity(), another_listener, getSinchServiceInterface());
                    notification_list.setAdapter(adapter);
                } else if (button1.getText().toString().equalsIgnoreCase("candidates")) {
                    common_list = candidates_list;
                    adapter = new UserNotificationAdapter(candidates_list, getActivity(), another_listener, getSinchServiceInterface());
                    notification_list.setAdapter(adapter);
                }
            } else if (view == button2) {
                button2.setBackgroundColor(getActivity().getResources().getColor(R.color.list_background_color));
                button1.setBackgroundColor(getActivity().getResources().getColor(R.color.button_color));
                button2.setTextColor(getResources().getColor(android.R.color.black));
                button1.setTextColor(getResources().getColor(android.R.color.white));
                if (button2.getText().toString().equalsIgnoreCase("managers")) {
                    common_list = managers_list;
                    adapter = new UserNotificationAdapter(managers_list, getActivity(), another_listener, getSinchServiceInterface());
                    notification_list.setAdapter(adapter);
                } else if (button2.getText().toString().equalsIgnoreCase("recruiters")) {
                    common_list = recruiters_list;
                    adapter = new UserNotificationAdapter(recruiters_list, getActivity(), another_listener, getSinchServiceInterface());
                    notification_list.setAdapter(adapter);
                } else if (button2.getText().toString().equalsIgnoreCase("candidates")) {
                    common_list = candidates_list;
                    adapter = new UserNotificationAdapter(candidates_list, getActivity(), another_listener, getSinchServiceInterface());
                    notification_list.setAdapter(adapter);
                }
            }

        }
    };
    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response IN ", response);
            if (response != null) {
                AllCandidateResponse candidate_response = new Gson().fromJson(response, AllCandidateResponse.class);
                if (candidate_response.isStatus()) {
                    if (check_value.equals("chat")) {
                        button1.setBackgroundColor(getActivity().getResources().getColor(R.color.list_background_color));
                        button2.setBackgroundColor(getActivity().getResources().getColor(R.color.button_color));
                        button2.setTextColor(getResources().getColor(android.R.color.white));
                        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
                            candidates_list = candidate_response.getCandidates();
                            recruiters_list = candidate_response.getRecruiters();
                        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                            managers_list = candidate_response.getManagers();
                            candidates_list = candidate_response.getCandidates();
                        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                            managers_list = candidate_response.getManagers();
                            recruiters_list = candidate_response.getRecruiters();
                        }
                        if (button1.getText().toString().equalsIgnoreCase("managers")) {
                            common_list = managers_list;
                            adapter = new UserNotificationAdapter(managers_list, getActivity(), another_listener, getSinchServiceInterface());
                            notification_list.setAdapter(adapter);
                        } else if (button1.getText().toString().equalsIgnoreCase("recruiters")) {
                            common_list = recruiters_list;
                            adapter = new UserNotificationAdapter(recruiters_list, getActivity(), another_listener, getSinchServiceInterface());
                            notification_list.setAdapter(adapter);
                        } else if (button1.getText().toString().equalsIgnoreCase("candidates")) {
                            common_list = candidates_list;
                            adapter = new UserNotificationAdapter(candidates_list, getActivity(), another_listener, getSinchServiceInterface());
                            notification_list.setAdapter(adapter);
                        }
                    } else {
                        candidates_list = candidate_response.getData();
                        adapter = new UserNotificationAdapter(candidates_list, getActivity(), another_listener, null);
                        notification_list.setAdapter(adapter);
                    }

                } else {
                    AlertManger.getAlert_instance().showAlert("No Billing Candidate available !", getActivity());
                }
            }

        }
    };

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }
}
