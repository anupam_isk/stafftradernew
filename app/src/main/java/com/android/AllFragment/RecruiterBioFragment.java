package com.android.AllFragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.CategoryResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.SkillAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterBioFragment extends Fragment {
    private onFragmentListener listener_back;
    private TextView upload_resume, file_name, upload_video, video_file_name;
    private final int REQUEST_CODE_DOC = 3;
    private String file_nam = "";
    private static final int REQUEST_VIDEO_CAPTURE = 1;
    private final int REQUEST_CODE_VEDIO = 4;
    private File file2;
    private File file1;
    String buffer_id = "", selectedPath = null;
    private Button submit;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
   /* try
    {
        Document myDoc = new Document(path);
        myDoc.get

        //creating Bitmap and canvas to draw the page into
        int width = (int)Math.ceil(page.getDisplayWidth());
        int height = (int)Math.ceil(page.getDisplayHeight());

        Bitmap bm = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas c = new Canvas(bm);
        page.paintPage(c);

        //Saving the Bitmap
        OutputStream os = new FileOutputStream("/sdcard/GeneratedImageByQoppa.jpg");
        bm.compress(CompressFormat.JPEG, 80, os);
        os.close();
    } catch (PDFException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }*/

    public RecruiterBioFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_recruiter_bio, container, false);
        initializeUI(v);
        setListener();
        return v;
    }

    private void setListener() {
        upload_resume.setOnClickListener(listener);
        submit.setOnClickListener(listener);
        upload_video.setOnClickListener(listener);
    }

    private void initializeUI(View v) {
        upload_resume = (TextView) v.findViewById(R.id.upload_resume);
        file_name = (TextView) v.findViewById(R.id.file_name);
        upload_video = (TextView) v.findViewById(R.id.upload_video);
        video_file_name = (TextView) v.findViewById(R.id.video_file_name);
        submit = (Button) v.findViewById(R.id.submit);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener_back = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    private void chooseVideo() {
        final CharSequence[] items = {"Record Video", "Choose from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Video!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                boolean result = UtilityPermission.checkPermission(getActivity());
                if (items[position].equals("Record Video")) {
                    if (result) {
                        boolean check = UtilityPermission.checkCameraPermission(getActivity());
                        if (check)
                            cameraIntent();
                    }
                } else if (items[position].equals("Choose from gallery")) {
                    if (result)
                        galleryIntent();
                } else if (items[position].equals("Cancel")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();
        //  Intent intent = new Intent();
    }
    private void cameraIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }
    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a Video "), REQUEST_CODE_VEDIO);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == submit) {
                if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                    if (validation()) {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                        CallService.getInstance().passDataToMultiPartOther(api_listener, Constants.UPLOAD_RESUME_URL, params, true, file1, file2, "1", getActivity(), "video");
                    }
                } else {
                    AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
                }
            } else if (v == upload_video) {
                chooseVideo();
            } else
                getDocument();


        }
    };

    private boolean validation() {
        if (file1 != null) {
            return true;

        } else {
            AlertManger.getAlert_instance().showAlert("Please Select your resume", getActivity());
            return false;
        }
    }

    private void getDocument() {
        // Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        // intent.addCategory(Intent.CATEGORY_OPENABLE);
        // intent.setType("*/*");
        // String[] mimetypes = {"application/*","text/plain","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
        // intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        // startActivityForResult(Intent.createChooser(intent, "Select File"),REQUEST_CODE_DOC);
        Intent intent = new Intent();
        Log.e("version", String.valueOf(Build.VERSION.SDK_INT));
        if (Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.KITKAT) {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        } else
            intent.setAction(Intent.ACTION_GET_CONTENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/*");
        startActivityForResult(Intent.createChooser(intent, "Select a file "), REQUEST_CODE_DOC);


       /* try {
            startActivityForResult(chooserIntent,  REQUEST_CODE_DOC);
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }*/
       /*  *//*Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
         intent.setType("file*//**//*");*//*
        Intent intent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        intent.setType("file*//*");
      //  intent.setType("file*//*");
     //   intent.addCategory(Intent.CATEGORY_OPENABLE);
      *//*  Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/msword");
        intent.addCategory(Intent.CATEGORY_OPENABLE);*//*
        // Only the system receives the ACTION_OPEN_DOCUMENT, so no need to test.
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    REQUEST_CODE_DOC);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }*/
        //startActivityForResult(intent, REQUEST_CODE_DOC);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("on activity ","on activity");
        Log.e("request code ",String.valueOf(requestCode));
        Log.e("result  code ",String.valueOf(resultCode));
        if (requestCode == REQUEST_CODE_DOC) {
            if (resultCode == Activity.RESULT_OK)
            {
                final String state = Environment.getExternalStorageState();
                Log.e("result ok", resultCode + "" + requestCode);
                String selectedPath = UtilityPermission.getPath(getActivity(), data.getData());
//                Log.e("selected path",selectedPath);
                File file = null;
                if (selectedPath != null) {
                    file = new File(selectedPath);
                } else {
                    Uri selectedFileURI = data.getData();
                    file = new File(selectedFileURI.getPath().toString());
                }
                // that file object send to server like image upload
                Log.e("", "File : " + file.getName() + " length " + file.length() + " PATH" + file.getPath());
                if (file.getPath().toString().contains(":")) {
                    StringTokenizer tokens = new StringTokenizer(file.getPath().toString(), ":");
                    String first = tokens.nextToken();
                    Log.e("", "first: " + first);
                    String file_1 = tokens.nextToken();
                    Log.e("", "second" + file_1);
                    file_nam = file_1;
                    String sd1path = "";
                    if (new File("/storage/extSdCard/").exists()) {
                        sd1path = "/storage/extSdCard/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("Sd Cardext Path", sd1path);
                    }
                    if (new File("/storage/sdcard1/").exists()) {
                        sd1path = "/storage/sdcard1/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("Sd Card1 Path", sd1path);
                    }
                    if (new File("/storage/usbcard1/").exists()) {
                        sd1path = "/storage/usbcard1/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("USB Path", sd1path);
                    }
                 /*   if(new File("/storage/sdcard0/").exists())
                    {
                        sd1path="/storage/sdcard0/";
                        file1 = new File(sd1path+
                                file_nam);
                        Log.e("Sd Card0 Path",sd1path);
                    }*/
                    if (new File("/storage/sdcard1/").exists()) {
                        sd1path = "/storage/sdcard1/";
                        file1 = new File(sd1path +
                                file_nam);
                        Log.e("Sd Card0 Path", sd1path);
                    }

                  /*  file1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                            file_nam);*/
                    //file1 = new File("/mnt/external_sd/"+file_nam);
                    Log.e("File Path : ", file1.getPath() + ", File size : " + file1.length() + " KB");
                } else {
                    // file_nam = file.getName().toString();
                    // file_nam= "Download/pdfurl_guide.pdf";
                    file1 = file;
                    file_nam = file1.getName();
                }
                try {
                    //  file = new File(selectedPath);
                    long length = file1.length();
                    length = length / 1024;
                    Log.e("File Path : ", file.getPath() + ", File size : " + length + " KB");
                    if (length > 2048) {
                        AlertManger.getAlert_instance().showAlert("You cant select more than 2 mb Resume", getActivity());
                    } else {
                        file_name.setText(file_nam);
                    }
                } catch (Exception e) {
                    Log.e("File not found : ", e.getMessage() + e);
                }
            }
            else
            {
                Log.e("result not ok","result not ok");
            }
        } else if (requestCode == REQUEST_CODE_VEDIO) {
            System.out.println("SELECT_VIDEO");
            Log.e("result ok", resultCode + "" + requestCode);
            selectedPath = UtilityPermission.getPath(getActivity(), data.getData());
            video_file_name.setText(selectedPath);
            file2 = new File(selectedPath);
            //  Uri selectedImageUri = data.getData();
            // String  selectedPath = getPath(data);
            Log.e("vedio path", selectedPath);
            //videoStart();

        }
        else if (requestCode == REQUEST_VIDEO_CAPTURE) {
            selectedPath = UtilityPermission.getPath(getActivity(), data.getData());
            file2 = new File(selectedPath);
            video_file_name.setText(selectedPath);
            Log.e("video uri", selectedPath);
            // videoStart();
               /* Uri videoUri = data.getData();
                video_player_view.setVideoURI(videoUri);*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        listener_back.showDrawerToggle(false);
    }

    /* public void showsimpleWord() {
         File file = new File("/sdcard/test.doc");

         HWPFDocumentCore wordDocument = null;
         try {
             wordDocument = WordToHtmlUtils.loadDoc(new FileInputStream(file));
         } catch (Exception e1) {
             // TODO Auto-generated catch block
             e1.printStackTrace();
         }

         WordToHtmlConverter wordToHtmlConverter = null;
         try {
             wordToHtmlConverter = new WordToHtmlConverter(
                     DocumentBuilderFactory.newInstance().newDocumentBuilder()
                             .newDocument());
             wordToHtmlConverter.processDocument(wordDocument);
             org.w3c.dom.Document htmlDocument = wordToHtmlConverter
                     .getDocument();
             ByteArrayOutputStream out = new ByteArrayOutputStream();
             DOMSource domSource = new DOMSource(htmlDocument);
             StreamResult streamResult = new StreamResult(out);
             TransformerFactory tf = TransformerFactory.newInstance();
             Transformer serializer = tf.newTransformer();
             serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
             serializer.setOutputProperty(OutputKeys.INDENT, "yes");
             serializer.setOutputProperty(OutputKeys.METHOD, "html");
             serializer.transform(domSource, streamResult);
             out.close();
             String result = new String(out.toByteArray());
             System.out.println(result);
            *//* ((WebView) findViewById(R.id.webview)).loadData(result,
                    "text/html", "utf-8");*//*

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String reponse, String request_id) {
            if (request_id.equals("1")) {

                Log.e("response pdf", reponse);
                if (reponse != null) {
                    try {
                        JSONObject json_response = new JSONObject(reponse);
                        if (json_response.getBoolean("status")) {
                            file_name.setText("");
                            AlertManger.getAlert_instance().showAlert("Resume Upload Successfully", getActivity());
                        } else {
                            AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    };
}
