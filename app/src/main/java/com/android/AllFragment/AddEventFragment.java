package com.android.AllFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.FragmentInterface.OnItemClickListener;
import com.android.FragmentInterface.onLoadMoreListener;
import com.android.GsonResponse.EventResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.helper.Validation;
import com.android.stafftraderapp.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * <p/>
 * created by Anupam tyagi
 */
public class AddEventFragment extends Fragment {
    private EditText meeting_subject, phone, email, meeting, link_recruiter, link_manager, link_candidate, start_date, comment;
    private Button submit_event, delete_event, update_event;
    private TableRow edit_row;
    private CheckBox send_to_outlook, follow_up_reminder;
    private ArrayList<HashMap<String, String>> first_list;
    ListAdapter adapter;
    private EventResponse.Event event;
    private DatePickerDialog dpd;
    private int start_value = 0;
    private int limit_value = 6;
    String role_id, s1, s2, s3, outlook = "N", follow_reminder = "N";
    private EditText common_view;
    private AlertDialog deleteDialog;
    private String email_string;
    private int new_pos;
    public AddEventFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        first_list = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                date_listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_addevent, container, false);
        initializeUI(v);
        setListener();
        return v;
    }

    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            start_date.setText(date);
        }
    };
    private void setListener() {
        link_manager.setOnClickListener(click_listener);
        link_recruiter.setOnClickListener(click_listener);
        link_candidate.setOnClickListener(click_listener);
        start_date.setOnClickListener(click_listener);
        delete_event.setOnClickListener(click_listener);
        update_event.setOnClickListener(click_listener);
        meeting.setOnClickListener(click_listener);
    }
    @Override
    public void onStart() {
        super.onStart();
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
            link_recruiter.setVisibility(View.VISIBLE);
            link_manager.setVisibility(View.GONE);
            link_candidate.setVisibility(View.VISIBLE);
        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
            link_recruiter.setVisibility(View.GONE);
            link_manager.setVisibility(View.VISIBLE);
            link_candidate.setVisibility(View.VISIBLE);
        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
            link_recruiter.setVisibility(View.VISIBLE);
            link_manager.setVisibility(View.VISIBLE);
            link_candidate.setVisibility(View.GONE);
        }
        Bundle b = getArguments();
        if (b != null) {
            edit_row.setVisibility(View.VISIBLE);
            submit_event.setVisibility(View.GONE);
            Log.e("edit event", "edit event");
            event = b.getParcelable("edit_event");
            new_pos = b.getInt("position");
            //assert event != null;
            if (event != null) {
                if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
                    s2 = event.getLink_recruiter();
                    s3 = event.getLink_candidate();
                    link_recruiter.setText(event.getLink_recruiter_name());
                    link_candidate.setText(event.getLink_candidate_name());
                } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                    s1 = event.getLink_manager();
                    s3 = event.getLink_candidate();
                    link_manager.setText(event.getLink_manager_name());
                    link_candidate.setText(event.getLink_candidate_name());
                } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                    s1 = event.getLink_manager();
                    s2 = event.getLink_recruiter();
                    link_recruiter.setText(event.getLink_recruiter_name());
                    link_manager.setText(event.getLink_manager_name());
                }
                meeting_subject.setText(event.getSubject());
                phone.setText(event.getPhone());
                email.setText(event.getEmail());
                meeting.setText(event.getMeeting());

                start_date.setText(event.getDate());
                comment.setText(event.getComments());
                if (event.getOutlook().equals("Y")) {
                    outlook = "Y";
                    send_to_outlook.setChecked(true);
                }
                if (event.getReminder().equals("Y")) {
                    follow_reminder = "Y";
                    follow_up_reminder.setChecked(true);
                }
            }
        } else {
            submit_event.setVisibility(View.VISIBLE);
            submit_event.setOnClickListener(click_listener);
            edit_row.setVisibility(View.GONE);

        }
    }
    private void initializeUI(View v) {
        meeting_subject = (EditText) v.findViewById(R.id.meeting_subject);
        phone = (EditText) v.findViewById(R.id.phone);
        email = (EditText) v.findViewById(R.id.email);
        meeting = (EditText) v.findViewById(R.id.meeting);
        link_recruiter = (EditText) v.findViewById(R.id.link_recruiter);
        link_manager = (EditText) v.findViewById(R.id.link_manager);
        link_candidate = (EditText) v.findViewById(R.id.link_candidate);
        start_date = (EditText) v.findViewById(R.id.start_date);
        comment = (EditText) v.findViewById(R.id.comment);
        submit_event = (Button) v.findViewById(R.id.submit_event);
        delete_event = (Button) v.findViewById(R.id.delete_event);
        update_event = (Button) v.findViewById(R.id.update_event);
        send_to_outlook = (CheckBox) v.findViewById(R.id.send_to_outlook);
        follow_up_reminder = (CheckBox) v.findViewById(R.id.follow_up_reminder);
        edit_row = (TableRow) v.findViewById(R.id.edit_row);
        send_to_outlook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (send_to_outlook.isPressed()) {
                    if(send_to_outlook.isChecked()) {
                        outlook = "Y";
                        showEmailPopup();
                    }
                 else
                    outlook = "N";
                }
            }
        });
        follow_up_reminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (follow_up_reminder.isChecked()) {
                    follow_reminder = "Y";
                    showRadioButtonPopup();
                } else
                    follow_reminder = "N";
            }
        });
    }

    private void showRadioButtonPopup() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.show_radio, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageButton image_button = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        final RadioGroup rgOpinion = (RadioGroup) deleteDialogView.findViewById(R.id.rgOpinion);
        Button save = (Button) deleteDialogView.findViewById(R.id.save);
        image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioButton selectRadio = (RadioButton) deleteDialogView.findViewById(rgOpinion
                        .getCheckedRadioButtonId());
                String opinion = selectRadio.getText().toString();
                UtilityPermission.showToast(getActivity(), "Your Opinion is : " + opinion);
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    private void showEmailPopup() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.add_email, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageButton image_button = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        final EditText edit_email = (EditText) deleteDialogView.findViewById(R.id.email);
        Button save = (Button) deleteDialogView.findViewById(R.id.save);
        image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email_string = edit_email.getText().toString();
                if (email_string != null)
                    deleteDialog.dismiss();
                else
                    UtilityPermission.showToast(getActivity(), "Please enter email.");
            }
        });
        deleteDialog.show();
    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == link_manager) {
                start_value = 0;
                limit_value = 6;
                role_id = "2";
                common_view = link_manager;
                first_list.clear();
                HashMap<String, String> params = new HashMap<>();
                params.put("role_id", role_id);
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, true, "2", getActivity());
            } else if (view == meeting) {
                showRadioMeetingPopup();
            } else if (view == link_recruiter) {
                start_value = 0;
                limit_value = 6;
                role_id = "3";
                common_view = link_recruiter;
                first_list.clear();
                HashMap<String, String> params = new HashMap<>();
                params.put("role_id", role_id);
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, true, "2", getActivity());
            } else if (view == link_candidate) {
                start_value = 0;
                limit_value = 6;
                role_id = "4";
                common_view = link_candidate;
                first_list.clear();
                HashMap<String, String> params = new HashMap<>();
                params.put("role_id", role_id);
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, true, "2", getActivity());
            } else if (view == start_date) {
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

            } else if (view == delete_event) {
                showDeletePopup();
            } else if (view == update_event) {
                if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                    if (validate()) {
                        HashMap<String, String> params = new HashMap<>();
                        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
                            params.put("link_recruiter", s2);
                            params.put("link_candidate", s3);
                        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                            params.put("link_manager", s1);
                            params.put("link_candidate", s3);
                        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                            params.put("link_recruiter", s2);
                            params.put("link_manager", s1);
                        }
                        params.put("event_id", event.getId());
                        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                        params.put("email", email.getText().toString());
                        params.put("phone", phone.getText().toString());
                        params.put("subject", meeting_subject.getText().toString());
                        params.put("meeting", meeting.getText().toString());
                        params.put("date", start_date.getText().toString());
                        params.put("outlook", outlook);
                        params.put("reminder", follow_reminder);
                        params.put("comment", comment.getText().toString());
                        CallService.getInstance().passInfromation(api_listener, Constants.EDIT_EVENT_URL, params, true, "4", getActivity());
                    }
                } else {
                    AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
                }
            } else if (view == submit_event) {

                if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                    if (validate()) {

                        HashMap<String, String> params = new HashMap<>();
                        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
                            params.put("link_recruiter", s2);
                            params.put("link_candidate", s3);
                        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                            params.put("link_manager", s1);
                            params.put("link_candidate", s3);
                        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                            params.put("link_recruiter", s2);
                            params.put("link_manager", s1);
                        }
                        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                        params.put("email", email.getText().toString());
                        params.put("phone", phone.getText().toString());
                        params.put("subject", meeting_subject.getText().toString());
                        params.put("meeting", meeting.getText().toString());
                        params.put("date", start_date.getText().toString());
                        params.put("outlook", outlook);
                        params.put("reminder", follow_reminder);
                        params.put("comment", comment.getText().toString());
                        CallService.getInstance().passInfromation(api_listener, Constants.ADD_EVENT_URL, params, true, "1", getActivity());
                    }
                } else {
                    AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
                }
            }
        }
    };

    private void showRadioMeetingPopup() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.show_radio_meeting, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageButton image_button = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        final RadioGroup rgOpinion = (RadioGroup) deleteDialogView.findViewById(R.id.rgOpinion);
        Button save = (Button) deleteDialogView.findViewById(R.id.save);
        image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioButton selectRadio = (RadioButton) deleteDialogView.findViewById(rgOpinion
                        .getCheckedRadioButtonId());
                final String opinion = selectRadio.getText().toString();
                meeting.setText(opinion);
                deleteDialog.dismiss();
            }

        });
        deleteDialog.show();


    }

    private boolean validate() {
        if (UtilityPermission.checkLength(meeting_subject)) {
            if (Validation.isValidMobile(phone.getText().toString())) {
                if (UtilityPermission.checkLength(email)) {
                    if (UtilityPermission.checkLength(meeting)) {
                        if (UtilityPermission.checkLength(start_date)) {
                            return true;
                        } else {
                            return UtilityPermission.setError(start_date, "Please Enter Date");
                        }
                    } else {
                        return UtilityPermission.setError(meeting, "Please Enter meeting");
                    }
                } else {
                    return UtilityPermission.setError(email, "Please Enter Valid email");
                }
            } else {
                return UtilityPermission.setError(phone, "Please Enter phone no");
            }
        } else {
            return UtilityPermission.setError(meeting_subject, "Please Enter Subject");
        }
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            Log.e("request_id", request_id);
            if (request_id.equals("1")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), "Event Add Successfully");
                            FragmentManager manager = getActivity().getSupportFragmentManager();
                            manager.popBackStack();
                        } else {
                            AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("4")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), "Event Update Successfully");
                            FragmentManager manager = getActivity().getSupportFragmentManager();
                            manager.popBackStack();
                        } else {
                            AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("5")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            NoteBookFirstFragment.pos = String.valueOf(new_pos);
                            UtilityPermission.showToast(getActivity(), "Event Delete Successfully");
                            FragmentManager manager = getActivity().getSupportFragmentManager();
                            manager.popBackStack();
                        } else {

                            AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("2"))
            {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            JSONArray data_array = json_response.getJSONArray("data");
                            for (int i = 0; i < data_array.length(); i++) {
                                HashMap<String, String> map = new HashMap<>();
                                map.put("id", data_array.getJSONObject(i).getString("id"));
                                map.put("name", data_array.getJSONObject(i).getString("first_name") + " " + data_array.getJSONObject(i).getString("first_name"));
                                first_list.add(map);
                            }
                            showPopup();
                        } else {
                            AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("3")) {
                //   remove progress item
                first_list.remove(first_list.size() - 1);
                adapter.notifyItemRemoved(first_list.size());
                //add items one by one
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            JSONArray data_array = json_response.getJSONArray("data");
                            for (int i = 0; i < data_array.length(); i++) {
                                HashMap<String, String> map = new HashMap<>();
                                map.put("id", data_array.getJSONObject(i).getString("id"));
                                map.put("name", data_array.getJSONObject(i).getString("first_name") + " " + data_array.getJSONObject(i).getString("first_name"));
                                first_list.add(map);
                            }
                            adapter.setLoaded();
                            adapter.notifyDataSetChanged();
                            // showPopup();
                        } else {
                            //   AlertManger.getAlert_instance().showAlert("There is no more data", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    private void showDeletePopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Staff Trader App");
        builder.setMessage("Are you sure you want to Delete this event ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("event_id", event.getId());
                CallService.getInstance().passInfromation(api_listener, Constants.DELETE_EVENT_URL, params, true, "5", getActivity());
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void showPopup() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.dialog_view, null);
        deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RecyclerView recyclerview = (RecyclerView) deleteDialogView.findViewById(R.id.recycler_view);
        TextView cancelBT = (TextView) deleteDialogView.findViewById(R.id.cancelBT);
        TextView select_skill = (TextView) deleteDialogView.findViewById(R.id.select_skill);
        select_skill.setGravity(Gravity.LEFT);
        select_skill.setText("Select One");
        ImageButton closeBT = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        Button saveBT = (Button) deleteDialogView.findViewById(R.id.saveBT);
        cancelBT.setVisibility(View.GONE);
        saveBT.setVisibility(View.GONE);
        closeBT.setVisibility(View.VISIBLE);
        closeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start_value = 0;
                limit_value = 6;
                deleteDialog.dismiss();
            }
        });
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        adapter = new ListAdapter(first_list, getActivity(), recyclerview);
        recyclerview.setAdapter(adapter);
        adapter.setOnLoadMoreListener(new onLoadMoreListener() {
            @Override
            public void onLoadMore() {
                first_list.add(null);
                adapter.notifyItemInserted(first_list.size() - 1);
                start_value = limit_value + 1;
                limit_value = limit_value + 3;
                Log.e("start_value", String.valueOf(start_value));
                Log.e("limit_value", String.valueOf(limit_value));
                HashMap<String, String> params = new HashMap<>();
                params.put("role_id", role_id);
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, false, "3", getActivity());
            }
        });
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                start_value = 0;
                limit_value = 6;
                if (common_view == link_manager) {
                    s1 = first_list.get(position).get("id");
                    Log.e("s1", s1);
                } else if (common_view == link_recruiter) {
                    s2 = first_list.get(position).get("id");
                    Log.e("s2", s2);
                } else {
                    s3 = first_list.get(position).get("id");
                }
                common_view.setText(first_list.get(position).get("name"));
                deleteDialog.dismiss();
                // Toast.makeText(getActivity(), "name " + first_list.get(position).get("name") + "position " + position + "id " + first_list.get(position).get("id"), Toast.LENGTH_LONG).show();
            }
        });
      /*

      we can use interface like this .we will make a method in any class to register the listener

        OnItemClickListener item_listener=new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }
        };
        adapter.setOnItemClickListener(item_listener);*/
        deleteDialog.show();
    }

    public class ListAdapter extends RecyclerView.Adapter {
        private final int VIEW_ITEM = 1;
        private final int VIEW_PROG = 0;
        private ArrayList<HashMap<String, String>> manager_list;
        private int visibleThreshold = 3;
        private int lastVisibleItem, totalItemCount;
        private boolean loading;
        private onLoadMoreListener onListener;
        private OnItemClickListener cliclistener;
        Context context;

        public ListAdapter(ArrayList<HashMap<String, String>> manager_list, Context context, RecyclerView recyclerview) {
            this.manager_list = manager_list;
            this.context = context;
            if (recyclerview.getLayoutManager() instanceof LinearLayoutManager) {

                Log.e("yes", "yes");
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerview
                        .getLayoutManager();


                recyclerview
                        .addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView,
                                                   int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                Log.e("on scroll", "on scroll");
                                totalItemCount = linearLayoutManager.getItemCount();
                                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                    // End has been reached
                                    // Do something
                                    if (onListener != null) {
                                        onListener.onLoadMore();
                                    }
                                    loading = true;
                                }

                            }
                        });
            } else {
                Log.e("no", "no");
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;
            if (viewType == VIEW_ITEM) {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.linear_recruiter, parent, false);

                vh = new MyViewHolder(v);
            } else {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.progress_bar_item, parent, false);

                vh = new ProgressViewHolder(v);
            }
            return vh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof MyViewHolder) {
                ((MyViewHolder) holder).bind(position, cliclistener);

            } else {
                ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }

        }

        @Override
        public int getItemViewType(int position) {
            return manager_list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }

        public void setOnItemClickListener(OnItemClickListener cliclistener) {
            this.cliclistener = cliclistener;
        }

        public void setOnLoadMoreListener(onLoadMoreListener onListener) {
            this.onListener = onListener;
        }

        public void setLoaded() {
            loading = false;
        }

        @Override
        public int getItemCount() {
            return manager_list.size();
        }

        public class ProgressViewHolder extends RecyclerView.ViewHolder {
            public ProgressBar progressBar;

            public ProgressViewHolder(View v) {
                super(v);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name;

            public MyViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
            }

            public void bind(final int position, final OnItemClickListener cliclistener) {
                Log.e("position", position + "");
                name.setText(manager_list.get(position).get("name"));
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cliclistener.onItemClick(position);
                    }
                });
            }
        }

    }
}
