package com.android.AllFragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.android.FragmentInterface.onFragmentListener;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import java.util.ArrayList;
import java.util.List;
// created by Anupam tyagi
/**
 * A simple {@link Fragment} subclass.
 */
public class NotebookFragment extends Fragment {
    private onFragmentListener listener;
    private TabLayout tabLayout;
    ViewPagerAdapter adapter;
    private Bundle b;
    private ViewPager viewPager;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b=getArguments();
        Log.e("Notebook on create","Notebook on create");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("Notebook on createview","Notebook on createview");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_notebook, container, false);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setPageTransformer(true, new RotateUpTransformer());
        tabLayout = (TabLayout)v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        return v;
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    private void setupViewPager(ViewPager viewPager) {
       adapter = new ViewPagerAdapter(getChildFragmentManager());
        if(b!=null)
            setOtherFragment();
        else
            setAdapterFragment();

        viewPager.setAdapter(adapter);
    }

    private void setOtherFragment() {
        adapter.addFragment(new OpenJobRequirementFragment(), "Open Requirements");
        adapter.addFragment(new ClosedJobRequiremenrt(), "Closed Requirements");
    }

    private void setAdapterFragment() {
        adapter.addFragment(new NoteBookFirstFragment(), "NoteBook");
        if(UserPref.getuser_instance(getActivity()).getRoleId().equals("3"))
            adapter.addFragment(new HoteBookFragment(), "Saved Profiles & Jobs");
        else if(UserPref.getuser_instance(getActivity()).getRoleId().equals("4"))
            adapter.addFragment(new HoteBookFragment(), "Saved Jobs");
        else
            adapter.addFragment(new HoteBookFragment(), "Saved Profiles");
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            if(position==0)
            return mFragmentList.get(position);
               else
                return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }




}
