package com.android.AllFragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.ViewProfileActivity;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HoteBookFragment extends Fragment {
    private RecyclerView recyclerview;
    private HoteBookAdapter adapter;
    private boolean _hasLoadedOnce = false;
    private Button saved_profile;
    private Button saved_profile_button, saved_jobs_button;
    private TableRow table_new_row;
    private ArrayList<HashMap<String, String>> hotebook_list, new_hotebook_list;
    int pos;
    boolean check = false;

    public HoteBookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hotebook_list = new ArrayList<>();
        new_hotebook_list = new ArrayList<>();
    }

    @Override
    public void onStart() {
        super.onStart();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
            check = false;
            saved_profile.setVisibility(View.GONE);
            adapter = new HoteBookAdapter(hotebook_list, getActivity());
        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
            check = true;
            adapter = new HoteBookAdapter(new_hotebook_list, getActivity());
        } else
            adapter = new HoteBookAdapter(hotebook_list, getActivity());
        recyclerview.setAdapter(adapter);
        Log.e("hotebook onStart", "hotebook onStart");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_hote_book, container, false);
        recyclerview = (RecyclerView) v.findViewById(R.id.recycler_view);
        table_new_row = (TableRow) v.findViewById(R.id.table_new_row);
        saved_profile_button = (Button) v.findViewById(R.id.saved_profile_button);
        saved_jobs_button = (Button) v.findViewById(R.id.saved_jobs_button);
        saved_profile = (Button) v.findViewById(R.id.saved_profile);
        saved_jobs_button.setBackgroundColor(getResources().getColor(R.color.transparent_blue_color));
        saved_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEmailPopup();
            }
        });
        saved_jobs_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdapter(hotebook_list);
                saved_jobs_button.setBackgroundColor(getResources().getColor(R.color.transparent_blue_color));
                saved_profile_button.setBackgroundColor(getResources().getColor(R.color.light_blue));
                check = false;
            }
        });
        saved_profile_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdapter(new_hotebook_list);
                saved_profile_button.setBackgroundColor(getResources().getColor(R.color.transparent_blue_color));
                saved_jobs_button.setBackgroundColor(getResources().getColor(R.color.light_blue));
                check = true;
            }
        });
        return v;
    }

    private void setAdapter(ArrayList<HashMap<String, String>> hotebook_list) {
        adapter = new HoteBookAdapter(hotebook_list, getActivity());
        recyclerview.setAdapter(adapter);
    }

    private void showEmailPopup() {
        final String[] item = new String[1];
        item[0] = null;
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.add_email, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView cancel = (TextView) deleteDialogView.findViewById(R.id.cancel);
        final EditText email = (EditText) deleteDialogView.findViewById(R.id.email);
        final Spinner frequency = (Spinner) deleteDialogView.findViewById(R.id.distribution);
        frequency.setVisibility(View.VISIBLE);
        Button save = (Button) deleteDialogView.findViewById(R.id.save);
        List<String> categories = new ArrayList<String>();
        categories.add("Select Frequency");
        categories.add("Weekly");
        categories.add("By Weekly");
        categories.add("Monthly");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        frequency.setAdapter(dataAdapter);
        frequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item[0] = parent.getItemAtPosition(position).toString();
                if (item[0].equalsIgnoreCase("Select Frequency"))
                    item[0] = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendToServer(email.getText().toString(), item[0]);
            }
        });
        deleteDialog.show();
    }

    private void sendToServer(String s, String frequency) {
        if (frequency != null) {
            HashMap<String, String> hamp = new HashMap<>();
            hamp.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
            hamp.put("email", s);
            hamp.put("type", "manager");
            hamp.put("limit", frequency);
            hamp.put("data_type", "profile");
            CallService.getInstance().passInfromation(api_listener, Constants.FREQUENCY_OF_DISTRIBUTION, hamp, true, "3", getActivity());
        } else {
            AlertManger.getAlert_instance().showAlert("Please Select Frequency Type", getActivity());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("hote bookActcreated", "hote book actvity created");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("hote book resume", "hote book resume");
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (request_id.equals("1")) {
                if (response != null) {
                    try {
                        clearList();
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            hotebook_list.clear();
                            JSONArray job_array = json_response.getJSONArray("job_data");
                            JSONArray profile_array = json_response.getJSONArray("data");
                            if (job_array.length() > 0) {
                                for (int i = 0; i < job_array.length(); i++) {
                                    HashMap<String, String> hmp = new HashMap<>();
                                    hmp.put("id", job_array.getJSONObject(i).getString("job_id"));
                                    //  hmp.put("name", hotebook_array.getJSONObject(i).getString("first_name") + " " + hotebook_array.getJSONObject(i).getString("last_name"));
                                    hmp.put("name", job_array.getJSONObject(i).getString("job_title"));
                                    hotebook_list.add(hmp);
                                }
                            }
                            if (profile_array.length() > 0) {
                                for (int i = 0; i < profile_array.length(); i++) {
                                    HashMap<String, String> hmp = new HashMap<>();
                                    hmp.put("id", profile_array.getJSONObject(i).getString("candidate_id"));
                                    hmp.put("name", profile_array.getJSONObject(i).getString("first_name") + " " + profile_array.getJSONObject(i).getString("last_name"));
                                    new_hotebook_list.add(hmp);
                                }
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            AlertManger.getAlert_instance().showAlert("No Saved jobs available.", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                                hotebook_list.remove(pos);
                                adapter.notifyDataSetChanged();
                                UtilityPermission.showToast(getActivity(), "This candidate has been Removed to Saved Profiles");
                            } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                                if (check) {
                                    new_hotebook_list.remove(pos);
                                    adapter.notifyDataSetChanged();
                                    UtilityPermission.showToast(getActivity(), "This candidate has been Removed to Saved Profiles");
                                } else {
                                    hotebook_list.remove(pos);
                                    adapter.notifyDataSetChanged();
                                    UtilityPermission.showToast(getActivity(), "Job has been Removed to Saved Profiles");
                                }
                            } else {
                                new_hotebook_list.remove(pos);
                                adapter.notifyDataSetChanged();
                                UtilityPermission.showToast(getActivity(), "Job has been Removed to Saved Profiles");
                            }

                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error ", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("3")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), "Saved Profiles has been sent successfully.");
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error. ", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    private void clearList() {
        hotebook_list.clear();
        new_hotebook_list.clear();
    }

    // always  keep in mind
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            Log.e("inside visible ", "in side visible" + isFragmentVisible_);
            String type = setTable();
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                // new NetCheck().execute();
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                params.put("type", type);
                CallService.getInstance().passInfromation(api_listener, Constants.LIST_HOTEBOOK_URL, params, true, "1", getActivity());
                Log.e("fragment visible", "fragment visible");
                _hasLoadedOnce = true;
            } else {
                _hasLoadedOnce = true;
                Log.e("fragment in visible", "fragment in visible" + _hasLoadedOnce + " " + isFragmentVisible_);
            }
        }
    }

    private String setTable() {
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4"))
            return "candidate";
        else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
            table_new_row.setVisibility(View.VISIBLE);
            return "recruiter";
        } else
            return "manager";
    }

    public class HoteBookAdapter extends RecyclerView.Adapter<HoteBookAdapter.MyViewHolder> {
        private ArrayList<HashMap<String, String>> hotebook_list;
        Context context;

        public HoteBookAdapter(ArrayList<HashMap<String, String>> hotebook_list, Context context) {
            this.hotebook_list = hotebook_list;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.hotebook_layout, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.hotebook_name.setText(hotebook_list.get(position).get("name"));
            holder.delete_candidate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = position;
                    String type = null;
                    HashMap<String, String> params = new HashMap<String, String>();
                    if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                        type = "candidate";
                        params.put("job_id", hotebook_list.get(position).get("id"));
                    } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                        type = "recruiter";
                        if (check) {
                            params.put("candidate_id", new_hotebook_list.get(position).get("id"));
                        } else {
                            params.put("job_id", hotebook_list.get(position).get("id"));
                        }
                    } else {
                        type = "manager";
                        params.put("candidate_id", new_hotebook_list.get(position).get("id"));
                    }
                    params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                    params.put("type", type);
                    CallService.getInstance().passInfromation(api_listener, Constants.ADD_REMOVE_HOTEBOOK_URL, params, true, "2", context);
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!UserPref.getuser_instance(context).getRoleId().equals("3")) {
                        if (check) {
                            Fragment fragment = new ViewQualificationFragment();
                            Bundle b = new Bundle();
                            b.putString("role_id", "4");
                            b.putString("bio", "bio");
                            b.putString("skill", "skill");
                            b.putString("qualification", "qualification");
                            fragment.setArguments(b);
                            UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
                        } else {
                            anotherActivity(context, position);
                        }
                    } else {
                        if (check) {
                            Fragment fragment = new ViewQualificationFragment();
                            Bundle b = new Bundle();
                            b.putString("role_id", "4");
                            b.putString("bio", "bio");
                            b.putString("skill", "skill");
                            b.putString("qualification", "qualification");
                            fragment.setArguments(b);
                            UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
                        } else {
                            anotherActivity(context, position);
                        }

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return hotebook_list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView hotebook_name;
            public Button delete_candidate;

            public MyViewHolder(View view) {
                super(view);
                hotebook_name = (TextView) view.findViewById(R.id.hotebook_name);
                delete_candidate = (Button) view.findViewById(R.id.delete_candidate);
            }
        }
    }

    private void anotherActivity(Context context, int position) {
        Intent i = new Intent(context, ViewProfileActivity.class);
        Bundle b = new Bundle();
        b.putString("open_job", "open_job");
        b.putString("job_title", hotebook_list.get(position).get("name"));
        b.putString("brief", "brief static data");
        b.putString("skill", "skill static data");
        b.putString("duration", "static  duration");
        b.putString("budget", "static  budget");
        b.putString("desired_location", "static desired location");
        i.putExtra("bundle", b);
        context.startActivity(i);
    }
}
