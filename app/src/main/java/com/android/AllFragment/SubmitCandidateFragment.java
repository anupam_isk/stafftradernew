package com.android.AllFragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.ChooseCredential;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.GsonResponse.OpenJobResponse;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.ViewProfileActivity;
import com.android.adapter.AvailableAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam tyagi on 7/4/2016.
 */
public class SubmitCandidateFragment extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<AllCandidateResponse.Candidate> candidate_list;
    private SubmitCandidateAdapter adapter;
    private onFragmentListener listener;
    private SubmitCandidateJobAdapter jobadapter;
    private ArrayList<HashMap<String, String>> map_list;
    private String job_id;
    private Bundle b = null;
    private int pos, second_pos;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_seek_jobs, container, false);
        initializeUI(rootView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        b = getArguments();
        if (b != null) {
            job_id = getArguments().getString("job_id");
            candidate_list = new ArrayList<>();
            if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                HashMap<String, String> params = new HashMap<>();
                params.put("job_id", job_id);
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                CallService.getInstance().passInfromation(api_listener, Constants.CANDIDATE_TO_SUBMIT_URL, params, true, "1", getActivity());
                //  Callservice
            } else {
                AlertManger.getAlert_instance().showAlertPop(Constants.NETWORK_STRING, getActivity());
            }
        } else {
            map_list = new ArrayList<>();
            if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                    CallService.getInstance().passInfromation(api_listener, Constants.JOBS_APPLIED_TO_URL, params, true, "5", getActivity());
                } else {
                    CallService.getInstance().passInfromation(api_listener, Constants.SUBMITTED_JOBS_URL, params, true, "3", getActivity());
                }
                //  Callservice
            } else {
                AlertManger.getAlert_instance().showAlertPop(Constants.NETWORK_STRING, getActivity());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    private void initializeUI(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response candidate", response);
            if (request_id.equals("1")) {
                if (response != null) {
                    AllCandidateResponse candidate_response = new Gson().fromJson(response, AllCandidateResponse.class);
                    if (!candidate_response.isStatus()) {
                        candidate_list = candidate_response.getData();
                        adapter = new SubmitCandidateAdapter(getActivity(), candidate_list);
                        recyclerView.setAdapter(adapter);
                    } else {
                        AlertManger.getAlert_instance().showAlert("No Submit Candidate Available", getActivity());
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            if (!json_response.getBoolean("is_submit"))
                                candidate_list.get(pos).setIs_job(false);
                            else
                                candidate_list.get(pos).setIs_job(false);

                            adapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("3")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            JSONArray data_array = json_response.getJSONArray("data");
                            for (int i = 0; i < data_array.length(); i++) {
                                HashMap<String, String> hmap = new HashMap<>();
                                hmap.put("id", data_array.getJSONObject(i).getString("id"));
                                hmap.put("title", data_array.getJSONObject(i).getString("job_title"));
                                hmap.put("is_withdraw", data_array.getJSONObject(i).getString("is_withdraw"));
                                hmap.put("recruiter_id", data_array.getJSONObject(i).getString("recruiter_id"));
                                map_list.add(hmap);
                            }
                            jobadapter = new SubmitCandidateJobAdapter(getActivity(), map_list);
                            recyclerView.setAdapter(jobadapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("4")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            if (json_response.getString("withdraw_status").equals("Withdrawn")) {
                                UtilityPermission.showToast(getActivity(), "SUCCESS!  You have been withdrawn from the job.");
                                map_list.get(second_pos).put("is_withdraw", "1");
                            } else
                                map_list.get(second_pos).put("is_withdraw", "0");
                        }
                        jobadapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("5")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            JSONArray data_array = json_response.getJSONArray("data");
                            for (int i = 0; i < data_array.length(); i++) {
                                HashMap<String, String> hmap = new HashMap<>();
                                hmap.put("id", data_array.getJSONObject(i).getString("id"));
                                hmap.put("title", data_array.getJSONObject(i).getString("first_name"));
                                hmap.put("is_withdraw", data_array.getJSONObject(i).getString("is_withdraw"));
                                map_list.add(hmap);
                            }
                            jobadapter = new SubmitCandidateJobAdapter(getActivity(), map_list);
                            recyclerView.setAdapter(jobadapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    public class SubmitCandidateJobAdapter extends RecyclerView.Adapter<SubmitCandidateJobAdapter.MyViewHolder> {
        private ArrayList<HashMap<String, String>> map_list;
        Context context;

        public SubmitCandidateJobAdapter(Context context, ArrayList<HashMap<String, String>> map_list) {
            this.map_list = map_list;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_withdraw, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int second_position) {
            holder.job_title.setText(map_list.get(second_position).get("title"));

            if (map_list.get(second_position).get("is_withdraw").equals("0")) {
                holder.withdrawBT.setText("Withdraw");
                holder.withdrawBT.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.new_button_background));
            } else {
                holder.withdrawBT.setText("Withdrawn");
                holder.withdrawBT.setEnabled(false);
                holder.withdrawBT.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.reverse_button_background));
            }
            holder.job_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (UserPref.getuser_instance(context).getRoleId().equals("3")) {
                        Fragment fragment = new ViewQualificationFragment();
                        Bundle b = new Bundle();
                        b.putString("bio", "brief");
                        b.putString("skill", "skill");
                        b.putString("qualification", "qualification");
                        fragment.setArguments(b);
                        UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
                    } else {
                        Intent i = new Intent(context, ViewProfileActivity.class);
                        Bundle b = new Bundle();
                        b.putString("open_job", "open_job");
                        b.putString("job_title", map_list.get(second_position).get("title"));
                        b.putString("brief", "brief about this job");
                        b.putString("duration", "Duration ");
                        b.putString("budget", "budget");
                        b.putString("desired_location", "desired location");
                        i.putExtra("bundle", b);
                        context.startActivity(i);
                    }
                }
            });

            holder.withdrawBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    second_pos = second_position;
                    showAlertPop("Alert!  Please confirm you want to be withdrawn from the job", context);

                }
            });

        }

        private void showAlertPop(String s, Context context) {
            try {
                new AlertDialog.Builder(context)
                        .setTitle("Staff Trader App")
                        .setMessage(s)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                showRadioPopup();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            } catch (Exception e) {
                e.getMessage();
            }
        }

        private void showRadioPopup() {
            LayoutInflater factory = LayoutInflater.from(context);
            final View deleteDialogView = factory.inflate(
                    R.layout.show_radio_withdraw, null);
            final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
            deleteDialog.setView(deleteDialogView);
            deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final TextView cancel = (TextView) deleteDialogView.findViewById(R.id.cancel);
            final RadioGroup rgOpinion = (RadioGroup) deleteDialogView.findViewById(R.id.rgOpinion);
            Button save = (Button) deleteDialogView.findViewById(R.id.save);
            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RadioButton selectRadio = (RadioButton) deleteDialogView.findViewById(rgOpinion
                            .getCheckedRadioButtonId());
                    final String withdraw_reason = selectRadio.getText().toString();
                    sendToServer(withdraw_reason);
                    deleteDialog.dismiss();
                }

                private void sendToServer(String withdraw_reason) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("id", map_list.get(second_pos).get("id"));
                    params.put("user_id", UserPref.getuser_instance(context).getUserId());
                    params.put("withdraw_reason", withdraw_reason);
                    if (UserPref.getuser_instance(context).getRoleId().equals("3"))
                        CallService.getInstance().passInfromation(api_listener, Constants.WITHDRAW_REQUESTED_URL, params, true, "4", context);
                    else
                        CallService.getInstance().passInfromation(api_listener, Constants.WITHDRAW_SUBMITTED_TO_URL, params, true, "4", context);
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });
            deleteDialog.show();

        }


        /*  public void setAnother_listener(AnotherListener another_listener)
          {
              this.another_listener=another_listener;
              this.another_listener.onClickItem(2);
              Log.e("another lister isnull","another listener is null"+ this.another_listener +another_listener);
          }*/
        /*public void onBindViewHolder(MyViewHolder holder, final int position) {
           // holder.bind(position,another_listener);
        holder.name.setText(openjob_list.get(position).getJob_title());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("on click","on click");
                another_listener.onClickItem(position);
            }
        });
    }*/
        @Override
        public int getItemCount() {
            return map_list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public Button withdrawBT;
            public TextView job_title;

            public MyViewHolder(View itemView) {
                super(itemView);
                job_title = (TextView) itemView.findViewById(R.id.job_title);
                withdrawBT = (Button) itemView.findViewById(R.id.withdrawBT);
            }
        }
    }

    public class SubmitCandidateAdapter extends RecyclerView.Adapter<SubmitCandidateAdapter.MyViewHolder> {
        private ArrayList<AllCandidateResponse.Candidate> candidate_list;
        Context context;

        public SubmitCandidateAdapter(Context context, ArrayList<AllCandidateResponse.Candidate> candidate_list) {
            this.candidate_list = candidate_list;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.submit_candidate_layout, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.name.setText(candidate_list.get(position).getFirst_name() + " " + candidate_list.get(position).getLast_name());
            holder.submit_job.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = position;
                    showAlertPop("ALERT!  Please confirm you want to submit this candidate.", context);

                }
            });

            if (candidate_list.get(position).is_job())
                holder.submit_job.setText("Candidate Submitted");
            else
                holder.submit_job.setText("Submit Candidate");
            Log.e("image name", candidate_list.get(position).getImage());
            Picasso.with(getActivity())
                    .load(candidate_list.get(position).getImage())
                    .placeholder(R.drawable.splash_logo)
                    .into(holder.seek_image);

        }

        public void showAlertPop(String message, final Context context) {
            try {
                new AlertDialog.Builder(context)
                        .setTitle("Staff Trader App")
                        .setMessage(message)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                showPopUp(pos);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            } catch (Exception e) {
                e.getMessage();
            }
        }

        /*  public void setAnother_listener(AnotherListener another_listener)
          {
              this.another_listener=another_listener;
              this.another_listener.onClickItem(2);
              Log.e("another lister isnull","another listener is null"+ this.another_listener +another_listener);
          }*/
        /*public void onBindViewHolder(MyViewHolder holder, final int position) {
           // holder.bind(position,another_listener);
        holder.name.setText(openjob_list.get(position).getJob_title());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("on click","on click");
                another_listener.onClickItem(position);
            }
        });
    }*/
        @Override
        public int getItemCount() {
            return candidate_list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public Button submit_job;
            public TextView job_title, name, willing_to_relocate, address;
            public ImageView seek_image;

            public MyViewHolder(View itemView) {
                super(itemView);
                job_title = (TextView) itemView.findViewById(R.id.job_title);
                name = (TextView) itemView.findViewById(R.id.name);
                willing_to_relocate = (TextView) itemView.findViewById(R.id.willing_to_relocate);
                address = (TextView) itemView.findViewById(R.id.address);
                submit_job = (Button) itemView.findViewById(R.id.submit_job);
                seek_image = (ImageView) itemView.findViewById(R.id.seek_image);
            }
        }
    }

    private void showPopUp(final int pos) {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View hire_view = factory.inflate(
                R.layout.hire_popup, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(hire_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        TextView cancel = (TextView) hire_view.findViewById(R.id.cancel);
        EditText candidate_x = (EditText) hire_view.findViewById(R.id.candidate_x);
        EditText project_duration = (EditText) hire_view.findViewById(R.id.project_duration);
        EditText start_date = (EditText) hire_view.findViewById(R.id.start_date);
        EditText job_title = (EditText) hire_view.findViewById(R.id.job_title);
        EditText manager_contact_name = (EditText) hire_view.findViewById(R.id.manager_contact_name);
        EditText manager_number = (EditText) hire_view.findViewById(R.id.manager_number);
        EditText work_location = (EditText) hire_view.findViewById(R.id.work_location);
        EditText manager_email = (EditText) hire_view.findViewById(R.id.manager_email);
        final Button agree = (Button) hire_view.findViewById(R.id.agree);
        final Button cancelBT = (Button) hire_view.findViewById(R.id.cancelBT);
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("candidate_id", candidate_list.get(pos).getId());
                params.put("job_id", job_id);
                params.put("recruiter_id", UserPref.getuser_instance(getActivity()).getUserId());
                CallService.getInstance().passInfromation(api_listener, Constants.SUBMITTED_JOB_URL, params, true, "2", getActivity());
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });

        deleteDialog.show();
    }

}