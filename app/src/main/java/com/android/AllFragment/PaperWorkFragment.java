package com.android.AllFragment;


import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.CanvasActivity;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.PaymentAdapter;
import com.android.helper.Constants;
import com.android.helper.Signature;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class PaperWorkFragment extends Fragment {
    private Button selected_file, sign_file, save, clear;
    private RelativeLayout selected_layout, scan_layout, sign_layout;
    private LinearLayout signature_layout;
    private ImageView sign_image;
    private RecyclerView recyler_view;
    private onFragmentListener listener_back;
    private ArrayList<HashMap<String, String>> image_list;
    private Signature mSignature;
    private String uniqueId, current = null, id, json_string;
    File mypath, directory = null;
    private Bundle b, new_bitmap;
    public static Bitmap bit;
    public PaperWorkFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = getArguments();
        if (b == null)
            getImageFromServer();
        else {
            id = b.getString("id");
            json_string = b.getString("json_string");
        }

    }
    private void getImageFromServer() {
        HashMap<String, String> hmap = new HashMap<>();
        hmap.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        hmap.put("role_id", UserPref.getuser_instance(getActivity()).getRoleId());
        CallService.getInstance().passInfromation(api_listener, Constants.PAPERWORK_URL, hmap, true, "1", getActivity());
    }

    private void createFile() {
        ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        directory = cw.getDir("imageDirectory", Context.MODE_PRIVATE);
        // Create imageDir
        uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
        current = uniqueId + ".png";
        mypath = new File(directory, current);
    }

    public static String getTodaysDate() {
        final Calendar c = Calendar.getInstance();
        int todaysDate = (c.get(Calendar.YEAR) * 10000) +
                ((c.get(Calendar.MONTH) + 1) * 100) +
                (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:", String.valueOf(todaysDate));
        return (String.valueOf(todaysDate));
    }

    public static String getCurrentTime() {
        final Calendar c = Calendar.getInstance();
        int currentTime = (c.get(Calendar.HOUR_OF_DAY) * 10000) +
                (c.get(Calendar.MINUTE) * 100) +
                (c.get(Calendar.SECOND));
        Log.w("TIME:", String.valueOf(currentTime));
        return (String.valueOf(currentTime));
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments() != null) {
            selected_file.setVisibility(View.GONE);
            selected_layout.setVisibility(View.GONE);
            sign_layout.setVisibility(View.VISIBLE);
            mSignature = new Signature(getActivity(), null);
            mSignature.setBackgroundColor(getResources().getColor(R.color.transparent_blue_color));
            signature_layout.addView(mSignature, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        } else {
            sign_file.setVisibility(View.GONE);
        }
        createFile();
//        signature_layout.addView(mSignature, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_paper_work, container, false);
        initializeUI(v);
        setListener();
        return v;
    }

    private void setListener() {
        selected_file.setOnClickListener(listener);
        sign_file.setOnClickListener(listener);
        save.setOnClickListener(listener);
        clear.setOnClickListener(listener);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener_back = (onFragmentListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        signature_layout.removeView(mSignature);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (b != null)
            listener_back.otherCheck(true);
        listener_back.showDrawerToggle(false);

    }

    private void initializeUI(View v) {
        selected_file = (Button) v.findViewById(R.id.selected_file);
        sign_file = (Button) v.findViewById(R.id.sign_file);
        save = (Button) v.findViewById(R.id.save);
        clear = (Button) v.findViewById(R.id.clear);
        recyler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        selected_layout = (RelativeLayout) v.findViewById(R.id.selected_layout);
        sign_layout = (RelativeLayout) v.findViewById(R.id.sign_layout);
        signature_layout = (LinearLayout) v.findViewById(R.id.signature_layout);
        sign_image = (ImageView) v.findViewById(R.id.sign_image);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyler_view.setHasFixedSize(true);
        recyler_view.setLayoutManager(mLayoutManager);
        recyler_view.setItemAnimator(new DefaultItemAnimator());
        recyler_view.addItemDecoration(new SimpleDividerDecoration(getActivity()));
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == selected_file) {
                selected_layout.setVisibility(View.VISIBLE);
                sign_layout.setVisibility(View.GONE);
            } else if (v == sign_file) {

            } else if (v == clear) {
                mSignature.clear();
            } else if (v == save) {
                mSignature.setBackgroundColor(getResources().getColor(android.R.color.white));
                signature_layout.setDrawingCacheEnabled(true);
                mSignature.save(signature_layout, mypath);
                selected_layout.setVisibility(View.GONE);
                sign_layout.setVisibility(View.GONE);
                sign_image.setVisibility(View.VISIBLE);
                loadImageFromStorage(mypath);
                Intent i = new Intent(getActivity(), CanvasActivity.class);
                i.putExtra("id", id);
                i.putExtra("json_string", json_string);
                startActivity(i);
                //
            }
        }
    };
    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            try {
                JSONObject image_object = new JSONObject(response);
                image_list = new ArrayList<>();
                JSONArray json_array = image_object.getJSONArray("data");
                Log.e("jdon array1 string ", json_array.toString());
                for (int position = 0; position < json_array.length(); position++) {
                    HashMap<String, String> hmap = new HashMap<>();
                    hmap.put("id", json_array.getJSONObject(position).getString("id"));
                    hmap.put("image", json_array.getJSONObject(position).getString("image"));
                    hmap.put("file_name", json_array.getJSONObject(position).getString("name"));
                    String json_string = json_array.getJSONObject(position).getString("sign_cordinate");
                    Log.e("js array string length ", json_string);
                    hmap.put("sign_array", json_string);
                    image_list.add(hmap);
                }
                PaperWorkAdapter adapter = new PaperWorkAdapter(image_list);
                recyler_view.setAdapter(adapter);
            } catch (JSONException e) {
                e.getMessage();
            }
        }
    };

    public static void loadImageFromStorage(File path) {

        try {
            //      File f = new File(directory, path);
            bit = BitmapFactory.decodeStream(new FileInputStream(path));
            Log.e("bitm", "New bit height " + bit.getWidth() + " height " + bit.getHeight());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static Bitmap loadImageFromStorage1(File path) {

        try {
            //      File f = new File(directory, path);
            bit = BitmapFactory.decodeStream(new FileInputStream(path));
            Log.e("bitm", "New bit height " + bit.getWidth() + " height " + bit.getHeight());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bit;
    }

    class PaperWorkAdapter extends RecyclerView.Adapter<PaperWorkAdapter.PaperWorkHolder> {
        private ArrayList<HashMap<String, String>> image_list;

        public PaperWorkAdapter(ArrayList<HashMap<String, String>> image_list) {
            this.image_list = image_list;
        }

        @Override
        public void onBindViewHolder(PaperWorkHolder holder, final int position) {
            holder.image_name.setText(image_list.get(position).get("id")+". "+ image_list.get(position).get("file_name"));
            holder.view_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialogFragment(image_list.get(position).get("id"),image_list.get(position).get("image"), image_list.get(position).get("sign_array"));
                }
            });
        }

        @Override
        public PaperWorkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View item_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.paperwork_layout, parent, false);
            return new PaperWorkHolder(item_view);
        }

        @Override
        public int getItemCount() {
            return image_list.size();
        }

        public class PaperWorkHolder extends RecyclerView.ViewHolder {
            private TextView image_name;
            private Button view_button;

            public PaperWorkHolder(View view) {
                super(view);
                image_name = (TextView) view.findViewById(R.id.image_name);
                view_button = (Button) view.findViewById(R.id.view_button);
            }
        }
    }

    private void showDialogFragment(String id, String image, String json_string) {
        ShowImageFragment fragment = new ShowImageFragment();
        Bundle b = new Bundle();
        b.putString("image", image);
        b.putString("id", id);
        b.putString("json_string", json_string);
        fragment.setArguments(b);
        fragment.show(getActivity().getSupportFragmentManager(), "nn");
    }
}
