package com.android.AllFragment;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.OnItemClickListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.FragmentInterface.onLoadMoreListener;
import com.android.GsonResponse.MilestoneResponse;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewMilestoneTaskFragment extends Fragment {
    private String milestone_id;
    private RecyclerView recycler_view;
    private MilestoneTaskAdapter adapter;
    private onFragmentListener listener;
    private RelativeLayout add_task;
    ListAdapter list_adapter;
    private EditText common_view, link_candidate, link_recruiter;
    private AlertDialog deleteDialog, edit_dialog;
    float rating_one = 0.0f, rating_two = 0.0f, rating_three = 0.0f, rating_four = 0.0f, rating_five = 0.0f, rating_six = 0.0f, rating_seven = 0.0f, rating_eight = 0.0f;
    private ArrayList<MilestoneResponse.MilestoneClass> task_list;
    private ArrayList<HashMap<String, String>> first_list;
    private String s1, s2;
    int pos;

    public ViewMilestoneTaskFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3"))
            add_task.setVisibility(View.GONE);
        else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4"))
            add_task.setVisibility(View.GONE);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        task_list = new ArrayList<>();
        milestone_id = getArguments().getString("milestone_id");
        getData();

    }

    private void getData() {
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3"))
            getDataForCandidate();
        else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4"))
            getDataForCandidate();
        else
            getDataFromServer();
    }

    private void getDataForCandidate() {
        HashMap<String, String> params = new HashMap<>();
        params.put("milestone_id", milestone_id);
        CallService.getInstance().passInfromation(api_listener, Constants.MILESTONE_BY_TASK_ROLE_URL, params, true, "1", getActivity());
    }

    private void getDataFromServer() {
        HashMap<String, String> params = new HashMap<>();
        params.put("milestone_id", milestone_id);
        CallService.getInstance().passInfromation(api_listener, Constants.VIEW_MILESTONE_TASK_URL, params, true, "1", getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_task, container, false);
        initializeUI(v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    private void initializeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        add_task = (RelativeLayout) v.findViewById(R.id.add_task);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        add_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditPopUp(null, null);
            }
        });
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (response != null) {
                if (request_id.equals("1")) {
                    task_list.clear();
                    MilestoneResponse milestone_response = new Gson().fromJson(response, MilestoneResponse.class);
                    if (milestone_response.isStatus()) {
                        task_list = milestone_response.getData();
                        adapter = new MilestoneTaskAdapter(getActivity(), task_list);
                        recycler_view.setAdapter(adapter);
                    } else {
                        AlertManger.getAlert_instance().showAlert("There is no task added in this milestone.", getActivity());
                    }
                }
                if (request_id.equals("6")) {
                    Log.e(" delete response", response);
                    if (response != null) {
                        try {
                            JSONObject json_response = new JSONObject(response);
                            if (json_response.getBoolean("status")) {
                                UtilityPermission.showToast(getActivity(), "Task has been deleted successfully.");
                                task_list.remove(pos);
                                adapter.notifyDataSetChanged();
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                                adapter.notifyDataSetChanged();
                            } else {
                                AlertManger.getAlert_instance().showAlert("Something went wrong on server side ", getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.e(" response is null ", "reponse is null");
                    }
                }
                if (request_id.equals("2")) {
                    Log.e("  vcresponse", response);
                    if (response != null) {
                        first_list.clear();
                        try {
                            JSONObject json_response = new JSONObject(response);
                            if (json_response.getBoolean("status")) {
                                JSONArray data_array = json_response.getJSONArray("data");
                                for (int i = 0; i < data_array.length(); i++) {
                                    HashMap<String, String> map = new HashMap<>();
                                    map.put("id", data_array.getJSONObject(i).getString("id"));
                                    map.put("name", data_array.getJSONObject(i).getString("first_name") + " " + data_array.getJSONObject(i).getString("last_name"));
                                    first_list.add(map);
                                }
                                showPopup();
                            } else {
                                AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (request_id.equals("3")) {
                    if (response != null) {
                        try {
                            JSONObject json_response = new JSONObject(response);
                            if (json_response.getBoolean("status")) {
                                edit_dialog.dismiss();
                                getDataFromServer();
                            } else {
                                AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                if (request_id.equals("4")) {
                    if (response != null) {
                        try {
                            JSONObject json_response = new JSONObject(response);
                            if (json_response.getBoolean("status")) {
                                UtilityPermission.showToast(getActivity(), "This Task  has been succeesfully completed.");
                                getDataFromServer();
                            } else {
                                AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
                if (request_id.equals("5")) {
                    if (response != null) {
                        try {
                            JSONObject json_response = new JSONObject(response);
                            if (json_response.getBoolean("status")) {
                                UtilityPermission.showToast(getActivity(), "Rating has been succeesfully submitted.");
                                deleteDialog.dismiss();
                                getData();
                            } else {
                                UtilityPermission.showToast(getActivity(), "Something went wrong");
                                deleteDialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    };

    private void showEditPopUp(final String id, MilestoneResponse.MilestoneClass milestoneClass) {
        first_list = new ArrayList<>();
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View hire_view = factory.inflate(
                R.layout.edit_task_popup, null);
        edit_dialog = new AlertDialog.Builder(getActivity()).create();
        edit_dialog.setView(hire_view);
        edit_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView task = (TextView) hire_view.findViewById(R.id.task);
        if (id != null)
            task.setText("EDIT MILESTONE TASK");
        else
            task.setText("ADD MILESTONE TASK");
        final EditText weightage = (EditText) hire_view.findViewById(R.id.weightage);
        final EditText task_name = (EditText) hire_view.findViewById(R.id.task_name);
        final TextView cancel = (TextView) hire_view.findViewById(R.id.cancel);
        final TextView done = (TextView) hire_view.findViewById(R.id.done);
        link_recruiter = (EditText) hire_view.findViewById(R.id.link_recruiter);
        link_candidate = (EditText) hire_view.findViewById(R.id.link_candidate);
        final EditText comment = (EditText) hire_view.findViewById(R.id.comment);
        final TextView submit_button = (TextView) hire_view.findViewById(R.id.submit_button);
        if (milestoneClass != null) {
            weightage.setText(milestoneClass.getWeightage());
            task_name.setText(milestoneClass.getTask_name());
            comment.setText(milestoneClass.getComments());
            link_candidate.setText(milestoneClass.getCandidate_name());
            link_recruiter.setText(milestoneClass.getRecruiter_name());
        }
        View.OnClickListener click_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String role_id = " ";
                if (view == submit_button) {
                    sendTOServer();
                } else if (view == link_candidate) {
                    role_id = "4";
                    common_view = link_candidate;
                    HashMap<String, String> params = new HashMap<>();
                    params.put("role_id", role_id);
                    params.put("start", String.valueOf("0"));
                    params.put("limit", String.valueOf("6"));
                    CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, true, "2", getActivity());
                } else if (view == link_recruiter) {
                    role_id = "3";
                    common_view = link_recruiter;
                    HashMap<String, String> params = new HashMap<>();
                    params.put("role_id", role_id);
                    params.put("start", String.valueOf("0"));
                    params.put("limit", String.valueOf("6"));
                    CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, true, "2", getActivity());
                } else if (view == cancel) {
                    edit_dialog.dismiss();
                } else if (view == done) {
                    edit_dialog.dismiss();
                }

            }

            private void sendTOServer() {
                HashMap<String, String> params = new HashMap<>();
                params.put("milestone_id", milestone_id);
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                params.put("task_name", task_name.getText().toString());
                params.put("weightage", weightage.getText().toString());
                params.put("comments", comment.getText().toString());
                params.put("candidate_id", s2);
                params.put("recruiter_id", s1);
                if (id != null) {
                    params.put("task_id", id);
                    CallService.getInstance().passInfromation(api_listener, Constants.EDIT_MILESTONE_TASK_URL, params, true, "3", getActivity());
                } else {
                    CallService.getInstance().passInfromation(api_listener, Constants.ADD_MILESTONE_TASK_URL, params, true, "3", getActivity());
                }
            }
        };
        submit_button.setOnClickListener(click_listener);
        link_candidate.setOnClickListener(click_listener);
        link_recruiter.setOnClickListener(click_listener);
        cancel.setOnClickListener(click_listener);
        done.setOnClickListener(click_listener);
        edit_dialog.show();
    }

    public class MilestoneTaskAdapter extends RecyclerView.Adapter<MilestoneTaskAdapter.MyViewHolder> {
        private ArrayList<MilestoneResponse.MilestoneClass> milestone_list;
        private Context context;

        public MilestoneTaskAdapter(Context context, ArrayList<MilestoneResponse.MilestoneClass> milestone_list) {
            this.milestone_list = milestone_list;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.task_row, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final MilestoneResponse.MilestoneClass milestone_response = milestone_list.get(position);
            holder.candidate_name.setText(milestone_response.getCandidate_name());
            holder.task_name.setText(milestone_response.getTask_name());
            holder.weightage.setText(milestone_response.getWeightage());
            holder.status.setText(milestone_response.getStatus());
            if (UserPref.getuser_instance(context).getRoleId().equals("2")) {
                if (milestone_response.getStatus().equals("Pending")) {
                    holder.statusBT.setVisibility(View.GONE);
                } else if (milestone_response.getStatus().equals("Completed")) {
                    holder.statusBT.setText("Approve Task");// popup should show accept decline
                    holder.statusBT.setEnabled(true);
                } else if (milestone_response.getStatus().equals("Approved")) {
                    holder.statusBT.setVisibility(View.GONE);
                }
            } else if (UserPref.getuser_instance(context).getRoleId().equals("4")) {
                if (milestone_response.getStatus().equals("Pending")) {
                    holder.statusBT.setText("Complete Task"); // notification
                    holder.statusBT.setEnabled(true);
                } else if (milestone_response.getStatus().equals("Completed")) {
                    holder.statusBT.setVisibility(View.GONE);
                    holder.table_row_task.setVisibility(View.GONE);
                } else if (milestone_response.getStatus().equals("Approved")) {
                    holder.statusBT.setVisibility(View.GONE);
                    holder.table_row_task.setVisibility(View.GONE);
                }
            } else {
                holder.table_row_task.setVisibility(View.GONE);
            }
            if (UserPref.getuser_instance(context).getRoleId().equals("3")) {
                holder.editBT.setVisibility(View.GONE);
                holder.deleteBT.setVisibility(View.GONE);
            } else if (UserPref.getuser_instance(context).getRoleId().equals("4")) {
                holder.editBT.setVisibility(View.GONE);
                holder.deleteBT.setVisibility(View.GONE);
            }
            holder.editBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showEditPopUp(milestone_list.get(position).getId(), milestone_list.get(position));
                }
            });
            holder.deleteBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pos = position;
                    showDeletePpopup(milestone_list.get(position).getId());
                }
            });
            holder.statusBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserPref.getuser_instance(context).getRoleId().equals("2")) {
                        showAcceptPopup(position);
                    } else
                        showNewPopup(position);

                }
            });
        }

        private void showNewPopup(final int position) {
            try {
                new AlertDialog.Builder(context)
                        .setTitle("Staff Trader App")
                        .setMessage("ALERT!  Please confirm you have completed this milestone/task?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                sendStatusOnServer(milestone_list.get(position).getId(), "Completed");
                                // sendStatusOnServer(milestone_list.get(position).getId(), "Approved");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
            } catch (Exception e) {
                e.getMessage();
            }
        }

        private void showAcceptPopup(final int position) {
            try {
                new AlertDialog.Builder(context)
                        .setTitle("Staff Trader App")
                        .setMessage("Are you sure you want to Approved or Decline this task ?")
                        .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                showRatingPopup(milestone_list.get(position).getId(),milestone_list.get(position).getLink_candidate());
                                // sendStatusOnServer(milestone_list.get(position).getId(), "Approved");
                            }
                        })
                        .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                sendStatusOnServer(milestone_list.get(position).getId(), "Pending");
                            }
                        })
                        .show();
            } catch (Exception e) {
                e.getMessage();
            }
        }

        private void showRatingPopup(final String milestone_id, final String link_candidate) {
            LayoutInflater factory = LayoutInflater.from(getActivity());
            final View rating_view = factory.inflate(
                    R.layout.rating_view, null);
            deleteDialog = new AlertDialog.Builder(getActivity()).create();
            deleteDialog.setView(rating_view);
            deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final RatingBar rating1 = (RatingBar) rating_view.findViewById(R.id.rating1);
            final RatingBar rating2 = (RatingBar) rating_view.findViewById(R.id.rating2);
            final RatingBar rating3 = (RatingBar) rating_view.findViewById(R.id.rating3);
            final RatingBar rating4 = (RatingBar) rating_view.findViewById(R.id.rating4);
            final RatingBar rating5 = (RatingBar) rating_view.findViewById(R.id.rating5);
            final RatingBar rating6 = (RatingBar) rating_view.findViewById(R.id.rating6);
            final RatingBar rating7 = (RatingBar) rating_view.findViewById(R.id.rating7);
            final RatingBar rating8 = (RatingBar) rating_view.findViewById(R.id.rating8);
            final EditText comment = (EditText) rating_view.findViewById(R.id.comment);
            final Button button = (Button) rating_view.findViewById(R.id.submit_event);
            final TextView cancel=(TextView) rating_view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });
            RatingBar.OnRatingBarChangeListener rating_listener = new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                    if (ratingBar == rating1) {
                        rating_one = rating;
                        Log.e("rating 1", String.valueOf(rating));
                    } else if (ratingBar == rating2) {
                        rating_two = rating;
                        Log.e("rating 2", String.valueOf(rating));
                    } else if (ratingBar == rating3) {
                        rating_three = rating;
                        Log.e("rating 3", String.valueOf(rating));
                    } else if (ratingBar == rating4) {
                        rating_four = rating;
                        Log.e("rating 4", String.valueOf(rating));
                    } else if (ratingBar == rating5) {
                        rating_five = rating;
                        Log.e("rating 5", String.valueOf(rating));
                    } else if (ratingBar == rating6) {
                        rating_six = rating;
                        Log.e("rating 6", String.valueOf(rating));
                    } else if (ratingBar == rating7) {
                        rating_seven = rating;
                        Log.e("rating 7", String.valueOf(rating));
                    } else if (ratingBar == rating8) {
                        rating_eight = rating;
                        Log.e("rating 8", String.valueOf(rating));
                    }
                }
            };
            rating1.setOnRatingBarChangeListener(rating_listener);
            rating2.setOnRatingBarChangeListener(rating_listener);
            rating3.setOnRatingBarChangeListener(rating_listener);
            rating4.setOnRatingBarChangeListener(rating_listener);
            rating5.setOnRatingBarChangeListener(rating_listener);
            rating6.setOnRatingBarChangeListener(rating_listener);
            rating7.setOnRatingBarChangeListener(rating_listener);
            rating8.setOnRatingBarChangeListener(rating_listener);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("punctuality", String.valueOf(rating_one));
                    params.put("professional", String.valueOf(rating_two));
                    params.put("personality", String.valueOf(rating_three));
                    params.put("potential", String.valueOf(rating_four));
                    params.put("competency", String.valueOf(rating_five));
                    params.put("confidence", String.valueOf(rating_six));
                    params.put("communication", String.valueOf(rating_seven));
                    params.put("capable", String.valueOf(rating_eight));
                    params.put("comment", comment.getText().toString());
                    params.put("instance_id", milestone_id);
                    params.put("candidate_id",link_candidate);
                    params.put("manager_id",UserPref.getuser_instance(getActivity()).getUserId());
                    params.put("type", "Milestone");
                    CallService.getInstance().passInfromation(api_listener, Constants.CANDIDATE_ASSIGNMENT_REPORT_URL, params, true, "5", getActivity());
                }
            });

            deleteDialog.show();
        }

        private void showDeletePpopup(final String id) {
            try {
                new AlertDialog.Builder(context)
                        .setTitle("Staff Trader App")
                        .setMessage("Are you sure to delete this task ?")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                deletMilestoneFromServer(id);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            } catch (Exception e) {
                e.getMessage();
            }
        }

        private void deletMilestoneFromServer(String id) {
            HashMap<String, String> params = new HashMap<>();
            params.put("task_id", id);
            CallService.getInstance().passInfromation(api_listener, Constants.DELETE_MILESTONE_TASK_URL, params, true, "6", context);
        }

        @Override
        public int getItemCount() {
            return milestone_list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView candidate_name, task_name, weightage, status;
            public Button statusBT, editBT, deleteBT;
            public TableRow table_row_task;

            public MyViewHolder(View view) {
                super(view);
                candidate_name = (TextView) view.findViewById(R.id.candidate_name);
                task_name = (TextView) view.findViewById(R.id.task_name);
                weightage = (TextView) view.findViewById(R.id.weightage);
                status = (TextView) view.findViewById(R.id.status);
                statusBT = (Button) view.findViewById(R.id.statusBT);
                editBT = (Button) view.findViewById(R.id.editBT);
                deleteBT = (Button) view.findViewById(R.id.deleteBT);
                table_row_task=(TableRow) view.findViewById(R.id.table_row_task);
            }
        }

    }

    private void sendStatusOnServer(String id, String approved) {
        HashMap<String, String> params = new HashMap<>();
        params.put("task_id", id);
        params.put("status", approved);
        CallService.getInstance().passInfromation(api_listener, Constants.CHANGE_TASK_STATUS_URL, params, true, "4", getActivity());
    }

    private void showPopup() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.dialog_view, null);
        deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RecyclerView recyclerview = (RecyclerView) deleteDialogView.findViewById(R.id.recycler_view);
        TextView cancelBT = (TextView) deleteDialogView.findViewById(R.id.cancelBT);
        TextView select_skill = (TextView) deleteDialogView.findViewById(R.id.select_skill);
        select_skill.setGravity(Gravity.LEFT);
        select_skill.setText("Select One");
        ImageButton closeBT = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        TextView saveBT = (TextView) deleteDialogView.findViewById(R.id.saveBT);
        cancelBT.setVisibility(View.GONE);
        saveBT.setVisibility(View.GONE);
        closeBT.setVisibility(View.VISIBLE);
        closeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        list_adapter = new ListAdapter(first_list, getActivity(), recyclerview);
        recyclerview.setAdapter(list_adapter);
        list_adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if (common_view == link_recruiter) {
                    s1 = first_list.get(position).get("id");
                    Log.e("s1", s1);
                } else {
                    s2 = first_list.get(position).get("id");
                }
                common_view.setText(first_list.get(position).get("name"));
                deleteDialog.dismiss();
            }
        });

      /*

      we can use interface like this .we will make a method in any class to register the listener

        OnItemClickListener item_listener=new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }
        };
        adapter.setOnItemClickListener(item_listener);*/
        deleteDialog.show();
    }

    public class ListAdapter extends RecyclerView.Adapter {
        private final int VIEW_ITEM = 1;
        private final int VIEW_PROG = 0;
        private ArrayList<HashMap<String, String>> manager_list;
        private int visibleThreshold = 3;
        private int lastVisibleItem, totalItemCount;
        private boolean loading;
        private onLoadMoreListener onListener;
        private OnItemClickListener cliclistener;
        Context context;

        public ListAdapter(ArrayList<HashMap<String, String>> manager_list, Context context, RecyclerView recyclerview) {
            this.manager_list = manager_list;
            this.context = context;
            if (recyclerview.getLayoutManager() instanceof LinearLayoutManager) {

                Log.e("yes", "yes");
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerview
                        .getLayoutManager();


                recyclerview
                        .addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView,
                                                   int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                Log.e("on scroll", "on scroll");
                                totalItemCount = linearLayoutManager.getItemCount();
                                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                    // End has been reached
                                    // Do something
                                    if (onListener != null) {
                                        onListener.onLoadMore();
                                    }
                                    loading = true;
                                }

                            }
                        });
            } else {
                Log.e("no", "no");
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;
            if (viewType == VIEW_ITEM) {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.linear_recruiter, parent, false);

                vh = new ListAdapter.MyViewHolder(v);
            } else {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.progress_bar_item, parent, false);

                vh = new ListAdapter.ProgressViewHolder(v);
            }
            return vh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof MyViewHolder) {
                ((MyViewHolder) holder).bind(position, cliclistener);

            } else {
                ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }

        }

        @Override
        public int getItemViewType(int position) {
            return manager_list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }

        public void setOnItemClickListener(OnItemClickListener cliclistener) {
            this.cliclistener = cliclistener;
        }

        public void setOnLoadMoreListener(onLoadMoreListener onListener) {
            this.onListener = onListener;
        }

        public void setLoaded() {
            loading = false;
        }

        @Override
        public int getItemCount() {
            return manager_list.size();
        }

        public class ProgressViewHolder extends RecyclerView.ViewHolder {
            public ProgressBar progressBar;

            public ProgressViewHolder(View v) {
                super(v);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name;

            public MyViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
            }

            public void bind(final int position, final OnItemClickListener cliclistener) {
                Log.e("position", position + "");
                name.setText(manager_list.get(position).get("name"));
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cliclistener.onItemClick(position);
                    }
                });
            }
        }
    }

}
