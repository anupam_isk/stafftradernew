package com.android.AllFragment;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.CategoryResponse;
import com.android.ServerCommunication.CallService;
import com.android.adapter.SkillAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecruiterVedioFragment extends Fragment {
    private EditText brief_bio, qualification, Select_skil;
    private onFragmentListener listener_back;
    private DisplayMetrics dm;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    private SurfaceView sur_View;
    private MediaController media_Controller;
    private Button view_resume, view_video;
    private TableRow table_row;
    private final int REQUEST_CODE_VEDIO = 3;
    private File file1;
    String buffer_id = "", selectedPath = null;
    private ArrayList<CategoryResponse.Skill> skill_list;
    private Button submit;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        skill_list = new ArrayList<>();
        getCategoryData();
    }
    private void getCategoryData() {
        CallService.getInstance().passCategoryinformation(api_listener, Constants.CATEGORY_URL, true, "2", getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recruiter_vedio, container, false);
        initializeUI(v);
        setListener();
        return v;
    }
    private void setListener() {
        Select_skil.setOnClickListener(listener);
        view_video.setOnClickListener(listener);
        submit.setOnClickListener(listener);
        view_resume.setOnClickListener(listener);
    }
    private void initializeUI(View v) {
        Select_skil = (EditText) v.findViewById(R.id.Select_skil);
        brief_bio = (EditText) v.findViewById(R.id.brief_bio);
        view_resume = (Button) v.findViewById(R.id.view_resume);
        view_video = (Button) v.findViewById(R.id.view_video);
        view_video = (Button) v.findViewById(R.id.view_video);
        table_row = (TableRow) v.findViewById(R.id.table_row);
        qualification = (EditText) v.findViewById(R.id.qualification);
        submit = (Button) v.findViewById(R.id.submit);
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3"))
            table_row.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener_back = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener_back.showDrawerToggle(false);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == view_video) {
                //chooseVideo();
                ViewQualificationFragment.showEmailPopup("http://iskdemo.com/stafftrader/public/upload/video/2016/11/22/58343e2a9a9df.3gp", getActivity());
            } else if (v == view_resume) {
                ViewQualificationFragment.showResume("http://docs.google.com/viewer?url=http://online.verypdf.com/examples/pdfeditor.pdf", getActivity());
            } else if (v == Select_skil) {
                showDialog();
            } else {
                if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                    if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                        sendDataToServer();
                    } else {
                        if (file1 != null) {
                            sendDataToServer();
                        } else {
                            AlertManger.getAlert_instance().showAlert("Please first select the video", getActivity());
                        }
                    }
                } else {
                    AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
                }
            }
        }
    };

    private void sendDataToServer() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("bio", brief_bio.getText().toString());
        params.put("skills", buffer_id);
        params.put("qualification", qualification.getText().toString());
        if (file1 != null)
            CallService.getInstance().passDataToMultiPart(api_listener, Constants.UPDATE_BIO_URL, params, true, file1, "1", getActivity(), "video");
        else
            CallService.getInstance().passInfromation(api_listener, Constants.UPDATE_BIO_URL, params, true, "1", getActivity());
    }

    private void showDialog() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.dialog_view, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        RecyclerView recyclerview = (RecyclerView) deleteDialogView.findViewById(R.id.recycler_view);
        TextView cancelBT = (TextView) deleteDialogView.findViewById(R.id.cancelBT);
        final SkillAdapter adapter = new SkillAdapter(skill_list, getActivity());
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });

        TextView saveBT = (TextView) deleteDialogView.findViewById(R.id.saveBT);
        saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Select_skil.setText("");
                StringBuffer buffer = new StringBuffer();
                for (String s : adapter.new_skill_list) {
                    buffer.append(s + " , ");
                }
                StringBuffer buffer2 = new StringBuffer();
                for (String s : adapter.new_id_list) {
                    // Log.e("id",s);
                    buffer_id = buffer_id.concat(s + ",");
                    //  buffer2.append(s + " , ");
                }
                Log.e("buffer id", buffer_id);
                Select_skil.setText(buffer);
                deleteDialog.dismiss();
                // CandidateActivity.showToast(getActivity(), "Under development");
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(adapter);
        deleteDialog.show();
    }


    private void videoStart() {
        try {
            file1 = new File(selectedPath);
            long length = file1.length();
            length = length / 1024;
            Log.e("File Path : ", file1.getPath() + ", File size : " + length + " KB");
            if (length > 2048) {
                AlertManger.getAlert_instance().showAlert("You cant select more than 2 mb vedio", getActivity());
            } else {

            }
        } catch (Exception e) {
            Log.e("File not found : ", e.getMessage() + e);
        }
        if (!file1.isFile()) {
            Log.e("Huzza", "Source File Does not exist");
        }
    }

    public String getPath(Intent data) {
        Uri originalUri = data.getData();
        String pathsegment[] = originalUri.getLastPathSegment().split(":");
        Log.e("pathsegment", pathsegment.toString());
        String id = pathsegment[1];
        Log.e("id", id);
        final String[] imageColumns = {MediaStore.Images.Media.DATA};
        final String imageOrderBy = null;

        Uri second_uri = getUri();
        Cursor cursor = getActivity().getContentResolver().query(second_uri, imageColumns,
                MediaStore.Images.Media._ID + "=" + id, null, null);
        String path = null;
        try {
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Failed to get image", Toast.LENGTH_LONG).show();
        }


        return path;
    }

    // By using this method get the Uri of Internal/External Storage for Media
    private Uri getUri() {
        String state = Environment.getExternalStorageState();
        if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String reponse, String request_id) {
            if (request_id.equals("1")) {

                Log.e("response pdf", reponse);
                if (reponse != null) {
                    try {
                        JSONObject json_response = new JSONObject(reponse);
                        if (json_response.getBoolean("status")) {
                            brief_bio.setText("");
                            qualification.setText("");
                            // file_name.setText("");
                            AlertManger.getAlert_instance().showAlert(" Upload Successfully", getActivity());
                        } else {
                            AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("2")) {

                if (reponse != null) {
                    CategoryResponse category_response = new Gson().fromJson(reponse, CategoryResponse.class);
                    if (category_response.isStatus()) {
                        skill_list = category_response.getSkill();
                        Log.e("size skill", "" + skill_list.size());
                    } else {
                        UtilityPermission.showToast(getActivity(), "There is some error on server side");
                    }
                }

            }
        }
    };
}
