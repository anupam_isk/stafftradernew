package com.android.AllFragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.FragmentInterface.onFragmentListener;
import com.android.stafftraderapp.R;

/**
 * Created by Anupam tyagi
 */
public class HelpCenter extends Fragment {
    private onFragmentListener listener;
    public HelpCenter() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }
}
