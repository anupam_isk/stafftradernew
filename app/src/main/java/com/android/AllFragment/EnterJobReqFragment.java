package com.android.AllFragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.CategoryResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.SeekJobResponse;
import com.android.ServerCommunication.CallService;
import com.android.adapter.SkillAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Anupam tyagi .
 */
public class EnterJobReqFragment extends Fragment implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {
    private EditText job_title, desired_location, duration_of_project, budget, start_date, spinner_skill_sets, spinner_industry, brief_desc;
    private TextView submitBT;
    private String check;
    public static CharSequence[] items = null;
    private onFragmentListener listener;
    private ArrayList<CategoryResponse.Skill> skill_list;
    private SeekJobResponse.SeekJob seek_job;
    private com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd;
    public EnterJobReqFragment()
    {

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        skill_list = new ArrayList<>();
        if (getArguments() != null) {
            seek_job = getArguments().getParcelable("seek_job");
            check = "check";
        } else {
            getCategoryData();
        }
        Calendar now = Calendar.getInstance();
        dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        dpd.setOnDateSetListener(this);
    }
    @Override
    public void onStart() {
        super.onStart();
        if (check != null) {
            submitBT.setVisibility(View.GONE);
            spinner_skill_sets.setEnabled(false);
            spinner_industry.setEnabled(false);
            job_title.setText(seek_job.getJob_title());
            desired_location.setText(seek_job.getDesired_location());
            duration_of_project.setText(seek_job.getDuration_project());
            budget.setText(seek_job.getBudget());
            start_date.setText(seek_job.getStart_date());
            spinner_skill_sets.setText(seek_job.getSkill_set());
            spinner_industry.setText(seek_job.getIndustry_type());
            brief_desc.setText(seek_job.getDescription());
            // show all data here
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_enter_job_req_hiringmanager, container, false);
        initializeUI(rootView);
        setListener();
        return rootView;
    }

    private void setListener() {
        spinner_skill_sets.setOnClickListener(click_listener);
        spinner_industry.setOnClickListener(click_listener);
        submitBT.setOnClickListener(click_listener);
        start_date.setOnClickListener(click_listener);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    private void initializeUI(View v) {
        job_title = (EditText) v.findViewById(R.id.job_title);
        desired_location = (EditText) v.findViewById(R.id.desired_location);
        duration_of_project = (EditText) v.findViewById(R.id.duration_of_project);
        budget = (EditText) v.findViewById(R.id.budget);
        start_date = (EditText) v.findViewById(R.id.start_date);
        spinner_skill_sets = (EditText) v.findViewById(R.id.spinner_skill_sets);
        spinner_industry = (EditText) v.findViewById(R.id.spinner_industry);
        brief_desc = (EditText) v.findViewById(R.id.brief_desc);
        submitBT = (TextView) v.findViewById(R.id.submitBT);
    }

    public boolean validation() {
        if (UtilityPermission.checkLength(job_title)) {
            if (UtilityPermission.checkLength(desired_location)) {
                if (UtilityPermission.checkLength(duration_of_project)) {
                    if (UtilityPermission.checkLength(budget)) {
                        if (UtilityPermission.checkLength(start_date)) {
                            if (UtilityPermission.checkLength(spinner_skill_sets)) {
                                if (UtilityPermission.checkLength(spinner_industry)) {
                                    if (UtilityPermission.checkLength(brief_desc)) {
                                        return true;
                                    } else {
                                        return UtilityPermission.setError(brief_desc, "Please Enter Job Title");
                                    }
                                } else {
                                    return UtilityPermission.setError(spinner_industry, "Please Enter Job Title");
                                }
                            } else {
                                return UtilityPermission.setError(spinner_skill_sets, "Please Enter Job Title");
                            }
                        } else {
                            return UtilityPermission.setError(start_date, "Please Enter Job Title");
                        }
                    } else {
                        return UtilityPermission.setError(budget, "Please Enter Job Title");
                    }
                } else {
                    return UtilityPermission.setError(duration_of_project, "Please Enter Job Title");
                }
            } else {
                return UtilityPermission.setError(desired_location, "Please Enter Job Title");
            }
        } else {
            return UtilityPermission.setError(job_title, "Please Enter Job Title");
        }
    }
    private void showDialog() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(R.layout.dialog_view, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RecyclerView recyclerview = (RecyclerView) deleteDialogView.findViewById(R.id.recycler_view);
        TextView cancelBT = (TextView) deleteDialogView.findViewById(R.id.cancelBT);
        final SkillAdapter adapter = new SkillAdapter(skill_list, getActivity());
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        TextView saveBT = (TextView) deleteDialogView.findViewById(R.id.saveBT);
        saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinner_skill_sets.setText("");
                StringBuffer buffer = new StringBuffer();
                for (String s : adapter.new_skill_list)
                    buffer.append(s + " , ");

                spinner_skill_sets.setText(buffer);
                deleteDialog.dismiss();
                // CandidateActivity.showToast(getActivity(), "Under development");
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(adapter);
        deleteDialog.show();
    }

    private void showSpinnerItem() {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Category");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                spinner_industry.setText(items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == spinner_skill_sets) {
                showDialog();
            } else if (v == spinner_industry) {
                showSpinnerItem();
            } else if (v == start_date) {
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            } else {
                if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                    if (validation()) {
                        sendToServer();
                    }
                } else {
                    UtilityPermission.showToast(getActivity(), Constants.NETWORK_STRING);
                }
            }
        }
    };

    private void sendToServer() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("job_title", getString(job_title));
        params.put("budget", getString(budget));
        params.put("start_date", getString(start_date));
        params.put("street", UserPref.getuser_instance(getActivity()).getStreet());
        params.put("city", UserPref.getuser_instance(getActivity()).getCity());
        params.put("country", UserPref.getuser_instance(getActivity()).getCountry());
        params.put("duration_project", getString(duration_of_project));
        params.put("industry_type", getString(spinner_industry));
        params.put("description,", getString(brief_desc));
        params.put("skill_set", getString(spinner_skill_sets));
        params.put("lat", UserPref.getuser_instance(getActivity()).getLattitude());
        params.put("lon",UserPref.getuser_instance(getActivity()).getLongitude());
        CallService.getInstance().passInfromation(api_listener, Constants.ENTER_JOB_REQUIREMENT, params, true, "1", getActivity());
    }

    String getString(EditText et) {
        return et.getText().toString();
    }

    private void getCategoryData() {
        CallService.getInstance().passCategoryinformation(api_listener, Constants.CATEGORY_URL, true, "2", getActivity());
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {

            Log.e("response enetr job", response);
            if (request_id.equals("1")) {
                if (response != null)
                {

                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            job_title.setText("");
                            desired_location.setText("");
                            duration_of_project.setText("");
                            budget.setText("");
                            start_date.setText("");
                            spinner_skill_sets.setText("");
                            spinner_industry.setText("");
                            brief_desc.setText("");
                            AlertManger.getAlert_instance().showAlertPop("Job Created Successfully", getActivity());
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            if (request_id.equals("2")) {

                if (response != null) {
                    CategoryResponse category_response = new Gson().fromJson(response, CategoryResponse.class);
                    if (category_response.isStatus()) {
                        ArrayList<CategoryResponse.CategoryType> list = category_response.getIndustry();
                        skill_list = category_response.getSkill();
                        Log.e("size skill", "" + skill_list.size());
                        items = new CharSequence[list.size()];
                        for (int i = 0; i < list.size(); i++) {
                            items[i] = list.get(i).getIndustry_name();
                        }

                    } else {
                        UtilityPermission.showToast(getActivity(), "There is some error on server side");
                    }
                }

            }
        }

    };

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        start_date.setText(date);
    }
}
