package com.android.AllFragment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RemoteViews;

import com.android.HomeActivity;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Anupam Tyagi.
 */
public class MoreFragment_candidate extends Fragment {
    private ImageView available_candidate_map, submit_candidate, job_matrix, milestone, hire_paperwork_lay, talktolive, my_assignment_lay, recruiter_bio_lay, seek_open_jobs_lay, notebook_lay, status_report_hire_lay, upload_resume, matrix, open_job_lay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_more_candidate, container, false);
        initializeUI(rootView);
        setListener();
        return rootView;
    }

    /* private void sendNotification() {
         int icon = R.mipmap.ic_launcher;
         long when = System.currentTimeMillis();
         Notification notification = new Notification(icon, "Custom Notification", when);

         NotificationManager mNotificationManager = (NotificationManager)getActivity().getSystemService(NOTIFICATION_SERVICE);

         RemoteViews contentView = new RemoteViews(getActivity().getPackageName(), R.layout.layout_withdraw);
         notification.contentView = contentView;

         Intent notificationIntent = new Intent(getActivity(), HomeActivity.class);
         PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, notificationIntent, 0);
         notification.contentIntent = contentIntent;

         notification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
         notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
         notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
         notification.defaults |= Notification.DEFAULT_SOUND; // Sound

         mNotificationManager.notify(1, notification);
     }
 */
    private void initializeUI(View v) {
        talktolive = (ImageView) v.findViewById(R.id.talktolive);
        hire_paperwork_lay = (ImageView) v.findViewById(R.id.hire_paperwork_lay);
        available_candidate_map = (ImageView) v.findViewById(R.id.available_candidate_map);
        my_assignment_lay = (ImageView) v.findViewById(R.id.my_assignment_lay);
        recruiter_bio_lay = (ImageView) v.findViewById(R.id.recruiter_bio_lay);
        seek_open_jobs_lay = (ImageView) v.findViewById(R.id.seek_open_jobs_lay);
        job_matrix = (ImageView) v.findViewById(R.id.job_matrix);
        notebook_lay = (ImageView) v.findViewById(R.id.notebook_lay);
        upload_resume = (ImageView) v.findViewById(R.id.upload_resume);
        milestone = (ImageView) v.findViewById(R.id.milestone);
        submit_candidate = (ImageView) v.findViewById(R.id.submit_candidate);
        status_report_hire_lay = (ImageView) v.findViewById(R.id.status_report_hire_lay);
        matrix = (ImageView) v.findViewById(R.id.matrix);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void setListener() {
        hire_paperwork_lay.setOnClickListener(listener);
        my_assignment_lay.setOnClickListener(listener);
        job_matrix.setOnClickListener(listener);
        recruiter_bio_lay.setOnClickListener(listener);
        seek_open_jobs_lay.setOnClickListener(listener);
        available_candidate_map.setOnClickListener(listener);
        talktolive.setOnClickListener(listener);
        milestone.setOnClickListener(listener);
        notebook_lay.setOnClickListener(listener);
        available_candidate_map.setOnClickListener(listener);
        status_report_hire_lay.setOnClickListener(listener);
        upload_resume.setOnClickListener(listener);
        submit_candidate.setOnClickListener(listener);
        matrix.setOnClickListener(listener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_splash, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            ShowFragment fragment = new ShowFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "nn");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment = null;
            if (v == hire_paperwork_lay) {
                fragment = new PaperWorkFragment();
            } else if (v == available_candidate_map) {
                fragment = new AvailableCandidateMap_hiringManager();
                Bundle b = new Bundle();
                b.putString("candidate", "candidate");
                fragment.setArguments(b);
            } else if (v == my_assignment_lay) {
                fragment = new MyAssignmentFragment();
            } else if (v == recruiter_bio_lay) {
                fragment = new RecruiterVedioFragment();
            } else if (v == seek_open_jobs_lay) {
                fragment = new SeekJobsFragment();
            } else if (v == notebook_lay) {
                fragment = new NotebookFragment();
            } else if (v == status_report_hire_lay) {
                fragment = new StatusReportFragment();
            } else if (v == upload_resume) {
                fragment = new RecruiterBioFragment();
            } else if (v == talktolive) {
                fragment = new UserAgreementFragment();
                Bundle b = new Bundle();
                b.putString("check_value", "chat");
                fragment.setArguments(b);
            } else if (v == milestone) {
                fragment = new MilestoneFragment();
            } else if (v == submit_candidate) {
                fragment = new SubmitCandidateFragment();
            } else if (v == job_matrix) {
                fragment = new StatusReportFragment();
                Bundle b = new Bundle();
                b.putString("job_matrix", "job_matrix");
                fragment.setArguments(b);
            } else if (v == matrix) {
                fragment = new HireMatrixFragment();
                Bundle b = new Bundle();
                b.putString("check_value", "assignment");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else {
                fragment = new Fragmentvailable();
            }
            UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
        }
    };

}
