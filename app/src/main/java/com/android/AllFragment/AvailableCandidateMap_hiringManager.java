package com.android.AllFragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.FragmentInterface.onFragmentListener;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.model.MyItem;
import com.android.stafftraderapp.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rahul Sonkhiya.
 */

/**
 * Modified by Anupam tyagi.
 */
public class AvailableCandidateMap_hiringManager extends Fragment implements OnMapReadyCallback, ClusterManager.OnClusterItemInfoWindowClickListener<MyItem> {
    private GoogleMap mMap = null;
    private SupportMapFragment mapFragment;
    private ClusterManager<MyItem> mClusterManager;
    private onFragmentListener listener;
    private LatLngBounds.Builder builder;
    private PlaceAutocompleteFragment autocompleteFragment;
    private static View view;
    int PLACE_PICKER_REQUEST = 1;
    private Bundle b = null;
    private ArrayList<MyItem> map_list = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_availablecandidate_map, container, false);

        } catch (InflateException e) {
    /* map is already there, just return view as it is */
        }

           /* if(mapFragment==null)
                mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_marker);
            else
                Log.e("map fragemnt not null","map fragment is not null");*/
        /* map is already there, just return view as it is */
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(this);
        }
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.map_container, mapFragment).commit(); // keep in mind replace and add
        Log.e("on create view map", "on create view map ");
        autocompleteFragment = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 10));
                //  CandidateActivity.showToast(getActivity(),"place name"+ "Place: " + place.getName() + place.getLatLng() + place.getAddress() + place.getLocale() + place.getPhoneNumber());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Toast.makeText(getActivity(), "place name An error occurred: " + status.getStatusMessage() + "status" + status, Toast.LENGTH_LONG).show();
            }
        });

       /* PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }*/


        // Place Picker class useful of get your current address
        // it will show the attractive ui of map in android always keep in mind of this topic
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
        return view;
    }

    private void showPopup() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View interested_view = factory.inflate(
                R.layout.filter_layout, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(interested_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView recruiter = (TextView) interested_view.findViewById(R.id.recruiter);
        final ImageButton cross_button = (ImageButton) interested_view.findViewById(R.id.cross_button);
        final TextView manager = (TextView) interested_view.findViewById(R.id.manager);
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
            recruiter.setText("Candidate");
            manager.setText("Recruiter");
        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
            recruiter.setText("Candidate");
            manager.setText("Manager");
        } else {
            recruiter.setText("Recruiter");
            manager.setText("Manager");
        }
        recruiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recruiter.getText().toString().equals("Candidate"))
                    getSortedUsers("4");
                else
                    getSortedUsers("3");

                deleteDialog.dismiss();
            }
        });
        manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (manager.getText().toString().equals("Recruiter"))
                    getSortedUsers("3");
                else
                    getSortedUsers("2");

                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }
    private void getSortedUsers(String role_id) {
        mClusterManager.clearItems();
        Log.e("map list size", String.valueOf(map_list.size()) + "role id" + role_id);
        for (MyItem item : map_list) {
            if (item.getRole_id().equals(role_id)) {
                Log.e("role id ", item.getRole_id());
                mClusterManager.addItem(item);
                // map_list.add(offsetItem);
                builder.include(item.getPosition());
            }
        }
        LatLngBounds bounds = builder.build();
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 10));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(28.596026, 77.31449359999999), 9));
        setUpCluster();
        if (mMap == null)
            Log.e("google map is null", "google map is null");
        else
            Log.e("google map is not null", "google map is not null");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = getArguments();
        Log.e("on create", "on create");

    }

    public AvailableCandidateMap_hiringManager() {
        super();
    }

    @Override
    public void onPause() {
        super.onPause();
// o my god finally i got it
    }

    @Override
    public void onStop() {
        super.onStop();
        Fragment f = (Fragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on start", "on start");
        if (mapFragment != null) {

        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.e("on destroy view", "on destroy view");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.e("on Attach", "on Attach");
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    private void setUpCluster() {
        builder = new LatLngBounds.Builder();
        mClusterManager = new ClusterManager<MyItem>(getActivity(), mMap);
        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mClusterManager.setOnClusterItemInfoWindowClickListener(this); //added
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setRenderer(new OwnRendring(getActivity(), mMap, mClusterManager));
        getUsers();

        //   addItems();

        // we can make a custom window adapterin marker event
      /*  mClusterManager
                .setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MyItem>() {
                    @Override
                    public boolean onClusterItemClick(MyItem item) {
                        CandidateActivity.showToast(getActivity(),"cluster item "+ item.getTitle());
                        return true;
                    }
                });*/
    }

    private void getUsers() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("role_id", UserPref.getuser_instance(getActivity()).getRoleId());
        CallService.getInstance().passInfromation(api_listener, Constants.INTERACTIVE_MAP, params, true, "1", getActivity());
    }

    /* private void addItems() {
         // Set some lat/lng coordinates to start with.
         double lat = 28.596026;
         double lng = 77.31449359999999;
         // Add ten cluster items in close proximity, for purposes of this example.
         for (int i = 0; i < 100; i++) {
             double offset = i / 120d;
             lat = lat + offset;
             lng = lng + offset;
             MyItem offsetItem = new MyItem(lat, lng, " Location - Noida", "Near Metro Station ,sec 7");
             mClusterManager.addItem(offsetItem);
             builder.include(offsetItem.getPosition());
         }
     }
 */
    @Override
    public void onClusterItemInfoWindowClick(MyItem myItem) {
        //   AllCandidateResponse.Candidate candidate = (new AllCandidateResponse()).new Candidate();
        //candidate.setJob_title(myItem.getTitle());
       /* candidate.setDesired_location(myItem.getDesired_location());
        candidate.setImage(myItem.getImage());
        candidate.setIs_hired("1");
        candidate.setIs_interested(myItem.is_interested());
        candidate.setQualification(myItem.getQualification());*/
        if (myItem.getRole_id().equals("2")) {
            JSONArray json_array = myItem.getJoblist();
            Log.e("map", "need to updated");
        } else {
            Fragment fragment = new ViewQualificationFragment();
            Bundle b = new Bundle();
            b.putString("bio", myItem.getBrif_bio());
            b.putString("qualification", myItem.getQualification());
            b.putString("skill", myItem.getSkill());
            fragment.setArguments(b);
            UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
        }
        //  UtilityPermission.showToast(getActivity(), "window item " + myItem.getTitle()+ myItem.getQualification());
    }

    public class OwnRendring extends DefaultClusterRenderer<MyItem> {

        public OwnRendring(Context context, GoogleMap map,
                           ClusterManager<MyItem> clusterManager) {
            super(context, map, clusterManager);
        }


        protected void onBeforeClusterItemRendered(MyItem item, MarkerOptions markerOptions) {

            markerOptions.snippet(item.getSnippet());
            markerOptions.title(item.getTitle());
            super.onBeforeClusterItemRendered(item, markerOptions);
        }

        @Override
        protected void onClusterItemRendered(MyItem clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
            marker.showInfoWindow();
            //here you have access to the marker itself
        }
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e(" map response", response);
            if (response != null) {
                try {
                    JSONObject json_response = new JSONObject(response);
                    if (json_response.getBoolean("status")) {
                        JSONArray data_array = json_response.getJSONArray("data");
                        for (int i = 0; i < data_array.length(); i++) {
                            double offset = i / 100d;
                            Log.e("position", data_array.getJSONObject(i).getString("lat") + " " + data_array.getJSONObject(i).getString("lon"));
                            if (!data_array.getJSONObject(i).getString("lat").equals("") || !data_array.getJSONObject(i).getString("lon").equals("")) {
                                MyItem offsetItem = new MyItem(Double.parseDouble(data_array.getJSONObject(i).getString("lat")) + offset, Double.parseDouble(data_array.getJSONObject(i).getString("lon")) + offset, data_array.getJSONObject(i).getString("first_name") + " " + data_array.getJSONObject(i).getString("last_name"), data_array.getJSONObject(i).getString("job_title"), data_array.getJSONObject(i).getString("image"), data_array.getJSONObject(i).getString("brif_bio"), data_array.getJSONObject(i).getString("qualification"), data_array.getJSONObject(i).getString("skill"), data_array.getJSONObject(i).getJSONArray("job_list"), data_array.getJSONObject(i).getString("role_id"));
                                mClusterManager.addItem(offsetItem);
                                map_list.add(offsetItem);
                                builder.include(offsetItem.getPosition());
                            }
                        }
                        LatLngBounds bounds = builder.build();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));
                    } else {
                        AlertManger.getAlert_instance().showAlert("No Candidate found ", getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(), data);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Toast.makeText(getActivity(), status.getStatusCode() + status.getStatusMessage() + status.getStatus(), Toast.LENGTH_LONG).show();
            }
        }
    }


}
