package com.android.AllFragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.SeekJobResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.SeekJobAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

public class SeekJobsFragment extends Fragment implements ApiResponseListener {
    private RecyclerView recycler_view;
    private onFragmentListener listener;
    private ArrayList<SeekJobResponse.SeekJob> seek_list;
    private SeekJobAdapter adapter;
    public SeekJobsFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        seek_list = new ArrayList<>();
    }
    @Override
    public void onStart() {
        super.onStart();
        adapter = new SeekJobAdapter(seek_list, getActivity(), UserPref.getuser_instance(getActivity()).getUserId());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(adapter);
        if (!UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
            Log.e("role id",UserPref.getuser_instance(getActivity()).getRoleId());
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
            CallService.getInstance().passInfromation(this, Constants.SEEK_OPEN_JOB_URL, params, true, "1", getActivity());
        }
        else {
            CallService.getInstance().passCategoryinformation(this, Constants.OPEN_JOB_REQUIREMENT, true, "1", getActivity());
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_seek_jobs, container, false);
        initilaizeUI(v);
        return v;
    }
    private void initilaizeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
    @Override
    public void getResponse(String response, String request_id) {
        Log.e("response", response);
        if (response != null) {
            SeekJobResponse seek_response = new Gson().fromJson(response, SeekJobResponse.class);
            if (seek_response.isStatus()) {
                seek_list = seek_response.getData();
                adapter = new SeekJobAdapter(seek_list, getActivity(), UserPref.getuser_instance(getActivity()).getUserId());
                recycler_view.setAdapter(adapter);
            } else {
                AlertManger.getAlert_instance().showAlert("There is some error ", getActivity());
            }
        }
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
