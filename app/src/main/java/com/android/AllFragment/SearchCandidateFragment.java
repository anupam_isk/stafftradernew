package com.android.AllFragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;

import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.SearchResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.MyRecyclerAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * created by Anupam tyagi
 */
public class SearchCandidateFragment extends Fragment {
    private LinearLayout recycler_row, linear_row;
    private EditText search_text, weekly, contact, email, jobtype, no_hours, completed, rating, entered;
    private ListView search_recycler_view;
    private Button approved, decline, run_report, candidate_report_card;
    private MyRecyclerAdapter adapter;
    private Button common_view;
    private DatePickerDialog dpd;
    private ArrayList<SearchResponse.SearchJob> search_list;
    private int start_value = 0;
    private int limit_value = 10;
    SearchResponse.SearchJob search_job;
    float rating_one = 0.0f, rating_two = 0.0f, rating_three = 0.0f, rating_four = 0.0f, rating_five = 0.0f, rating_six = 0.0f, rating_seven = 0.0f, rating_eight = 0.0f;
    private String search_string;
    private AlertDialog deleteDialog;
    private onFragmentListener back_listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                date_listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search_candidate, container, false);
        Log.e("on createView", "on create view");
        initializeView(v);
        setListener();
        return v;
    }

    private void setListener() {
        run_report.setOnClickListener(listener);
        candidate_report_card.setOnClickListener(listener);
        approved.setOnClickListener(listener);
        decline.setOnClickListener(listener);
        weekly.setOnClickListener(listener);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on start fragment", "on start fragment");
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
            approved.setVisibility(View.GONE);
            decline.setVisibility(View.GONE);
        }
        search_list = new ArrayList<>();
        search_recycler_view.setOnScrollListener(onScrollListener());
    }

    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            // if compare date and searching with id
            if (search_job != null) {
                weekly.setText(date);
                HashMap<String, String> params = new HashMap<>();
                params.put("candidate_id", search_job.getId());
                params.put("date", date);
                CallService.getInstance().passInfromation(api_listener, Constants.SEARCH_CANDIDATES_URL, params, true, "2", getActivity());
            } else {
                AlertManger.getAlert_instance().showAlert("Please first select the candidate", getActivity());
            }
        }
    };
    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            if (request_id.equals("1")) {

                if (response != null) {
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    if (search_response.isStatus()) {
                        search_list.addAll(search_response.getData());
                        Log.e("search list size", search_list.size() + "");
                        adapter = new MyRecyclerAdapter(getActivity(), search_list);
                        search_recycler_view.setAdapter(adapter);
                    } else {
                        if (search_list.size() > 0) {

                        } else {
                            AlertManger.getAlert_instance().showAlert("No Record Found", getActivity());
                            linear_row.setVisibility(View.VISIBLE);
                            recycler_row.setVisibility(View.GONE);
                        }
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    if (search_response.isStatus()) {
                        search_job = search_response.getData().get(0);
                        email.setText(search_job.getEmail());
                        jobtype.setText(search_job.getJob_title());
                        contact.setText(search_job.getPhone());
                        no_hours.setText(search_job.getHours());
                        completed.setText(search_job.getCompleted());
                    } else {
                        UtilityPermission.showToast(getActivity(), "There is no data of this date");
                    }
                }
            }
            if (request_id.equals("3")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                                approved.setVisibility(View.GONE);
                                decline.setVisibility(View.GONE);
                            } else {
                                if (common_view == decline) {
                                    decline.setEnabled(false);
                                    decline.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent_green_color));
                                    approved.setEnabled(true);
                                    approved.setBackgroundColor(getActivity().getResources().getColor(R.color.green_color));
                                } else {
                                    approved.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent_green_color));
                                    approved.setEnabled(false);
                                    decline.setEnabled(true);
                                    decline.setBackgroundColor(getActivity().getResources().getColor(R.color.green_color));
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("4")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            rating.setText(json_response.getString("data"));
                        } else {
                            UtilityPermission.showToast(getActivity(), "No Record found");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("5")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), "Report sucessfuly generated");
                            deleteDialog.dismiss();
                        } else {
                            UtilityPermission.showToast(getActivity(), "Something went wrong");
                            deleteDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    private AbsListView.OnScrollListener onScrollListener() {
        return new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int threshold = 1;
                int count = search_recycler_view.getCount();
                if (scrollState == SCROLL_STATE_IDLE) {
                    if (search_recycler_view.getLastVisiblePosition() >= count - threshold) {
                        Log.e("start value", start_value + "");
                        Log.e("limit value ", limit_value + "");
                        start_value = start_value + 10;
                        limit_value = limit_value + 10;
                        getData();

                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
            }

        };
    }

    private void getData() {
        HashMap<String, String> params = new HashMap<>();
        params.put("start", String.valueOf(start_value));
        params.put("limit", String.valueOf(limit_value));
        params.put("search_term", search_string);
        CallService.getInstance().passInfromation(api_listener, Constants.SEARCH_CANDIDATES_URL, params, true, "1", getActivity());
    }

    private void initializeView(View v) {
        recycler_row = (LinearLayout) v.findViewById(R.id.recycler_row);
        linear_row = (LinearLayout) v.findViewById(R.id.linear_row);
        search_text = (EditText) v.findViewById(R.id.search_text);
        weekly = (EditText) v.findViewById(R.id.weekly);
        contact = (EditText) v.findViewById(R.id.contact);
        rating = (EditText) v.findViewById(R.id.rating);
        email = (EditText) v.findViewById(R.id.email);
        jobtype = (EditText) v.findViewById(R.id.jobtype);
        entered = (EditText) v.findViewById(R.id.entered);
        no_hours = (EditText) v.findViewById(R.id.no_hours);
        completed = (EditText) v.findViewById(R.id.completed);
        approved = (Button) v.findViewById(R.id.approved);
        decline = (Button) v.findViewById(R.id.decline);
        run_report = (Button) v.findViewById(R.id.run_report);
        candidate_report_card = (Button) v.findViewById(R.id.candidate_report_card);
        search_recycler_view = (ListView) v.findViewById(R.id.search_recycler_view);
        search_recycler_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                search_job = search_list.get(position);
                linear_row.setVisibility(View.VISIBLE);
                recycler_row.setVisibility(View.GONE);
                if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                    approved.setVisibility(View.GONE);
                    decline.setVisibility(View.GONE);
                } else {
                    if (search_job.getAssign_id() != null) {
                        if (search_job.getStatus().equals("0")) {
                            decline.setEnabled(false);
                            decline.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent_green_color));
                            approved.setEnabled(true);
                            approved.setBackgroundColor(getActivity().getResources().getColor(R.color.green_color));
                        } else {
                            approved.setBackgroundColor(getActivity().getResources().getColor(R.color.transparent_green_color));
                            approved.setEnabled(false);
                            decline.setEnabled(true);
                            decline.setBackgroundColor(getActivity().getResources().getColor(R.color.green_color));
                        }
                    } else {
                        AlertManger.getAlert_instance().showAlert("There is no entry for this candidate", getActivity());
                    }
                }
                email.setText(search_job.getEmail());
                jobtype.setText(search_job.getJob_title());
                contact.setText(search_job.getPhone());
                weekly.setText(search_job.getDate());
                no_hours.setText(search_job.getHours());
                entered.setText(search_job.getEntered());
                completed.setText(search_job.getCompleted());

            }
        });
        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 3) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    search_list.clear();
                    linear_row.setVisibility(View.GONE);
                    recycler_row.setVisibility(View.VISIBLE);
                    if (weekly.getText().length() > 0)
                        getDataFromserverWithDate(s.toString());
                    else
                        getDataFromServer(s.toString());
                } else if (s.length() == 0) {
                    linear_row.setVisibility(View.VISIBLE);
                    recycler_row.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getDataFromserverWithDate(String content) {
        Log.e("if", "if");
        search_string = content;
        HashMap<String, String> params = new HashMap<>();
        params.put("start", String.valueOf(start_value));
        params.put("limit", String.valueOf(limit_value));
        params.put("search_term", search_string);
        params.put("date", weekly.getText().toString());
        CallService.getInstance().passInfromation(api_listener, Constants.SEARCH_CANDIDATES_URL, params, true, "1", getActivity());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            back_listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        back_listener.showDrawerToggle(false);
    }

    private void getDataFromServer(String content) {
        Log.e("else", "else");
        search_string = content;
        HashMap<String, String> params = new HashMap<>();
        params.put("start", String.valueOf(start_value));
        params.put("limit", String.valueOf(limit_value));
        params.put("search_term", search_string);
        CallService.getInstance().passInfromation(api_listener, Constants.SEARCH_CANDIDATES_URL, params, true, "1", getActivity());
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == approved) {
                common_view = approved;
                HashMap<String, String> params = new HashMap<>();
                params.put("candidate_id", search_job.getId());
                params.put("manager_id", UserPref.getuser_instance(getActivity()).getUserId());
                CallService.getInstance().passInfromation(api_listener, Constants.CHANGE_ASSIGNMENT_STATUS_URL, params, true, "3", getActivity());
            } else if (v == decline) {
                common_view = decline;
                HashMap<String, String> params = new HashMap<>();
                params.put("candidate_id", search_job.getId());
                params.put("manager_id", UserPref.getuser_instance(getActivity()).getUserId());
                CallService.getInstance().passInfromation(api_listener, Constants.CHANGE_ASSIGNMENT_STATUS_URL, params, true, "3", getActivity());
            } else if (v == run_report) {
                if (search_job != null) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("candidate_id", search_job.getId());
                    params.put("manager_id", UserPref.getuser_instance(getActivity()).getUserId());
                    CallService.getInstance().passInfromation(api_listener, Constants.SHOW_ASSIGNMENT_REPORT_URL, params, true, "4", getActivity());
                } else {
                    AlertManger.getAlert_instance().showAlert("Please first select the candidate", getActivity());
                }

            } else if (v == candidate_report_card) {
                showDialog();
            } else if (v == weekly) {
                dpd.show(getActivity().getFragmentManager(), "DatePickerDialog");
            }
        }
    };

    private void showDialog() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View rating_view = factory.inflate(R.layout.rating_view, null);
        deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(rating_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final RatingBar rating1 = (RatingBar) rating_view.findViewById(R.id.rating1);
        final RatingBar rating2 = (RatingBar) rating_view.findViewById(R.id.rating2);
        final RatingBar rating3 = (RatingBar) rating_view.findViewById(R.id.rating3);
        final RatingBar rating4 = (RatingBar) rating_view.findViewById(R.id.rating4);
        final RatingBar rating5 = (RatingBar) rating_view.findViewById(R.id.rating5);
        final RatingBar rating6 = (RatingBar) rating_view.findViewById(R.id.rating6);
        final RatingBar rating7 = (RatingBar) rating_view.findViewById(R.id.rating7);
        final RatingBar rating8 = (RatingBar) rating_view.findViewById(R.id.rating8);
        final EditText comment = (EditText) rating_view.findViewById(R.id.comment);
        final Button button = (Button) rating_view.findViewById(R.id.submit_event);
        final ImageButton cross_button = (ImageButton) rating_view.findViewById(R.id.cross_button);
        cross_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        RatingBar.OnRatingBarChangeListener rating_listener = new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                if (ratingBar == rating1) {
                    rating_one = rating;
                    Log.e("rating 1", String.valueOf(rating));
                } else if (ratingBar == rating2) {
                    rating_two = rating;
                    Log.e("rating 2", String.valueOf(rating));
                } else if (ratingBar == rating3) {
                    rating_three = rating;
                    Log.e("rating 3", String.valueOf(rating));
                } else if (ratingBar == rating4) {
                    rating_four = rating;
                    Log.e("rating 4", String.valueOf(rating));
                } else if (ratingBar == rating5) {
                    rating_five = rating;
                    Log.e("rating 5", String.valueOf(rating));
                } else if (ratingBar == rating6) {
                    rating_six = rating;
                    Log.e("rating 6", String.valueOf(rating));
                } else if (ratingBar == rating7) {
                    rating_seven = rating;
                    Log.e("rating 7", String.valueOf(rating));
                } else if (ratingBar == rating8) {
                    rating_eight = rating;
                    Log.e("rating 8", String.valueOf(rating));
                }
            }
        };
        rating1.setOnRatingBarChangeListener(rating_listener);
        rating2.setOnRatingBarChangeListener(rating_listener);
        rating3.setOnRatingBarChangeListener(rating_listener);
        rating4.setOnRatingBarChangeListener(rating_listener);
        rating5.setOnRatingBarChangeListener(rating_listener);
        rating6.setOnRatingBarChangeListener(rating_listener);
        rating7.setOnRatingBarChangeListener(rating_listener);
        rating8.setOnRatingBarChangeListener(rating_listener);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (search_job != null) {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("punctuality", String.valueOf(rating_one));
                    params.put("professional", String.valueOf(rating_two));
                    params.put("personality", String.valueOf(rating_three));
                    params.put("potential", String.valueOf(rating_four));
                    params.put("competency", String.valueOf(rating_five));
                    params.put("confidence", String.valueOf(rating_six));
                    params.put("communication", String.valueOf(rating_seven));
                    params.put("capable", String.valueOf(rating_eight));
                    params.put("comment", comment.getText().toString());
                    params.put("instance_id", search_job.getAssign_id());
                    params.put("type", "Assignment");
                    params.put("candidate_id", search_job.getId());
                    params.put("manager_id", UserPref.getuser_instance(getActivity()).getUserId());
                    CallService.getInstance().passInfromation(api_listener, Constants.CANDIDATE_ASSIGNMENT_REPORT_URL, params, true, "5", getActivity());
                } else {
                    AlertManger.getAlert_instance().showAlert("Please first select the candidate", getActivity());
                }
            }
        });

        deleteDialog.show();
    }

}
