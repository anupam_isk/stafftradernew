package com.android.AllFragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.android.ChooseCredential;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.SinchCommunication.BaseFragment;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.helper.Validation;
import com.android.stafftraderapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Anupam tyagi
 */
public class SettingsFragment extends BaseFragment implements View.OnClickListener {
    private RelativeLayout phone_number_lay, chnagePasswd_lay, help_center_lay, user_agreement_lay, privacy_policy_lay, signout_lay,report_a_problem;
    private Switch switchButton;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        initializeUI(rootView);
        setListener();
        return rootView;
    }

    private void setListener() {
        phone_number_lay.setOnClickListener(this);
        chnagePasswd_lay.setOnClickListener(this);
        help_center_lay.setOnClickListener(this);
        user_agreement_lay.setOnClickListener(this);
        privacy_policy_lay.setOnClickListener(this);
        signout_lay.setOnClickListener(this);
    }

    private void initializeUI(View rootView) {
        phone_number_lay = (RelativeLayout) rootView.findViewById(R.id.phone_number_lay);
        chnagePasswd_lay = (RelativeLayout) rootView.findViewById(R.id.chnagePasswd_lay);
        help_center_lay = (RelativeLayout) rootView.findViewById(R.id.help_center_lay);
        user_agreement_lay = (RelativeLayout) rootView.findViewById(R.id.user_agreement_lay);
        privacy_policy_lay = (RelativeLayout) rootView.findViewById(R.id.privacy_policy_lay);
        signout_lay = (RelativeLayout) rootView.findViewById(R.id.signout_lay);
        switchButton = (Switch) rootView.findViewById(R.id.switchButton);
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    UtilityPermission.showToast(getActivity(), "True");
                else
                    UtilityPermission.showToast(getActivity(), "False");
            }
        });
    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        if (v == phone_number_lay) {
            showPhoneNumberPopup();
        }
        if (v == chnagePasswd_lay)
            showChangePassword();
        if (v == help_center_lay) {
            fragment = new PrivacyPolicyFragment();
            Bundle b = new Bundle();
            b.putString("value", "contact");
            fragment.setArguments(b);
        }
        if (v == user_agreement_lay) {
            Bundle b = new Bundle();
            b.putString("value", "agree");
            fragment = new PrivacyPolicyFragment();
            fragment.setArguments(b);
        }
        // fragment = new UserAgreementFragment();
        if (v == privacy_policy_lay) {
            Bundle b = new Bundle();
            b.putString("value", "policy");
            fragment = new PrivacyPolicyFragment();
            fragment.setArguments(b);
        }
        if (v == signout_lay)
            showPopup();
        if (v != signout_lay && v!=phone_number_lay && v!=chnagePasswd_lay)
            UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
    }

    private void showChangePassword() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View interested_view = factory.inflate(
                R.layout.change_password_layout, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(interested_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final EditText oldpasswd = (EditText) interested_view.findViewById(R.id.oldPasswd);
        final  EditText Newpaaswd = (EditText) interested_view.findViewById(R.id.newpasswd);
        final EditText confrmPaaswd = (EditText) interested_view.findViewById(R.id.newconfrmPasswd);
        TextView submit = (TextView) interested_view.findViewById(R.id.btnSubmit);
        TextView cancel=(TextView) interested_view.findViewById(R.id.cancel);
        TextView done=(TextView) interested_view.findViewById(R.id.done);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                    if (ValidatePassword(oldpasswd,Newpaaswd,confrmPaaswd)) {
                        HashMap<String,String> params=new HashMap<String,String>();
                        params.put("user_id",UserPref.getuser_instance(getActivity()).getUserId());
                        params.put("old_p", oldpasswd.getText().toString());
                        params.put("new_p",Newpaaswd.getText().toString());
                        params.put("confirm_p", confrmPaaswd.getText().toString());
                        CallService.getInstance().passInfromation(api_listener, Constants.CHANGE_PASSWORD_URL,params,true,"2",getActivity());
                    }
                    //   getRemoteJSON(URL.change_phoneNumber + "&user_id=" + UserPreferences.GetUserID() + "&phone=" + NewNumValue);
                }
                else
                {
                    UtilityPermission.showToast(getActivity(), Constants.NETWORK_STRING);
                }
            }
        });
        deleteDialog.show();
    }
    public boolean ValidatePassword(EditText oldpasswd, EditText Newpaaswd,EditText confrmPaaswd)
    {
        String old_password=oldpasswd.getText().toString();
        String new_password= Newpaaswd.getText().toString();
        if(old_password.length()>0) {
            if(new_password.length() >0)
            {
                if(confrmPaaswd.getText().toString().length()>0 && new_password.equals(confrmPaaswd.getText().toString()))
                {
                    return  true;
                }
                else
                {
                    confrmPaaswd.setError("Please Enter Confirm password");
                    return  false;
                }

            }
            else
            {
                Newpaaswd.setError("Please Enter New Password");
                return  false;
            }
        }
        else
        {
            oldpasswd.setError("Please Enter Password");
            return false;
        }

    }
    private void showPhoneNumberPopup() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View interested_view = factory.inflate(
                R.layout.update_phone_number, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(interested_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final EditText oldNum = (EditText)interested_view.findViewById(R.id.oldPhonenumber);
        final EditText NewNum = (EditText) interested_view.findViewById(R.id.newPhonenumber);
        TextView submit = (TextView) interested_view.findViewById(R.id.SubmitNewCOntactNumber);
        TextView cancel=(TextView) interested_view.findViewById(R.id.cancel);
        TextView done=(TextView) interested_view.findViewById(R.id.done);
        oldNum.setText(UserPref.getuser_instance(getActivity()).getUserPhone());
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                    if (Validate(oldNum,NewNum)) {
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                        params.put("phone", NewNum.getText().toString());
                        CallService.getInstance().passInfromation(api_listener, Constants.CHANGE_PHONE_NUM_URL, params, true, "1", getActivity());
                    }
                    //   getRemoteJSON(URL.change_phoneNumber + "&user_id=" + UserPreferences.GetUserID() + "&phone=" + NewNumValue);
                } else {
                    UtilityPermission.showToast(getActivity(), Constants.NETWORK_STRING);
                }
            }
        });
        deleteDialog.show();

    }

    private boolean Validate(EditText oldNum, EditText NewNum) {
        final String oldNumValue = oldNum.getText().toString();
        final String NewNumValue = NewNum.getText().toString();
        if (oldNum.getText().toString().length() > 0 && Validation.isValidMobile(oldNumValue)) {
            if (NewNum.getText().toString().length() > 0 && Validation.isValidMobile(NewNumValue)) {
                return true;
            } else {
                oldNum.setError(null);
                NewNum.setError("Invalid Number");
                return false;
            }
        } else {
            oldNum.setError("Invalid Number");
            NewNum.setError(null);
            return false;
        }

    }
    ApiResponseListener api_listener=new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (response != null) {
                if(request_id.equals("1")) {
                    try {
                        JSONObject json = new JSONObject(response.toString());
                        if (json.getBoolean("status")) {
                            UserPref.getuser_instance(getActivity()).setUserPhone(json.getString("phone"));
                            UtilityPermission.showToast(getActivity(), "Phone Number update successfully");
                        } else {
                            UtilityPermission.showToast(getActivity(), "There is some error on server side.");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(request_id.equals("2"))
                {
                    try {
                        JSONObject json = new JSONObject(response);
                        if (json.getBoolean("status")) {
                            UserPref.getuser_instance(getActivity()).setUserPassword(json.getString("password"));
                            UtilityPermission.showToast(getActivity(),"Password update successfully");
                        } else {
                            UtilityPermission.showToast(getActivity(),"There is some error on server side.");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    };

    private void showPopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Staff Trader App");
        builder.setMessage("Are you sure you want to logout ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               /* if (getSinchServiceInterface() != null) {
                    Log.e("client stop", "client stop");
                    getSinchServiceInterface().stopClient();
                }*/
                UserPref.getuser_instance(getActivity()).clearPref();
                Intent i = new Intent(getActivity(), ChooseCredential.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                getActivity().finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}

