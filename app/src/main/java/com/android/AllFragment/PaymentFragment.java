package com.android.AllFragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.PaymentAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment {
    private LinearLayout recycler_row, linear_row;
    private ArrayList<HashMap<String, String>> row_list;
    private PaymentAdapter adapter;
    private TextView searchBT;
    private EditText search_by_month, search_by_name;
    private ListView search_recycler_view;
    private ArrayList<HashMap<String, String>> search_list;
    private onFragmentListener listener;
    private String customer_id = null, date_string = null;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recycler_view;
    private static RelativeLayout upper_layout;
    private boolean other_check = true;
    private HashMap<String, String> params = new HashMap<>();
    private int limit = 10;
    private int offset = -10;
    private String current_date = null;
    private SearchAdapter search_adapter;
    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private DatePickerDialog dpd;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.e("on attache payment", "on attach payment");
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("on actcreated payment", "on activity created payment");
    }

    public PaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on start payment ", "on start paymrnt");
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
        Log.e("on start payment", "on start payment");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("on create payment", "on create payment");
        row_list = new ArrayList<>();
        search_list = new ArrayList<>();
        offset = offset + 10;
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                date_listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
            if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
                params.clear();
                current_date = getCurrentDate();
                Log.e("current_date", getCurrentDate());
                getCallToServer(Constants.PAYMENTS_URL, params, "1", offset, getCurrentDate(), true);
            } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                params.clear();
                getCallToServer(Constants.RECRUITER_PAYMENT_URL, params, "2", offset, getCurrentDate(), true);
            }
        } else {
            AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
        }
    }

    String getCurrentDate() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dateobj = new Date();
        return df.format(dateobj);
    }

    private void getCallToServer(String paymentsUrl, HashMap<String, String> params, String request_id, int offset, String currentDate, boolean b) {
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("limit", String.valueOf(limit));
        params.put("offset", String.valueOf(offset));
        params.put("date", currentDate);
        CallService.getInstance().passInfromation(api_listener, paymentsUrl, params, b, request_id, getActivity());
    }

    private void initializeUI(View v) {
        mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_row = (LinearLayout) v.findViewById(R.id.recycler_row);
        linear_row = (LinearLayout) v.findViewById(R.id.linear_row);
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        search_by_month = (EditText) v.findViewById(R.id.search_by_month);
        search_recycler_view = (ListView) v.findViewById(R.id.search_recycler_view);
        upper_layout = (RelativeLayout) v.findViewById(R.id.loadItemsLayout_recyclerView);
        search_by_name = (EditText) v.findViewById(R.id.search_by_name);
        searchBT = (TextView) v.findViewById(R.id.searchBT);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        adapter = new PaymentAdapter(row_list, getActivity());
        recycler_view.setAdapter(adapter);
        search_by_month.setOnClickListener(click_listener);
        searchBT.setOnClickListener(click_listener);
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
            search_by_name.setVisibility(View.GONE);
            searchBT.setVisibility(View.GONE);
        }
        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Here get the child count, item count and visibleitems
                // from layout manager
                if (dy < 0) {
                    Log.e("Scrolled Upwards", "Scrolled Upwards");

                } else {
                    Log.e("Scrolled downwords", "Scrolled downwords");
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    // Now check if userScrolled is true and also check if
                    // the item is end then update recycler view and set
                    // userScrolled to false
                    if (userScrolled && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                        Log.e("in condition", "in condition");
                        userScrolled = false;
                        updateRecyclerView();

                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // If scroll state is touch scroll then set userScrolled
                // true
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    if (other_check)
                        userScrolled = true;
                }
            }
        });
        search_by_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 3) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    linear_row.setVisibility(View.GONE);
                    recycler_row.setVisibility(View.VISIBLE);
                    search_list.clear();
                    //getDataFromServer(editable.toString());
                } else if (editable.length() == 0) {
                    linear_row.setVisibility(View.VISIBLE);
                    recycler_row.setVisibility(View.GONE);
                }
            }
        });
        search_recycler_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> hmap = search_list.get(position);
                linear_row.setVisibility(View.VISIBLE);
                recycler_row.setVisibility(View.GONE);
                customer_id = hmap.get("candidate_id");
            }
        });
        //   adapter.setAnother_listener(another_listener);
    }

    private void updateRecyclerView() {
        offset = offset + 10;
        upper_layout.setVisibility(View.VISIBLE);
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2")) {
            Log.e("in if", "in if");
            params.clear();
            getCallToServer(Constants.PAYMENTS_URL, params, "3", offset, current_date, false);
        } else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
            params.clear();
            getCallToServer(Constants.RECRUITER_PAYMENT_URL, params, "4", offset, getCurrentDate(), false);
        }
    }

    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            //   date_string=(monthOfYear + 1)+","+year;
            search_by_month.setText(date);
            current_date = date;
            offset = 0;
            params.clear();
            row_list.clear();
            if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2"))
                getCallToServer(Constants.PAYMENTS_URL, params, "1", offset, date, true);
            else
                getCallToServer(Constants.RECRUITER_PAYMENT_URL, params, "2", offset, date, true);
        }
    };

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == search_by_month) {
                Log.e("in click", "in click");
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            } else if (v == searchBT) {
                getSearchDataBasedonCustomerId();
            }

        }
    };

    private void getSearchDataBasedonCustomerId() {
        if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
            params.put("candidate_id", customer_id);
            params.put("month", date_string);
            CallService.getInstance().passInfromation(api_listener, Constants.RECRUITER_PAYMENT_URL, params, true, "1", getActivity());
        } else {
            AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("on destroy view payment", "on destroy view payment");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("on destroy payment", "on destroy payment");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("on stop payment", "on stop payment");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("on pause", "on pause");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_payment, container, false);
        initializeUI(v);
        Log.e("on create view payment", "on create view payment");
        return v;
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response ", response + request_id);
            if (request_id.equals("3")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONArray json_array = json.getJSONArray("data");
                        for (int i = 0; i < json_array.length(); i++) {
                            HashMap<String, String> hmap = new HashMap<>();
                            hmap.put("id", json_array.getJSONObject(i).getString("id"));
                            hmap.put("date", json_array.getJSONObject(i).getString("date"));
                            hmap.put("payment_status", json_array.getJSONObject(i).getString("payment_status"));
                            hmap.put("payment_date", json_array.getJSONObject(i).getString("payment_date"));
                            hmap.put("payment", json_array.getJSONObject(i).getString("payment"));
                            hmap.put("rate", json_array.getJSONObject(i).getString("rate"));
                            hmap.put("hour", json_array.getJSONObject(i).getString("hour"));
                            Log.e("in loop", "in loop");
                            row_list.add(hmap);
                        }
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter = new PaymentAdapter(row_list, getActivity());
                        recycler_view.setAdapter(adapter);
                        upper_layout.setVisibility(View.GONE);
                    } else {
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter.notifyDataSetChanged();
                        upper_layout.setVisibility(View.GONE);
                        other_check = false;
                        UtilityPermission.showToast(getActivity(), "There is no more candidate!!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (request_id.equals("1")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONArray json_array = json.getJSONArray("data");
                        for (int i = 0; i < json_array.length(); i++) {
                            HashMap<String, String> hmap = new HashMap<>();
                            hmap.put("id", json_array.getJSONObject(i).getString("id"));
                            hmap.put("date", json_array.getJSONObject(i).getString("date"));
                            hmap.put("payment_status", json_array.getJSONObject(i).getString("payment_status"));
                            hmap.put("payment_date", json_array.getJSONObject(i).getString("payment_date"));
                            hmap.put("payment", json_array.getJSONObject(i).getString("payment"));
                            hmap.put("rate", json_array.getJSONObject(i).getString("rate"));
                            hmap.put("hour", json_array.getJSONObject(i).getString("hour"));
                            Log.e("in loop", "in loop");
                            row_list.add(hmap);
                        }
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter = new PaymentAdapter(row_list, getActivity());
                        recycler_view.setAdapter(adapter);

                    } else {
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (request_id.equals("2")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONArray json_array = json.getJSONArray("data");
                        for (int i = 0; i < json_array.length(); i++) {
                            HashMap<String, String> hmap = new HashMap<>();
                            hmap.put("id", json_array.getJSONObject(i).getString("id"));
                            hmap.put("date", json_array.getJSONObject(i).getString("date"));
                            hmap.put("payment_status", json_array.getJSONObject(i).getString("payment_status"));
                            hmap.put("payment_date", json_array.getJSONObject(i).getString("payment_date"));
                            hmap.put("payment", json_array.getJSONObject(i).getString("payment"));
                            hmap.put("rate", json_array.getJSONObject(i).getString("rate"));
                            hmap.put("hour", json_array.getJSONObject(i).getString("hour"));
                            Log.e("in loop", "in loop");
                            row_list.add(hmap);
                        }
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter = new PaymentAdapter(row_list, getActivity());
                        recycler_view.setAdapter(adapter);

                    } else {
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
            if (request_id.equals("4")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONArray json_array = json.getJSONArray("data");
                        for (int i = 0; i < json_array.length(); i++) {
                            HashMap<String, String> hmap = new HashMap<>();
                            hmap.put("id", json_array.getJSONObject(i).getString("id"));
                            hmap.put("date", json_array.getJSONObject(i).getString("date"));
                            hmap.put("payment_status", json_array.getJSONObject(i).getString("payment_status"));
                            hmap.put("payment_date", json_array.getJSONObject(i).getString("payment_date"));
                            hmap.put("payment", json_array.getJSONObject(i).getString("payment"));
                            hmap.put("rate", json_array.getJSONObject(i).getString("rate"));
                            hmap.put("hour", json_array.getJSONObject(i).getString("hour"));
                            Log.e("in loop", "in loop");
                            row_list.add(hmap);
                        }
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter = new PaymentAdapter(row_list, getActivity());
                        recycler_view.setAdapter(adapter);
                        upper_layout.setVisibility(View.GONE);
                    } else {
                        Log.e("row list length", String.valueOf(row_list.size()));
                        adapter.notifyDataSetChanged();
                        upper_layout.setVisibility(View.GONE);
                        other_check = false;
                        UtilityPermission.showToast(getActivity(), "There is no more candidate!!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    };

    public class SearchAdapter extends BaseAdapter {
        ArrayList<HashMap<String, String>> search_list;
        Context context;
        private LayoutInflater inflater = null;

        public SearchAdapter(Context mainActivity, ArrayList<HashMap<String, String>> search_list) {
            // TODO Auto-generated constructor stub
            this.search_list = search_list;
            context = mainActivity;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return search_list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public class Holder {
            TextView tv;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            View rowView = convertView;
            if (rowView == null) {
                holder = new Holder();
                rowView = inflater.inflate(R.layout.list_row, null);
                holder.tv = (TextView) rowView.findViewById(R.id.name);
                rowView.setTag(holder);
            } else
                holder = (Holder) convertView.getTag();
            holder.tv.setText(search_list.get(position).get("first_name"));

            return rowView;
        }
    }
}
