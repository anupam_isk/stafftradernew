package com.android.AllFragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.AvailableAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragmentvailable extends Fragment implements ApiResponseListener, SearchView.OnQueryTextListener {
    private RecyclerView recycler_view;
    private onFragmentListener listener;
    private AvailableAdapter adapter;
    private ArrayList<AllCandidateResponse.Candidate> candidates_list;
    private int lastFirstVisiblePosition;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        candidates_list = new ArrayList<>();
        if (getArguments() != null) {
            Bundle b = getArguments();
            AllCandidateResponse.Candidate candidate = b.getParcelable("candidate");
            candidates_list.add(candidate);
        } else {
            if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
                CallService.getInstance().passCategoryinformation(this, Constants.AVAILABLE_CANDIDATES_URL, true, "1", getActivity());
            } else {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                CallService.getInstance().passInfromation(this, Constants.SHOW_ACTIVE_CANDIDATES_URL, params, true, "1", getActivity());
            }
        }
    }
    @Override
    public void onStart() {
        super.onStart();
       /* candidates_list = new ArrayList<>();
        if(getArguments()!=null)
        {
            Log.e("not null","not null");
            Bundle b=getArguments();
            AllCandidateResponse.Candidate candidate=b.getParcelable("candidate");
            candidates_list.add(candidate);
        }
        else {
            if(user_pref.getRoleId().equals("3"))
            {
                CallService.getInstance().passCategoryinformation(this, Constants.AVAILABLE_CANDIDATES_URL, true, "1", getActivity());
            }
            else {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_pref.getUserId());
                CallService.getInstance().passInfromation(this, Constants.SHOW_ACTIVE_CANDIDATES_URL, params, true, "1", getActivity());
            }
        }*/
        setAdapter();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());

    }

    private void setAdapter() {
        adapter = new AvailableAdapter(candidates_list, getActivity(), UserPref.getuser_instance(getActivity()).getUserId());
        recycler_view.setAdapter(adapter);
    }

    public Fragmentvailable() {
        super();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_recruiter_registration, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        lastFirstVisiblePosition = ((LinearLayoutManager) recycler_view.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        Log.e("position rcy", lastFirstVisiblePosition + "");
        // always keep in mind
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
        ((LinearLayoutManager) recycler_view.getLayoutManager()).scrollToPosition(lastFirstVisiblePosition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragmentvailable, container, false);
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        return v;
    }

    @Override
    public void getResponse(String response, String request_id) {
        Log.e("candidate response ", response);
        if (response != null) {
            AllCandidateResponse candidate_response = new Gson().fromJson(response, AllCandidateResponse.class);
            if (candidate_response.isStatus()) {
                candidates_list = candidate_response.getData();
                setAdapter();
            } else {
                AlertManger.getAlert_instance().showAlert("No Active Candidate Available", getActivity());
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }


}
