package com.android.AllFragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.StatusReportResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.StatusReportAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatusReportFragment extends Fragment implements ApiResponseListener {
    private RecyclerView recycler_view;
    private onFragmentListener listener;
    private StatusReportAdapter adapter;
    public Bundle b = null;
    private ArrayList<StatusReportResponse.StatusReport> status_list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        status_list = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
            if (UserPref.getuser_instance(getActivity()).getRoleId().equals("2"))
                CallService.getInstance().passInfromation(this, Constants.STATUS_REPORT_ON_HIRE_URL, params, true, "1", getActivity());
            else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4")) {
                b = getArguments();
                if (b != null) {
                    Log.e("roleid", UserPref.getuser_instance(getActivity()).getRoleId());
                    params.put("role_id", UserPref.getuser_instance(getActivity()).getRoleId());
                    CallService.getInstance().passInfromation(this, Constants.JOB_MATRIX_URL, params, true, "1", getActivity());
                } else
                    CallService.getInstance().passInfromation(this, Constants.STATUS_REPORT_ON_JOB_URL, params, true, "1", getActivity());
            } else {
                b = getArguments();
                if (b != null) {
                    if (b.containsKey("recruiter"))
                        CallService.getInstance().passInfromation(this, Constants.RECRUITER_HIRE_MATRIX, params, true, "1", getActivity());
                    else
                        CallService.getInstance().passInfromation(this, Constants.SUBMITTED_CANDIDATES_URL, params, true, "1", getActivity());

                } else
                    CallService.getInstance().passInfromation(this, Constants.STATUS_REPORT_ON_HIRE_URL, params, true, "1", getActivity());
            }
        } else
            AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_status_report, container, false);
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        return v;
    }

    @Override
    public void getResponse(String response, String request_id) {
        if (response != null) {
            Log.e("response", response);
            if (request_id.equals("1")) {
                StatusReportResponse status_response = new Gson().fromJson(response, StatusReportResponse.class);
                if (status_response.isStatus()) {
                    status_list = status_response.getData();
                    adapter = new StatusReportAdapter(status_list, getActivity(), b);
                    recycler_view.setAdapter(adapter);
                } else {
                    AlertManger.getAlert_instance().showAlert("There is some error ", getActivity());
                }
            } else if (request_id.equals("2")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


           /* "status": true,
                    "data": [
            {
                   "job_title": "java",
                    "ranking": "1.7500",
                    "percentage": 35
            }
            ],*/
        }

    }
}
