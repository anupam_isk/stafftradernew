package com.android.AllFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class CandidateMatrixFragment extends Fragment {
    private RecyclerView recycler_view;
    private Button matrix_predictor;
    private CandidateMatrixAdapter matrix_adapter;
    private ArrayList<AllCandidateResponse.Candidate> candidate_list;
    private ArrayList<HashMap<String, String>> hire_result_list;
    private String job_id;
    private AlertDialog deleteDialog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        candidate_list = new ArrayList<>();
        Bundle b = getArguments();
        if (b != null)
            job_id = b.getString("job_id");
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("job_id", job_id);
        CallService.getInstance().passInfromation(api_listener, Constants.SHOW_ACTIVE_CANDIDATES_URL, params, true, "1", getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_candidate_matrix, container, false);
        initializeUI(v);
        return v;
    }

    // we should always remember and alway keep in mind
    //
    @Override
    public void onStart() {
        super.onStart();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        matrix_adapter = new CandidateMatrixAdapter(candidate_list, getActivity());
        recycler_view.setAdapter(matrix_adapter);
    }

    private void initializeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        matrix_predictor = (Button) v.findViewById(R.id.matrix_predictor);
        matrix_predictor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hire_result_list = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> params = new HashMap<>();
                params.put("job_id", job_id);
                CallService.getInstance().passInfromation(api_listener, Constants.HIRE_MATRIX_URL, params, true, "2", getActivity());
            }
        });
    }

    private void showDialog() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View matrix_view = factory.inflate(
                R.layout.hire_matrix_result_layout, null);
        deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(matrix_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView result_text = (TextView) matrix_view.findViewById(R.id.result_text);
        final ImageView matrix_img = (ImageView) matrix_view.findViewById(R.id.matrix_img);
        final RecyclerView recyclerView = (RecyclerView) matrix_view.findViewById(R.id.recycler_view);
        final ImageButton cross_button = (ImageButton) matrix_view.findViewById(R.id.cross_button);
        Picasso.with(getActivity())
                .load(hire_result_list.get(0).get("image"))
                .resize(145, 145)
                .into(matrix_img);
        final MatrixPredictorAdapter adapter = new MatrixPredictorAdapter(hire_result_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        recyclerView.setAdapter(adapter);
        result_text.setText("Candidate " + hire_result_list.get(0).get("first_name") + " " + hire_result_list.get(0).get("last_name") + " meets " + hire_result_list.get(0).get("percentage") + " % of your hire matrix!");
        cross_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response",response);
            if (request_id.equals("1")) {
                if (response != null) {
                    AllCandidateResponse candidate_response = new Gson().fromJson(response, AllCandidateResponse.class);
                    if (candidate_response.isStatus()) {
                        candidate_list = candidate_response.getData();
                        matrix_adapter = new CandidateMatrixAdapter(candidate_list, getActivity());
                        recycler_view.setAdapter(matrix_adapter);
                    } else {
                        matrix_predictor.setVisibility(View.GONE);
                        AlertManger.getAlert_instance().showAlertPop("No Active Candidate Available", getActivity());
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            JSONArray json_array = json_response.getJSONArray("data");
                            for (int i = 0; i < json_array.length(); i++) {
                                HashMap<String, String> map = new HashMap<>();
                                map.put("first_name", json_array.getJSONObject(i).getString("first_name"));
                                map.put("second_name", json_array.getJSONObject(i).getString("last_name"));
                                map.put("image", json_array.getJSONObject(i).getString("image"));
                                map.put("percentage", json_array.getJSONObject(i).getString("percentage"));
                                hire_result_list.add(map);
                            }
                            showDialog();
                        } else {
                            AlertManger.getAlert_instance().showAlertPop("There is no candidate matrix!!", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    };
    public class CandidateMatrixAdapter extends RecyclerView.Adapter<CandidateMatrixAdapter.MyViewHolder> {
        private ArrayList<AllCandidateResponse.Candidate> candidate_list;
        private Context context;
        public CandidateMatrixAdapter(ArrayList<AllCandidateResponse.Candidate> candidate_list, Context context) {
            this.candidate_list = candidate_list;
            this.context = context;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.status_report_row, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.background.setText(candidate_list.get(position).getBudget());
            holder.background.setBackground(context.getResources().getDrawable(R.drawable.rounded_edittext));
            holder.collected.setText(candidate_list.get(position).getComment());
            holder.collected.setBackground(context.getResources().getDrawable(R.drawable.rounded_edittext));
            holder.references.setText(candidate_list.get(position).getRanking());
            holder.references.setBackground(context.getResources().getDrawable(R.drawable.rounded_edittext));
            String value = candidate_list.get(position).getIs_hired().equals("1") ? "hired" : "hire ?";
            holder.good_to_start.setText(value);
            holder.good_to_start.setBackground(context.getResources().getDrawable(R.drawable.rounded_edittext));
            holder.designation_text.setText(candidate_list.get(position).getJob_title());
            holder.name.setText(candidate_list.get(position).getFirst_name() + " " + candidate_list.get(position).getLast_name());
           // loader.DisplayImage(candidate_list.get(position).getImage(), 1, holder.user_img);
            Picasso.with(getActivity())
                    .load(candidate_list.get(position).getImage())
                    .resize(135, 135)
                    .into(holder.user_img);
        }


        @Override
        public int getItemCount() {
            return candidate_list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView background, collected, references, good_to_start, designation_text, name;
            public ImageView user_img;

            public MyViewHolder(View itemView) {
                super(itemView);
                background = (TextView) itemView.findViewById(R.id.background);
                collected = (TextView) itemView.findViewById(R.id.collected);
                references = (TextView) itemView.findViewById(R.id.references);
                good_to_start = (TextView) itemView.findViewById(R.id.good_to_start);
                designation_text = (TextView) itemView.findViewById(R.id.designation_text);
                name = (TextView) itemView.findViewById(R.id.name);
                user_img = (ImageView) itemView.findViewById(R.id.user_img);
            }
        }
    }

    public class MatrixPredictorAdapter extends RecyclerView.Adapter<MatrixPredictorAdapter.MyViewHolder> {
        private ArrayList<HashMap<String, String>> result_list;

        public MatrixPredictorAdapter(ArrayList<HashMap<String, String>> result_list) {
            this.result_list = result_list;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.name.setText(result_list.get(position).get("first_name") + " " + result_list.get(position).get("last_name"));
            holder.percentage.setText(result_list.get(position).get("percentage") + " %");
          //  loader.DisplayImage(result_list.get(position).get("image"), 1, holder.user_img);
            Log.e("image predictor",result_list.get(position).get("image"));
            Picasso.with(getActivity())
                    .load(result_list.get(position).get("image"))
                    .resize(145, 145)
                    .into(holder.user_img);
            if (position == 0)
                setAnimation(holder.frame_layout);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView percentage, name;
            private RelativeLayout frame_layout;
            public ImageView user_img;

            public MyViewHolder(View itemView) {
                super(itemView);
                percentage = (TextView) itemView.findViewById(R.id.percentage);
                name = (TextView) itemView.findViewById(R.id.name);
                user_img = (ImageView) itemView.findViewById(R.id.user_img);
                frame_layout = (RelativeLayout) itemView.findViewById(R.id.frame_layout);
            }

        }

        @Override
        public int getItemCount() {
            return result_list.size();
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.selected_hire_layout, parent, false);
            return new MyViewHolder(itemView);
        }

    }

    private void setAnimation(RelativeLayout frame_layout) {
        Animation animation = AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left);
        //animation.setStartOffset(200); // setting the offset
        animation.setDuration(3000);
        frame_layout.startAnimation(animation);
    }
}
