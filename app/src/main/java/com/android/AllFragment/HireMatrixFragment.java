package com.android.AllFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.ChooseCredential;
import com.android.FragmentInterface.AnotherListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.OpenJobResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam tyagi on 7/25/2016.
 */
public class HireMatrixFragment extends Fragment {
    private onFragmentListener listener;
    private RecyclerView recycler_view;
    private Bundle b;
    private ArrayList<OpenJobResponse.OpenJob> open_job_list;
    private ArrayList<HashMap<String, String>> hash_map;
    private HireMatrixAdapter adapter;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        listener.otherCheck(false);
        listener.showDrawerToggle(false);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            b = getArguments();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        if (b != null) {
            if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                hash_map = new ArrayList<>();
                params.put("role_id", UserPref.getuser_instance(getActivity()).getRoleId());
                CallService.getInstance().passInfromation(api_listner, Constants.PROJECT_MATRIX_URL, params, true, "2", getActivity());
            } else {
                AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
            }
        } else {
            if (NetworkCheck.getInstance(getActivity()).isNetworkAvailable()) {
                open_job_list = new ArrayList<>();
                CallService.getInstance().passInfromation(api_listner, Constants.OPEN_JOB_REQUIREMENT, params, true, "1", getActivity());
            } else {
                AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, getActivity());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_seek_jobs, container, false);
        initializeUI(v);
        return v;
    }

    private void initializeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        if (recycler_view == null)
            Log.e("null", "null");
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        if (b != null) {
            adapter = new HireMatrixAdapter(getActivity(), hash_map);
            recycler_view.setAdapter(adapter);
        } else {
            adapter = new HireMatrixAdapter(getActivity(), open_job_list, another_listener);
            recycler_view.setAdapter(adapter);
        }
        //   adapter.setAnother_listener(another_listener);
    }

    AnotherListener another_listener = new AnotherListener() {
        @Override
        public void onClickItem(int position) {
            Fragment fragment = new CandidateMatrixFragment();
            Bundle b = new Bundle();
            b.putString("job_id", open_job_list.get(position).getId());
            fragment.setArguments(b);
            UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        /*adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                CandidateActivity.showToast(getActivity(),"Job id "+ open_job_list.get(position).getId());
            }
        });*/
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    ApiResponseListener api_listner = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {

            Log.e("response", response+ request_id);
            if (request_id.equals("1")) {
                if (response != null) {
                    OpenJobResponse open_job_response = new Gson().fromJson(response, OpenJobResponse.class);
                    if (open_job_response.isStatus()) {
                        open_job_list = open_job_response.getData();
                        adapter = new HireMatrixAdapter(getActivity(), open_job_list, another_listener);
                        recycler_view.setAdapter(adapter);
                    } else {
                        AlertManger.getAlert_instance().showAlert("There is some error ", getActivity());
                    }
                }
            } else if (request_id.equals("2")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONArray data_arrray = json.getJSONArray("data");
                        for (int i = 0; i < data_arrray.length(); i++) {
                            HashMap<String, String> hmap = new HashMap<>();
                            hmap.put("milestone_name", data_arrray.getJSONObject(i).getString("milestone_name"));
                            hmap.put("task_name", data_arrray.getJSONObject(i).getString("task_name"));
                            hmap.put("first_name", data_arrray.getJSONObject(i).getString("first_name"));
                            hmap.put("total_task", data_arrray.getJSONObject(i).getString("total_task"));
                            hmap.put("completed_task", data_arrray.getJSONObject(i).getString("completed_task"));
                            hash_map.add(hmap);
                        }
                        adapter = new HireMatrixAdapter(getActivity(), hash_map);
                        recycler_view.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    };

    public class HireMatrixAdapter extends RecyclerView.Adapter<HireMatrixAdapter.MyViewHolder> {
        private AnotherListener another_listener;
        private ArrayList<OpenJobResponse.OpenJob> openjob_list;
        private ArrayList<HashMap<String, String>> map_list;
        Context context;

        public HireMatrixAdapter(Context context, ArrayList<OpenJobResponse.OpenJob> openjob_list, AnotherListener another_listener) {
            this.openjob_list = openjob_list;
            this.context = context;
            this.another_listener = another_listener;
        }

        public HireMatrixAdapter(Context context, ArrayList<HashMap<String, String>> map_list) {
            this.context = context;
            this.map_list = map_list;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int id = 0;
            if (b != null)
                id = R.layout.project_matrix_row;
            else
                id = R.layout.hire_matrix_layout;
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(id, parent, false);
            return new MyViewHolder(itemView);
        }
        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            if (b != null) {
                holder.candidate_name.setText(map_list.get(position).get("first_name"));
                holder.task_name.setText(map_list.get(position).get("task_name"));
                holder.milestone_name.setText(map_list.get(position).get("milestone_name"));
                int percentage = (Integer.parseInt(map_list.get(position).get("completed_task")) * 100) / Integer.parseInt(map_list.get(position).get("total_task"));
                if (percentage <= 60)
                    holder.work_percentage.setBackgroundColor(getResources().getColor(R.color.redcolor));
                else if (percentage >= 60 && percentage <= 79)
                    holder.work_percentage.setBackgroundColor(getResources().getColor(R.color.yellowcolor));
                else
                    holder.work_percentage.setBackgroundColor(getResources().getColor(R.color.green_color));
                holder.work_percentage.setText(percentage + " %");
            } else {
                holder.name.setText(openjob_list.get(position).getJob_title());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("on click", "on click");
                        goItem(position);

                    }
                });
            }
        }

        private void goItem(int position) {
            another_listener.onClickItem(position);
        }

        /*  public void setAnother_listener(AnotherListener another_listener)
          {
              this.another_listener=another_listener;
              this.another_listener.onClickItem(2);
              Log.e("another lister isnull","another listener is null"+ this.another_listener +another_listener);
          }*/
        /*public void onBindViewHolder(MyViewHolder holder, final int position) {
           // holder.bind(position,another_listener);
        holder.name.setText(openjob_list.get(position).getJob_title());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("on click","on click");
                another_listener.onClickItem(position);
            }
        });
    }*/
        @Override
        public int getItemCount() {
            int size = 0;
            if (b != null)
                size = hash_map.size();
            else
                size = openjob_list.size();
            return size;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name, milestone_name, task_name, candidate_name, work_percentage;

            public MyViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.job_name);
                milestone_name = (TextView) itemView.findViewById(R.id.milestone_name);
                task_name = (TextView) itemView.findViewById(R.id.task_name);
                candidate_name = (TextView) itemView.findViewById(R.id.candidate_name);
                work_percentage = (TextView) itemView.findViewById(R.id.work_percentage);
            }


        }

    }
}
