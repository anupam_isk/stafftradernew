package com.android.AllFragment;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import com.android.FragmentInterface.AnotherListener;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.ServerCommunication.CallService;
import com.android.ViewProfileActivity;
import com.android.adapter.AvailableAdapter;
import com.android.adapter.UserNotificationAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam tyagi on 8/10/2016.
 */
public class ShowFragment extends DialogFragment{
    private ImageButton back;
    private EditText search_text;
    private UserNotificationAdapter adapter;
    private RecyclerView notification_list;
    private ArrayList<AllCandidateResponse.Candidate> candidate_list;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        candidate_list=new ArrayList<>();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.dialog_fragment, container,false);
        initializeUi(view);
        return view;
    }
    private void initializeUi(View view) {
        back=(ImageButton) view.findViewById(R.id.back);
        search_text=(EditText) view.findViewById(R.id.search_text);
        notification_list = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        notification_list.setLayoutManager(mLayoutManager);
        notification_list.setItemAnimator(new DefaultItemAnimator());
        notification_list.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        back.setOnClickListener(listener);
        search_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 4) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                       // notification_list.clear();
                    getDataFromServer(editable.toString());
                }
                else
                {

                }
            }
        });
    }
    private void getDataFromServer(String content) {
        HashMap<String, String> params = new HashMap<>();
        params.put("term",content);
        params.put("user_id",UserPref.getuser_instance(getActivity()).getUserId());
        CallService.getInstance().passInfromation(api_listener, Constants.USER_LIST_URL, params, true, "1", getActivity());
    }
    AnotherListener another_listener = new AnotherListener() {
        @Override
        public void onClickItem(int position) {
            Intent i=new Intent(getActivity(), ViewProfileActivity.class);
            Bundle b=new Bundle();
            b.putString("other_id",candidate_list.get(position).getId());
            b.putString("name",candidate_list.get(position).getFirst_name() + " "+candidate_list.get(position).getLast_name());
            b.putString("brief",candidate_list.get(position).getBrif_bio());
            b.putString("qualification",candidate_list.get(position).getQualification());
            b.putString("duration",candidate_list.get(position).getDuration_project());
            b.putString("budget",candidate_list.get(position).getBudget());
            i.putExtra("bundle",b);
            startActivity(i);
        }
    };
    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        Log.e("height ",height+"");
        Log.e("width",width+"");
        // safety check
        if (getDialog() == null)
            return;
        int dialogWidth =width-5;// specify a value here
        int dialogHeight = height-20; // specify a value here
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().getWindow().setLayout(dialogWidth, dialogHeight);
        adapter = new UserNotificationAdapter(candidate_list, getActivity(),another_listener,null);
        notification_list.setAdapter(adapter);
    }
    View.OnClickListener listener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view==back)
            {
               dismiss();
            }
        }
    };
   ApiResponseListener api_listener=new ApiResponseListener() {
    @Override
    public void getResponse(String response, String request_id) {
        if (response != null) {
            AllCandidateResponse candidate_response = new Gson().fromJson(response, AllCandidateResponse.class);
            if (candidate_response.isStatus()) {
                candidate_list = candidate_response.getData();
                adapter = new UserNotificationAdapter(candidate_list, getActivity(),another_listener,null);
                notification_list.setAdapter(adapter);
            } else {
                AlertManger.getAlert_instance().showAlert("No Active Candidate Available", getActivity());
            }
        }
    }
};
}
