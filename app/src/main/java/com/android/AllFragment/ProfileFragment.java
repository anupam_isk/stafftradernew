package com.android.AllFragment;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.FragmentInterface.AnotherListener;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.adapter.UserNotificationAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.ImageTrans_CircleTransform;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam tyagi .
 */
public class ProfileFragment extends Fragment implements View.OnClickListener {
    private TextView name, count;
    private RelativeLayout relative;
    private ImageView user_img, profile_nxt;
    private RecyclerView notification_list;
    private UserNotificationAdapter adapter;
    private ArrayList<AllCandidateResponse.Candidate> candidate_list;
    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        init(rootView);
        return rootView;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        candidate_list = new ArrayList<>();
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        CallService.getInstance().passInfromation(api_listener, Constants.WHO_VIEWED_PROFILE_URL, params, true, "1", getActivity());
    }
    public void init(View v) {
        profile_nxt = (ImageView) v.findViewById(R.id.profile_nxt);
        name = (TextView) v.findViewById(R.id.name);
        user_img = (ImageView) v.findViewById(R.id.user_img);
        count = (TextView) v.findViewById(R.id.count);
        notification_list = (RecyclerView) v.findViewById(R.id.recycler_view);
        relative = (RelativeLayout) v.findViewById(R.id.relative);
        name.setText(UserPref.getuser_instance(getActivity()).getUserFirstName() + " " + UserPref.getuser_instance(getActivity()).getUserLastName());
         relative.setOnClickListener(this);
        Log.e("user image ", UserPref.getuser_instance(getActivity()).getUserImage());
        if (!(UserPref.getuser_instance(getActivity()).getUserImage().equals("")))
        {
            Log.e("in profile fragment ", "in profile fragment" + UserPref.getuser_instance(getActivity()).getUserImage());
            Picasso.with(getActivity())
                    .load(UserPref.getuser_instance(getActivity()).getUserImage())
                    .error(R.drawable.splash_logo)
                    .resize(200,200)
                    .transform(new ImageTrans_CircleTransform(getActivity()))
                    .into(user_img);
        } else
            user_img.setBackground(getResources().getDrawable(R.drawable.user_profle));
    }
    private int dpToPx(int dp)
    {
        float density = getActivity().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }
    AnotherListener another_listener = new AnotherListener() {
        @Override
        public void onClickItem(int position) {
            Fragment fragment = new ViewQualificationFragment();
            Bundle b = new Bundle();
            b.putString("role_id",candidate_list.get(position).getRole_id());
            b.putString("bio", "bio");
            b.putString("skill", "skill");
            b.putString("qualification", "qualification");
            fragment.setArguments(b);
            UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
        }
    };
    @Override
    public void onStart() {
        super.onStart();
        adapter = new UserNotificationAdapter(candidate_list, getActivity(), another_listener, null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        notification_list.setLayoutManager(mLayoutManager);
        notification_list.setItemAnimator(new DefaultItemAnimator());
        notification_list.setAdapter(adapter);
        count.setText(String.valueOf(candidate_list.size()));
    }

    @Override
    public void onClick(View v) {
        if (v == relative) {
            ProfileUpdateFragment phone = new ProfileUpdateFragment();
            Bundle b = new Bundle();
            b.putString("profile", "profile");
            phone.setArguments(b);
            UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), phone);
        }
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (response != null) {
                AllCandidateResponse candidate_response = new Gson().fromJson(response, AllCandidateResponse.class);
                if (candidate_response.isStatus()) {
                    candidate_list = candidate_response.getData();
                    adapter = new UserNotificationAdapter(candidate_list, getActivity(), another_listener, null);
                    notification_list.setAdapter(adapter);
                    count.setText(String.valueOf(candidate_list.size()));
                } else {
                    AlertManger.getAlert_instance().showAlert("No Active Candidate Available", getActivity());
                }
            }
        }
    };

}
