package com.android.AllFragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.MilestoneResponse;
import com.android.GsonResponse.OpenJobResponse;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.adapter.MilestoneAdapter;
import com.android.adapter.OpenJobAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MilestoneFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MilestoneFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MilestoneFragment extends Fragment {
    private RecyclerView recycler_view;
    private MilestoneAdapter adapter;
    private RelativeLayout add_milestone;
    private ArrayList<MilestoneResponse.MilestoneClass> milestone_list;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private onFragmentListener listener;
    public MilestoneFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MilestoneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MilestoneFragment newInstance(String param1, String param2) {
        MilestoneFragment fragment = new MilestoneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        milestone_list = new ArrayList<>();

    }
    private void getDataForCandidate() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("role_id", UserPref.getuser_instance(getActivity()).getRoleId());
        CallService.getInstance().passInfromation(api_listener, Constants.MILESTONE_BY_ROLE_URL, params, true, "1", getActivity());
    }
    private void getDataFromServer() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        CallService.getInstance().passInfromation(api_listener, Constants.MILESTONE_LIST, params, true, "1", getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3"))
            getDataForCandidate();
        else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4"))
            getDataForCandidate();
        else
            getDataFromServer();
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals("3"))
            add_milestone.setVisibility(View.GONE);
        else if (UserPref.getuser_instance(getActivity()).getRoleId().equals("4"))
            add_milestone.setVisibility(View.GONE);
        adapter = new MilestoneAdapter(getActivity(), milestone_list);
        recycler_view.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_milestone, container, false);
        initializeUI(v);
        return v;
    }

    private void initializeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        add_milestone = (RelativeLayout) v.findViewById(R.id.add_milestone);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        add_milestone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new EditMilestoneFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }
        });
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (response != null) {
                MilestoneResponse milestone_response = new Gson().fromJson(response, MilestoneResponse.class);
                if (milestone_response.isStatus()) {
                    milestone_list = milestone_response.getData();
                   adapter = new MilestoneAdapter(getActivity(), milestone_list);
                    recycler_view.setAdapter(adapter);
                } else {
                    AlertManger.getAlert_instance().showAlert("There is no milestone.", getActivity());
                }
            }
        }
    };
}
