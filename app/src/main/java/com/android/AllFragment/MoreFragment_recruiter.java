package com.android.AllFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

/**
 * Created by Anupam tyagi.
 */
public class MoreFragment_recruiter extends Fragment {
    private ImageView hire_paperwork_lay, interactive_map, candidate_assignment, recruiter_bio_lay, notebook_lay, submit_candidate_lay, jobs, upload_candidate, enter_job, status_report_hire_lay, search_active_candidate, open_job_requirement, talktolive_recruiter, milestone, payment, recruiter_hire_matrix;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_more_recruiter, container, false);
        initializeUI(rootView);
        setListener();
        return rootView;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Log.e("on create more ", "on create more");
    }
    private void initializeUI(View v) {
        hire_paperwork_lay = (ImageView) v.findViewById(R.id.hire_paperwork_lay);
        candidate_assignment = (ImageView) v.findViewById(R.id.candidate_assignment);
        interactive_map = (ImageView) v.findViewById(R.id.interactive_map);
        recruiter_bio_lay = (ImageView) v.findViewById(R.id.recruiter_bio_lay);
        notebook_lay = (ImageView) v.findViewById(R.id.notebook_lay);
        submit_candidate_lay = (ImageView) v.findViewById(R.id.submit_candidate_lay);
        status_report_hire_lay = (ImageView) v.findViewById(R.id.status_report_hire_lay);
        search_active_candidate = (ImageView) v.findViewById(R.id.search_active_candidate);
        open_job_requirement = (ImageView) v.findViewById(R.id.open_job_requirement);
        talktolive_recruiter = (ImageView) v.findViewById(R.id.talktolive_recruiter);
        milestone = (ImageView) v.findViewById(R.id.milestone);
        payment = (ImageView) v.findViewById(R.id.payment);
        jobs = (ImageView) v.findViewById(R.id.jobs);
        upload_candidate = (ImageView) v.findViewById(R.id.upload_candidate);
        recruiter_hire_matrix = (ImageView) v.findViewById(R.id.recruiter_hire_matrix);
        enter_job = (ImageView) v.findViewById(R.id.enter_job);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_splash, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            ShowFragment fragment = new ShowFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "nn");

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setListener() {
        hire_paperwork_lay.setOnClickListener(listener);
        candidate_assignment.setOnClickListener(listener);
        recruiter_bio_lay.setOnClickListener(listener);
        notebook_lay.setOnClickListener(listener);
        submit_candidate_lay.setOnClickListener(listener);
        status_report_hire_lay.setOnClickListener(listener);
        search_active_candidate.setOnClickListener(listener);
        open_job_requirement.setOnClickListener(listener);
        talktolive_recruiter.setOnClickListener(listener);
        milestone.setOnClickListener(listener);
        payment.setOnClickListener(listener);
        jobs.setOnClickListener(listener);
        upload_candidate.setOnClickListener(listener);
        enter_job.setOnClickListener(listener);
        recruiter_hire_matrix.setOnClickListener(listener);
        interactive_map.setOnClickListener(listener);
    }
    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment fragment = null;
            if (v == hire_paperwork_lay) {
                fragment = new PaperWorkFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == candidate_assignment) {
                fragment = new UserAgreementFragment();
                Bundle b = new Bundle();
                b.putString("check_value", "assignment");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == recruiter_bio_lay) {
                fragment = new RecruiterVedioFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == open_job_requirement) {
                fragment = new SeekJobsFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == notebook_lay) {
                fragment = new NotebookFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == submit_candidate_lay) {
                Bundle b = new Bundle();
                b.putString("submit", "submit");
                fragment = new StatusReportFragment();
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == status_report_hire_lay) {
                fragment = new StatusReportFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == talktolive_recruiter) {
                fragment = new UserAgreementFragment();
                Bundle b = new Bundle();
                b.putString("check_value", "chat");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == milestone) {
                fragment = new MilestoneFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == payment) {
                fragment = new PaymentFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == jobs) {
                fragment = new SubmitCandidateFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == upload_candidate) {
                fragment = new ProfileUpdateFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == enter_job) {
                fragment = new EnterJobReqFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == recruiter_hire_matrix) {
                fragment = new StatusReportFragment();
                Bundle b = new Bundle();
                b.putString("recruiter", "recruiter");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == interactive_map) {
                fragment = new AvailableCandidateMap_hiringManager();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else {
                fragment = new Fragmentvailable();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }


        }
    };
}
