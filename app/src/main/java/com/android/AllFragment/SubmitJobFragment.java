package com.android.AllFragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.FragmentInterface.AnotherListener;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.OpenJobResponse;
import com.android.ServerCommunication.CallService;
import com.android.ViewProfileActivity;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam on 8/4/2016.
 */
public class SubmitJobFragment extends Fragment {
    private onFragmentListener listener;
    private RecyclerView recycler_view;
    private ArrayList<OpenJobResponse.OpenJob> open_job_list;
    private SubmitJobAdapter adapter;
    String candidate_id;
    int pos;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            candidate_id = getArguments().getString("candidate_id");
            Log.e("candidate id", candidate_id);
            HashMap<String, String> params = new HashMap<>();
            params.put("recruiter_id", UserPref.getuser_instance(getActivity()).getUserId());
            params.put("candidate_id", candidate_id);
            CallService.getInstance().passInfromation(api_listner, Constants.SUBMITTED_JOB_LIST_URL, params, true, "1", getActivity());
        }
        open_job_list = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_seek_jobs, container, false);
        initializeUI(v);
        return v;
    }

    AnotherListener another_listener = new AnotherListener() {
        @Override
        public void onClickItem(int position) {
            Intent i = new Intent(getActivity(), ViewProfileActivity.class);
            Bundle b = new Bundle();
            b.putString("open_job", "open_job");
            b.putString("job_title", open_job_list.get(position).getJob_title());
            b.putString("brief", "brief");
            b.putString("skill", "skills");
            b.putString("duration", open_job_list.get(position).getDuration_project());
            b.putString("budget", open_job_list.get(position).getBudget());
            b.putString("desired_location", open_job_list.get(position).getDesired_location());
            i.putExtra("bundle", b);
            getActivity().startActivity(i);
        }
    };

    private void initializeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
        if (recycler_view == null)
            Log.e("null", "null");
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        adapter = new SubmitJobAdapter(getActivity(), open_job_list, another_listener);
        recycler_view.setAdapter(adapter);
        //   adapter.setAnother_listener(another_listener);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    ApiResponseListener api_listner = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {

            Log.e("response", response);
            if (request_id.equals("1")) {

                if (response != null) {
                    OpenJobResponse open_job_response = new Gson().fromJson(response, OpenJobResponse.class);
                    if (open_job_response.isStatus()) {
                        open_job_list = open_job_response.getData();
                        adapter = new SubmitJobAdapter(getActivity(), open_job_list, another_listener);
                        recycler_view.setAdapter(adapter);
                    } else {
                        AlertManger.getAlert_instance().showAlert("There is some error ", getActivity());
                    }
                }
            }
            if (request_id.equals("2")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            if (!json_response.getBoolean("is_submit"))
                                open_job_list.get(pos).setSubmit_id(null);
                            else
                                open_job_list.get(pos).setSubmit_id("");

                            adapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    };

    public class SubmitJobAdapter extends RecyclerView.Adapter<SubmitJobAdapter.MyViewHolder> {
        private ArrayList<OpenJobResponse.OpenJob> openjob_list;
        Context context;
        private AnotherListener another_listener;

        public SubmitJobAdapter(Context context, ArrayList<OpenJobResponse.OpenJob> openjob_list, AnotherListener another_listener) {
            this.openjob_list = openjob_list;
            this.context = context;
            this.another_listener = another_listener;

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.hotebook_layout, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.hotebook_name.setText(openjob_list.get(position).getJob_title());
            holder.delete_candidate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAlertPop("ALERT!  Please confirm you want to submit this candidate.", context);
                }
            });
            if (openjob_list.get(position).getSubmit_id() != null) {
                holder.delete_candidate.setText("Submitted To Job");
                holder.delete_candidate.setBackgroundDrawable(getResources().getDrawable(R.drawable.reverse_button_background));
            } else
                holder.delete_candidate.setText("Submit To Job");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("on click", "on click");
                    another_listener.onClickItem(position);

                }
            });

        }


        /*  public void setAnother_listener(AnotherListener another_listener)
          {
              this.another_listener=another_listener;
              this.another_listener.onClickItem(2);
              Log.e("another lister isnull","another listener is null"+ this.another_listener +another_listener);
          }*/
        /*public void onBindViewHolder(MyViewHolder holder, final int position) {
           // holder.bind(position,another_listener);
        holder.name.setText(openjob_list.get(position).getJob_title());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("on click","on click");
                another_listener.onClickItem(position);
            }
        });
    }*/
        @Override
        public int getItemCount() {
            return openjob_list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public Button delete_candidate;
            public TextView hotebook_name;

            public MyViewHolder(View itemView) {
                super(itemView);
                hotebook_name = (TextView) itemView.findViewById(R.id.hotebook_name);
                delete_candidate = (Button) itemView.findViewById(R.id.delete_candidate);
            }
        }

    }

    public void showAlertPop(String message, final Context context) {
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Staff Trader App")
                    .setMessage(message)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            showPopUp(pos);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void showPopUp(final int pos) {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View hire_view = factory.inflate(
                R.layout.hire_popup, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(hire_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        TextView cancel=(TextView) hire_view.findViewById(R.id.cancel);
        EditText candidate_x = (EditText) hire_view.findViewById(R.id.candidate_x);
        EditText project_duration = (EditText) hire_view.findViewById(R.id.project_duration);
        EditText start_date = (EditText) hire_view.findViewById(R.id.start_date);
        EditText job_title = (EditText) hire_view.findViewById(R.id.job_title);
        EditText manager_contact_name = (EditText) hire_view.findViewById(R.id.manager_contact_name);
        EditText manager_number = (EditText) hire_view.findViewById(R.id.manager_number);
        EditText work_location = (EditText) hire_view.findViewById(R.id.work_location);
        EditText manager_email = (EditText) hire_view.findViewById(R.id.manager_email);
        final Button agree = (Button) hire_view.findViewById(R.id.agree);
        final Button cancelBT = (Button) hire_view.findViewById(R.id.cancelBT);
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("candidate_id", candidate_id);
                params.put("job_id", open_job_list.get(pos).getJob_req_id());
                params.put("recruiter_id", UserPref.getuser_instance(getActivity()).getUserId());
                CallService.getInstance().passInfromation(api_listner, Constants.SUBMITTED_JOB_URL, params, true, "2", getActivity());
            }
        });
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }
}

