package com.android.AllFragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.OnItemClickListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.FragmentInterface.onLoadMoreListener;
import com.android.GsonResponse.EventResponse;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteBookFirstFragment extends Fragment {
    private Button add_event;
    private RecyclerView recyclerview;
    private int start_value = 0;
    private EventAdapter adapter;
    private Spinner spinner;
    private LinearLayoutManager mLayoutManager;
    private EditText search_by_name;
    private TextView searchBT;
    public ArrayList<EventResponse.Event> event_list;
    private int limit_value = 6;
    private String item = "All";
    public static String pos = null;

    public NoteBookFirstFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pos = null;
        Log.e("on Notebook Firstcreate", "on Notebook Firstcreate");
        event_list = new ArrayList<>();
        if (event_list.size() > 2) {
            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
            params.put("start", String.valueOf(start_value));
            params.put("limit", String.valueOf(limit_value));
            params.put("type", item);
            CallService.getInstance().passInfromation(api_listener, Constants.GET_EVENT_URL, params, true, "1", getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("on NoteFirstcreate view", "on NoteFirstcreate view");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_note_book_first, container, false);
        initializeUI(v);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on NoteFirst start view", "on NoteFirst start view");
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        adapter = new EventAdapter(event_list, getActivity(), recyclerview);
        recyclerview.setAdapter(adapter);
        /*if(event_list.size()==1 || event_list.size()==2)
        {

        }*/
        Log.e("event list size", String.valueOf(event_list.size()));
        if (pos != null) {
            event_list.remove(Integer.parseInt(pos));
            adapter.notifyDataSetChanged();
            pos = null;
        } else {
            if (event_list.size() == 0 || event_list.size() == 1 || event_list.size() == 2) {
                start_value = 0;
                limit_value = 6;
                event_list.clear();
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                params.put("type", item);
                CallService.getInstance().passInfromation(api_listener, Constants.GET_EVENT_URL, params, true, "1", getActivity());
            }
        }
        adapter.setOnLoadMoreListener(new onLoadMoreListener() {
            @Override
            public void onLoadMore() {
                event_list.add(null);
                adapter.notifyItemInserted(event_list.size() - 1);
                start_value = event_list.size() - 1;
                limit_value = (event_list.size() - 1) + 6;
                Log.e("start_value", String.valueOf(start_value));
                Log.e("limit_value", String.valueOf(limit_value));
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_EVENT_URL, params, false, "3", getActivity());
            }
        });
      /*  adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Fragment fragment = new AddEventFragment();
                Bundle b=new Bundle();
                b.putParcelable("edit_event",event_list.get(position));
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
                Toast.makeText(getActivity(), "name " + event_list.get(position).getSubject() + "position " + position+ "id "+ event_list.get(position).getId(), Toast.LENGTH_LONG).show();
            }
        });*/


        //  we can also use interface like this .we will make a method in any class to register the listener

        OnItemClickListener item_listener = new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Fragment fragment = new AddEventFragment();
                Bundle b = new Bundle();
                b.putParcelable("edit_event", event_list.get(position));
                b.putInt("position", position);
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
                // Toast.makeText(getActivity(), "name " + event_list.get(position).getSubject() + "position " + position + "id " + event_list.get(position).getId(), Toast.LENGTH_LONG).show();
            }
        };
        adapter.setOnItemClickListener(item_listener);
    }

    private void initializeUI(View v) {
        add_event = (Button) v.findViewById(R.id.add_event);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        recyclerview = (RecyclerView) v.findViewById(R.id.recycler_view);
        search_by_name = (EditText) v.findViewById(R.id.search_by_name);
        searchBT = (TextView) v.findViewById(R.id.searchBT);
        add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new AddEventFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }
        });
        searchBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromServer();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                //   String item = adapterView.getItemAtPosition(position).toString();
                item = spinner.getSelectedItem().toString();
                Log.e("spinner", item);
                if (item.equals("Select Filter")) {

                } else {
                    if (item.equals("Meeting"))
                        getFilterDataById(item);
                    else if (item.equals("Call"))
                        getFilterDataById(item);
                    else if (item.equals("By Recruiter Name"))
                        item = "By_recruitername";
                    else if (item.equals("By Candidate Name"))
                        item = "By_candidatename";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        setSpinnerData();
    }

    private void getDataFromServer() {
        start_value = 0;
        limit_value = 6;
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("start", String.valueOf(start_value));
        params.put("limit", String.valueOf(limit_value));
        params.put("type", item);
        params.put("term", search_by_name.getText().toString());
        CallService.getInstance().passInfromation(api_listener, Constants.GET_EVENT_URL, params, false, "1", getActivity());
    }

    private void getFilterDataById(String item) {
        start_value = 0;
        limit_value = 6;
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("start", String.valueOf(start_value));
        params.put("limit", String.valueOf(limit_value));
        params.put("type", item);
        CallService.getInstance().passInfromation(api_listener, Constants.GET_EVENT_URL, params, false, "1", getActivity());
    }

    private void setSpinnerData() {
        ArrayList<String> spinner_list = new ArrayList<>();
        spinner_list.add("Select Filter");
        spinner_list.add("Meeting");
        spinner_list.add("Call");
        spinner_list.add("By Recruiter Name");
        spinner_list.add("By Candidate Name");
        ArrayAdapter<String> data_adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinner_list);
        spinner.setAdapter(data_adapter);

    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            if (request_id.equals("1")) {
                Log.e("response 1", response);
                if (response != null) {
                    event_list.clear();
                    EventResponse event_response = new Gson().fromJson(response, EventResponse.class);
                    if (event_response.isStatus()) {
                        event_list.addAll(event_response.getData());
                        adapter.notifyDataSetChanged();
                    } else {
                        AlertManger.getAlert_instance().showAlert("There is no more data", getActivity());
                    }
                }
            }
            if (request_id.equals("3")) {
                Log.e("response 3", response);
                event_list.remove(event_list.size() - 1);
                adapter.notifyItemRemoved(event_list.size());
                if (response != null) {
                    EventResponse event_response = new Gson().fromJson(response, EventResponse.class);
                    if (event_response.isStatus()) {
                        event_list.addAll(event_response.getData());
                        adapter.setLoaded();
                        adapter.notifyDataSetChanged();
                    } else {
//                       UtilityPermission.showToast(getActivity(),"There is no more data");
                    }
                }
            }

        }
    };

    public class EventAdapter extends RecyclerView.Adapter {
        private final int VIEW_ITEM = 1;
        private final int VIEW_PROG = 0;
        private ArrayList<EventResponse.Event> event_list;
        private int visibleThreshold = 4;
        private int lastVisibleItem, totalItemCount;
        private boolean loading;
        private onLoadMoreListener onListener;
        private OnItemClickListener cliclistener;
        Context context;

        public EventAdapter(ArrayList<EventResponse.Event> event_list, Context context, RecyclerView recyclerview) {
            this.event_list = event_list;
            this.context = context;
            if (recyclerview.getLayoutManager() instanceof LinearLayoutManager) {

                Log.e("yes", "yes");
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerview
                        .getLayoutManager();


                recyclerview
                        .addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView,
                                                   int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                Log.e("on scroll", "on scroll");
                                totalItemCount = linearLayoutManager.getItemCount();
                                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                    // End has been reached
                                    // Do something
                                    if (onListener != null) {
                                        onListener.onLoadMore();
                                    }
                                    loading = true;
                                }

                            }
                        });
            } else {
                Log.e("no", "no");
            }
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;
            if (viewType == VIEW_ITEM) {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.linear_row, parent, false);

                vh = new MyViewHolder(v);
            } else {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.progress_bar_item, parent, false);

                vh = new ProgressViewHolder(v);
            }
            return vh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof MyViewHolder) {
                ((MyViewHolder) holder).bind(position, cliclistener);

            } else {
                ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }
        }

        @Override
        public int getItemViewType(int position) {
            return event_list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }

        public void setOnItemClickListener(OnItemClickListener cliclistener) {
            this.cliclistener = cliclistener;
        }

        public void setOnLoadMoreListener(onLoadMoreListener onListener) {
            this.onListener = onListener;
        }

        public void setLoaded() {
            loading = false;
        }

        @Override
        public int getItemCount() {
            return event_list.size();
        }

        public class ProgressViewHolder extends RecyclerView.ViewHolder {
            public ProgressBar progressBar;

            public ProgressViewHolder(View v) {
                super(v);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView title_name, name, date, activity, candidate_name;
            public LinearLayout commment;

            public MyViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
                title_name = (TextView) itemView.findViewById(R.id.title_name);
                date = (TextView) itemView.findViewById(R.id.date);
                activity = (TextView) itemView.findViewById(R.id.activity);
                commment = (LinearLayout) itemView.findViewById(R.id.comment);
                candidate_name = (TextView) itemView.findViewById(R.id.candidate_name);
            }

            public void bind(final int position, final OnItemClickListener cliclistener) {
                name.setText(event_list.get(position).getSubject());
                date.setText(event_list.get(position).getDate());
                activity.setText(event_list.get(position).getMeeting());
                candidate_name.setText(event_list.get(position).getLink_candidate_name());
                title_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment fragment = new AddEventFragment();
                        Bundle b = new Bundle();
                        b.putParcelable("edit_event", event_list.get(position));
                        b.putInt("position", position);
                        fragment.setArguments(b);
                        UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
                    }
                });
                commment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showCommentPopup(event_list.get(position).getComments());

                    }
                });
            }
        }

        private void showCommentPopup(String comments) {
            LayoutInflater factory = LayoutInflater.from(getActivity());
            final View deleteDialogView = factory.inflate(
                    R.layout.comment_pop_up, null);
            final AlertDialog deleteDialog = new AlertDialog.Builder(getActivity()).create();
            deleteDialog.setView(deleteDialogView);
            deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final TextView cancel = (TextView) deleteDialogView.findViewById(R.id.cancel);
            final TextView done = (TextView) deleteDialogView.findViewById(R.id.done);
            final EditText comment = (EditText) deleteDialogView.findViewById(R.id.comment);
            comment.setText(comments);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteDialog.dismiss();
                }

            });
            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });
            deleteDialog.show();

        }

    }
}
