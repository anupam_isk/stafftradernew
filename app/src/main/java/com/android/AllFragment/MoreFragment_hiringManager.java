package com.android.AllFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anupam tyagi.
 */
public class MoreFragment_hiringManager extends Fragment {
    private ImageView matrix,talktolive, hire_matrix,payment, open_job_requirement, notebook, enter_job_requirement, search_active_candidate, paperwork, candidate_assignment, available_candidate_map, status_report_hire_lay,milestone;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("on activitycreated more", "on activitycreated more hiring");
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("on Attach more", "on Attach more hiring");
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.e("on Pause more", "on pause more hiring");
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.e("on stop more", "on stop hiring");
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("on destroy  more", "on destroy more hiring");
    }
    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("on detach more", "on detach more hiring");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_more_hiringmanager, container, false);
        Log.e("on create view more", "on create view more hiring");
        initializeUI(rootView);
        setListener();
        return rootView;
    }
    private void initializeUI(View v) {
        talktolive = (ImageView) v.findViewById(R.id.talktolive);
        hire_matrix = (ImageView) v.findViewById(R.id.hire_matrix);
        open_job_requirement = (ImageView) v.findViewById(R.id.open_job_requirement);
        notebook = (ImageView) v.findViewById(R.id.notebook);
        enter_job_requirement = (ImageView) v.findViewById(R.id.enter_job_requirement);
        search_active_candidate = (ImageView) v.findViewById(R.id.search_active_candidate);
        paperwork = (ImageView) v.findViewById(R.id.paperwork);
        candidate_assignment = (ImageView) v.findViewById(R.id.candidate_assignment);
        available_candidate_map = (ImageView) v.findViewById(R.id.available_candidate_map);
        status_report_hire_lay = (ImageView) v.findViewById(R.id.status_report_hire_lay);
        milestone=(ImageView) v.findViewById(R.id.milestone);
        payment=(ImageView) v.findViewById(R.id.payment);
        matrix=(ImageView) v.findViewById(R.id.matrix);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_splash, menu);
    }

    private void setListener() {
        talktolive.setOnClickListener(listener);
        hire_matrix.setOnClickListener(listener);
        open_job_requirement.setOnClickListener(listener);
        notebook.setOnClickListener(listener);
        enter_job_requirement.setOnClickListener(listener);
        search_active_candidate.setOnClickListener(listener);
        paperwork.setOnClickListener(listener);
        status_report_hire_lay.setOnClickListener(listener);
        candidate_assignment.setOnClickListener(listener);
        available_candidate_map.setOnClickListener(listener);
        milestone.setOnClickListener(listener);
        payment.setOnClickListener(listener);
        matrix.setOnClickListener(listener);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(getArguments()!=null)
        {
            Log.e("notification hire ", "notification");
            Log.e("type hire ",getArguments().getString("type"));
        }
        else
        {
            Log.e(" hire null","hire null");
        }
     //   UtilityPermission.getIPAddress(true);
        Log.e("on create more hiring ", "on create more hiring" + "ip address ");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on start more hiring ", "on start more hiring");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("on Resume more hiring", "on Resume more hiring");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            ShowFragment fragment = new ShowFragment();
            fragment.show(getActivity().getSupportFragmentManager(), "nn");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
     View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(getActivity()==null)
            {
                Log.e("NULL","get Activity null") ;
            }
            else {
                Log.e("not NULL","get Activity not  null") ;
            }
            Fragment fragment = null;
            if (v == open_job_requirement) {
                fragment = new NotebookFragment();
                Bundle b=new Bundle();
                b.putString("check_value","chat");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == enter_job_requirement) {
                fragment = new EnterJobReqFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == paperwork) {
                fragment = new PaperWorkFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == available_candidate_map) {
                fragment = new AvailableCandidateMap_hiringManager();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == status_report_hire_lay) {
                fragment = new StatusReportFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == search_active_candidate) {
                fragment = new Fragmentvailable();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == candidate_assignment) {
                fragment = new UserAgreementFragment();
                Bundle b=new Bundle();
                b.putString("check_value","assignment");
                fragment.setArguments(b);
               // fragment = new SearchCandidateFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == notebook) {
                fragment = new NotebookFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
                //  fragment=new RecruiterBioFragment();
            } else if (v == hire_matrix) {
                fragment = new HireMatrixFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            } else if (v == talktolive) {
                fragment = new UserAgreementFragment();
                Bundle b=new Bundle();
                b.putString("check_value","chat");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }
            else if (v == payment) {
                fragment = new PaymentFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }
            else if (v == milestone)
            {
                fragment=new MilestoneFragment();
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }
            else if (v == matrix)
            {
                fragment = new HireMatrixFragment();
                Bundle b=new Bundle();
                b.putString("check_value","assignment");
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(getActivity().getSupportFragmentManager(), fragment);
            }

        }
    };
}
