package com.android.AllFragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableRow;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.SearchResponse;
import com.android.ServerCommunication.CallService;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Anupam tyagi
 */
public class MyAssignmentFragment extends Fragment {
    private EditText search_text, weekly, contact, email, jobtype, no_hours, completed, rating, entered;
    private Button add_new_time_sheet, edit, delete;
    private TableRow table_row;
    private SearchResponse.SearchJob search_job;
    private DatePickerDialog dpd;
    private AlertDialog deleteDialog;
    private onFragmentListener listener;
    private EditText common_view;
    private final CharSequence[] items = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};

    public MyAssignmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                date_listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_assignment, container, false);
        initializeUI(v);
        setListener();
        return v;
    }

    private void initializeUI(View v) {
        search_text = (EditText) v.findViewById(R.id.search_text);
        weekly = (EditText) v.findViewById(R.id.weekly);
        contact = (EditText) v.findViewById(R.id.contact);
        rating = (EditText) v.findViewById(R.id.rating);
        email = (EditText) v.findViewById(R.id.email);
        jobtype = (EditText) v.findViewById(R.id.jobtype);
        entered = (EditText) v.findViewById(R.id.entered);
        no_hours = (EditText) v.findViewById(R.id.no_hours);
        completed = (EditText) v.findViewById(R.id.completed);
        add_new_time_sheet = (Button) v.findViewById(R.id.add_new_time_sheet);
        edit = (Button) v.findViewById(R.id.edit);
        table_row = (TableRow) v.findViewById(R.id.table_row);
        delete = (Button) v.findViewById(R.id.delete);
        table_row.setVisibility(View.GONE);
    }

    private void setListener() {
        search_text.setOnClickListener(click_listener);
        add_new_time_sheet.setOnClickListener(click_listener);
        edit.setOnClickListener(click_listener);
        delete.setOnClickListener(click_listener);
    }

    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            common_view.setText(date);
            if (common_view == search_text) {
                HashMap<String, String> params = new HashMap<>();
                params.put("candidate_id", UserPref.getuser_instance(getActivity()).getUserId());
                params.put("date", date);
                CallService.getInstance().passInfromation(api_listener, Constants.SEARCH_CANDIDATES_URL, params, true, "2", getActivity());
            }
        }
    };
    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == search_text) {
                common_view = search_text;
                dpd.show(getActivity().getFragmentManager(), "DatePickerDialog");
            } else if (view == add_new_time_sheet) {
                showDialog();
            } else if (view == edit) {
                if (search_job != null) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                    params.put("date", common_view.getText().toString());
                    params.put("hours", no_hours.getText().toString());
                    params.put("assign_id", search_job.getAssign_id());
                    CallService.getInstance().passInfromation(api_listener, Constants.EDIT_ASSIGNMENT_URL, params, true, "3", getActivity());
                }
            } else {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                params.put("assign_id", search_job.getAssign_id());
                CallService.getInstance().passInfromation(api_listener, Constants.DELETE_ASSIGNMENT_URL, params, true, "4", getActivity());
            }
        }
    };
    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            if (request_id.equals("2")) {
                if (response != null) {
                    SearchResponse search_response = new Gson().fromJson(response, SearchResponse.class);
                    if (search_response.isStatus()) {
                        table_row.setVisibility(View.VISIBLE);
                        search_job = search_response.getData().get(0);
                        email.setText(search_job.getEmail());
                        jobtype.setText(search_job.getJob_title());
                        contact.setText(search_job.getPhone());
                        no_hours.setText(search_job.getHours());
                        completed.setText(search_job.getCompleted());
                    } else {
                        UtilityPermission.showToast(getActivity(), "There is no data of this date");
                    }
                }
            }
            if (request_id.equals("1")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            deleteDialog.dismiss();
                            UtilityPermission.showToast(getActivity(), "Time Sheet Generate Successfully");
                        } else {
                            deleteDialog.dismiss();
                            UtilityPermission.showToast(getActivity(), "There is some error");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
            if (request_id.equals("3")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {

                            UtilityPermission.showToast(getActivity(), "Time Sheet Edit Successfully");
                        } else {
                            UtilityPermission.showToast(getActivity(), "There is some error");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
            if (request_id.equals("4")) {
                if (response != null) {
                    try {
                        JSONObject json_object = new JSONObject(response);
                        if (json_object.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), "Time Sheet Delete Successfully");
                            delete.setVisibility(View.GONE);
                            edit.setVisibility(View.GONE);
                            search_job = null;
                            email.setText("");
                            jobtype.setText("");
                            contact.setText("");
                            no_hours.setText("");
                            completed.setText("");
                        } else {
                            UtilityPermission.showToast(getActivity(), "There is some error");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    private void showDialog() {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View time_sheet_view = factory.inflate(R.layout.add_time_sheet_layout, null);
        deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(time_sheet_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageButton cross_button = (ImageButton) time_sheet_view.findViewById(R.id.cross_button);
        final EditText select_date = (EditText) time_sheet_view.findViewById(R.id.select_date);
        final EditText comment = (EditText) time_sheet_view.findViewById(R.id.comment);
        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                common_view = select_date;
                dpd.show(getActivity().getFragmentManager(), "DatePickerDialog");
            }
        });
        final EditText select_hours = (EditText) time_sheet_view.findViewById(R.id.select_hours);
        select_hours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectHours();
            }

            private void selectHours() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Hours");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        // Do something with the selection
                        select_hours.setText(items[item]);
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        final Button submit = (Button) time_sheet_view.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                params.put("hours", select_hours.getText().toString());
                params.put("date", select_date.getText().toString());
                params.put("comment", comment.getText().toString());
                CallService.getInstance().passInfromation(api_listener, Constants.ADD_ASSIGNMENT_URL, params, true, "1", getActivity());
            }
        });


        cross_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });

        deleteDialog.show();
    }

}
