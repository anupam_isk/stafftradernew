package com.android.AllFragment;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.ChooseCredential;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.LoginUserResponse;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;

import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.ImageTrans_CircleTransform;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by Anupam tyagi.
 */
public class ProfileUpdateFragment extends Fragment implements View.OnClickListener, ApiResponseListener {
    private EditText fName, lName, email, job_title, location, indus_type, phone, bio, qualification, skills;
    private ImageView user_img;
    private TableRow table_row;
    private String userChoosenTask, current;
    private TextView update;
    private final int REQUEST_CAMERA = 1;
    private Bitmap bitmap = null;
    private final int SELECT_FILE = 2;
    private onFragmentListener listener;
    private final int REQUEST_CODE_DOC = 3;
    private TextView select_resume, text_resume;
    private File myPath;
    private File file1;
    private String file_nam = "";
    private Bundle b = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = getArguments();
    }

    public ProfileUpdateFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_update_profile, container, false);
        init(v);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    public void init(View v) {
        user_img = (ImageView) v.findViewById(R.id.user_img);
        fName = (EditText) v.findViewById(R.id.first_name);
        lName = (EditText) v.findViewById(R.id.last_name);
        phone = (EditText) v.findViewById(R.id.phone);
        email = (EditText) v.findViewById(R.id.email);
        job_title = (EditText) v.findViewById(R.id.job_title);
        indus_type = (EditText) v.findViewById(R.id.indus_type);
        table_row = (TableRow) v.findViewById(R.id.table_row_second);
        location = (EditText) v.findViewById(R.id.new_location);
        bio = (EditText) v.findViewById(R.id.bio);
        qualification = (EditText) v.findViewById(R.id.qualification);
        select_resume = (TextView) v.findViewById(R.id.select_resume);
        text_resume = (TextView) v.findViewById(R.id.text_resume);
        skills = (EditText) v.findViewById(R.id.skills);
        update = (TextView) v.findViewById(R.id.update);
        update.setOnClickListener(this);
        user_img.setOnClickListener(this);
        indus_type.setOnClickListener(this);
        select_resume.setOnClickListener(this);
        if (UserPref.getuser_instance(getActivity()).getRoleId().equals(Constants.RECRUITER_ROLEID)) {
            if (b != null) {
                job_title.setVisibility(View.GONE);
                indus_type.setVisibility(View.GONE);
            }
        }
        if (b != null)
            setUserData();
        else {
            table_row.setVisibility(View.VISIBLE);
            bio.setVisibility(View.VISIBLE);
            qualification.setVisibility(View.VISIBLE);
            skills.setVisibility(View.VISIBLE);
        }
    }

    private void setUserData() {
        fName.setText(UserPref.getuser_instance(getActivity()).getUserFirstName());
        lName.setText(UserPref.getuser_instance(getActivity()).getUserLastName());
        email.setText(UserPref.getuser_instance(getActivity()).getUserEmail());
        job_title.setText(UserPref.getuser_instance(getActivity()).getUserjobtitle());
        phone.setText(UserPref.getuser_instance(getActivity()).getUserPhone());
        location.setText(UserPref.getuser_instance(getActivity()).getStreet() + " , " + UserPref.getuser_instance(getActivity()).getCity() + " , " + UserPref.getuser_instance(getActivity()).getCountry());
        Log.e("user image", UserPref.getuser_instance(getActivity()).getUserImage());
        if (!(UserPref.getuser_instance(getActivity()).getUserImage().equals(""))) {
            Log.e("in if", "in if");
            Picasso.with(getActivity())
                    .load(UserPref.getuser_instance(getActivity()).getUserImage())
                    .error(R.drawable.splash_logo)
                    .resize(200, 200)
                    .transform(new ImageTrans_CircleTransform(getActivity()))
                    .into(user_img);
        } else
            user_img.setBackground(getResources().getDrawable(R.drawable.user_profle));

    }

    private void getDocument() {
        Intent intent = new Intent();
        Log.e("version", String.valueOf(Build.VERSION.SDK_INT));
        if (Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.KITKAT) {

            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        } else
            intent.setAction(Intent.ACTION_GET_CONTENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/*");
        startActivityForResult(Intent.createChooser(intent, "Select a file "), REQUEST_CODE_DOC);
    }

    @Override
    public void onClick(View v) {
        if (v == user_img) {
            selectImage();
        } else if (v == indus_type) {
            showSpinnerItem();
        } else if (v == select_resume) {
            getDocument();
        } else {

            try {
                if (b != null) {
                    Log.e("b is not null", "b is not null");
                    updateProfile();
                } else {
                    Log.e("b is  null", "b is null");
                    updateCandidate();
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateCandidate() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("job_title", job_title.getText().toString());
        params.put("InsType", indus_type.getText().toString());
        //  params.put("image", path1);
       /* String path1 = "/mnt/sdcard/Khairdonate/myimage1.jpg";
        File fyl = new File(path1);
        params.put("image",fy1);*/
        params.put("bio", bio.getText().toString());
        params.put("qualification", qualification.getText().toString());
        params.put("skills", skills.getText().toString());
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("first_name", fName.getText().toString());
        params.put("last_name", lName.getText().toString());
        params.put("email", email.getText().toString());
        params.put("street", UserPref.getuser_instance(getActivity()).getStreet());
        params.put("phone", phone.getText().toString());
        params.put("city", UserPref.getuser_instance(getActivity()).getCity());
        params.put("state", UserPref.getuser_instance(getActivity()).getState());
        params.put("country", UserPref.getuser_instance(getActivity()).getCountry());
        params.put("lat", UserPref.getuser_instance(getActivity()).getLattitude());
        params.put("lon", UserPref.getuser_instance(getActivity()).getLongitude());
        params.put("role_id", "4");
        if (myPath == null && file1 == null) {
            CallService.getInstance().passInfromation(this, Constants.UPLOAD_NEW_CANDIDATE, params, true, "2", getActivity());
            Log.e("both file null", "both file null");
        } else if (myPath != null && file1 != null) {
            CallService.getInstance().passDataToMultiPartOther(this, Constants.UPLOAD_NEW_CANDIDATE, params, true, file1, myPath, "2", getActivity(), "img");
            Log.e("both file not null", "both file  not null");
        } else if (myPath != null && file1 == null) {
            CallService.getInstance().passDataToMultiPart(this, Constants.UPLOAD_NEW_CANDIDATE, params, true, myPath, "2", getActivity(), "img");
            Log.e("My path not null", "My path not null");
        } else if (myPath == null && file1 != null) {
            Log.e("else if my path null ", "else if my path null");
            CallService.getInstance().passDataToMultiPart(this, Constants.UPLOAD_NEW_CANDIDATE, params, true, file1, "2", getActivity(), "doc");
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                boolean result = UtilityPermission.checkPermission(getActivity());
                if (items[position].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result) {
                        boolean check = UtilityPermission.checkCameraPermission(getActivity());
                        if (check)
                            cameraIntent();
                    }
                } else if (items[position].equals("Choose from gallery")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[position].equals("Cancel")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        i.setType("image/*");
        startActivityForResult(Intent.createChooser(i, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Log.e("request code", requestCode + "");
            if (requestCode == SELECT_FILE)
                onSelectGalleryImageResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == REQUEST_CODE_DOC) {
                if (resultCode == Activity.RESULT_OK) {
                    final String state = Environment.getExternalStorageState();
                    Log.e("result ok", resultCode + "" + requestCode);
                    String selectedPath = UtilityPermission.getPath(getActivity(), data.getData());
//                Log.e("selected path",selectedPath);
                    File file = null;
                    if (selectedPath != null) {
                        file = new File(selectedPath);
                    } else {
                        Uri selectedFileURI = data.getData();
                        file = new File(selectedFileURI.getPath().toString());
                    }

                    // that file object send to server like image upload
                    Log.e("", "File : " + file.getName() + " length " + file.length() + " PATH" + file.getPath());
                    if (file.getPath().toString().contains(":")) {
                        StringTokenizer tokens = new StringTokenizer(file.getPath().toString(), ":");
                        String first = tokens.nextToken();
                        Log.e("", "first: " + first);
                        String file_1 = tokens.nextToken();
                        Log.e("", "second" + file_1);
                        file_nam = file_1;
                        String sd1path = "";
                        if (new File("/storage/extSdCard/").exists()) {
                            sd1path = "/storage/extSdCard/";
                            file1 = new File(sd1path +
                                    file_nam);
                            Log.e("Sd Cardext Path", sd1path);
                        }
                        if (new File("/storage/sdcard1/").exists()) {
                            sd1path = "/storage/sdcard1/";
                            file1 = new File(sd1path +
                                    file_nam);
                            Log.e("Sd Card1 Path", sd1path);
                        }
                        if (new File("/storage/usbcard1/").exists()) {
                            sd1path = "/storage/usbcard1/";
                            file1 = new File(sd1path +
                                    file_nam);
                            Log.e("USB Path", sd1path);
                        }
                 /*   if(new File("/storage/sdcard0/").exists())
                    {
                        sd1path="/storage/sdcard0/";
                        file1 = new File(sd1path+
                                file_nam);
                        Log.e("Sd Card0 Path",sd1path);
                    }*/
                        if (new File("/storage/sdcard1/").exists()) {
                            sd1path = "/storage/sdcard1/";
                            file1 = new File(sd1path +
                                    file_nam);
                            Log.e("Sd Card0 Path", sd1path);
                        }

                  /*  file1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                            file_nam);*/
                        //file1 = new File("/mnt/external_sd/"+file_nam);
                        Log.e("File Path : ", file1.getPath() + ", File size : " + file1.length() + " KB");
                    } else {
                        // file_nam = file.getName().toString();
                        // file_nam= "Download/pdfurl_guide.pdf";
                        file1 = file;
                        file_nam = file1.getName();
                    }
                    try {
                        //  file = new File(selectedPath);
                        long length = file1.length();
                        length = length / 1024;
                        Log.e("File Path : ", file.getPath() + ", File size : " + length + " KB");
                        if (length > 2048) {
                            AlertManger.getAlert_instance().showAlert("You cant select more than 2 mb Resume", getActivity());
                        } else {
                            text_resume.setText(file_nam);
                        }
                    } catch (Exception e) {
                        Log.e("File not found : ", e.getMessage() + e);
                    }
                }
            }
        }
    }

    private void createDirectory() {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("StaffTraderProfilePic", Context.MODE_PRIVATE);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                Log.e("directory is null", "directory is null");
            }
        }
        // Create imageDir
        // uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
        current = "profile.png";
        myPath = new File(directory.getPath() + File.separator + current);
    }

    private void onSelectGalleryImageResult(Intent data) {
        if (data != null) {
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                String selectedPath = UtilityPermission.getPath(getActivity(), data.getData());
                Log.e("Image  path", selectedPath);
                try {
                    myPath = new File(selectedPath);
                    if (myPath.exists()) {
                        Log.e("File name: ", myPath.getAbsolutePath());
                    }
                } catch (Exception e) {
                    Log.e("File not found : ", e.getMessage() + e);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bitmap != null)
                user_img.setImageBitmap(bitmap);
            else
                Log.e("bitmap is null", "bitmap is null");
            // loadImageFromStorage(myPath);
        } else {
            Log.e("data is null", "data is null");
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        user_img.setImageBitmap(bitmap);
        createDirectory();
        // SaveToInternal(myPath);
        //  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (myPath == null) {
            Log.d("null",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        // bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
      /*  myPath= new File(Environment.getExternalStorageDirectory()+File.separator+"StaffTrader"+File.separator,
                System.currentTimeMillis() + ".png");*/
        saveBitMap();
    }

    private void saveBitMap() {
        Log.e("mypath: ", myPath.getAbsolutePath());
        FileOutputStream fo = null;
        try {
            //   myPath.createNewFile();
            fo = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fo);
            // fo.write(bytes.toByteArray());
            fo.close();
            if (myPath.exists()) {
                Log.e("File name: ", myPath.getAbsolutePath());
                Log.e("file name ", myPath.getName());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void SaveToInternal(File file) {
        try {
            FileOutputStream mFileOutStream = new FileOutputStream(file);
            mFileOutStream.flush();
            mFileOutStream.close();
            if (myPath.exists()) {
                Log.e("File name: ", myPath.getAbsolutePath());
            }
            String url = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "title", null);
            Log.e("log_tag", "url: " + url);
            //In case you want to delete the file
            //boolean deleted = mypath.delete();
            //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
            //If you want to convert the image to string use base64 converter

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.v("log_tag", e.toString());
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case UtilityPermission.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }

    private void showSpinnerItem() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Category");
        builder.setItems(ChooseCredential.items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                indus_type.setText(ChooseCredential.items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    void updateProfile() throws UnsupportedEncodingException {
    /*    String path1 = "/mnt/sdcard/Khairdonate/myimage1.jpg";
        File fyl = new File(path1);*/

        // http://oycdemo.com/stafftrader/web_service.php?REQUEST=REGISTRATION&
        // username=r@gmail.com&first_name=t2vbest&last_name=admin&e=admin@gmail.com&p=12322
        // &phone=1234567891&street=d-5&city=po&state=up&country=fsd&lat=234.244&lon=32.234
        // &image=fsdfsdf&role_id=4&job_title=software&about=dfnan&InsType=IT
        HashMap<String, String> params = new HashMap<String, String>();
        if (!UserPref.getuser_instance(getActivity()).getRoleId().equals("3")) {
            params.put("job_title", job_title.getText().toString());
            params.put("InsType", indus_type.getText().toString());
        }
//  params.put("image", path1);
       /* String path1 = "/mnt/sdcard/Khairdonate/myimage1.jpg";
        File fyl = new File(path1);
        params.put("image",fy1);*/
        params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        params.put("first_name", fName.getText().toString());
        params.put("last_name", lName.getText().toString());
        params.put("e", email.getText().toString());
        params.put("street", UserPref.getuser_instance(getActivity()).getStreet());
        params.put("phone", phone.getText().toString());
        params.put("city", UserPref.getuser_instance(getActivity()).getCity());
        params.put("state", UserPref.getuser_instance(getActivity()).getState());
        params.put("country", UserPref.getuser_instance(getActivity()).getCountry());
        params.put("lat", UserPref.getuser_instance(getActivity()).getLattitude());
        params.put("lon", UserPref.getuser_instance(getActivity()).getLongitude());
        params.put("role_id", UserPref.getuser_instance(getActivity()).getRoleId());
        if (myPath == null)
            CallService.getInstance().passInfromation(this, Constants.UPDATE_PROFILE_URL, params, true, "1", getActivity());
        else
            CallService.getInstance().passDataToMultiPart(this, Constants.UPDATE_PROFILE_URL, params, true, myPath, "1", getActivity(), "img");
    }


    @Override
    public void getResponse(String response, String request_id) {
        Log.e("response ", response);
        Log.e("request _id  ", request_id);
        if (response != null) {
            if (request_id.equals("1")) {
                Log.e("response ", response);
                LoginUserResponse user_response = new Gson().fromJson(response, LoginUserResponse.class);
                if (user_response.isStatus()) {
                    LoginUserResponse.UserData user_data = user_response.getData();
                    Log.e("user1 image ", user_data.getImage());
                    UserPref.getuser_instance(getActivity()).setUserId(user_data.getId());
                    UserPref.getuser_instance(getActivity()).setUserFirstName(user_data.getFirst_name());
                    UserPref.getuser_instance(getActivity()).setUserLastName(user_data.getLast_name());
                    UserPref.getuser_instance(getActivity()).setUserEmail(user_data.getEmail());
                    UserPref.getuser_instance(getActivity()).setUserPhone(user_data.getPhone());
                    UserPref.getuser_instance(getActivity()).setRoleId(user_data.getRole_id());
                    UserPref.getuser_instance(getActivity()).setuserImage(user_data.getImage());
                    UserPref.getuser_instance(getActivity()).setuserjobtitle(user_data.getJob_title());
                    UserPref.getuser_instance(getActivity()).setAutoLogin(true);
                    UtilityPermission.showToast(getActivity(), "Profile Update Successfully");
                    Log.e("user image ", UserPref.getuser_instance(getActivity()).getUserImage());
                } else {
                    AlertManger.getAlert_instance().showAlert("Something went wrong", getActivity());
                }
            } else if (request_id.equals("2")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        UtilityPermission.showToast(getActivity(), "Candidate has been Successfully submitted.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
