package com.android.AllFragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.OnItemClickListener;
import com.android.FragmentInterface.onLoadMoreListener;
import com.android.GsonResponse.MilestoneResponse;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditMilestoneFragment extends Fragment {
    private EditText milestone_name, milestone_result, start_date, due_date, link_recruiter, link_candidate, comment, common_date;
    private TextView submitBT;
    private DatePickerDialog dpd;
    private String recruiter_id, candidate_id;
    private Bundle b = null;
    private ListAdapter adapter;
    private int start_value = 0;
    private int limit_value = 6;
    private String role_id;
    private AlertDialog deleteDialog;
    private EditText common_view;
    private ArrayList<HashMap<String, String>> first_list;
    private MilestoneResponse.MilestoneClass milestone_object;

    public EditMilestoneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        first_list = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                date_listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        b = getArguments();
        if (b != null) {
            milestone_object = getArguments().getParcelable("milestone_job");
            milestone_name.setText(milestone_object.getMilestone_name());
            milestone_result.setText(milestone_object.getResult());
            start_date.setText(milestone_object.getStart_date());
            due_date.setText(milestone_object.getDue_date());
            comment.setText(milestone_object.getComments());
            recruiter_id = milestone_object.getLink_recruiter();
            candidate_id = milestone_object.getLink_candidate();
            link_candidate.setText(milestone_object.getCandidate_name());
            link_recruiter.setText(milestone_object.getRecruiter_name());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            common_date.setText(date);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_edit_milestone, container, false);
        intializeUI(v);
        setListener();
        return v;
    }

    private void setListener() {
        link_recruiter.setOnClickListener(click_listener);
        link_candidate.setOnClickListener(click_listener);
        submitBT.setOnClickListener(click_listener);
        start_date.setOnClickListener(click_listener);
        due_date.setOnClickListener(click_listener);
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            if (request_id.equals("2")) {
                if (response != null) {
                    first_list.clear();
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            JSONArray data_array = json_response.getJSONArray("data");
                            for (int i = 0; i < data_array.length(); i++) {
                                HashMap<String, String> map = new HashMap<>();
                                map.put("id", data_array.getJSONObject(i).getString("id"));
                                map.put("name", data_array.getJSONObject(i).getString("first_name") + " " + data_array.getJSONObject(i).getString("last_name"));
                                first_list.add(map);
                            }
                            showPopup();
                        } else {

                            AlertManger.getAlert_instance().showAlert("Something went wrong on server side", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("1")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            UtilityPermission.showToast(getActivity(), "New Milestone has been added.");
                            FragmentManager manager = ((HomeActivity) getActivity()).getSupportFragmentManager();
                            manager.popBackStack();
                        } else {

                            AlertManger.getAlert_instance().showAlert("Something went wrong on server side.", getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    private void intializeUI(View v) {
        milestone_name = (EditText) v.findViewById(R.id.milestone_name);
        milestone_result = (EditText) v.findViewById(R.id.milestone_result);
        start_date = (EditText) v.findViewById(R.id.start_date);
        due_date = (EditText) v.findViewById(R.id.due_date);
        link_candidate = (EditText) v.findViewById(R.id.link_candidate);
        link_recruiter = (EditText) v.findViewById(R.id.link_recruiter);
        comment = (EditText) v.findViewById(R.id.comment);
        submitBT = (TextView) v.findViewById(R.id.submitBT);
    }

    View.OnClickListener click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == submitBT) {
                editDataToServer();
            } else if (view == link_candidate) {
                start_value = 0;
                limit_value = 6;
                role_id = "4";
                common_view = link_candidate;
                // first_list.clear();
                HashMap<String, String> params = new HashMap<>();
                params.put("role_id", role_id);
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, true, "2", getActivity());
            } else if (view == link_recruiter) {
                start_value = 0;
                limit_value = 6;
                role_id = "3";
                common_view = link_recruiter;
                // first_list.clear();
                HashMap<String, String> params = new HashMap<>();
                params.put("role_id", role_id);
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, true, "2", getActivity());
            } else if (view == start_date) {
                common_date = start_date;
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

            } else if (view == due_date) {

                common_date = due_date;
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

            }

        }
    };

    private void editDataToServer() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
        map.put("milestone_name", milestone_name.getText().toString());
        map.put("start_date", start_date.getText().toString());
        map.put("due_date", due_date.getText().toString());
        map.put("result", milestone_result.getText().toString());
        map.put("candidate_id", candidate_id);
        map.put("recruiter_id", recruiter_id);
        map.put("comments", comment.getText().toString());
        if (b != null) {
            map.put("milestone_id", milestone_object.getId());
            CallService.getInstance().passInfromation(api_listener, Constants.EDIT_MILESTONE_URL, map, true, "1", getActivity());
        } else
            CallService.getInstance().passInfromation(api_listener, Constants.ADD_MILESTONE_URL, map, true, "1", getActivity());
    }

    private void showPopup() {

        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View deleteDialogView = factory.inflate(
                R.layout.dialog_view, null);
        deleteDialog = new AlertDialog.Builder(getActivity()).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        RecyclerView recyclerview = (RecyclerView) deleteDialogView.findViewById(R.id.recycler_view);
        TextView cancelBT = (TextView) deleteDialogView.findViewById(R.id.cancelBT);
        TextView select_skill = (TextView) deleteDialogView.findViewById(R.id.select_skill);
        select_skill.setGravity(Gravity.LEFT);
        select_skill.setText("Select One");
        ImageButton closeBT = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        TextView saveBT = (TextView) deleteDialogView.findViewById(R.id.saveBT);
        cancelBT.setVisibility(View.GONE);
        saveBT.setVisibility(View.GONE);
        closeBT.setVisibility(View.VISIBLE);
        closeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start_value = 0;
                limit_value = 6;
                deleteDialog.dismiss();
            }
        });
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.addItemDecoration(new SimpleDividerDecoration(getActivity()));
        adapter = new ListAdapter(first_list, getActivity(), recyclerview);
        recyclerview.setAdapter(adapter);
        adapter.setOnLoadMoreListener(new onLoadMoreListener() {
            @Override
            public void onLoadMore() {
                first_list.add(null);
                adapter.notifyItemInserted(first_list.size() - 1);
                start_value = limit_value + 1;
                limit_value = limit_value + 3;
                Log.e("start_value", String.valueOf(start_value));
                Log.e("limit_value", String.valueOf(limit_value));
                HashMap<String, String> params = new HashMap<>();
                params.put("role_id", role_id);
                params.put("start", String.valueOf(start_value));
                params.put("limit", String.valueOf(limit_value));
                CallService.getInstance().passInfromation(api_listener, Constants.GET_USERS_URL, params, false, "3", getActivity());
            }
        });
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                start_value = 0;
                limit_value = 6;
                if (common_view == link_candidate)
                    candidate_id = first_list.get(position).get("id");
                else
                    recruiter_id = first_list.get(position).get("id");

                common_view.setText(first_list.get(position).get("name"));

                deleteDialog.dismiss();
                // Toast.makeText(getActivity(), "name " + first_list.get(position).get("name") + "position " + position + "id " + first_list.get(position).get("id"), Toast.LENGTH_LONG).show();
            }
        });
      /*

      we can use interface like this .we will make a method in any class to register the listener

        OnItemClickListener item_listener=new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

            }
        };
        adapter.setOnItemClickListener(item_listener);*/
        deleteDialog.show();
    }

    public class ListAdapter extends RecyclerView.Adapter {
        private final int VIEW_ITEM = 1;
        private final int VIEW_PROG = 0;
        private ArrayList<HashMap<String, String>> manager_list;
        private int visibleThreshold = 3;
        private int lastVisibleItem, totalItemCount;
        private boolean loading;
        private onLoadMoreListener onListener;
        private OnItemClickListener cliclistener;
        Context context;

        public ListAdapter(ArrayList<HashMap<String, String>> manager_list, Context context, RecyclerView recyclerview) {
            this.manager_list = manager_list;
            this.context = context;
            if (recyclerview.getLayoutManager() instanceof LinearLayoutManager) {

                Log.e("yes", "yes");
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerview
                        .getLayoutManager();


                recyclerview
                        .addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView,
                                                   int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                Log.e("on scroll", "on scroll");
                                totalItemCount = linearLayoutManager.getItemCount();
                                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                    // End has been reached
                                    // Do something
                                    if (onListener != null) {
                                        onListener.onLoadMore();
                                    }
                                    loading = true;
                                }

                            }
                        });
            } else {
                Log.e("no", "no");
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;
            if (viewType == VIEW_ITEM) {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.linear_recruiter, parent, false);

                vh = new ListAdapter.MyViewHolder(v);
            } else {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.progress_bar_item, parent, false);

                vh = new ListAdapter.ProgressViewHolder(v);
            }
            return vh;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof ListAdapter.MyViewHolder) {
                ((ListAdapter.MyViewHolder) holder).bind(position, cliclistener);

            } else {
                ((ListAdapter.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }
        }

        @Override
        public int getItemViewType(int position) {
            return manager_list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }

        public void setOnItemClickListener(OnItemClickListener cliclistener) {
            this.cliclistener = cliclistener;
        }

        public void setOnLoadMoreListener(onLoadMoreListener onListener) {
            this.onListener = onListener;
        }

        public void setLoaded() {
            loading = false;
        }

        @Override
        public int getItemCount() {
            return manager_list.size();
        }

        public class ProgressViewHolder extends RecyclerView.ViewHolder {
            public ProgressBar progressBar;

            public ProgressViewHolder(View v) {
                super(v);
                progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
            }
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name;

            public MyViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
            }

            public void bind(final int position, final OnItemClickListener cliclistener) {
                Log.e("position", position + "");
                name.setText(manager_list.get(position).get("name"));
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cliclistener.onItemClick(position);
                    }
                });
            }
        }

    }

}
