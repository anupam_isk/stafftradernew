package com.android.AllFragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.FragmentInterface.onFragmentListener;
import com.android.GsonResponse.OpenJobResponse;
import com.android.ServerCommunication.CallService;
import com.android.adapter.OpenJobAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.SimpleDividerDecoration;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClosedJobRequiremenrt extends Fragment {
    public ClosedJobRequiremenrt() {
        // Required empty public constructor
    }

    private RecyclerView recycler_view;
    private onFragmentListener listener;
    private OpenJobAdapter adapter;
    private boolean _hasLoadedOnce = false;
    private ArrayList<OpenJobResponse.OpenJob> open_job_list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        open_job_list = new ArrayList<>();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter = new OpenJobAdapter(open_job_list, getActivity(), UserPref.getuser_instance(getActivity()).getUserId(), "close");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_closed_job_requiremenrt, container, false);
        initilaizeUI(v);
        return v;
    }

    private void initilaizeUI(View v) {
        recycler_view = (RecyclerView) v.findViewById(R.id.recycler_view);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (onFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        listener.showDrawerToggle(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // always  keep in mind
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            Log.e("inside visible ", "in side visible" + isFragmentVisible_);
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", UserPref.getuser_instance(getActivity()).getUserId());
                CallService.getInstance().passInfromation(api_listner, Constants.OPEN_JOB_CLOSE_URL, params, true, "1", getActivity());
                _hasLoadedOnce = true;
            } else {
                _hasLoadedOnce = false;
                Log.e("fragment in visible", "fragment in visible" + _hasLoadedOnce + " " + isFragmentVisible_);
            }
        }
    }

    ApiResponseListener api_listner = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (response != null) {
                OpenJobResponse open_job_response = new Gson().fromJson(response, OpenJobResponse.class);
                if (open_job_response.isStatus()) {
                    open_job_list = open_job_response.getClosed_job();
                    adapter = new OpenJobAdapter(open_job_list, getActivity(), UserPref.getuser_instance(getActivity()).getUserId(), "close");
                    recycler_view.setAdapter(adapter);
                } else {
                    AlertManger.getAlert_instance().showAlert("There is some error on server side", getActivity());
                }
            }
        }

    };

}
