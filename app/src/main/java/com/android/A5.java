package com.android;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Collections;

/**
 * Created by anupam  on 3/8/2017.
 */

class A5 {
    void m() throws IOException {
        System.out.println(this);
        throw new IOException();//prints same reference ID
    }

    void m1() throws FileNotFoundException {
        try {
            m();
        } catch (Exception e) {
            System.out.println("inner catch m1");
            throw new FileNotFoundException();
        }
        //prints same reference ID
    }

    public static void main(String args[]) {
        A5 obj = new A5();
        System.out.println(obj);//prints the reference ID
        try {
            obj.m1();
        } catch (FileNotFoundException e) {
            System.out.println("File Not found exception");
        }

    }
}