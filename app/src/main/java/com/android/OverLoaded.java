package com.android;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by admin on 2/1/2017.
 */

public class OverLoaded {
    public void foo(String s) {

    }
    public void foo(Integer i) {

    }
    public void foo(Object o) {

    }
    // Count Number of String
    public static void Count(String str) {

        char[] c = str.toCharArray();
        for (int i = 0; i < c.length; i++) {
            int count = 0;
            for (int j = i + 1; j < c.length; j++) {
                if (c[i] == c[j]) {
                    ++count;
                }
            }
            System.out.println(c[i] + " count " + count);
        }
    }

    public static HashMap<Character, Integer> Count2(String str) {
        char[] character_string = str.toCharArray();
        HashMap<Character, Integer> counter_map = new HashMap<>();
        for (char ch : character_string) {
            if (counter_map.containsKey(ch)) {
                counter_map.put(ch, counter_map.get(ch) + 1);
            } else {
                counter_map.put(ch, 1);
            }
        }
        Set<Character> key_set = counter_map.keySet();
        for (char key : key_set) {
            if (counter_map.get(key) > 1) {
                System.out.println(key + "value " + counter_map.get(key));
            }
        }
        return counter_map;
    }

    public static void main(String... args) {
        //  new OverLoaded().foo();
        String s1 = "anupamtyagiptyyyiis";
        // Count("anupamtyagi");
        Count2(s1);
        //  System.out.println(Count2(s1));
    }
}
