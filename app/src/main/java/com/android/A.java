package com.android;

/**
 * Created by admin on 12/28/2016.
 */

public abstract class A {
    protected  int a ;
    public abstract void a();
    public abstract void b();
}

class B extends A
{
    @Override
    public void a() {
        System.out.println("a method in class B");
        a=7;
    }

    @Override
    public void b() {
        System.out.println("b method in class B");
    }

    public void c() {
        System.out.println("c method in class B");
    }
}

class C extends B {
    @Override
    public void a() {
        System.out.println("a method in class C");
    }

    public void c() {
        this.g();
        System.out.println("c method in class C");
    }
     public void g() {
        System.out.println("g method in class c");
    }
    public void d() {
        System.out.println("d method in class C");
    }

    public void e() {
        System.out.println("e method in class C");
    }

}

class D extends C {
    public void f() {
        System.out.println("d method in class D");
    }

    public void d() {
        System.out.println("d method in class D");
    }
}
abstract class G extends D{
    public void f() {
        System.out.println("F method in class G");
    }
    public void d() {
        System.out.println("d method in class G");
    }
    public void c()
    {
        System.out.println("c method in class G");
    }
    public void j()
    {
        System.out.println("j method in class G");
    }
}
class F extends G{
    public void f() {
        System.out.println("F method in class f");
    }
    public void d() {
        System.out.println("d method in class f");
    }
    public void c()
    {
        System.out.println("c method in class f");
    }
    public void j()
    {
        System.out.println("j method in class f");
    }
}
class E {
    public static void main(String... args) {
        A a = new B();
        B a1 = new C();
        A a3=new C();
        D d=new D();
        G g=new F();
     /*   g.f();
        g.d();
        g.c();
        g.j();
        g.a();*/
        int x=10;
       /* int i=x++ + ++x + ++x + x++;
        System.out.println("i value "+ i + "x value "+ x);
        int i1=x++ + ++x;
        System.out.println("i value "+ i1 + "x value "+ x);*/
         int i=x++ + ++x + ++x + x++;
        System.out.println("i value "+ i + "x value "+ x);
    /*    d.f();
        d.c();
        d.d();
        d.a();*/

      /*  A a2 = new D();
        B b = new C();*/
      /*  a.a();
        a.b();
        a1.a();
        a1.b();
        a1.c();
        a3.a();
        a3.b();*/

    }
}