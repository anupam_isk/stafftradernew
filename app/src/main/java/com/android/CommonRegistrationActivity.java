package com.android;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.NetworkCheck;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.helper.Validation;
import com.android.stafftraderapp.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Anupam tyagi on 6/28/2016.
 * i did use this activity for common registartion based on role id
 */
public class CommonRegistrationActivity extends AppCompatActivity implements View.OnClickListener, ApiResponseListener, GoogleApiClient.ConnectionCallbacks {
    private EditText fName, lName, phone, email, password, conf_password, job_title, location, indus_type, spinner;
    private ImageView back, user_img;
    private TextView submit;
    private String userChoosenTask, current;
    private final int REQUEST_CAMERA = 1;
    private Bitmap bitmap = null;
    private final int SELECT_FILE = 2;
    private File myPath;
    GoogleApiClient mGoogleApiClient;
    private String role_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hiringmanager_registration);
        // user_pref = new UserPref(CommonRegistrationActivity.this);
        buildGoogleApiClient();
        initializeUI();
        setListener();
        if (getIntent() != null)
            role_id = getIntent().getStringExtra("role_id");
        if (role_id.equals(Constants.RECRUITER_ROLEID)) {
            job_title.setVisibility(View.GONE);
            spinner.setVisibility(View.GONE);
        }
        Log.e("role_id in on create", role_id);
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            Log.e("api client is null", "api client is null");
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).build();
            mGoogleApiClient.connect();
        }
    }

    private void getNewLocation() {
        buildGoogleApiClient();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            getCompleteAddressString(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            UserPref.getuser_instance(this).setLattitude(String.valueOf(mLastLocation.getLatitude()));
            UserPref.getuser_instance(this).setLongitude(String.valueOf(mLastLocation.getLongitude()));
            Log.e("lattiude", mLastLocation.getLatitude() + "");
            Log.e("longitude", mLastLocation.getLongitude() + "");
        } else {
            Log.e("mLas Location again", "null");
        }
    }

    private void setListener() {
        back.setOnClickListener(this);
        user_img.setOnClickListener(this);
        spinner.setOnClickListener(this);
        submit.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("registration", "registration");
    }

    private void initializeUI() {
        user_img = (ImageView) findViewById(R.id.user_img);
        back = (ImageView) findViewById(R.id.back);
        fName = (EditText) findViewById(R.id.first_name);
        lName = (EditText) findViewById(R.id.last_name);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        conf_password = (EditText) findViewById(R.id.conf_password);
        spinner = (EditText) findViewById(R.id.spinner);
        job_title = (EditText) findViewById(R.id.job_title);
        location = (EditText) findViewById(R.id.location);
        submit = (TextView) findViewById(R.id.submit);
        //  location.setText(user_pref.getStreet() + "," + user_pref.getCity() + "," + user_pref.getState() + "," + user_pref.getCountry());
    }

    @Override
    public void onClick(View v) {
        if (v == user_img) {
            selectImage();
        }
        if (v == back) {
            finish();
        }
        if (v == submit) {
            sendDataToServer();
        }
        if (v == spinner) {
            showSpinnerItem();
        }

    }

    private void getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                UserPref.getuser_instance(this).setStreet(returnedAddress.getAddressLine(0));
                Log.e("street", UserPref.getuser_instance(this).getStreet());
                UserPref.getuser_instance(this).setCity(returnedAddress.getLocality());
                UserPref.getuser_instance(this).setState(returnedAddress.getAdminArea());
                UserPref.getuser_instance(this).setCountry(returnedAddress.getCountryName());
                location.setText(UserPref.getuser_instance(this).getStreet() + "," + UserPref.getuser_instance(this).getCity() + "," + UserPref.getuser_instance(this).getState() + "," + UserPref.getuser_instance(this).getCountry());
               /* StringBuilder strReturnedAddress = new StringBuilder("");
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();*/

            }
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
            Log.e("My Current]address", "Canont get Address!");
        }

    }
    private boolean validate() {
        if (!(UserPref.getuser_instance(this).getLattitude().equals("") && UserPref.getuser_instance(this).getLongitude().equals(""))) {
            if (UtilityPermission.checkLength(fName)) {
                if (UtilityPermission.checkLength(lName)) {
                    if (UtilityPermission.checkLength(phone)) {
                        if (Validation.isValidMobile(phone.getText().toString())) {
                            if (Validation.isValidEmail(email.getText().toString()) && UtilityPermission.checkLength(email)) {
                                if (UtilityPermission.checkLength(password)) {
                                    if (password.getText().toString().equals(conf_password.getText().toString())) {
                                        if (!role_id.equals("3")) {
                                            if (UtilityPermission.checkLength(job_title)) {
                                                return true;
                                            } else {
                                                return UtilityPermission.setError(job_title, "Please Enter Job Title");
                                            }
                                        } else {
                                            return true;
                                        }

                                    } else {
                                        return UtilityPermission.setError(conf_password, "Please Confirm Your Password");
                                    }
                                } else {
                                    return UtilityPermission.setError(password, "Please Enter Password");
                                }

                            } else {
                                return UtilityPermission.setError(email, "Please Enter Valid Email Id");
                            }
                        } else {
                            return UtilityPermission.setError(phone, "Please Enter Valid Mobile Number");
                        }
                    } else {
                        return UtilityPermission.setError(phone, "Please Enter Mobile Number");
                    }
                } else {
                    return UtilityPermission.setError(lName, "Please Enter Last Name");
                }
            } else {

                return UtilityPermission.setError(fName, "Please Enter First Name");

            }
        } else {
            showAlertPop("Please unable your gps location", this);
            return false;
        }
    }

    public void showAlertPop(String message, final Context context) {
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Staff Trader App")
                    .setMessage(message)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void showSpinnerItem() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Category");
        builder.setItems(ChooseCredential.items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                spinner.setText(ChooseCredential.items[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CommonRegistrationActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                boolean result = UtilityPermission.checkPermission(CommonRegistrationActivity.this);
                if (items[position].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (items[position].equals("Choose from gallery")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                } else if (items[position].equals("Cancel")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        i.setType("image/*");//
        startActivityForResult(Intent.createChooser(i, "Select File"), SELECT_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Log.e("request code", requestCode + "");
            if (requestCode == SELECT_FILE)
                onSelectGalleryImageResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void createDirectory() {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("StaffTraderProfilePic", Context.MODE_PRIVATE);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                Log.e("directory is null", "directory is null");
            }
        }
        // Create imageDir
        // uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
        current = "profile.png";
        myPath = new File(directory.getPath() + File.separator + current);

    }

    private void onSelectGalleryImageResult(Intent data) {
        if (data != null) {

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                String selectedPath = UtilityPermission.getPath(CommonRegistrationActivity.this, data.getData());
                Log.e("Image  path", selectedPath);
                try {
                    myPath = new File(selectedPath);
                    if (myPath.exists()) {
                        Log.e("File name: ", myPath.getAbsolutePath());
                    }
                } catch (Exception e) {
                    Log.e("File not found : ", e.getMessage() + e);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            user_img.setImageBitmap(bitmap);
            // loadImageFromStorage(myPath);
        } else {
            Log.e("data is null", "data is null");
        }
    }

    private void onCaptureImageResult(Intent data) {
        bitmap = (Bitmap) data.getExtras().get("data");
        user_img.setImageBitmap(bitmap);
        createDirectory();
        // SaveToInternal(myPath);
        //  ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (myPath == null) {
            Log.d("null",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        // bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
      /*  myPath= new File(Environment.getExternalStorageDirectory()+File.separator+"StaffTrader"+File.separator,
                System.currentTimeMillis() + ".png");*/
        FileOutputStream fo = null;
        try {
            //   myPath.createNewFile();
            fo = new FileOutputStream(myPath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fo);
            // fo.write(bytes.toByteArray());
            fo.close();
            if (myPath.exists()) {
                Log.e("File name: ", myPath.getAbsolutePath());
                Log.e("file name ", myPath.getName());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void SaveToInternal(File file) {
        try {
            FileOutputStream mFileOutStream = new FileOutputStream(file);
            mFileOutStream.flush();
            mFileOutStream.close();
            if (myPath.exists()) {
                Log.e("File name: ", myPath.getAbsolutePath());
            }
            String url = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "title", null);
            Log.e("log_tag", "url: " + url);
            //In case you want to delete the file
            //boolean deleted = mypath.delete();
            //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
            //If you want to convert the image to string use base64 converter

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.v("log_tag", e.toString());
        }

    }

    private void sendDataToServer() {
        if (NetworkCheck.getInstance(this).isNetworkAvailable()) {
            if (validate()) {
                HashMap<String, String> params = new HashMap<String, String>();
                if (!role_id.equals("3")) {
                    Log.e("role_id in if", role_id);
                    params.put("job_title", job_title.getText().toString());
                    params.put("InsType", spinner.getText().toString());
                }
                params.put("username", email.getText().toString());
                params.put("first_name", fName.getText().toString());
                params.put("last_name", lName.getText().toString());
                params.put("e", email.getText().toString());
                params.put("p", password.getText().toString());
                params.put("phone", phone.getText().toString());
                params.put("street", UserPref.getuser_instance(this).getStreet());
                params.put("city", UserPref.getuser_instance(this).getCity());
                params.put("state", UserPref.getuser_instance(this).getState());
                params.put("country", UserPref.getuser_instance(this).getCountry());
                params.put("lat", UserPref.getuser_instance(this).getLattitude());
                params.put("lon", UserPref.getuser_instance(this).getLongitude());
                params.put("role_id", role_id);
                params.put("about", "gdgdg");
                if (myPath == null)
                    CallService.getInstance().passInfromation(this, Constants.REGISTRAION_URL, params, true, "1", CommonRegistrationActivity.this);
                else
                    CallService.getInstance().passDataToMultiPart(this, Constants.REGISTRAION_URL, params, true, myPath, "1", CommonRegistrationActivity.this, "img");
            }
        } else {
            AlertManger.getAlert_instance().showAlert(Constants.NETWORK_STRING, CommonRegistrationActivity.this);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case UtilityPermission.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }


    @Override
    public void getResponse(String response, String request_id) {
        if (response != null) {
            try {
                JSONObject response_object = new JSONObject(response);
                if (response_object.getBoolean("status")) {
                    Intent i = new Intent(this, ChooseCredential.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                } else {
                    UtilityPermission.showToast(this, "There is some error on server side");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getNewLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
