package com.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.helper.UserPref;
import com.android.stafftraderapp.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 9/13/2016.
 */
public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<HashMap<String,String>> map_list;
    Context context;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private final int VIEW_NEW = 2;
    public ChatAdapter(ArrayList<HashMap<String, String>> map_list, Context context) {
        this.map_list=map_list;
        Log.e("chat adapter","chat adapter");
        this.context=context;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType()==VIEW_ITEM || holder.getItemViewType()==VIEW_PROG) {
            MyViewHolder holder1=(MyViewHolder) holder;
            if (map_list.get(position).get("message").length() <= 17) {
                holder1.time.setVisibility(View.GONE);
                holder1.name.setText(map_list.get(position).get("message") + "  " + map_list.get(position).get("time").substring(12));
            } else {
                holder1.time.setVisibility(View.VISIBLE);
                holder1.time.setText(map_list.get(position).get("time").substring(12));
                holder1.name.setText(map_list.get(position).get("message"));
            }
        }
        else if(holder.getItemViewType()==VIEW_NEW)
        {
            DateHolder holder2=(DateHolder) holder;
            holder2.name.setText(map_list.get(position).get("date"));
        }
    }
    @Override
    public int getItemViewType(int position) {
        if(map_list.get(position).containsKey("date"))
            return VIEW_NEW;
        else
            return  map_list.get(position).get("user_id").equals(UserPref.getuser_instance(context).getUserId()) ? VIEW_ITEM: VIEW_PROG;
    }
    @Override
    public int getItemCount() {
        return map_list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("view type parameter",String.valueOf(viewType));
        View itemView=null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_PROG) {
           itemView = inflater
                    .inflate(R.layout.row_layout, parent, false);
            return new MyViewHolder(itemView);
        }
       else if(viewType==VIEW_ITEM)
        {
            itemView = inflater
                    .inflate(R.layout.row_layout_right, parent, false);
            return new MyViewHolder(itemView);

        }
        else
        {
            itemView = inflater
                    .inflate(R.layout.date_show, parent, false);
            return new DateHolder(itemView);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,time;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            time = (TextView) view.findViewById(R.id.time);
        }
    }
     class DateHolder extends RecyclerView.ViewHolder {
        public TextView name;
        DateHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.date);
        }
    }
}
