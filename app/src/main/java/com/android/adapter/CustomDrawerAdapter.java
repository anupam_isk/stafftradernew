package com.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.model.DrawerItem;
import com.android.stafftraderapp.R;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi 1-7-2016.
 */
public class CustomDrawerAdapter extends ArrayAdapter {
    Context context;
     ArrayList<DrawerItem> drawerItemList;
    int layoutResID;
    public CustomDrawerAdapter(Context context, int layoutResourceID,
                               ArrayList<DrawerItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.ItemName = (TextView) view
                    .findViewById(R.id.text);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.image);

            view.setTag(drawerHolder);

        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }

        DrawerItem dItem = drawerItemList.get(position);
        drawerHolder.icon.setImageDrawable(context.getResources().getDrawable(
                dItem.getImgResID()));
        drawerHolder.ItemName.setText(dItem.getItemName());

        return view;
    }

    private static class DrawerItemHolder {
        TextView ItemName;
        ImageView icon;
    }

}
