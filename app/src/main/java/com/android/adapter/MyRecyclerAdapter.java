package com.android.adapter;

/**
 * Created by Anupam tyagi
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.android.GsonResponse.SearchResponse;
import com.android.stafftraderapp.R;

import java.util.ArrayList;

public class MyRecyclerAdapter extends BaseAdapter {
   ArrayList<SearchResponse.SearchJob> search_list;
    Context context;
    private static LayoutInflater inflater = null;
    public MyRecyclerAdapter(Context mainActivity, ArrayList<SearchResponse.SearchJob> search_list) {
        // TODO Auto-generated constructor stub
        this.search_list=search_list;
        context = mainActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return search_list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView tv;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        View rowView=convertView;
        if(rowView==null) {
            holder=new Holder();
            rowView = inflater.inflate(R.layout.list_row, null);
            holder.tv = (TextView) rowView.findViewById(R.id.name);
             rowView.setTag(holder);
        }

        else
           holder= (Holder) convertView.getTag();
     holder.tv.setText(search_list.get(position).getFirst_name());

        return rowView;
    }

}
