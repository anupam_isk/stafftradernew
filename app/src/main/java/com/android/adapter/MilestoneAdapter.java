package com.android.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.AllFragment.EditJobRequirementFragment;
import com.android.AllFragment.EditMilestoneFragment;
import com.android.AllFragment.ViewMilestoneTaskFragment;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.MilestoneResponse;
import com.android.GsonResponse.OpenJobResponse;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam  on 09-Nov-16.
 */

public class MilestoneAdapter extends RecyclerView.Adapter<MilestoneAdapter.MyViewHolder> {
    private ArrayList<MilestoneResponse.MilestoneClass> milestone_list;
    private Context context;

    public MilestoneAdapter(Context context, ArrayList<MilestoneResponse.MilestoneClass> milestone_list) {
        this.milestone_list = milestone_list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.milestone_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final MilestoneResponse.MilestoneClass milestone_response = milestone_list.get(position);
        holder.milestone_name.setText(milestone_response.getMilestone_name());
        Log.e("milestone result", milestone_response.getResult());
        holder.milestone_result.setText("Work Percentage :" + milestone_response.getResult());
        holder.ending_date.setText("Date " + milestone_response.getStart_date());
        holder.open_pop_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMilestonePopupMenu(v, position, milestone_response);
            }
        });
       /*if (UserPref.getuser_instance(context).getRoleId().equals("3"))
        {
            holder.editBT.setVisibility(View.GONE);
            holder.deleteBT.setVisibility(View.GONE);
        }
        else if (UserPref.getuser_instance(context).getRoleId().equals("4")) {
            holder.editBT.setVisibility(View.GONE);
            holder.deleteBT.setVisibility(View.GONE);
        }*/
       /* holder.editBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new EditMilestoneFragment();
                Bundle b = new Bundle();
                b.putParcelable("milestone_job", milestone_response);
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
            }
        });
        holder.deleteBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeletePpopup(milestone_list.get(position).getId());
            }
        });
        holder.viewtaskBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new ViewMilestoneTaskFragment();
                Bundle b = new Bundle();
                b.putString("milestone_id", milestone_list.get(position).getId());
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
            }
        });*/
    }
    private void showMilestonePopupMenu(View view, final int position, final MilestoneResponse.MilestoneClass milestone_response) {
        MenuBuilder menuBuilder = new MenuBuilder(context);
        MenuInflater inflater = new MenuInflater(context);
        inflater.inflate(R.menu.milestone_menu, menuBuilder);
        if (UserPref.getuser_instance(context).getRoleId().equals("3") || UserPref.getuser_instance(context).getRoleId().equals("4")) {
            menuBuilder.findItem(R.id.menu_edit).setVisible(false);
            menuBuilder.findItem(R.id.menu_delete).setVisible(false);
        }
        MenuPopupHelper optionsMenu = new MenuPopupHelper(context, menuBuilder, view);
        optionsMenu.setForceShowIcon(true);

// Set Item Click Listener
        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_edit: // Handle option1 Click
                        showEditMilstone(milestone_response);
                        return true;
                    case R.id.menu_delete:
                        showDeletePpopup(milestone_list.get(position).getId());
                        return true;
                    case R.id.menu_task:
                        Fragment fragment = new ViewMilestoneTaskFragment();
                        Bundle b = new Bundle();
                        b.putString("milestone_id", milestone_list.get(position).getId());
                        fragment.setArguments(b);
                        UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {
            }
        });

        optionsMenu.show();
    }

    private void showEditMilstone(MilestoneResponse.MilestoneClass milestone_response) {
        Fragment fragment = new EditMilestoneFragment();
        Bundle b = new Bundle();
        b.putParcelable("milestone_job", milestone_response);
        fragment.setArguments(b);
        UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
    }

    private void showDeletePpopup(final String id) {
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Staff Trader App")
                    .setMessage("Are you sure to delete this milestone ?")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deletMilestoneFromServer(id);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void deletMilestoneFromServer(String id) {
        HashMap<String, String> params = new HashMap<>();
        params.put("milestone_id", id);
        CallService.getInstance().passInfromation(api_listener, Constants.DELETE_MILESTONE_URL, params, true, "1", context);
    }

    @Override
    public int getItemCount() {
        return milestone_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView milestone_name, milestone_result, ending_date;
        public LinearLayout open_pop_menu;

        public MyViewHolder(View view) {
            super(view);
            milestone_name = (TextView) view.findViewById(R.id.milestone_name);
            milestone_result = (TextView) view.findViewById(R.id.milestone_result);
            ending_date = (TextView) view.findViewById(R.id.ending_date);
            open_pop_menu = (LinearLayout) view.findViewById(R.id.open_pop_menu);
        }
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e(" response", response);
            Log.e("request_id", request_id);
            if (request_id.equals("1")) {
                Log.e(" delete response", response);
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            //   milestone_list.remove(pos);
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                            notifyDataSetChanged();
                        } else {
                            AlertManger.getAlert_instance().showAlert("Something went wrong on server side ", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(" response is null ", "reponse is null");
                }
            }
        }
    };
}
