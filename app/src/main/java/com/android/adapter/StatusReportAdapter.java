package com.android.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.AllFragment.StatusReportFragment;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.StatusReportResponse;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.ImageTrans_CircleTransform;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam tyagi on 7/6/2016.
 */
public class StatusReportAdapter extends RecyclerView.Adapter<StatusReportAdapter.MyViewHolder> {
    private ArrayList<StatusReportResponse.StatusReport> status_list;
    Context context;
    private Bundle b;
    private int pos;
    private String type;
    private String status, id;

    public StatusReportAdapter(ArrayList<StatusReportResponse.StatusReport> status_list, Context context, Bundle b) {
        this.status_list = status_list;
        this.b = b;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final StatusReportResponse.StatusReport status_report = status_list.get(position);
        holder.name.setText(status_report.getFirst_name() + " " + status_report.getLast_name());
        holder.designation_text.setText(status_report.getJob_title());
        Picasso.with(context)
                .load(status_report.getImage())
                .resize(160, 160)
                .into(holder.user_img);
        if (!UserPref.getuser_instance(context).getRoleId().equals("3")) {
            if (status_report.isBackground())
                holder.background.setBackgroundColor(context.getResources().getColor(R.color.green_color));
            if (status_report.isL9())
                holder.collected.setBackgroundColor(context.getResources().getColor(R.color.green_color));
            if (status_report.isReference())
                holder.references.setBackgroundColor(context.getResources().getColor(R.color.green_color));
            if (status_report.isGood_to_start())
                holder.good_to_start.setBackgroundColor(context.getResources().getColor(R.color.green_color));
        } else {
            if (b != null)
            {
                if (b.containsKey("recruiter")) {
                    holder.background.setText("Percentage " + status_list.get(position).getPercentage() + "%");
                    holder.collected.setText("Ranking " + status_list.get(position).getRanking());
                    holder.references.setVisibility(View.GONE);
                    holder.good_to_start.setVisibility(View.GONE);
                } else {
                    holder.background.setText("Submitted");
                    holder.background.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                    holder.collected.setText("Interviewed");
                    holder.references.setText("Hired");
                    if (status_report.isInterviewed())
                        holder.collected.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                    if (status_report.isHired())
                        holder.references.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                    if (status_report.isInterviewed() && status_report.isHired())
                        holder.good_to_start.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                }
            } else {
                Log.e("recruiter case ", "recruiter case");
                if (status_report.isBackground()) {
                    holder.background.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                } else {
                    holder.background.setBackgroundColor(context.getResources().getColor(R.color.greyish));
                }
                if (status_report.isL9()) {
                    holder.collected.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                } else {
                    holder.collected.setBackgroundColor(context.getResources().getColor(R.color.greyish));
                }
                if (status_report.isReference()) {
                    holder.references.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                } else {
                    holder.references.setBackgroundColor(context.getResources().getColor(R.color.greyish));
                }
                if (status_report.isGood_to_start()) {
                    holder.good_to_start.setBackgroundColor(context.getResources().getColor(R.color.green_color));
                } else {
                    holder.good_to_start.setBackgroundColor(context.getResources().getColor(R.color.greyish));
                }
                holder.background.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (status_report.isBackground()) {
                            showPopup("background", status_list.get(position).getId(), position);
                        } else {
                            pos = position;
                            type = "Background";
                            changeStatus("background", status_list.get(position).getId());

                        }

                    }
                });
                holder.collected.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (status_report.isL9()) {
                            showPopup("l9", status_list.get(position).getId(), position);
                        } else {
                            pos = position;
                            type = "Collected";
                            changeStatus("l9", status_list.get(position).getId());
                        }
                    }
                });
                holder.references.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (status_report.isReference()) {
                            showPopup("reference", status_list.get(position).getId(), position);
                        } else {
                            pos = position;
                            type = "References";
                            changeStatus("reference", status_list.get(position).getId());
                        }
                    }
                });
                holder.good_to_start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (status_report.isReference()) {
                            showPopup("good_to_start", status_list.get(position).getId(), position);
                        } else {
                            pos = position;
                            type = "Good To Start";
                            changeStatus("good_to_start", status_list.get(position).getId());
                        }
                    }
                });
            }
        }
    }

    private void showPopup(final String status, final String id, final int position) {
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Staff Trader App")
                    .setMessage("ALERT!  Please confirm you want to change status back to not complete?")
                    .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            type = "notcomplete";

                            changeStatus(status, id);
                        }
                    })
                    .setNegativeButton("Decline", null)
                    .show();
        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Override
    public int getItemCount() {
        return status_list.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.status_report_row, parent, false);
        return new MyViewHolder(itemView);
    }


    private void changeStatus(String background, String id) {
        HashMap<String, String> hmap = new HashMap<>();
        hmap.put("id", id);
        hmap.put("status", background);
        CallService.getInstance().passInfromation(api_listener, Constants.CHANGE_HIRED_STATUS_URL, hmap, true, "1", context);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView user_img;
        public TextView designation_text, name, background, collected, references, good_to_start;

        public MyViewHolder(View view) {
            super(view);
            user_img = (ImageView) view.findViewById(R.id.user_img);
            designation_text = (TextView) view.findViewById(R.id.designation_text);
            name = (TextView) view.findViewById(R.id.name);
            background = (TextView) view.findViewById(R.id.background);
            collected = (TextView) view.findViewById(R.id.collected);
            references = (TextView) view.findViewById(R.id.references);
            good_to_start = (TextView) view.findViewById(R.id.good_to_start);
        }
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e(" response", response);
            if (request_id.equals("1")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            status_list.get(pos).setGood_to_start(json_response.getBoolean("good_to_start"));
                            status_list.get(pos).setBackground(json_response.getBoolean("background"));
                            status_list.get(pos).setL9(json_response.getBoolean("l9"));
                            status_list.get(pos).setReference(json_response.getBoolean("reference"));
                            notifyDataSetChanged();
                            if (type.equalsIgnoreCase("Background"))
                                UtilityPermission.showToast(context, "SUCCESS! Background has been completed");
                            else if (type.equalsIgnoreCase("l9"))
                                UtilityPermission.showToast(context, "SUCCESS! Paperwork has been completed");
                            else if (type.equalsIgnoreCase("References"))
                                UtilityPermission.showToast(context, "SUCCESS! References  have been completed");
                            else if (type.equalsIgnoreCase("Good To Start"))
                                UtilityPermission.showToast(context, "SUCCESS! Candidate is GOOD TO START!");
                            else
                                UtilityPermission.showToast(context, "SUCCESS! This Status has been back to not complete.");
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error on server side.", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    };
}
