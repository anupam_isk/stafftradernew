package com.android.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.AllFragment.EnterJobReqFragment;
import com.android.AllFragment.SubmitCandidateFragment;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.SeekJobResponse;
import com.android.HomeActivity;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Anupam tyagi on 7/5/2016.
 */
public class SeekJobAdapter extends RecyclerView.Adapter<SeekJobAdapter.MyViewHolder> {
    private ArrayList<SeekJobResponse.SeekJob> seek_list;
    Context context;
    String user_id;
    private DatePickerDialog dpd;
    private TimePickerDialog tpd;
    private EditText edit_view;
    int pos;

    public SeekJobAdapter(ArrayList<SeekJobResponse.SeekJob> seek_list, Context context, String user_id) {
        this.seek_list = seek_list;
        this.context = context;
        this.user_id = user_id;
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                date_listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        tpd = TimePickerDialog.newInstance(time_listener, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND), false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.seek_jobs_row, parent, false);
        return new MyViewHolder(itemView);
    }

    TimePickerDialog.OnTimeSetListener time_listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            String time = hourOfDay + ":" + minute + ":" + second;
            edit_view.setText(time);
        }
    };
    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            edit_view.setText(date);
        }
    };

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SeekJobResponse.SeekJob seek_job = seek_list.get(position);
        holder.job_title.setText(seek_job.getJob_title());
        holder.availability.setText(seek_job.getAvailability());
        holder.willing_to_relocate.setText(seek_job.getIs_relocate());
        holder.address.setText(seek_job.getDesired_location());
        if (UserPref.getuser_instance(context).getRoleId().equals("3")) {
            holder.interest_job.setVisibility(View.GONE);
            holder.accept_job.setVisibility(View.GONE);
            holder.submit_candidate.setVisibility(View.VISIBLE);
        } else {
            Log.e("hired status", seek_list.get(position).getHired());
            Log.e("ACCEPTED status", seek_list.get(position).getIs_accepted());
            if (seek_list.get(position).getHired().equals("0") && seek_list.get(position).getIs_accepted().equals("0") && seek_list.get(position).is_intrested()) {
                holder.accept_job.setBackgroundColor(context.getResources().getColor(R.color.transparent_green_color));
                holder.accept_job.setEnabled(false);
                holder.interest_job.setText("Interview Activity");
            } else if (seek_list.get(position).getHired().equals("1") && seek_list.get(position).getIs_accepted().equals("0") && seek_list.get(position).is_intrested()) {
                holder.accept_job.setVisibility(View.VISIBLE);
                holder.accept_job.setBackgroundColor(context.getResources().getColor(android.R.color.white));
                holder.accept_job.setEnabled(true);
                holder.interest_job.setText("Interview Activity");
            } else if (seek_list.get(position).getHired().equals("1") && seek_list.get(position).getIs_accepted().equals("1") && seek_list.get(position).is_intrested()) {
                holder.accept_job.setVisibility(View.VISIBLE);
                holder.accept_job.setText("Accepted");
                holder.accept_job.setBackgroundColor(context.getResources().getColor(android.R.color.white));
                holder.accept_job.setEnabled(false);
                holder.interest_job.setText("Interview Activity");
            } else if (seek_list.get(position).getHired().equals("1") && seek_list.get(position).getIs_accepted().equals("0") && !seek_list.get(position).is_intrested()) {
                holder.accept_job.setVisibility(View.VISIBLE);
                holder.accept_job.setBackgroundColor(context.getResources().getColor(android.R.color.white));
                holder.accept_job.setEnabled(true);
                holder.interest_job.setVisibility(View.GONE);
            } else if (seek_list.get(position).getHired().equals("1") && seek_list.get(position).getIs_accepted().equals("1") && !seek_list.get(position).is_intrested()) {
                holder.accept_job.setText("Accepted");
                holder.interest_job.setVisibility(View.GONE);
                holder.accept_job.setEnabled(false);
            }
            else if (seek_list.get(position).getHired().equals("0") && seek_list.get(position).getIs_accepted().equals("0") && !seek_list.get(position).is_intrested()) {
                holder.accept_job.setBackgroundColor(context.getResources().getColor(R.color.transparent_green_color));
                holder.accept_job.setEnabled(false);
            }
        }
        holder.interest_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                showDialog();
            }
        });
        holder.accept_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                showHireDialog();
            }
        });
        holder.view_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  showPopmenu(view);
                Fragment fragment = new EnterJobReqFragment();
                Bundle b = new Bundle();
                b.putParcelable("seek_job", seek_job);
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
            }
        });
        holder.submit_candidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new SubmitCandidateFragment();
                Bundle b = new Bundle();
                b.putString("job_id", seek_job.getId());
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
            }
        });
        holder.hotebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String type = null;
                if (UserPref.getuser_instance(context).getRoleId().equals("4"))
                    type = "candidate";
                else if (UserPref.getuser_instance(context).getRoleId().equals("3"))
                    type = "recruiter";
                else
                    type = "manager";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("job_id", seek_job.getJob_req_id());
                params.put("type", type);
                CallService.getInstance().passInfromation(api_listener, Constants.ADD_REMOVE_HOTEBOOK_URL, params, true, "6", context);
            }
        });
        Picasso.with(context)
                .load(seek_job.getImage())
                .resize(135, 135)
                .into(holder.seek_image);
    }

    private void showCandidatePopup() {
        UtilityPermission.showToast(context, "Under Development");
        // need to show pop up
    }

    @Override
    public int getItemCount() {
        return seek_list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView seek_image, hotebook;
        public TextView job_title, availability, willing_to_relocate, address;
        public Button view_job, interest_job, accept_job, submit_candidate;

        public MyViewHolder(View view) {
            super(view);
            seek_image = (ImageView) view.findViewById(R.id.seek_image);
            job_title = (TextView) view.findViewById(R.id.job_title);
            availability = (TextView) view.findViewById(R.id.availability);
            willing_to_relocate = (TextView) view.findViewById(R.id.willing_to_relocate);
            address = (TextView) view.findViewById(R.id.address);
            view_job = (Button) view.findViewById(R.id.view_job);
            interest_job = (Button) view.findViewById(R.id.interest_job);
            accept_job = (Button) view.findViewById(R.id.accept_job);
            hotebook = (ImageView) view.findViewById(R.id.hotebook);
            submit_candidate = (Button) view.findViewById(R.id.submit_candidate);
        }
    }

    private void showDialog() {
        final String[] phone = new String[1];
        final String[] mobile = new String[1];
        final String[] send_to = new String[1];
        send_to[0] = "N";
        final String[] outlook = new String[1];
        outlook[0] = "N";
        LayoutInflater factory = LayoutInflater.from(context);
        final View interested_view = factory.inflate(
                R.layout.interested_popup, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(interested_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView cancel = (TextView) interested_view.findViewById(R.id.cancel);
        final TextView done = (TextView) interested_view.findViewById(R.id.done);
        final EditText select_date = (EditText) interested_view.findViewById(R.id.start_date);
        final EditText select_time_one = (EditText) interested_view.findViewById(R.id.select_time_one);
        final EditText select_time_two = (EditText) interested_view.findViewById(R.id.select_time_two);
        final EditText select_time_three = (EditText) interested_view.findViewById(R.id.select_time_three);
        RadioGroup radio_group = (RadioGroup) interested_view.findViewById(R.id.radio_group);
        final CheckBox send_to_outlook = (CheckBox) interested_view.findViewById(R.id.send_to_outlook);
        send_to_outlook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (send_to_outlook.isChecked())
                    outlook[0] = "Y";
                else
                    outlook[0] = "N";
            }
        });
        Button accept_job = (Button) interested_view.findViewById(R.id.accept_job);
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                RadioButton button = (RadioButton) interested_view.findViewById(id);
                Log.e("button text", button.getText().toString());
                if (button.getText().toString().equalsIgnoreCase("phone")) {
                    phone[0] = "Y";
                    mobile[0] = "N";
                } else if (button.getText().toString().equalsIgnoreCase("Mobile Camera")) {
                    phone[0] = "N";
                    mobile[0] = "Y";
                }
            }
        });
        Button decline_job = (Button) interested_view.findViewById(R.id.decline_job);
        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_date;
                dpd.show(((HomeActivity) context).getFragmentManager(), "Datepickerdialog");
            }
        });
        select_time_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_time_one;
                tpd.show(((HomeActivity) context).getFragmentManager(), "Timepickerdialog");
            }
        });
        select_time_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_time_two;
                tpd.show(((HomeActivity) context).getFragmentManager(), "Timepickerdialog");
            }
        });
        select_time_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_time_three;
                tpd.show(((HomeActivity) context).getFragmentManager(), "Timepickerdialog");
            }
        });
        accept_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("dd", phone[0] + " " + mobile[0] + " " + outlook[0] + " " + send_to[0]);
                HashMap<String, String> params = new HashMap<>();
                params.put("candidate_id", user_id);
                params.put("user_id", seek_list.get(pos).getWho_post()); //need to check
                params.put("phone", phone[0]);
                params.put("mobile_camera", mobile[0]);
                params.put("date", select_date.getText().toString());
                params.put("option1", select_time_one.getText().toString());
                params.put("option2", select_time_two.getText().toString());
                params.put("option3", select_time_three.getText().toString());
                params.put("send_outlook", outlook[0]);
                params.put("follow_remainder", send_to[0]);
                //params.put("submit_id", seek_list.get(pos).getSubmit_id());
                CallService.getInstance().passInfromation(api_listener, Constants.INTERESTED_URL, params, true, "4", context);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        decline_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    private void showHireDialog() {
        LayoutInflater factory = LayoutInflater.from(context);
        final View hire_view = factory.inflate(
                R.layout.hire_popup, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(hire_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        EditText candidate_x = (EditText) hire_view.findViewById(R.id.candidate_x);
        final TextView cancel = (TextView) hire_view.findViewById(R.id.cancel);
        final TextView done = (TextView) hire_view.findViewById(R.id.done);
        EditText project_duration = (EditText) hire_view.findViewById(R.id.project_duration);
        EditText start_date = (EditText) hire_view.findViewById(R.id.start_date);
        EditText job_title = (EditText) hire_view.findViewById(R.id.job_title);
        EditText manager_contact_name = (EditText) hire_view.findViewById(R.id.manager_contact_name);
        EditText manager_number = (EditText) hire_view.findViewById(R.id.manager_number);
        EditText work_location = (EditText) hire_view.findViewById(R.id.work_location);
        EditText manager_email = (EditText) hire_view.findViewById(R.id.manager_email);
        final Button agree = (Button) hire_view.findViewById(R.id.agree);
        final Button cancelBT = (Button) hire_view.findViewById(R.id.cancelBT);
//        candidate_x.setText(seek_list.get(pos).getFirst_name() + " " + filtered_list.get(pos).getLast_name());
//        project_duration.setText(filtered_list.get(pos).getDuration_project());
//        job_title.setText(filtered_list.get(pos).getJob_title());
//        manager_contact_name.setText(user_pref.getUserFirstName() + " " + user_pref.getUserLastName());
//        manager_number.setText(user_pref.getUserPhone());
//        work_location.setText(filtered_list.get(pos).getDesired_location());
//        manager_email.setText(user_pref.getUserEmail());
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("submit_id", seek_list.get(pos).getId());
                CallService.getInstance().passInfromation(api_listener, Constants.CANDIDATE_ACCEPTED_JOB_URL, params, true, "5", context);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e(" response", response);
            if (request_id.equals("4")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            seek_list.get(pos).setIs_intrested(true);
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                            notifyDataSetChanged();
                            UtilityPermission.showToast(context, "Successfully submitted to interested");
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error ", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("5")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            seek_list.get(pos).setIs_accepted("1");
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                            notifyDataSetChanged();
                            UtilityPermission.showToast(context, "This candidate has been hired.");
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error ", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("6")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            if (json_response.getString("message").equalsIgnoreCase("Job added successfully"))
                                UtilityPermission.showToast(context, " SUCCESS! This Job has been added to your Saved Jobs.");
                            else
                                UtilityPermission.showToast(context, "Success!  This Job Requisition has been removed from your Saved Jobs. ");
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error ", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    };
}
