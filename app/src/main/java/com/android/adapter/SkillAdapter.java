package com.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.GsonResponse.CategoryResponse;
import com.android.stafftraderapp.R;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi on 7/8/2016.
 */
public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.MyViewHolder> {
    ArrayList<CategoryResponse.Skill> skill_list;
    Context context;
    public ArrayList<String> new_skill_list;
    public ArrayList<String> new_id_list;
    public SkillAdapter(ArrayList<CategoryResponse.Skill> skill_list, Context context) {
        this.skill_list = skill_list;
        this.context = context;
        new_skill_list=new ArrayList<>();
        new_id_list=new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.skill_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,  int position) {
        final int pos = position;
        holder.skill_test.setText(skill_list.get(position).getSkills());
        holder.skill_check.setTag(skill_list.get(position).getSkills());
           /* holder.skill_check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckBox) v).isChecked())
                    {
                     CandidateActivity.showToast(getActivity(),"Checked");
                    }
                    else
                    {
                        CandidateActivity.showToast(getActivity(),"Un Checked");
                    }
                }
            });*/

     /*   holder.skill_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    new_skill_list.add(skill_list.get(pos));
                    CandidateActivity.showToast(context, "Checked");
                } else {
                    new_skill_list.remove(pos);
                    CandidateActivity.showToast(context, "Un Checked");
                }
            }
        });*/
        holder.skill_check.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                String contact = (String) cb.getTag();
                if (cb.isChecked()) {
                    new_skill_list.add(skill_list.get(pos).getSkills());
                    new_id_list.add(skill_list.get(pos).getId());
                } else {
                    new_skill_list.remove(skill_list.get(pos).getSkills());
                    new_id_list.remove(skill_list.get(pos).getId());

                }
               // contact.setSelected(cb.isChecked());
              //  stList.get(pos).setSelected(cb.isChecked());

                /*Toast.makeText(
                        v.getContext(),
                        "Clicked on Checkbox: " + cb.getText() + " is "
                                + cb.isChecked(), Toast.LENGTH_LONG).show();*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return skill_list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView skill_test;
        public CheckBox skill_check;


        public MyViewHolder(View view) {
            super(view);
            skill_test = (TextView) view.findViewById(R.id.skill_test);
            skill_check = (CheckBox) view.findViewById(R.id.skill_check);

        }
    }

}
