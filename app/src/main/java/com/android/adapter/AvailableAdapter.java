package com.android.adapter;

import android.content.Context;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.AllFragment.EditJobRequirementFragment;
import com.android.AllFragment.SubmitJobFragment;
import com.android.AllFragment.ViewQualificationFragment;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.GsonResponse.OpenJobResponse;
import com.android.HomeActivity;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.ImageTrans_CircleTransform;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Anuypam tyag on 7/6/2016.
 */
public class AvailableAdapter extends RecyclerView.Adapter<AvailableAdapter.MyViewHolder> implements Filterable {
    private ArrayList<AllCandidateResponse.Candidate> original_list = null;
    private ArrayList<AllCandidateResponse.Candidate> filtered_list = null;
    private DatePickerDialog dpd;
    private TimePickerDialog tpd;
    private String user_id;
    private EditText edit_view;
    AlertDialog deleteDialog;
    private String email_string = " ";
    private ItemFilter mFilter = new ItemFilter();
    Context context;
    int pos;

    public AvailableAdapter(ArrayList<AllCandidateResponse.Candidate> candidates_list, Context context, String user_id) {
        this.original_list = candidates_list;
        this.filtered_list = candidates_list;
        this.user_id = user_id;
        this.context = context;
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                date_listener,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        tpd = TimePickerDialog.newInstance(time_listener, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND), false);
    }

    TimePickerDialog.OnTimeSetListener time_listener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
            String time = hourOfDay + ":" + minute + ":" + second;
            edit_view.setText(time);
        }
    };
    DatePickerDialog.OnDateSetListener date_listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            edit_view.setText(date);
        }
    };

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final AllCandidateResponse.Candidate candidate = filtered_list.get(position);
        holder.job_title.setText(candidate.getJob_title());
        holder.view_qualificationBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ViewQualificationFragment();
                Bundle b = new Bundle();
                b.putString("bio", candidate.getBrif_bio());
                b.putString("role_id", candidate.getRole_id());
                b.putString("skill", candidate.getSkill());
                b.putString("qualification", candidate.getQualification());
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
            }
        });
        if (candidate.is_hotbook())
            holder.hotebook.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.plus));
        else
            holder.hotebook.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.star_empty));
        holder.hotebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String type = null;
                if (UserPref.getuser_instance(context).getRoleId().equals("4"))
                    type = "candidate";
                else if (UserPref.getuser_instance(context).getRoleId().equals("3"))
                    type = "recruiter";
                else
                    type = "manager";
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("candidate_id", candidate.getId());
                params.put("type", type);
                CallService.getInstance().passInfromation(api_listener, Constants.ADD_REMOVE_HOTEBOOK_URL, params, true, "6", context);
            }
        });
        if (UserPref.getuser_instance(context).getRoleId().equals("3")) {
            holder.submit_job.setVisibility(View.VISIBLE);
            holder.hireBT.setVisibility(View.GONE);
            holder.interestedBT.setVisibility(View.GONE);
        } else {
            Log.e("hired status ", filtered_list.get(position).getIs_hired());
            Log.e("accepted status ", filtered_list.get(position).getIs_accepted());
            Log.e("iNTERSTED status ", String.valueOf(filtered_list.get(position).is_interested()));
            if (filtered_list.get(position).getIs_hired().equals("1") && filtered_list.get(position).getIs_accepted().equals("1") && !filtered_list.get(position).is_interested()) {
                holder.hiredBT.setVisibility(View.VISIBLE);
                holder.hireBT.setVisibility(View.GONE);
                holder.interestedBT.setVisibility(View.GONE);
                holder.interest.setVisibility(View.GONE);
            } else if (filtered_list.get(position).getIs_hired().equals("1") && filtered_list.get(position).getIs_accepted().equals("0") && !filtered_list.get(position).is_interested()) {
                holder.hireBT.setVisibility(View.VISIBLE);
                holder.hireBT.setText("Processing..");
                holder.hireBT.setEnabled(false);
                holder.hiredBT.setVisibility(View.GONE);
                holder.interestedBT.setVisibility(View.GONE);
                holder.interest.setVisibility(View.GONE);
            } else if (filtered_list.get(position).getIs_hired().equals("0") && filtered_list.get(position).getIs_accepted().equals("0") && !filtered_list.get(position).is_interested()) {
                holder.hireBT.setVisibility(View.VISIBLE);
                holder.hiredBT.setVisibility(View.GONE);
                holder.interestedBT.setVisibility(View.VISIBLE);
            } else if (filtered_list.get(position).getIs_hired().equals("0") && filtered_list.get(position).getIs_accepted().equals("0") && filtered_list.get(position).is_interested()) {
                holder.hiredBT.setVisibility(View.GONE);
                holder.hireBT.setVisibility(View.VISIBLE);
                holder.interestedBT.setText("Interview Activity");
            } else if (filtered_list.get(position).getIs_hired().equals("1") && filtered_list.get(position).getIs_accepted().equals("0") && filtered_list.get(position).is_interested()) {
                holder.hireBT.setVisibility(View.VISIBLE);
                holder.hireBT.setText("Processing..");
                holder.hiredBT.setVisibility(View.GONE);
                holder.interestedBT.setText("Interview Activity");
            } else if (filtered_list.get(position).getIs_hired().equals("1") && filtered_list.get(position).getIs_accepted().equals("1") && filtered_list.get(position).is_interested()) {
                holder.hiredBT.setVisibility(View.VISIBLE);
                holder.hireBT.setVisibility(View.GONE);
                holder.interestedBT.setText("Interview Activity");
            }
          /*  if (filtered_list.get(position).is_interested()) {
                holder.interest.setVisibility(View.VISIBLE);
                holder.interestedBT.setVisibility(View.GONE);
            } else {
                holder.interestedBT.setVisibility(View.VISIBLE);
                holder.interest.setVisibility(View.GONE);
            }*/
        }
        holder.submit_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new SubmitJobFragment();
                Bundle b = new Bundle();
                b.putString("candidate_id", candidate.getId());
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
            }
        });
        holder.hireBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                showHireDialog();
            }
        });
        holder.interestedBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                if (holder.interestedBT.getText().toString().equalsIgnoreCase("Interview Activity")) {
                    UtilityPermission.showToast(context, "Under Development");
                } else {
                    showDialog();
                }
            }
        });
        Log.e("candidate image", candidate.getImage());
        Picasso.with(context)
                .load(filtered_list.get(position).getImage())
                .resize(135, 135)
                .transform(new ImageTrans_CircleTransform(context))
                .into(holder.seek_image);
    }

    /*private void showInterestedPopmenu(View view, final int position) {
        MenuBuilder menuBuilder = new MenuBuilder(context);
        MenuInflater inflater = new MenuInflater(context);
        inflater.inflate(R.menu.interested_pop_up_menu, menuBuilder);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(context, menuBuilder, view);
        optionsMenu.setForceShowIcon(true);

// Set Item Click Listener
        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_qualification: // Handle option1 Click
                        return true;
                    case R.id.menu_hired:
                        return true;
                    case R.id.menu_interested:
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {
            }
        });

        optionsMenu.show();
    }*/

    private void showHireDialog() {
        LayoutInflater factory = LayoutInflater.from(context);
        final View hire_view = factory.inflate(
                R.layout.hire_popup, null);
        deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(hire_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView cancel = (TextView) hire_view.findViewById(R.id.cancel);
        final TextView done = (TextView) hire_view.findViewById(R.id.done);
        EditText candidate_x = (EditText) hire_view.findViewById(R.id.candidate_x);
        EditText project_duration = (EditText) hire_view.findViewById(R.id.project_duration);
        EditText start_date = (EditText) hire_view.findViewById(R.id.start_date);
        EditText job_title = (EditText) hire_view.findViewById(R.id.job_title);
        EditText manager_contact_name = (EditText) hire_view.findViewById(R.id.manager_contact_name);
        EditText manager_number = (EditText) hire_view.findViewById(R.id.manager_number);
        EditText work_location = (EditText) hire_view.findViewById(R.id.work_location);
        EditText manager_email = (EditText) hire_view.findViewById(R.id.manager_email);
        final Button agree = (Button) hire_view.findViewById(R.id.agree);
        final Button cancelBT = (Button) hire_view.findViewById(R.id.cancelBT);
        candidate_x.setText(filtered_list.get(pos).getFirst_name() + " " + filtered_list.get(pos).getLast_name());
        project_duration.setText(filtered_list.get(pos).getDuration_project());
        job_title.setText(filtered_list.get(pos).getJob_title());
        manager_contact_name.setText(UserPref.getuser_instance(context).getUserFirstName() + " " + UserPref.getuser_instance(context).getUserLastName());
        manager_number.setText(UserPref.getuser_instance(context).getUserPhone());
        work_location.setText(filtered_list.get(pos).getDesired_location());
        manager_email.setText(UserPref.getuser_instance(context).getUserEmail());
        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("submit_id", filtered_list.get(pos).getSubmit_id());
                CallService.getInstance().passInfromation(api_listener, Constants.HIRED_URL, params, true, "5", context);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }

    @Override
    public int getItemCount() {
        return filtered_list.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.available_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView seek_image;
        public TextView job_title, availability, willing_to_relocate, address;
        public Button view_qualificationBT, hireBT, interestedBT, hiredBT, interest, submit_job;
        public ImageView hotebook;

        public MyViewHolder(View view) {
            super(view);
            seek_image = (ImageView) view.findViewById(R.id.seek_image);
            job_title = (TextView) view.findViewById(R.id.job_title);
            availability = (TextView) view.findViewById(R.id.availability);
            willing_to_relocate = (TextView) view.findViewById(R.id.willing_to_relocate);
            address = (TextView) view.findViewById(R.id.address);
            view_qualificationBT = (Button) view.findViewById(R.id.view_qualificationBT);
            hireBT = (Button) view.findViewById(R.id.hireBT);
            hiredBT = (Button) view.findViewById(R.id.hiredBT);
            interestedBT = (Button) view.findViewById(R.id.interestedBT);
            interest = (Button) view.findViewById(R.id.interest);
            hotebook = (ImageView) view.findViewById(R.id.hotebook);
            submit_job = (Button) view.findViewById(R.id.submit_job);
        }
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<AllCandidateResponse.Candidate> list = original_list;

            int count = list.size();
            final ArrayList<AllCandidateResponse.Candidate> nlist = new ArrayList<AllCandidateResponse.Candidate>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getJob_title();
                if (filterableString.toLowerCase().contains(filterString)) {
                    AllCandidateResponse.Candidate candidate = list.get(i);
                    nlist.add(candidate);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_list = (ArrayList<AllCandidateResponse.Candidate>) results.values;
            notifyDataSetChanged();
        }
    }

    private void showDialog() {
        final String[] phone = new String[1];
        final String[] mobile = new String[1];
        final String[] send_to = new String[1];
        send_to[0] = "N";
        final String[] outlook = new String[1];
        outlook[0] = "N";
        LayoutInflater factory = LayoutInflater.from(context);
        final View interested_view = factory.inflate(
                R.layout.interested_popup, null);
        deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(interested_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView cancel = (TextView) interested_view.findViewById(R.id.cancel);
        final TextView done = (TextView) interested_view.findViewById(R.id.done);
        final EditText select_date = (EditText) interested_view.findViewById(R.id.start_date);
        final EditText select_time_one = (EditText) interested_view.findViewById(R.id.select_time_one);
        final EditText select_time_two = (EditText) interested_view.findViewById(R.id.select_time_two);
        final EditText select_time_three = (EditText) interested_view.findViewById(R.id.select_time_three);
        RadioGroup radio_group = (RadioGroup) interested_view.findViewById(R.id.radio_group);
        // final CheckBox send_to_colleague = (CheckBox) interested_view.findViewById(R.id.send_to_colleague);
       /* send_to_colleague.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (send_to_colleague.isChecked()) {
                    showEmailPopup();
                    send_to[0] = "Y";
                } else
                    send_to[0] = "N";
            }


        });
*/
        final CheckBox send_to_outlook = (CheckBox) interested_view.findViewById(R.id.send_to_outlook);
        send_to_outlook.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (send_to_outlook.isChecked())
                    outlook[0] = "Y";
                else
                    outlook[0] = "N";
            }
        });
        Button accept_job = (Button) interested_view.findViewById(R.id.accept_job);
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                RadioButton button = (RadioButton) interested_view.findViewById(id);
                Log.e("button text", button.getText().toString());
                if (button.getText().toString().equalsIgnoreCase("phone")) {
                    phone[0] = "Y";
                    mobile[0] = "N";
                } else if (button.getText().toString().equalsIgnoreCase("Mobile Camera")) {
                    phone[0] = "N";
                    mobile[0] = "Y";
                }
            }
        });
        Button decline_job = (Button) interested_view.findViewById(R.id.decline_job);
        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_date;
                dpd.show(((HomeActivity) context).getFragmentManager(), "Datepickerdialog");
            }
        });
        select_time_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_time_one;
                tpd.show(((HomeActivity) context).getFragmentManager(), "Timepickerdialog");
            }
        });
        select_time_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_time_two;
                tpd.show(((HomeActivity) context).getFragmentManager(), "Timepickerdialog");
            }
        });
        select_time_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_view = select_time_three;
                tpd.show(((HomeActivity) context).getFragmentManager(), "Timepickerdialog");
            }
        });
        accept_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("candidate_id", filtered_list.get(pos).getId());
                params.put("phone", phone[0]);
                params.put("mobile_camera", mobile[0]);
                params.put("date", select_date.getText().toString());
                params.put("option1", select_time_one.getText().toString());
                params.put("option2", select_time_two.getText().toString());
                params.put("option3", select_time_three.getText().toString());
                params.put("send_outlook", outlook[0]);
                params.put("follow_remainder", send_to[0]);
                params.put("email", email_string);
                params.put("submit_id", filtered_list.get(pos).getSubmit_id());
                params.put("job_id",filtered_list.get(pos).getJob_id());
                CallService.getInstance().passInfromation(api_listener, Constants.INTERESTED_URL, params, true, "4", context);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }


    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e(" response", response);
            if (request_id.equals("4")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            filtered_list.get(pos).setIs_interested(true);
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                            notifyDataSetChanged();
                            deleteDialog.dismiss();
                            UtilityPermission.showToast(context, "Successfully submitted to interested");

                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error ", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("5")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            filtered_list.get(pos).setIs_hired("1");
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                            notifyDataSetChanged();
                            deleteDialog.dismiss();
                            UtilityPermission.showToast(context, "This Request has been send to candidate.");
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error on server side.", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (request_id.equals("6")) {
                if (response != null) {
                    try {
                        JSONObject json_response = new JSONObject(response);
                        if (json_response.getBoolean("status")) {
                            if (json_response.getString("message").equalsIgnoreCase("candidate added successfully"))
                                UtilityPermission.showToast(context, " SUCCESS! Candidate has been added to your Saved Profiles.");
                            else
                                UtilityPermission.showToast(context, " ALERT! Candidate has been removed from your Saved Profiles! .");
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error ", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    };

    private void showEmailPopup() {
        LayoutInflater factory = LayoutInflater.from(context);
        final View deleteDialogView = factory.inflate(
                R.layout.add_email, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageButton image_button = (ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        final EditText edit_email = (EditText) deleteDialogView.findViewById(R.id.email);
        Button save = (Button) deleteDialogView.findViewById(R.id.save);
        image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email_string = edit_email.getText().toString();
                if (email_string != null)
                    deleteDialog.dismiss();
                else
                    UtilityPermission.showToast(context, "Please enter email.");
            }
        });
        deleteDialog.show();


    }


}


