package com.android.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.android.AllFragment.EditJobRequirementFragment;
import com.android.GsonResponse.OpenJobResponse;
import com.android.HomeActivity;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.ViewProfileActivity;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

import static com.android.application.ApplicationContextProvider.getContext;

/**
 * Created by Anupam tyagi on 7/7/2016.
 */
public class OpenJobAdapter extends RecyclerView.Adapter<OpenJobAdapter.MyViewHolder> {
    private ArrayList<OpenJobResponse.OpenJob> open_job_list;
    Context context;
    private String user_id;
    private String closed;
    int pos;
    public OpenJobAdapter(ArrayList<OpenJobResponse.OpenJob> open_job_list, Context context, String user_id,String closed) {
        this.open_job_list = open_job_list;
        this.context = context;
        this.closed=closed;
        this.user_id = user_id;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.open_job_row, parent, false);
         return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final OpenJobResponse.OpenJob open_job = open_job_list.get(position);
        holder.designation_text.setText(open_job.getJob_title());
        holder.ending_date.setText("Date : "+ open_job.getStart_date());
       /* if(closed!=null)
        {
            if (open_job.getEnabled().equals("N")) {
                holder.closeBT.setText("Closed");
                holder.openBT.setText("Re-Open");
                holder.openBT.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_blue_button));
                holder.openBT.setEnabled(true);
                holder.closeBT.setEnabled(false);
            } else {
                holder.openBT.setText("Opened");
                holder.openBT.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_blue_button));
                holder.closeBT.setText("Close");
                holder.openBT.setEnabled(false);
                holder.closeBT.setEnabled(true);
            }
        }
        else {
            if (open_job.getEnabled().equals("N")) {
                holder.closeBT.setText("Closed");
                holder.openBT.setText("Open");
                holder.openBT.setEnabled(true);
                holder.closeBT.setEnabled(false);
            } else {
                holder.openBT.setText("Opened");
                holder.openBT.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rounded_blue_button));
                holder.closeBT.setText("Close");
                holder.openBT.setEnabled(false);
                holder.closeBT.setEnabled(true);
            }
        }*/
        holder.designation_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(context, ViewProfileActivity.class);
                Bundle b=new Bundle();
                b.putString("open_job","open_job");
                b.putString("job_title",open_job.getJob_title());
                b.putString("brief",open_job.getDescription());
                b.putString("skill",open_job.getSkill_set());
                b.putString("duration",open_job.getDuration_project());
                b.putString("budget",open_job.getBudget());
                b.putString("desired_location",open_job.getDesired_location());
                i.putExtra("bundle",b);
                context.startActivity(i);
            }
        });
        holder.open_pop_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              showPopmenu(v,position,open_job);
            }
        });
      /*  holder.editBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new EditJobRequirementFragment();
                Bundle b = new Bundle();
                b.putParcelable("open_job", open_job);
                fragment.setArguments(b);
                UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
            }
        });
        holder.openBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                HashMap<String, String> params = new HashMap<>();
                params.put("job_id", open_job_list.get(pos).getId());
                params.put("user_id", user_id);
                CallService.getInstance().passInfromation(listener, Constants.CHANGE_JOB_STATUS_URL, params, true, "4", context);
            }
        });
        holder.closeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;
                showClosePopup();

            }
        });
        holder.deleteBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                HashMap<String, String> params = new HashMap<>();
                params.put("job_id", open_job_list.get(pos).getId());
                params.put("user_id", user_id);
                Log.e("user_id", open_job_list.get(pos).getId());
                CallService.getInstance().passInfromation(listener, Constants.DELETE_JOB, params, true, "3", context);
            }
        });
*/
    }
    private void showPopmenu(View view, final int position,final OpenJobResponse.OpenJob open_job) {
        MenuBuilder menuBuilder =new MenuBuilder(context);
        MenuInflater inflater = new MenuInflater(context);
        inflater.inflate(R.menu.popupmenu, menuBuilder);
        if (open_job.getEnabled().equals("Y"))
            menuBuilder.findItem(R.id.menu_open).setVisible(false);
        else
            menuBuilder.findItem(R.id.menu_close).setVisible(false);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(context, menuBuilder, view);
        optionsMenu.setForceShowIcon(true);

// Set Item Click Listener
        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_edit: // Handle option1 Click
                        Log.e("edit 1`", "Position "+ position);
                        Fragment fragment = new EditJobRequirementFragment();
                        Bundle b = new Bundle();
                        b.putParcelable("open_job", open_job);
                        fragment.setArguments(b);
                        UtilityPermission.replaceFragment(((HomeActivity) context).getSupportFragmentManager(), fragment);
                        return true;
                    case R.id.menu_delete:
                        pos = position;
                        HashMap<String, String> params = new HashMap<>();
                        params.put("job_id", open_job_list.get(pos).getId());
                        params.put("user_id", user_id);
                        Log.e("user_id", open_job_list.get(pos).getId());
                        CallService.getInstance().passInfromation(listener, Constants.DELETE_JOB, params, true, "3", context);// Handle option2 Click
                        return true;
                    case R.id.menu_close:
                        pos = position;
                        showClosePopup();
                    case R.id.menu_open:
                        pos = position;
                        openJob();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {}
        });

        optionsMenu.show();
    }
    private void openJob() {
        HashMap<String, String> params = new HashMap<>();
        params.put("job_id", open_job_list.get(pos).getId());
        params.put("user_id", user_id);
        CallService.getInstance().passInfromation(listener, Constants.CHANGE_JOB_STATUS_URL, params, true, "4", context);// Handle option2 Click
    }

    private void showClosePopup() {
            try {
           new AlertDialog.Builder(context)
                        .setTitle("Staff Trader App")
                        .setMessage("Are you sure you want to Close the Position ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                              showRadioButtonPopup();
                            }
                        })
                        .setNegativeButton("No",null)
                        .show();
            } catch (Exception e) {
                e.getMessage();
            }

    }

    private void showRadioButtonPopup() {
        LayoutInflater factory = LayoutInflater.from(context);
        final View deleteDialogView = factory.inflate(
                R.layout.show_another_radio, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(deleteDialogView);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageButton image_button=(ImageButton) deleteDialogView.findViewById(R.id.cross_button);
        final RadioGroup rgOpinion=(RadioGroup) deleteDialogView.findViewById(R.id.rgOpinion);
        Button save=(Button) deleteDialogView.findViewById(R.id.save);
        image_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioButton selectRadio = (RadioButton) deleteDialogView.findViewById(rgOpinion
                        .getCheckedRadioButtonId());
               final  String opinion = selectRadio.getText().toString();
                sendToServer(opinion);
                deleteDialog.dismiss();
            }

            private void sendToServer(String opinion) {
                HashMap<String, String> params = new HashMap<>();
                params.put("job_id", open_job_list.get(pos).getId());
                params.put("user_id", user_id);
                params.put("status",opinion);
                Log.e("user_id", open_job_list.get(pos).getId());
                CallService.getInstance().passInfromation(listener, Constants.CHANGE_JOB_STATUS_URL, params, true, "4", context);
            }
        });
        deleteDialog.show();
    }

    @Override
    public int getItemCount() {
        return open_job_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView designation_text, ending_date;
        public LinearLayout open_pop_menu;

      //  public Button openBT, editBT, deleteBT,closeBT;

        public MyViewHolder(View view) {
            super(view);
            designation_text = (TextView) view.findViewById(R.id.designation_text);
            ending_date = (TextView) view.findViewById(R.id.ending_date);
            open_pop_menu=(LinearLayout) view.findViewById(R.id.open_pop_menu);
      /*      openBT = (Button) view.findViewById(R.id.openBT);
            closeBT = (Button) view.findViewById(R.id.closeBT);
            editBT = (Button) view.findViewById(R.id.editBT);
            deleteBT = (Button) view.findViewById(R.id.deleteBT);*/
        }
    }
    ApiResponseListener listener = new ApiResponseListener() {
        @Override
        public void getResponse(String reponse, String request_id) {
            Log.e(" response", reponse);
            Log.e("request_id", request_id);
            if (request_id.equals("3")) {
                Log.e(" delete response", reponse);
                if (reponse != null) {
                    try {
                        JSONObject json_response = new JSONObject(reponse);
                        if (json_response.getBoolean("status")) {
                            open_job_list.remove(pos);
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                            notifyDataSetChanged();
                        } else {
                            AlertManger.getAlert_instance().showAlert("There is some error ", context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(" response is null ", "reponse is null");
                }
            }
            if (request_id.equals("4"))
            {
                if (reponse != null) {
                    try {
                        JSONObject json_response = new JSONObject(reponse);
                        if (json_response.getBoolean("status")) {
                          //  open_job_list.get(pos).setEnabled(json_response.getString("job_status"));
                            open_job_list.remove(pos);
                         /*   notifyItemRemoved(pos);
                            notifyItemRangeChanged(pos, open_job_list.size());*/
                            notifyDataSetChanged();
                          //  getDataFromServer();
                        } else {
                            open_job_list.get(pos).setEnabled("N");
                            notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(" response is null ", "reponse is null");
                }
            }
            if(request_id.equals("5"))
            {
                if (reponse != null)
                {
                    OpenJobResponse open_job_response = new Gson().fromJson(reponse, OpenJobResponse.class);
                    if (open_job_response.isStatus()) {
                        open_job_list.clear();
                        open_job_list.addAll(open_job_response.getData());
                        notifyDataSetChanged();
                    } else {
                        AlertManger.getAlert_instance().showAlert("There is some error 0",context);
                    }
                }
            }
        }
    };

    private void getDataFromServer() {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(context).getUserId());
        CallService.getInstance().passInfromation(listener, Constants.OPEN_JOB_REQUIREMENT, params, true, "5", context);
    }
}
