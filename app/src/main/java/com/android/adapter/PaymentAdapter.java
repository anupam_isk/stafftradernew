package com.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.stafftraderapp.R;
import com.google.android.gms.vision.text.Text;
import com.google.gson.internal.bind.ArrayTypeAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 12/6/2016.
 */

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MyViewHolder> {
    private ArrayList<HashMap<String, String>> hash_list;
    private Context context;
    @Override
    public int getItemCount() {
        return hash_list.size();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_layout, parent, false);
        return new MyViewHolder(itemView);
    }
    public PaymentAdapter(ArrayList<HashMap<String, String>> hash_list, Context context) {
        this.hash_list = hash_list;
        this.context = context;
    }
    @Override
    public void onBindViewHolder(PaymentAdapter.MyViewHolder holder, int position) {
        holder.candidate_name.setText(": " + hash_list.get(position).get("first_name"));
        holder.fee.setText(": $" + hash_list.get(position).get("rate"));
        holder.total.setText(": $" + hash_list.get(position).get("payment"));
        holder.hour.setText(": " + hash_list.get(position).get("hour"));
        if (hash_list.get(position).get("payment_status").equals("Paid")) {
            holder.status.setText(": " + hash_list.get(position).get("payment_status") + " date - " + hash_list.get(position).get("payment_date"));
            holder.status.setBackgroundColor(context.getResources().getColor(R.color.green_color));
        } else if (hash_list.get(position).get("payment_status").equals("Overdue")) {
            holder.status.setText(": " + hash_list.get(position).get("payment_status"));
            holder.status.setBackgroundColor(context.getResources().getColor(R.color.redcolor));
        } else {
            holder.status.setText(": " + hash_list.get(position).get("payment_status"));
            holder.status.setBackgroundColor(context.getResources().getColor(R.color.yellowcolor));
        }
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView candidate_name, date, fee, total, status, hour;

        public MyViewHolder(View view) {
            super(view);
            candidate_name = (TextView) view.findViewById(R.id.name);
            fee = (TextView) view.findViewById(R.id.fee);
            hour = (TextView) view.findViewById(R.id.hour);
            total = (TextView) view.findViewById(R.id.total);
            status = (TextView) view.findViewById(R.id.status);
        }
    }
}
