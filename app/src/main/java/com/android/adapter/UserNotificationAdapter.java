package com.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.ChatActivity;
import com.android.FragmentInterface.AnotherListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.ServerCommunication.CallService;
import com.android.SinchCommunication.CallScreenActivity;
import com.android.SinchCommunication.SinchService;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.ImageTrans_CircleTransform;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;
import com.sinch.android.rtc.calling.Call;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam on 7/19/2016.
 */
public class UserNotificationAdapter extends RecyclerView.Adapter<UserNotificationAdapter.MyViewHolder> {
    private AnotherListener another_listener;
    private Context context;
    private ItemFilter mFilter = new ItemFilter();
    private ArrayList<AllCandidateResponse.Candidate> original_list = null;
    private ArrayList<AllCandidateResponse.Candidate> filtered_list = null;
    private SinchService.SinchServiceInterface mSinchServiceInterface;
    private AlertDialog deleteDialog;
    float rating_one = 0.0f, rating_two = 0.0f, rating_three = 0.0f, rating_four = 0.0f, rating_five = 0.0f, rating_six = 0.0f, rating_seven = 0.0f, rating_eight = 0.0f;
    public UserNotificationAdapter(ArrayList<AllCandidateResponse.Candidate> candidates_list, Context context, AnotherListener another_listener,SinchService.SinchServiceInterface mSinchServiceInterface) {
        this.another_listener = another_listener;
        this.original_list = candidates_list;
        this.filtered_list = candidates_list;
        this.mSinchServiceInterface=mSinchServiceInterface;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row, parent, false);

        return new MyViewHolder(itemView);
    }
    public Filter getFilter() {
        return mFilter;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {
          if(mSinchServiceInterface!=null)
              holder.left_relative.setVisibility(View.VISIBLE);
        holder.name.setText(filtered_list.get(position).getFirst_name()+ "  "+ filtered_list.get(position).getLast_name());
        holder.chat_calling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ChatActivity.class);
                i.putExtra("other_id",filtered_list.get(position).getId());
                i.putExtra("user_name", filtered_list.get(position).getFirst_name() + " " + filtered_list.get(position).getLast_name());
                i.putExtra("user_image",filtered_list.get(position).getImage());
                context.startActivity(i);
            }
        });
        holder.video_calling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilityPermission.showToast(context,"Under Development");
               /* Call call = mSinchServiceInterface.callUserVideo(filtered_list.get(position).getUsername());
                String callId = call.getCallId();
                Log.e("call id",callId + call.getRemoteUserId());
                Intent callScreen = new Intent(context, CallScreenActivity.class);
                callScreen.putExtra(SinchService.CALL_ID, callId);
                callScreen.putExtra("call_type","video");
                context.startActivity(callScreen);*/
            }
        });
        holder.audio_calling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilityPermission.showToast(context,"Under Development");
               /* Call call = mSinchServiceInterface.callUser(filtered_list.get(position).getUsername());
                String callId = call.getCallId();
                Log.e("call id",callId + call.getRemoteUserId());
                Intent callScreen = new Intent(context, CallScreenActivity.class);
                callScreen.putExtra(SinchService.CALL_ID, callId);
                callScreen.putExtra("call_type","audio");
                context.startActivity(callScreen);*/
            }
        });
        Picasso.with(context)
                .load(filtered_list.get(position).getImage())
                .resize(135, 135)
                .transform(new ImageTrans_CircleTransform(context))
                .into(holder.user_img);
        if(filtered_list.get(position).getRole_id()!=null) {
            if (filtered_list.get(position).getRole_id().equals("3"))
                holder.artist.setText("Recruiter");
            else if (filtered_list.get(position).getRole_id().equals("4"))
                holder.artist.setText(filtered_list.get(position).getJob_title());
            else
                holder.artist.setText("Manager");
        }
         holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("on click", "on click");
                goItem(position);

            }
        });
        holder.ratingBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }
    private void goItem(int position) {
        another_listener.onClickItem(position);
    }
    @Override
    public int getItemCount() {
        return filtered_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView user_img;
        public ImageButton video_calling,audio_calling,chat_calling;
        public TableRow left_relative;
        public Button ratingBT;
        public TextView artist, name;

        public MyViewHolder(View view) {
            super(view);
            user_img = (ImageView) view.findViewById(R.id.user_img);
            artist=(TextView) view.findViewById(R.id.artist);
            name=(TextView) view.findViewById(R.id.name);
            left_relative=(TableRow) view.findViewById(R.id.left_relative);
            video_calling=(ImageButton) view.findViewById(R.id.video_calling);
            audio_calling=(ImageButton) view.findViewById(R.id.audio_calling);
            chat_calling=(ImageButton) view.findViewById(R.id.chat_calling);
            ratingBT=(Button) view.findViewById(R.id.ratingBT);
        }
    }
    private void showDialog() {
        LayoutInflater factory = LayoutInflater.from(context);
        final View rating_view = factory.inflate(R.layout.rating_view, null);
        deleteDialog = new AlertDialog.Builder(context).create();
        deleteDialog.setView(rating_view);
        deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final TextView cancel=(TextView) rating_view.findViewById(R.id.cancel);
        final RatingBar rating1 = (RatingBar) rating_view.findViewById(R.id.rating1);
        final RatingBar rating2 = (RatingBar) rating_view.findViewById(R.id.rating2);
        final RatingBar rating3 = (RatingBar) rating_view.findViewById(R.id.rating3);
        final RatingBar rating4 = (RatingBar) rating_view.findViewById(R.id.rating4);
        final RatingBar rating5 = (RatingBar) rating_view.findViewById(R.id.rating5);
        final RatingBar rating6 = (RatingBar) rating_view.findViewById(R.id.rating6);
        final RatingBar rating7 = (RatingBar) rating_view.findViewById(R.id.rating7);
        final RatingBar rating8 = (RatingBar) rating_view.findViewById(R.id.rating8);
        final EditText comment = (EditText) rating_view.findViewById(R.id.comment);
        final Button button = (Button) rating_view.findViewById(R.id.submit_event);
        RatingBar.OnRatingBarChangeListener rating_listener = new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                if (ratingBar == rating1) {
                    rating_one = rating;
                    Log.e("rating 1", String.valueOf(rating));
                } else if (ratingBar == rating2) {
                    rating_two = rating;
                    Log.e("rating 2", String.valueOf(rating));
                } else if (ratingBar == rating3) {
                    rating_three = rating;
                    Log.e("rating 3", String.valueOf(rating));
                } else if (ratingBar == rating4) {
                    rating_four = rating;
                    Log.e("rating 4", String.valueOf(rating));
                } else if (ratingBar == rating5) {
                    rating_five = rating;
                    Log.e("rating 5", String.valueOf(rating));
                } else if (ratingBar == rating6) {
                    rating_six = rating;
                    Log.e("rating 6", String.valueOf(rating));
                } else if (ratingBar == rating7) {
                    rating_seven = rating;
                    Log.e("rating 7", String.valueOf(rating));
                } else if (ratingBar == rating8) {
                    rating_eight = rating;
                    Log.e("rating 8", String.valueOf(rating));
                }
            }
        };
        rating1.setOnRatingBarChangeListener(rating_listener);
        rating2.setOnRatingBarChangeListener(rating_listener);
        rating3.setOnRatingBarChangeListener(rating_listener);
        rating4.setOnRatingBarChangeListener(rating_listener);
        rating5.setOnRatingBarChangeListener(rating_listener);
        rating6.setOnRatingBarChangeListener(rating_listener);
        rating7.setOnRatingBarChangeListener(rating_listener);
        rating8.setOnRatingBarChangeListener(rating_listener);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("punctuality", String.valueOf(rating_one));
                params.put("professional", String.valueOf(rating_two));
                params.put("personality", String.valueOf(rating_three));
                params.put("potential", String.valueOf(rating_four));
                params.put("competency", String.valueOf(rating_five));
                params.put("confidence", String.valueOf(rating_six));
                params.put("communication", String.valueOf(rating_seven));
                params.put("capable", String.valueOf(rating_eight));
                params.put("comment", comment.getText().toString());
                params.put("type", "Assignment");
                params.put("manager_id", UserPref.getuser_instance(context).getUserId());

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog.dismiss();
            }
        });
        deleteDialog.show();
    }
    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<AllCandidateResponse.Candidate> list = original_list;

            int count = list.size();
            final ArrayList<AllCandidateResponse.Candidate> nlist = new ArrayList<AllCandidateResponse.Candidate>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getJob_title();
                if (filterableString.toLowerCase().contains(filterString)) {
                    AllCandidateResponse.Candidate candidate = list.get(i);
                    nlist.add(candidate);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_list = (ArrayList<AllCandidateResponse.Candidate>) results.values;
            notifyDataSetChanged();
        }
    }
}
