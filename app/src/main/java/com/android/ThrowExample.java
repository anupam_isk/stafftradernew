package com.android;

import java.io.IOException;

/**
 * Created by admin on 3/15/2017.
 */

public class ThrowExample {
    void mymethod(int num) throws IOException, ClassNotFoundException {
        if (num == 1)
            throw new IOException("Exception Message1");
        else
            throw new ClassNotFoundException("Exception Message2");
    }

    public static int getANumber() {
        try {
            throw new NoSuchFieldException();
        } finally {
            return 43;
        }
    }
}

class Demo {
    public static void main(String args[]) {
        System.out.println("Return value " + ThrowExample.getANumber());
        try {
            ThrowExample obj = new ThrowExample();
            obj.mymethod(1);

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
