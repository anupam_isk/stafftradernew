package com.android.FragmentInterface;

/**
 * Created by Anupam tyagi on 7/4/2016.
 */
public interface onFragmentListener {
    public void showDrawerToggle(boolean showDrawerToggle);
    public void otherCheck(boolean value);
}
