package com.android.FragmentInterface;

/**
 * Created by Anupam tyagi on 7/21/2016.
 */
public interface OnItemClickListener {
    void onItemClick(int position);
}
