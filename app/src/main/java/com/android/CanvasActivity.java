package com.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.android.AllFragment.PaperWorkFragment;
import com.android.AllFragment.ShowImageFragment;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.helper.Constants;
import com.android.helper.DrawView;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class CanvasActivity extends AppCompatActivity {
    public DrawView draw_view;
    public ScrollView scroll_view;
    public HorizontalScrollView h_scroll_view;
    public LinearLayout lin_layout;
    private Button button;
    private RelativeLayout relative_layout;
    public Canvas this_canvas;
    String paperwork_id, json_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DrawView main_draw_view;
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent != null) {
            paperwork_id = intent.getStringExtra("id");
            json_string=intent.getStringExtra("json_string");
        }
        Log.e("paper_work id ", paperwork_id);
        setContentView(R.layout.activity_canvas);
        button = (Button) findViewById(R.id.button);
        draw_view = (DrawView) findViewById(R.id.draw_view);
        relative_layout = (RelativeLayout) findViewById(R.id.scroll_view);
        //  relative_layout.addView(draw_view, LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
        draw_view.setField(this,json_string);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draw_view.save(relative_layout, 792, 612);
                File f = draw_view.getFileObject();
                Log.e("path", f.getAbsolutePath());
                HashMap hmap1 = getHashMapObject();
                hmap1.put("paperwork_id", paperwork_id);
                hmap1.put("user_id", UserPref.getuser_instance(CanvasActivity.this).getUserId());
                hmap1.put("role_id", UserPref.getuser_instance(CanvasActivity.this).getRoleId());
                CallService.getInstance().passDataToMultiPart(api_listener, Constants.UPLOAD_PAPERWORK_URL, hmap1, true, f, "1", CanvasActivity.this, "img");
            }
        });
      /*  requestWindowFeature(Window.FEATURE_NO_TITLE);

        lin_layout = new LinearLayout(this);
        scroll_view = new ScrollView(this);
        h_scroll_view = new HorizontalScrollView(this);

        scroll_view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        h_scroll_view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        lin_layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        lin_layout.setOrientation(LinearLayout.VERTICAL);
        Button btnTag = new Button(this);
        btnTag.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        btnTag.setText("Button");

        //add button to the layout

        btnTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
*//*
        Bitmap mBitmap = Bitmap.createBitmap(630, 870, Bitmap.Config.ARGB_8888);
        this_canvas = new Canvas(mBitmap);*//*

        main_draw_view = new DrawView(this);
        main_draw_view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        scroll_view.addView(main_draw_view);
        h_scroll_view.addView(scroll_view);
        setContentView(h_scroll_view);*/
     /*   setContentView(new MyView(this));
        Intent intent = getIntent();*/
      /*  if(intent!=null)
        bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");*/

        //    Log.e("new bitmap ","height " + bitmap.getHeight() + "width "+ bitmap.getWidth());
    }

    private HashMap getHashMapObject() {
        HashMap<String, String> hmap = new HashMap<>();
        return hmap;
    }

      ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            try {
                Log.e("response",response);
                JSONObject image_object = new JSONObject(response);
                 if(image_object.getBoolean("status"))
                 {
                     UtilityPermission.showToast(CanvasActivity.this,"This Documents has been uploaded successfully.");
                     finish();
                 }
                else
                 {
                     UtilityPermission.showToast(CanvasActivity.this,"There is getting some error on server side.");
                 }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /*public class MyView extends View
    {
        Paint paint=new Paint();

        public MyView(Context context)
        {
            super(context);

        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            super.onDraw(canvas);
            canvas.drawBitmap(ShowImageFragment.bitma,0,0,paint);
            canvas.drawBitmap(PaperWorkFragment.bit,425,680,paint);

        }
    }*/

}
