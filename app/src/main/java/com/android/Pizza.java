package com.android;

/**
 * Created by admin on 3/9/2017.
 */

class Pizza {

    public void eat() {
        System.out.println("pizza");
    }
}

class Food {
    int a = 6;

    public void mi( final int b) {
        final int c=6;
    /* There is no semicolon(;)
     * semicolon is present at the curly braces of the method end.
     */
        Pizza p = new Pizza() {
            public void eat() {
                System.out.println("anonymous pizza" + a + " b variable  " + b + "c " + c);
            }
        };
    }
}