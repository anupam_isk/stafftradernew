package com.android;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.FragmentInterface.ApiResponseListener;
import com.android.ServerCommunication.CallService;
import com.android.adapter.ChatAdapter;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class ChatActivity extends AppCompatActivity {
    private EditText send_message;
    private ImageButton send_button;
    private String connection_id = null;
    private static RelativeLayout upper_layout;
    private int limit = 15;
    private int offset = -15;
    String compare_date = null;
    private LinearLayoutManager mLayoutManager;
    private boolean check;
    private TextView date_text;
    private RecyclerView recycler_view;
    private ChatAdapter adapter;
    private WebSocketClient web_socket;
    private String other_id, user_name;
    private boolean other_check = true;
    private ArrayList<HashMap<String, String>> map_list;
    // Variables for scroll listener
    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    URI uri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Intent i = getIntent();
        if (i != null) {
            other_id = i.getStringExtra("other_id");
            user_name = i.getStringExtra("user_name");
        }
        offset = offset + 15;
        getChatFromServer("1", offset, true);
        // db.getMessage(10,0,UserPref.getuser_instance(this).getUserId(),other_id);
        map_list = new ArrayList<>();
        initiliazeUI();
        intitializeToolBar();
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        recycler_view.setHasFixedSize(true);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                // Here get the child count, item count and visibleitems
                // from layout manager
                if (dy < 0)
                {
                    Log.e("Scrolled Upwards", "Scrolled Upwards");
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager
                            .findFirstVisibleItemPosition();
                    int position = mLayoutManager.findLastCompletelyVisibleItemPosition();
                    if(position==map_list.size()-1)
                    {
                        date_text.setVisibility(View.GONE);
                    }
                    else
                    {
                        date_text.setVisibility(View.VISIBLE);
                        if (map_list.get(position).containsKey("time"))
                        date_text.setText(map_list.get(position).get("time").substring(0, 12));
                    }
                    // Now check if userScrolled is true and also check if
                    // the item is end then update recycler view and set
                    // userScrolled to false
                    if (userScrolled
                            && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                        userScrolled = false;
                        updateRecyclerView();

                    }
                }
                else
                {
                    int position = mLayoutManager.findLastCompletelyVisibleItemPosition();
                    if(position==map_list.size()-1)
                    {
                        date_text.setVisibility(View.GONE);
                    }
                    else {
                        date_text.setVisibility(View.VISIBLE);
                        if (map_list.get(position).containsKey("time"))
                        date_text.setText(map_list.get(position).get("time").substring(0, 12));
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                // If scroll state is touch scroll then set userScrolled
                // true
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    if (other_check)
                        userScrolled = true;
                }
            }

        });

        adapter = new ChatAdapter(map_list, this);
        recycler_view.setAdapter(adapter);
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("message string", getJSonString(send_message.getText().toString()));
                if (check) {
                    if (send_message.getText().length() > 0) {
                        web_socket.send(getJSonString(send_message.getText().toString()));
                        HashMap<String, String> hmap = new HashMap<String, String>();
                        hmap.put("message", send_message.getText().toString());
                        hmap.put("user_id", UserPref.getuser_instance(ChatActivity.this).getUserId());
                        hmap.put("time", getDateTime());
                        map_list.add(hmap);
                        Collections.sort(map_list, new CustomDateComparator());
                        // Collections.reverse(map_list);
                        adapter.notifyDataSetChanged();
                        mLayoutManager.scrollToPosition(0);
                        send_message.setText("");
                    }
                } else {
                    UtilityPermission.showToast(ChatActivity.this, "You dont have internet connection !!");
                }
            }
        });
        connectWebSocket();
        if (uri != null)
            Log.e("uri is not null", "uri is not null");
        else
            Log.e("uri is  null", "uri is  null");
        web_socket = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                check = true;
                Log.e("Websocket", "Opened");

            }

            @Override
            public void onMessage(String message) {
                Log.e("message receive", message);
                try {
                    JSONObject json_string = new JSONObject(message);
                    if (json_string.has("connection_id")) {
                        Log.e("connection_id", json_string.getString("connection_id"));
                        connection_id = json_string.getString("connection_id");
                    } else {// Log.e("other_id",json_string.getString("other_id"));
                        Log.e("other id", other_id);
                        if (json_string.getString("other_id").equals(other_id) && json_string.getString("user_id").equals(UserPref.getuser_instance(ChatActivity.this).getUserId())) {
                            Log.e("in if ", "in if");
                            //  json_string.put("other_id", UserPref.getuser_instance(ChatActivity.this).getUserId());
                            // db.addMessage(json_string);
                            //  Log.e("author name",data_json.getString("author"));
                 /*   if(!data_json.getString("author").equalsIgnoreCase("Anupam"))
                    {
                        Log.e("author in if name",data_json.getString("author"));
                        Log.e("atext in if name",data_json.getString("text"));*/
                            setToList(json_string);
                        } else if (json_string.getString("other_id").equals(UserPref.getuser_instance(ChatActivity.this).getUserId()) && json_string.getString("user_id").equals(other_id)) {
                            Log.e("need to add ", "need to add");
                            setToList(json_string);
                        } else {
                            Log.e("send notification", "send notification");
                            sendNotification(json_string);
                        }
                    }

                    // }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                check = false;
                Log.e("Websocket", "Closed " + reason);
            }

            @Override
            public void onError(Exception ex) {
                check = false;
                Log.i("Websocket", "Error " + ex.getMessage());
            }
        };
        web_socket.connect();
    }

    private void sendNotification(JSONObject json_string) {
        String sender_name = "";
        Intent intent = new Intent(this, ChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        try {
            sender_name = json_string.getString("sender_name");
            intent.putExtra("other_id", json_string.getString("user_id"));
            intent.putExtra("user_name", sender_name);
            int requestCode = 0;
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(sender_name)
                    .setContentText(json_string.getString("message"))
                    .setAutoCancel(true)
                    .setSound(sound)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, noBuilder.build()); //0 = ID of notification
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setToList(JSONObject json_string) {
        HashMap<String, String> hmap = new HashMap<String, String>();
        try {
            hmap.put("message", json_string.getString("message"));
            hmap.put("user_id", json_string.getString("user_id"));
            hmap.put("time", getDateToString(json_string.getString("timestamp")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        map_list.add(hmap);
        Collections.sort(map_list, new CustomDateComparator());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void getChatFromServer(String request_id, int offset, boolean b) {
        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserPref.getuser_instance(this).getUserId());
        params.put("other_id", other_id);
        params.put("limit", String.valueOf(limit));
        params.put("offset", String.valueOf(offset));
        CallService.getInstance().passInfromation(api_listener, Constants.REQUESTCHAT_URL, params, b, request_id, this);
    }

    public class CustomDateComparator implements Comparator<HashMap<String, String>> {
        @Override
        public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
            return o2.get("time").compareTo(o1.get("time"));
        }
    }
    public static String getDateToString(String time) {
        long epoch = Long.parseLong(time);
        Date expiry = new Date(epoch * 1000);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Log.e("time", dateFormat.format(expiry));
        return dateFormat.format(expiry);
    }
    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    private String getJSonString(String s) {
        JSONObject json_object = new JSONObject();
        try {
            json_object.put("user_id", UserPref.getuser_instance(this).getUserId());
            json_object.put("other_id", other_id);
            json_object.put("message", s);
            json_object.put("connection_id", connection_id);
            //    db.addMessage(json_object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json_object.toString();
    }

    private void intitializeToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.findViewById(R.id.toolbar_title).setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle(user_name);
    }

    private void updateRecyclerView() {
        upper_layout.setVisibility(View.VISIBLE);
        offset = offset + 15;
        getChatFromServer("2", offset, false);
        // Handler to show refresh for a period of time you can use async task
        // while commnunicating serve

     /*   new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                // Loop for 3 items
                for (int i = 0; i < 3; i++) {
                    HashMap<String, String> hmap11 = new HashMap<String, String>();
                    hmap11.put("message", "i m in left?");
                    hmap11.put("left", "left");
                    map_list.add(hmap11);
                }
                for (int i = 0; i < 3; i++) {
                    HashMap<String, String> hmap11 = new HashMap<String, String>();
                    hmap11.put("message", "i m in right ?");
                    hmap11.put("left", "right");
                    map_list.add(hmap11);
                }
                adapter.notifyDataSetChanged();// notify adapter

                // Toast for task completion
                Toast.makeText(ChatActivity.this, "Items Updated.",
                        Toast.LENGTH_SHORT).show();

                // After adding new data hide the view.
                upper_layout.setVisibility(View.GONE);

            }
        }, 5000);*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initiliazeUI() {
        upper_layout = (RelativeLayout) findViewById(R.id.loadItemsLayout_recyclerView);
        date_text = (TextView) findViewById(R.id.date);
        send_message = (EditText) findViewById(R.id.send_message);
        send_button = (ImageButton) findViewById(R.id.send_button);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
    }

    private void connectWebSocket() {

        try {
            uri = new URI("ws://111.93.120.130:1337?user_id=" + UserPref.getuser_instance(this).getUserId());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        web_socket.close();
        map_list.clear();
    }


    ApiResponseListener api_listener = new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            Log.e("response", response);
            if (request_id.equals("1")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONArray json_array = json.getJSONArray("data");
                        String date = null;
                        String old_date = null;
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        SimpleDateFormat required_format = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                        for (int j = 0; j < json_array.length(); j++) {
                            String myDate = null;
                            old_date = ConvertUTCtolocal(json_array.getJSONObject(j).getString("created_at"));
                            try {
                                myDate = required_format.format(simpleDateFormat.parse(json_array.getJSONObject(j).getString("created_at")));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (date == null) {
                                Log.e("j value", String.valueOf(j));
                                date = myDate;
                                compare_date = myDate;
                                Log.e("date in if", date);
                            } else if (date.equals(myDate)) {
                                Log.e("j value", String.valueOf(j));
                                Log.e("date in else if ", myDate);
                            } else {
                                Log.e("j value", String.valueOf(j));
                                HashMap<String, String> hmap = new HashMap<>();
                                hmap.put("date", date);
                                hmap.put("time", ConvertUTCtolocal(json_array.getJSONObject(j).getString("created_at")));
                                map_list.add(hmap);
                                Log.e("date ", myDate);
                                date = myDate;
                            }
                            HashMap<String, String> hmap = new HashMap<>();
                            hmap.put("user_id", json_array.getJSONObject(j).getString("sender_id"));
                            hmap.put("message", json_array.getJSONObject(j).getString("message"));
                            hmap.put("time", ConvertUTCtolocal(json_array.getJSONObject(j).getString("created_at")));
                            map_list.add(hmap);
                        }
                        HashMap<String, String> hmap = new HashMap<>();
                        hmap.put("date", date);
                        hmap.put("time", old_date);
                        map_list.add(hmap);
                        compare_date = date;
                        Log.e("date ", date);
                        //   Collections.sort(map_list, new CustomDateComparator());
                        adapter.notifyDataSetChanged();
                    } else {
                        UtilityPermission.showToast(ChatActivity.this, "There is no message chat have been from this user !!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (request_id.equals("2")) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.getBoolean("status")) {
                        JSONArray json_array = json.getJSONArray("data");
                        String string_date = null;
                        String old_date = null;
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        SimpleDateFormat required_format = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                        Log.e("date in 2", compare_date);
                        try {
                            if (compare_date.equals(required_format.format(simpleDateFormat.parse(json_array.getJSONObject(0).getString("created_at")))))
                                map_list.remove(map_list.size() - 1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        for (int j = 0; j < json_array.length(); j++) {
                            String myDate = null;
                            old_date = ConvertUTCtolocal(json_array.getJSONObject(j).getString("created_at"));
                            try {
                                myDate = required_format.format(simpleDateFormat.parse(json_array.getJSONObject(j).getString("created_at")));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if (string_date == null) {
                                string_date = myDate;
                            } else if (string_date.equals(myDate)) {

                            } else {
                                HashMap<String, String> hmap = new HashMap<>();
                                hmap.put("date", string_date);
                                hmap.put("time", ConvertUTCtolocal(json_array.getJSONObject(j).getString("created_at")));
                                map_list.add(hmap);
                                Log.e("date ", myDate);
                                string_date = myDate;
                                compare_date = myDate;
                                // compare_date = myDate;
                            }
                            HashMap<String, String> hmap = new HashMap<>();
                            hmap.put("user_id", json_array.getJSONObject(j).getString("sender_id"));
                            hmap.put("message", json_array.getJSONObject(j).getString("message"));
                            hmap.put("time", ConvertUTCtolocal(json_array.getJSONObject(j).getString("created_at")));
                            map_list.add(hmap);
                        }
                        HashMap<String, String> hmap = new HashMap<>();
                        hmap.put("date", string_date);
                        hmap.put("time", old_date);
                        compare_date = string_date;
                        map_list.add(hmap);
                        // Collections.sort(map_list, new CustomDateComparator());
                        adapter.notifyDataSetChanged();
                        upper_layout.setVisibility(View.GONE);
                    }
                    else
                    {
                        upper_layout.setVisibility(View.GONE);
                        other_check = false;
                        UtilityPermission.showToast(ChatActivity.this, "There is no more mesage!!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    };

    private String ConvertUTCtolocal(String created_at) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat required_format = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.getDefault());
        Date myDate = null;
        try {
            myDate = simpleDateFormat.parse(created_at);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return required_format.format(myDate);

    }

    private String currentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        Date date = new Date();
            return  dateFormat.format(date);
    }

}
