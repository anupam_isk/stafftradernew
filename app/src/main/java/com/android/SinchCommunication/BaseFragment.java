package com.android.SinchCommunication;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * Created by admin on 12/8/2016.
 */

public abstract class BaseFragment extends Fragment  implements ServiceConnection {
    private SinchService.SinchServiceInterface mSinchServiceInterface;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("on create in base","on create in base");
        getActivity().getApplicationContext().bindService(new Intent(getActivity(), SinchService.class), this,
                BIND_AUTO_CREATE);
        Log.e("service started ","service started");
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            Log.e("service connected","service connected");
            onServiceConnected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    protected void onServiceConnected() {
        // for subclasses
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }
}
