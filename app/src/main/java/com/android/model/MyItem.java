package com.android.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.json.JSONArray;

/**
 * Created by Anupam on 7/7/2016.
 */
public class MyItem implements ClusterItem {
    private final LatLng mPosition;
    String title;
    String snippet;
    String job_title;
    String image;
    String  skill;
    String brif_bio;
    String qualification;
    String submit_id;
    boolean is_hired;
    boolean is_interested;
    String duration_project;
    String budget;
    String desired_location;
    JSONArray joblist;
    String role_id;
    String first_name,ranking,comment;

    public String getRole_id() {
        return role_id;
    }

    public JSONArray getJoblist() {
        return joblist;
    }

    public MyItem(double lat, double lng, String tit, String sni, String image, String brif_bio, String qualification, String skill, JSONArray joblist, String role_id) {
        mPosition = new LatLng(lat, lng);
        title = tit;
        snippet = sni;
        this.skill=skill;
        this.image=image;
        this.brif_bio=brif_bio;
        this.joblist=joblist;
        this.role_id=role_id;
        this.qualification=qualification;

    }
    public boolean is_hired() {
        return is_hired;
    }

    public boolean is_interested() {
        return is_interested;
    }

    public String getSkill() {
        return skill;
    }

    public String getImage() {
        return image;
    }

    public String getBrif_bio() {
        return brif_bio;
    }

    public String getQualification() {
        return qualification;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public String getTitle() {
        return title;
    }

    public String getDesired_location() {
        return desired_location;
    }

    public String getSnippet() {
        return snippet;
    }

}