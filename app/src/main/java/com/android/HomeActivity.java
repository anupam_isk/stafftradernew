package com.android;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.AllFragment.PrivacyPolicyFragment;
import com.android.AllFragment.SubmitCandidateFragment;
import com.android.AllFragment.MoreFragment_candidate;
import com.android.AllFragment.MoreFragment_hiringManager;
import com.android.AllFragment.MoreFragment_recruiter;
import com.android.AllFragment.NavigationDrawerFragment;
import com.android.AllFragment.ProfileFragment;
import com.android.AllFragment.SettingsFragment;
import com.android.AllFragment.UserAgreementFragment;
import com.android.FragmentInterface.onFragmentListener;
import com.android.helper.UserPref;
import com.android.helper.UtilityPermission;
import com.android.stafftraderapp.R;

public class HomeActivity extends AppCompatActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks, View.OnClickListener, onFragmentListener {
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private String TAG, type="", notification = null;
    private ActionBarDrawerToggle mDrawerToggle;
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private boolean value = false, other_check = false;
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private DrawerLayout mDrawerLayout;
    boolean doubleBackToExitPressedOnce = false;
    private ImageView setting_img, profile_img, home_img;
    private LinearLayout home_icon, profile_icon, messaging_icon, setting_icon;
    private View mFragmentContainerView;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer = true;
    private Toolbar toolbar;

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("on create home", "on create home");
        setContentView(R.layout.activity_home);
        Bundle extras = getIntent().getExtras();
        if (extras  != null) {
            Log.e("not null", "not null");
            type = extras.getString("type");
            notification = extras.getString("notification");
            Log.e("type home ", type);
            Log.e("tnotification  home ", notification);
        }
        initializeToolbar();
        initializeUI();
        setListener();
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        // getSupportFragmentManager().beginTransaction().replace(R.id.navigation_drawer,mNavigationDrawerFragment,"t").addToBackStack("navi").commit();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        // Set up the drawer.
        setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }

    private void setListener() {
        profile_icon.setOnClickListener(this);
        home_icon.setOnClickListener(this);
        messaging_icon.setOnClickListener(this);
        setting_icon.setOnClickListener(this);
    }

    private void initializeUI() {
        profile_icon = (LinearLayout) findViewById(R.id.profile_icon);
        home_icon = (LinearLayout) findViewById(R.id.home_icon);
        messaging_icon = (LinearLayout) findViewById(R.id.messaging_icon);
        setting_icon = (LinearLayout) findViewById(R.id.setting_icon);
        setting_img = (ImageView) findViewById(R.id.setting_img);
        home_img = (ImageView) findViewById(R.id.home_img);
        profile_img = (ImageView) findViewById(R.id.profile_img);
    }

    private void initializeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("on start home", "on start home");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("on resume home ", "on Resume home");
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                    /* host Activity */
                drawerLayout,                    /* DrawerLayout object */
                toolbar,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);


            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.e("drawer open", "drawer open");
                mNavigationDrawerFragment.setData();  //okk
                if (!mUserLearnedDrawer) {

                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(HomeActivity.this);
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }


            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onBackPressed() {
        //Checking for fragment count on backstack
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (value) {
                FragmentManager manager = getSupportFragmentManager();
                if (other_check)
                    setForbottomIcon();
                else
                    manager.popBackStack();
                //manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                setToolbarFalse();
                // if i go to A->B->C  then onResume() methos will call in  this i called a HomeActiity method that method does the indicator false or true
                // you can check the code
                mDrawerToggle.setDrawerIndicatorEnabled(true);
                value = false;
            }
            //  getSupportFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
            return;
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        onSectionAttached(position);
    }

    public void onSectionAttached(int number) {
        Bundle b = new Bundle();
        Log.e("on section  ","on section");
        if (mDrawerLayout != null)
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        switch (number) {
            case 0:
                Fragment fragment = null;
                if (UserPref.getuser_instance(this).getRoleId().equals("2"))
                {
                    Log.e("type in manger case ",type);
                    if (type.equals("enter_job_requirement")) {
                        Log.e("fragment open","fragment ");
                        b.putString("type", type);
                        fragment = new MoreFragment_hiringManager();
                        fragment.setArguments(b);
                    } else {
                        fragment = new MoreFragment_hiringManager();
                    }
                } else if (UserPref.getuser_instance(this).getRoleId().equals("3"))
                    fragment = new MoreFragment_recruiter();
                else
                    fragment = new MoreFragment_candidate();
                replaceFragment(fragment);
                break;
            case 1:
                b.putString("value", "career");
                fragment = new PrivacyPolicyFragment();
                fragment.setArguments(b);
                replaceFragment(fragment);
                break;
            case 2:
                b.putString("value", "about");
                fragment = new PrivacyPolicyFragment();
                fragment.setArguments(b);
                replaceFragment(fragment);
                break;
            case 3:
                b.putString("value", "contact");
                fragment = new PrivacyPolicyFragment();
                fragment.setArguments(b);
                replaceFragment(fragment);
                break;

        }
    }


    @Override
    public void onClick(View v) {
        if (v == home_icon) {
            if (value)
                setForbottomIcon();
            Fragment fragment = null;
            if (UserPref.getuser_instance(this).getRoleId().equals("2"))
                fragment = new MoreFragment_hiringManager();
            else if (UserPref.getuser_instance(this).getRoleId().equals("3"))
                fragment = new MoreFragment_recruiter();
            else
                fragment = new MoreFragment_candidate();

            replaceFragment(fragment);
            home_img.setBackground(getResources().getDrawable(R.mipmap.home_blue));
            //    more_img.setBackground(getResources().getDrawable(R.mipmap.more_icon2));
            setting_img.setBackground(getResources().getDrawable(R.mipmap.setting_icon2));
            profile_img.setBackground(getResources().getDrawable(R.mipmap.profile_icon2));
        } /*else if (v == more_icon) {
            if (value)
                setForbottomIcon();
            SubmitCandidateFragment home_fragment = new SubmitCandidateFragment();
            replaceFragment(home_fragment);
            more_img.setBackground(getResources().getDrawable(R.mipmap.more_icon_blue2));
            setting_img.setBackground(getResources().getDrawable(R.mipmap.setting_icon2));
            profile_img.setBackground(getResources().getDrawable(R.mipmap.profile_icon2));
            home_img.setBackground(getResources().getDrawable(R.mipmap.home_icon2));
        }*/ else if (v == profile_icon) {
            getSupportFragmentManager().popBackStack("navi", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            if (value)
                setForbottomIcon();
            ProfileFragment profile = new ProfileFragment();
            replaceFragment(profile);
            home_img.setBackground(getResources().getDrawable(R.mipmap.home_icon2));
            //more_img.setBackground(getResources().getDrawable(R.mipmap.more_icon2));
            setting_img.setBackground(getResources().getDrawable(R.mipmap.setting_icon2));
            profile_img.setBackground(getResources().getDrawable(R.mipmap.profile_icon_blue2));
        } else if (v == setting_icon) {
            if (value)
                setForbottomIcon();
            SettingsFragment setting_fragment = new SettingsFragment();
            replaceFragment(setting_fragment);
            home_img.setBackground(getResources().getDrawable(R.mipmap.home_icon2));
            // more_img.setBackground(getResources().getDrawable(R.mipmap.more_icon2));
            setting_img.setBackground(getResources().getDrawable(R.mipmap.setting_icon_blue2));
            profile_img.setBackground(getResources().getDrawable(R.mipmap.profile_icon2));
        } else {
            if (value)
                setForbottomIcon();
            Fragment fragment = new UserAgreementFragment();
            Bundle b = new Bundle();
            b.putString("check_value", "chat");
            fragment.setArguments(b);
            replaceFragment(fragment);
        }
    }

    private void setForbottomIcon() {
        Log.e("value", value + "");
        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        manager.beginTransaction().remove(mNavigationDrawerFragment);
        // always keep im nind this logic for pop up stack
        setToolbarFalse();
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        value = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (value) {
                    FragmentManager manager = getSupportFragmentManager();
                    if (other_check) {
                        setForbottomIcon();
                    } else {
                        manager.popBackStack();
                        //manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        setToolbarFalse();
                        // if i go to A->B->C  then onResume() methos will call in  this i called a HomeActiity method that method does the indicator false or true
                        // you can check the code
                        mDrawerToggle.setDrawerIndicatorEnabled(true);
                        value = false;
                    }
                } else {
                    mDrawerLayout.openDrawer(mFragmentContainerView);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void removeBackStak() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    private void setToolbarFalse() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragment_transaction = manager.beginTransaction();
        fragment_transaction.replace(R.id.container, fragment).commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("on stop home", "on stop home");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("on destroy home", "on destroy  home");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("on pause  home", "on pause home");
    }

    @Override
    public void showDrawerToggle(boolean showDrawerToggle) {
        Log.e("other check value ", String.valueOf(other_check));
        mDrawerToggle.setDrawerIndicatorEnabled(showDrawerToggle);
        value = true;
        setToolBar();
    }

    @Override
    public void otherCheck(boolean value) {
        Log.e("other check value ", String.valueOf(other_check));
        other_check = value;
        Log.e("other check value ", String.valueOf(other_check));
    }

    private void setToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
}
