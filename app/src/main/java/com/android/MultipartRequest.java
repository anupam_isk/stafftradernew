package com.android;

/**
 * Created by Rahul Sonkhiya.
 */

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;


import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class MultipartRequest extends Request<String> {
    // private MultipartEntity entity = new MultipartEntity();
    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpentity;
    private static final String FILE_PART_NAME = "image";
    private static final String RESUME_PART_NAME = "resume";
    private static final String VIDEO_PART_NAME = "video";
    private final Response.Listener<String> mListener;
    private File mFilePart, mFilepart2;
    private final Map<String, String> mStringPart;
    public Map<String, String> responseHeaders;

    public MultipartRequest(String url, Response.ErrorListener errorListener, Response.Listener<String> listener, File file, Map<String, String> mStringPart, String content_type) {
        super(Method.POST, url, errorListener);
        mListener = listener;
        mFilePart = file;
        this.mStringPart = mStringPart;
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (content_type.equals("doc"))
            buildResumePartentity();
        else if (content_type.equals("video"))
            buildVideoPartentity();
        else
            buildMultipartEntity();
    }

    public MultipartRequest(String url, Response.ErrorListener errorListener, Response.Listener<String> listener, File file, File file2, Map<String, String> mStringPart, String content_type) {
        super(Method.POST, url, errorListener);
        mListener = listener;
        mFilePart = file;
        mFilepart2 = file2;
        this.mStringPart = mStringPart;
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (content_type.equals("video"))
            buildVideoOtherPartentity();
        else
            buildImagePartEntity();
    }

    private void buildImagePartEntity() {
        Log.e("image file part ", mFilepart2.getPath());
        Log.e("resume file part ", mFilePart.getPath());
        entity.addPart(FILE_PART_NAME, new FileBody(mFilepart2));
        entity.addPart(RESUME_PART_NAME, new FileBody(mFilePart));
        for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
            entity.addTextBody(entry.getKey(), entry.getValue());
        }
    }

    private void buildVideoOtherPartentity() {
        entity.addPart(VIDEO_PART_NAME, new FileBody(mFilepart2));
        entity.addPart(RESUME_PART_NAME, new FileBody(mFilePart));
        for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
            entity.addTextBody(entry.getKey(), entry.getValue());
        }
    }

    private void buildVideoPartentity() {
        entity.addPart(VIDEO_PART_NAME, new FileBody(mFilePart));
        for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
            entity.addTextBody(entry.getKey(), entry.getValue());
        }
    }

    private void buildResumePartentity() {
        Log.e("resume", "resume");
        entity.addPart(RESUME_PART_NAME, new FileBody(mFilePart));
        for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
            entity.addTextBody(entry.getKey(), entry.getValue());
        }
    }

    public void addStringBody(String param, String value) {
        mStringPart.put(param, value);
    }

    private void buildMultipartEntity() {

        entity.addPart(FILE_PART_NAME, new FileBody(mFilePart));
        for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
            entity.addTextBody(entry.getKey(), entry.getValue());
        }
        String name = mFilePart.getName();
        entity.addPart(FILE_PART_NAME,
                new FileBody(mFilePart, ContentType.create("image/jpeg"),
                        mFilePart.getName()));

        //application/msword
//  if (mStringPart != null) {
//   for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
//    entity.addTextBody(entry.getKey(), entry.getValue());
//   }
    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity = entity.build();
            httpentity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        //return Response.success(response.toString(), getCacheEntry());
        //Initialise local responseHeaders map with response headers received
        responseHeaders = response.headers;

        //Pass the response data here
        return Response.success(new String(response.data), HttpHeaderParser.parseCacheHeaders(response));
        // Response.success(response.data, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String s) {
        mListener.onResponse(s);
    }

}
