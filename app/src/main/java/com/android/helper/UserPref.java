package com.android.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ANUPAM TYAGI on 6/29/2016.
 */
public class UserPref {
    private static UserPref user_instance=null;
    private static SharedPreferences sharedpreferences;
    private static SharedPreferences.Editor editor;
    private UserPref()
    {

    }
    public static UserPref getuser_instance(Context context)
    {
         if(user_instance==null) {
            user_instance=new UserPref();
            sharedpreferences = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE);
            editor = sharedpreferences.edit();
        }
        return  user_instance;
    }
  /*  public UserPref(Context context) {
        sharedpreferences = context.getSharedPreferences(Constants.USER_PREF, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
    }
*/
    public  void setUserFirstName(String name) {
        editor.putString(Constants.USER_FNAME, name).commit();
    }
    public  String getUserFirstName() {
        return sharedpreferences.getString(Constants.USER_FNAME, "");
    }
    public void setUserLastName(String name) {
        editor.putString(Constants.USER_LNAME, name).commit();
    }

    public String getUserLastName() {
        return sharedpreferences.getString(Constants.USER_LNAME, "");
    }
    public void setUserPassword(String password) {
        editor.putString(Constants.USER_PASSWORD, password).commit();
    }

    public String getUserPassword() {
        return sharedpreferences.getString(Constants.USER_PASSWORD, "");
    }

    public void setUserEmail(String email) {
        editor.putString(Constants.USER_EMAIL, email).commit();
    }

    public String getUserEmail() {
        return sharedpreferences.getString(Constants.USER_EMAIL, "");
    }

    public void setUserPhone(String phone) {
        editor.putString(Constants.USER_PHONE, phone).commit();
    }

    public String getUserPhone() {
        return sharedpreferences.getString(Constants.USER_PHONE, "");
    }

    public void setUserToken(String token) {
        editor.putString(Constants.USER_TOKEN, token).commit();
    }

    public String getUserToken() {
        return sharedpreferences.getString(Constants.USER_TOKEN, "");
    }

    public void setLattitude(String lat) {
        editor.putString(Constants.USER_LAT, lat).commit();
    }

    public String getLattitude() {
        return sharedpreferences.getString(Constants.USER_LAT, "");
    }

    public void setLongitude(String longitude) {
        editor.putString(Constants.USER_LONG, longitude).commit();
    }

    public String getLongitude() {
        return sharedpreferences.getString(Constants.USER_LONG, "");
    }

    public void setAutoLogin(boolean value) {
        editor.putBoolean(Constants.USER_AUTOLOGIN, value).commit();
    }

    public boolean getAutoLogin() {
        return sharedpreferences.getBoolean(Constants.USER_AUTOLOGIN, false);
    }

    public  void  setUserId(String user_id) {
        editor.putString(Constants.USER_ID, user_id).commit();
    }

    public String getUserId() {
        return sharedpreferences.getString(Constants.USER_ID, "");
    }

    public void setRoleId(String role_id) {
        editor.putString(Constants.USER_ROLEID, role_id).commit();
    }

    public String getRoleId() {
        return sharedpreferences.getString(Constants.USER_ROLEID, "");
    }

    public void clearPref() {
        editor.clear().commit();
    }

    public void setStreet(String street) {
        editor.putString(Constants.USER_STREET, street).commit();
    }

    public String getStreet() {
        return sharedpreferences.getString(Constants.USER_STREET, "");
    }

    public void setCity(String city) {
        editor.putString(Constants.USER_CITY, city).commit();
    }

    public String getCity() {
        return sharedpreferences.getString(Constants.USER_CITY, "");
    }

    public void setCountry(String country) {
        editor.putString(Constants.USER_COUNTRY, country).commit();
    }

    public String getCountry() {
        return sharedpreferences.getString(Constants.USER_COUNTRY, "");
    }

    public void setState(String state) {
        editor.putString(Constants.USER_STATE, state).commit();
    }

    public String getState() {
        return sharedpreferences.getString(Constants.USER_STATE, "");
    }
    public void setuserImage(String image) {
        editor.putString(Constants.USER_IMAGE, image).commit();
    }

    public String getUserImage() {
        return sharedpreferences.getString(Constants.USER_IMAGE, "");
    }
    public void setuserjobtitle(String title) {
        editor.putString(Constants.USER_JOB_TITLE,title).commit();
    }

    public String getUserjobtitle() {
        return sharedpreferences.getString(Constants.USER_JOB_TITLE, "");
    }
}
