package com.android.helper;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.android.AllFragment.PaperWorkFragment;
import com.android.AllFragment.ShowImageFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by admin on 3/1/2017.
 */
public class DrawView extends View {
    Paint paint = new Paint();
    Context app_context;
    String json_string;
    File f = null, directory = null;
    private ScaleGestureDetector scaleDetector;
    private float scaleFactor = 1.f;
    String uniqueId, current_image;
    Canvas canvas1;
    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        scaleDetector = new ScaleGestureDetector(context, new ScaleListener());

        Log.e("constructor call", "constructor call");
      /*  int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        DisplayMetrics metrics = new DisplayMetrics();
        app_context.getWindowManager().getDefaultDisplay().getMetrics(metrics);*/
    }
    private void createFile() {
        ContextWrapper cw = new ContextWrapper(app_context.getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        directory = cw.getDir("imageDirectory", Context.MODE_PRIVATE);
        // Create imageDir
        uniqueId = PaperWorkFragment.getTodaysDate() + "_" + PaperWorkFragment.getCurrentTime() + "_" + Math.random() + "_" + "document";
        current_image = uniqueId + ".png";
        f = new File(directory, current_image);
    }

    public File getFileObject() {
        return f;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        scaleDetector.onTouchEvent(ev);
        return true;
    }

    public void setField(Context context, String json_string) {
        this.json_string = json_string;
        app_context = context;
        createFile();
        WindowManager wm = (WindowManager) app_context.getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
    }
// onMeasure must be included otherwise one or both scroll views will be compressed to zero pixels
// and the scrollview will then be invisible

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = 1400;
        int height = 1400 + 50; // Since 3000 is bottom of last Rect to be drawn added and 50 for padding.
        setMeasuredDimension(width, height);
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.save();
        canvas.scale(scaleFactor, scaleFactor);
        JSONArray canvas_array = null;
        setDrawingCacheEnabled(true);
        canvas.drawBitmap(ShowImageFragment.bitma, 0, 0, paint);
        try {
            canvas_array = new JSONArray(json_string);
            for (int i = 0; i < canvas_array.length(); i++) {
                JSONObject json_object = canvas_array.getJSONObject(i);
                String[] split_array = json_object.getString("x1").split(",");
                Log.e("split coordinate", split_array[0] + "other split array " + split_array[1]);
                canvas.drawBitmap(PaperWorkFragment.bit, Float.parseFloat(split_array[0]), Float.parseFloat(split_array[1]) - 35, paint);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        canvas.restore();
        Log.e("Error--------->", f.getAbsolutePath());
        //
        super.onDraw(canvas);
        //  canvas1.drawBitmap(PaperWorkFragment.bit, 460,1350, paint);

// ToDo: Put drawing code in here
    }

    public void save(View v, int height, int width) {
        v.setDrawingCacheEnabled(true);
        Bitmap mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(mBitmap);
        try {
            FileOutputStream file_output = new FileOutputStream(f);
            v.draw(canvas);
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, file_output);
            file_output.flush();
            file_output.close();

        } catch (Exception e) {
            Log.e("Error--------->", e.getMessage());
        }

      /*       new_bitmap=UtilityPermission.getResizedBitmap(mBitmap,33,148);
            Log.e("log_tag", " Newbitmap Width: " + new_bitmap.getWidth());
            Log.e("log_tag", "Newbitmap Height: " + new_bitmap.getHeight());*/
    }

    /*   Bitmap new_bitmap=  getResizedBitmap(mBitmap,120,160);
       Log.v("log_tag", "Width: " +new_bitmap.getWidth());
       Log.v("log_tag", "Height: " + new_bitmap.getHeight());*/
    private class ScaleListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor *= detector.getScaleFactor();
            scaleFactor = Math.max(1f, Math.min(scaleFactor, 10.0f));
            invalidate();
            return true;
        }
    }
}

