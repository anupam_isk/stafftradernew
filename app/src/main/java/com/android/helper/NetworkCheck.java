package com.android.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Anupam tyagi on 6/28/2016.
 */
public class NetworkCheck {
    boolean connected = false;
    static Context context;
    private static NetworkCheck networkCheck=new NetworkCheck();
    public static NetworkCheck getInstance(Context contx)
    {
        context=contx.getApplicationContext();
        return networkCheck;
    }
    public boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return connected;
        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.v("connectivity", e.toString());
        }
        return connected;
    }
}
