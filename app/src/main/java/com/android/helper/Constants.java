package com.android.helper;

/**
 * Created by Anupam on 6/29/2016.
 */
public interface Constants {
    String NETWORK_STRING = "Network is not available !";
    String USER_PREF = "user_pref";
    //String API_URL = "http://oycdemo.com/stafftrader/web_service.php?";
    String API_URL = "http://iskdemo.com/stafftrader/api/v1/index.php?";
    // String API_URL = "http://192.168.30.126/stafftrader/api/v1/index.php?";
    String LOGIN_URL = API_URL + "REQUEST=LOGIN";
    String REGISTRAION_URL = API_URL + "REQUEST=REGISTRATION";
    String CHANGE_PHONE_NUM_URL = API_URL + "REQUEST=CHANGE_PHONE";
    String CATEGORY_URL = API_URL + "REQUEST=CONFIGURATION";
    String CHANGE_PASSWORD_URL = API_URL + "REQUEST=CHANGE_PASSWORD";
    String UPDATE_PROFILE_URL = API_URL + "REQUEST=UPDATE_PROFILE";
    String SEEK_OPEN_JOB_URL = API_URL + "REQUEST=SEEK_OPEN_JOB";
    String OPEN_JOB_REQUIREMENT = API_URL + "REQUEST=OPEN_JOB_REQUIREMENT";
    String ENTER_JOB_REQUIREMENT = API_URL + "REQUEST=ENTER_JOB_REQUIREMENT";
    String DELETE_JOB = API_URL + "REQUEST=DELETE_JOB";
    String EDIT_JOB_REQUIREMENT_URL = API_URL + "REQUEST=EDIT_JOB_REQUIREMENT";
    String SEARCH_CANDIDATES_URL = API_URL + "REQUEST=SEARCH_CANDIDATES";
    String SHOW_ACTIVE_CANDIDATES_URL = API_URL + "REQUEST=SHOW_ACTIVE_CANDIDATES";
    String STATUS_REPORT_ON_HIRE_URL = API_URL + "REQUEST=STATUS_REPORT_ON_HIRE";
    String CHANGE_JOB_STATUS_URL = API_URL + "REQUEST=CHANGE_JOB_STATUS";
    String JOB_MATRIX_URL = API_URL + "REQUEST=job_matrix";
    String UPLOAD_CANDIDATE_BIO_URL = API_URL + "REQUEST=upload_candidate_bio";
    String HIRED_URL = API_URL + "REQUEST=HIRED";
    String DELETE_ASSIGNMENT_URL = API_URL + "REQUEST=delete_assignment";
    String INTERESTED_URL = API_URL + "REQUEST=INTERESTED";
    String REQUESTCHAT_URL = API_URL + "REQUEST=user_chat";
    String UPLOAD_NEW_CANDIDATE = API_URL + "REQUEST=upload_new_candidate";
    String FREQUENCY_OF_DISTRIBUTION = API_URL + "REQUEST=auto_distribution";
    // String UPLOAD_RESUME_URL = API_URL + "REQUEST=UPLOAD_RESUME";
    String ADD_EVENT_URL = API_URL + "REQUEST=ADD_EVENT";
    String FORGET_PASSWORD = API_URL + "REQUEST=forget_password";
    String GET_EVENT_URL = API_URL + "REQUEST=GET_EVENT";
    String PROJECT_MATRIX_URL = API_URL + "REQUEST=project_matrix";
    String GET_USERS_URL = API_URL + "REQUEST=GET_USERS";
    String EDIT_EVENT_URL = API_URL + "REQUEST=EDIT_EVENT";
    String DELETE_EVENT_URL = API_URL + "REQUEST=DELETE_EVENT";
    String CHANGE_ASSIGNMENT_STATUS_URL = API_URL + "REQUEST=CHANGE_ASSIGNMENT_STATUS";
    String CANDIDATE_ASSIGNMENT_REPORT_URL = API_URL + "REQUEST=CANDIDATE_ASSIGNMENT_REPORT";
    String SHOW_ASSIGNMENT_REPORT_URL = API_URL + "REQUEST=SHOW_ASSIGNMENT_REPORT";
    String ADD_ASSIGNMENT_URL = API_URL + "REQUEST=ADD_ASSIGNMENT";
    String AVAILABLE_CANDIDATES_URL = API_URL + "REQUEST=AVAILABLE_CANDIDATES";
    String STATUS_REPORT_ON_JOB_URL = API_URL + "REQUEST=status_report_on_job";
    String EDIT_ASSIGNMENT_URL = API_URL + "REQUEST=edit_assignment";
    String ADD_REMOVE_HOTEBOOK_URL = API_URL + "REQUEST=add_remove_hotbook";
    String SUBMITTED_JOBS_URL = API_URL + "REQUEST=SUBMITTED_JOBS";
    String WITHDRAW_REQUESTED_URL = API_URL + "REQUEST=WITHDRAW_SUBMITTED_JOB";
    String LIST_HOTEBOOK_URL = API_URL + "REQUEST=list_hotbook";
    String UPDATE_BIO_URL = API_URL + "REQUEST=update_bio";
    String UPLOAD_RESUME_URL = API_URL + "REQUEST=upload_resume";
    String SUBMITTED_JOB_LIST_URL = API_URL + "REQUEST=SUBMITTED_JOB_LIST";
    String SUBMITTED_JOB_URL = API_URL + "REQUEST=SUBMIT_JOB";
    String CANDIDATE_TO_SUBMIT_URL = API_URL + "REQUEST=candidate_to_submit";
    String USER_LIST_URL = API_URL + "REQUEST=user_list";
    ;
    String CHAT_USER_LIST = API_URL + "REQUEST=chat_user_list";
    String VIEW_PROFILE = API_URL + "REQUEST=view_profile";
    String MILESTONE_LIST = API_URL + "REQUEST=milestone";
    String WHO_VIEWED_PROFILE_URL = API_URL + "REQUEST=who_viewed_profile";
    String JOBS_APPLIED_TO_URL = API_URL + "REQUEST=jobAppliedto";
    String WITHDRAW_SUBMITTED_TO_URL = API_URL + "REQUEST=withdraw_submitted_job";
    String EDIT_MILESTONE_URL = API_URL + "REQUEST=edit_milestone";
    String ADD_MILESTONE_URL = API_URL + "REQUEST=add_milestone";
    String DELETE_MILESTONE_URL = API_URL + "REQUEST=delete_milestone";
    String VIEW_MILESTONE_TASK_URL = API_URL + "REQUEST=milestone_task";
    String EDIT_MILESTONE_TASK_URL = API_URL + "REQUEST=edit_milestone_task";
    String DELETE_MILESTONE_TASK_URL = API_URL + "REQUEST=delete_milestone_task";
    String ADD_MILESTONE_TASK_URL = API_URL + "REQUEST=add_milestone_task";
    String CANDIDATE_ACCEPTED_JOB_URL = API_URL + "REQUEST=CANDIDATE_ACCEPT_JOB";
    String MILESTONE_BY_ROLE_URL = API_URL + "REQUEST=MILESTONE_BY_ROLE";
    String MILESTONE_BY_TASK_ROLE_URL = API_URL + "REQUEST=MILESTONE_TASK_BY_ROLE";
    String RECRUITER_HIRE_MATRIX = API_URL + "REQUEST=recruiter_hire_matrix";
    String RECRUITER_PAYMENT_URL = API_URL + "REQUEST=recruiter_payment";
    String CHANGE_TASK_STATUS_URL = API_URL + "REQUEST=change_task_status";
    String CHANGE_HIRED_STATUS_URL = API_URL + "REQUEST=change_hired_status";
    String PAYMENTS_URL = API_URL + "REQUEST=payments";
    String UPLOAD_PAPERWORK_URL = API_URL + "REQUEST=upload_paperwork";
    String INTERACTIVE_MAP = API_URL + "REQUEST=interactive_map";
    String PAPERWORK_URL = API_URL + "REQUEST=paperwork";
    String OPEN_JOB_CLOSE_URL = API_URL + "REQUEST=open_close_job";
    String CANDIDATE_WITH_RECRUITER_URL = API_URL + "REQUEST=candidate_with_recruiter";
    //String ASSIGNMENT_BY_DATE_URL = API_URL +"REQUEST=ASSIGNMENT_BY_DATE";
    String HIRE_MATRIX_URL = API_URL + "REQUEST=HIRE_MATRIX";
    String SUBMITTED_CANDIDATES_URL = API_URL + "REQUEST=SUBMITTED_CANDIDATES";
    String PRIVACY_POLICY_URL = "http://iskdemo.com/stafftrader/public/privacyPolicy";
    String LICENCE_AGREEMENT = "http://iskdemo.com/stafftrader/public/licenseAgreement";
    String TERMS_AND_CONDITIONS = "http://iskdemo.com/stafftrader/public/termsConditions";
    String CONTACT_US = "http://iskdemo.com/stafftrader/public/contactUs";
    String ABOUT_US = "http://iskdemo.com/stafftrader/public/aboutUs";
    String SERVICE = "http://iskdemo.com/stafftrader/public/termsConditions";
    String CANDIDATE_REGISTRATION = "";
    String HIRINGMANAGER_ROLEID = "2";
    String RECRUITER_ROLEID = "3";
    String CANDIDATE_ROLEID = "4";
    String USER_FNAME = "first_name";
    String USER_ID = "user_id";
    String USER_LNAME = "last_name";
    String USER_PASSWORD = "password";
    String USER_EMAIL = "email";
    String USER_PHONE = "phone";
    String USER_TOKEN = "token";
    String USER_LAT = "lattitude";
    String USER_LONG = "longitude";
    String USER_AUTOLOGIN = "auto_login";
    String USER_ROLEID = "role_id";
    String USER_STREET = "street";
    String USER_CITY = "city";
    String USER_COUNTRY = "country";
    String USER_STATE = "state";
    String USER_IMAGE = "user_image";
    String USER_JOB_TITLE = "job_title";
    // This file for constant variable which is used in project.


}
