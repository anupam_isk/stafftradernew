package com.android.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.ChatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Anupam  on 9/27/2016.
 */

public class DataBaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "chatinfo.db";
    private static final String TABLE_NAME = "chat_message";
    private static final String CREATE_CHAT_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + "id " + "integer primary key autoincrement not null, " + "user_id " + "varchar(10), " + "other_id " + "varchar(10), " + "message " + " text not null, " + "timestamp " + "sqltime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL " + ")";
    private static final String NEW_TABLE = "CREATE TABLE " + TABLE_NAME + "( " + "id " + "integer primary key autoincrement not null, " + "user_id " + "varchar(10) ," + "other_id " + "varchar(10) ," + "messsage " + " text not null " + ") ";
    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.e("database constructor", "database constructor");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_CHAT_TABLE);
        Log.e("on drop", "on drop");
        onCreate(db);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("Table created ", "table created ");
        db.execSQL(CREATE_CHAT_TABLE);
    }
    public void addMessage(JSONObject json) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try {
            values.put("user_id", json.getString("user_id"));
            values.put("other_id", json.getString("other_id"));
            values.put("message", json.getString("message"));
            if (json.has("timestamp"))
                values.put("timestamp", ChatActivity.getDateToString(json.getString("timestamp")));
            // Shop Name
        } catch (JSONException e) {
            e.printStackTrace();
        }
        long id = db.insert(TABLE_NAME, null, values);
        Log.e("long id ", String.valueOf(id));
        if (id != -1)
            Log.e("data inserted", "data inserted");
        else
            Log.e("data  not inserted", "data  not inserted");
        db.close(); // Closing database connection
    }

    public ArrayList<HashMap<String, String>> getMessage(int limit, int offset, String user_id, String other_id) {
        ArrayList<HashMap<String, String>> map_list = new ArrayList<>();
        String countQuery = "select * from " + TABLE_NAME + " where " + "(" + "user_id= " + user_id + " && other_id= " + other_id + ")" + " or (" + "user_id= " + other_id + " && other_id= " + user_id + ")" + " limit " + limit + " offset " + offset;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.getCount() == 0)
            return map_list;
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<>();
                map.put("user_id", cursor.getString(1));
                map.put("other_id", cursor.getString(2));
                map.put("message", cursor.getString(3));
                map.put("time", cursor.getString(4));
                map_list.add(map);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return map_list;


    }
}
