package com.android.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import com.android.HomeActivity;
/**
 * Created by Anupam tyagi on 7/11/2016.
 */
public class AlertManger  {
    private static AlertManger alert_instance=null;
    public static AlertManger getAlert_instance()
    {
        if(alert_instance==null)
            alert_instance=new AlertManger();
        return  alert_instance;
    }

    public void showAlert(String message,Context context)
    {
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Staff Trader App")
                    .setMessage(message)
                    .setPositiveButton("Ok", null)
                    .show();
        }
        catch (Exception e)
        {
            e.getMessage();
        }
    }
    public void showAlertPop(String message,final  Context context)
    {
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Staff Trader App")
                    .setMessage(message)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            FragmentManager manager = ((HomeActivity) context).getSupportFragmentManager();
                            manager.popBackStack();
                        }
                    })
                    .show();
        }
        catch (Exception e)
        {
            e.getMessage();
        }
    }
}
