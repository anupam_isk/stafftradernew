package com.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.FragmentInterface.ApiResponseListener;
import com.android.GsonResponse.AllCandidateResponse;
import com.android.ServerCommunication.CallService;
import com.android.adapter.UserNotificationAdapter;
import com.android.helper.AlertManger;
import com.android.helper.Constants;
import com.android.helper.UserPref;
import com.android.stafftraderapp.R;
import com.google.android.gms.vision.text.Text;
import com.google.gson.Gson;

import java.util.HashMap;

public class ViewProfileActivity extends AppCompatActivity
{
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    private LinearLayout linear1;
    private TextView brief_bio,qualification,skil, budget,duartion_project,desired_location,comment;
    private String other_id,job_title,brief,skill,duration,budget_text,desired;
    private AllCandidateResponse.Candidate candidate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        initializeUI();
        Intent intent=getIntent();
        if(intent!=null) {
            Bundle b = intent.getExtras().getBundle("bundle");
            if(b.containsKey("open_job")) {
                linear1.setVisibility(View.GONE);
                job_title = b.getString("job_title");
                brief = b.getString("brief");
                skill=b.getString("skill");
                duration=b.getString("duration");
                budget_text=b.getString("budget");
                desired=b.getString("desired_location");
                collapsingToolbarLayout.setTitle(job_title);
                brief_bio.setText(brief);
                duartion_project.setText(duration);
                desired_location.setText(desired);
                skil.setText(skill);
                duartion_project.setText(duration);
                budget.setText(budget_text);
            }
            else
            {
                other_id=b.getString("other_id");
                HashMap<String, String> params = new HashMap<>();
                params.put("other_id",other_id);
                params.put("user_id",UserPref.getuser_instance(this).getUserId());
                CallService.getInstance().passInfromation(api_listener, Constants.VIEW_PROFILE, params, true, "1", ViewProfileActivity.this);
            }
        }
    }

    private void initializeUI() {
        linear1=(LinearLayout) findViewById(R.id.linear1);
        brief_bio=(TextView) findViewById(R.id.brief_bio);
        qualification=(TextView) findViewById(R.id.qualification);
        skil=(TextView) findViewById(R.id.skil);
        budget=(TextView) findViewById(R.id.budget);
        duartion_project=(TextView) findViewById(R.id.duartion_project);
        desired_location=(TextView) findViewById(R.id.desired_location);
        comment=(TextView) findViewById(R.id.comment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        toolbarTextAppernce();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }
    ApiResponseListener api_listener=new ApiResponseListener() {
        @Override
        public void getResponse(String response, String request_id) {
            if (response != null) {

            }

        }
    };
}
