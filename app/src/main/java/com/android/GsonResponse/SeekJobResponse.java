package com.android.GsonResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi on 7/5/2016.
 */
public class SeekJobResponse {
    boolean status;
    String message;
    ArrayList<SeekJob> data;
    public ArrayList<SeekJob> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }
    public class SeekJob implements Parcelable
    {
        protected SeekJob(Parcel in) {
            id = in.readString();
            job_title = in.readString();
            desired_location = in.readString();
            image = in.readString();
            is_relocate = in.readString();
            availability = in.readString();
            duration_project = in.readString();
            budget = in.readString();
            industry_type = in.readString();
            description = in.readString();
            skill_set = in.readString();
            start_date = in.readString();
            is_accept = in.readByte() != 0;
            is_intrested = in.readByte() != 0;
        }

        public  final Creator<SeekJob> CREATOR = new Creator<SeekJob>() {
            @Override
            public SeekJob createFromParcel(Parcel in) {
                return new SeekJob(in);
            }

            @Override
            public SeekJob[] newArray(int size) {
                return new SeekJob[size];
            }
        };

        public String getJob_title() {
            return job_title;
        }

        public String getDesired_location() {
            return desired_location;
        }

        public String getId() {
            return id;
        }

        public String getImage() {
            return image;
        }

        public String getIs_relocate() {
            return is_relocate;
        }

        public String getAvailability() {
            return availability;
        }

        String id;
        String job_title;
        String desired_location;
        String image;
        String is_relocate;
        String availability;
        String duration_project;
        String budget;
        String industry_type;
        String description;
        String skill_set;
        String who_post;
        String job_req_id;

        public String getJob_req_id() {
            return job_req_id;
        }

        public void setJob_req_id(String job_req_id) {
            this.job_req_id = job_req_id;
        }

        public String getWho_post() {
            return who_post;
        }

        public String getStart_date() {
            return start_date;
        }

        public String getSkill_set() {
            return skill_set;
        }

        public String getDescription() {
            return description;
        }

        public String getIndustry_type() {
            return industry_type;
        }

        public String getBudget() {
            return budget;
        }

        public String getDuration_project() {
            return duration_project;
        }

        String start_date;
        String hired,is_accepted;

        public String getHired() {
            return hired;
        }

        public void setHired(String hired) {
            this.hired = hired;
        }

        public String getIs_accepted() {
            return is_accepted;
        }

        public void setIs_accepted(String is_accepted) {
            this.is_accepted = is_accepted;
        }

        boolean is_accept;

        public boolean is_intrested() {
            return is_intrested;
        }

        public void setIs_intrested(boolean is_intrested) {
            this.is_intrested = is_intrested;
        }

        public boolean is_accept() {
            return is_accept;
        }

        public void setIs_accept(boolean is_accept) {
            this.is_accept = is_accept;
        }

        boolean is_intrested;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(id);
            parcel.writeString(job_title);
            parcel.writeString(desired_location);
            parcel.writeString(image);
            parcel.writeString(is_relocate);
            parcel.writeString(availability);
            parcel.writeString(duration_project);
            parcel.writeString(budget);
            parcel.writeString(industry_type);
            parcel.writeString(description);
            parcel.writeString(skill_set);
            parcel.writeString(start_date);
            parcel.writeByte((byte) (is_accept ? 1 : 0));
            parcel.writeByte((byte) (is_intrested ? 1 : 0));
        }
    }
}
