package com.android.GsonResponse;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi on 7/4/2016.
 */
public class CategoryResponse  {
    boolean status;
    ArrayList<CategoryType>  industry;
    ArrayList<Skill>  skill;
    public ArrayList<Skill> getSkill() {
        return skill;
    }
    public boolean isStatus() {
        return status;
    }
    public ArrayList<CategoryType> getIndustry()
    {
        return industry;
    }
     public class CategoryType
     {
         public String getIndustry_name() {
             return industry_name;
         }
         public String getId() {
             return id;
         }
         String id,industry_name;
     }
    public class Skill
    {
        String id,skills;

        public String getId() {
            return id;
        }

        public String getSkills() {
            return skills;
        }
    }

}
