package com.android.GsonResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Anupam on 7/7/2016.
 */
public class OpenJobResponse {
    boolean status;
    String message;
    ArrayList<OpenJob> data;
    ArrayList<OpenJob> closed_job;
    public ArrayList<OpenJob> getData() {
        return data;
    }
    public ArrayList<OpenJob> getClosed_job() {
        return closed_job;
    }
    public String getMessage() {
        return message;
    }
    public boolean isStatus() {
        return status;
    }
    public class OpenJob implements Parcelable
    {
            protected OpenJob(Parcel in) {
            id = in.readString();
            job_title = in.readString();
            desired_location = in.readString();
            is_relocate = in.readString();
            availability = in.readString();
            budget = in.readString();
            duration_project = in.readString();
            industry_type = in.readString();
            description = in.readString();
            start_date = in.readString();
            when_post = in.readString();
            lat = in.readString();
            lon = in.readString();
            submit_id=in.readString();
            skill_set=in.readString();
        }
        public final Creator<OpenJob> CREATOR = new Creator<OpenJob>() {
            @Override
            public OpenJob createFromParcel(Parcel in) {
                return new OpenJob(in);
            }

            @Override
            public OpenJob[] newArray(int size) {
                return new OpenJob[size];
            }
        };

        public String getJob_title() {
            return job_title;
        }

        public String getDesired_location() {
            return desired_location;
        }

        public String getId() {
            return id;
        }

        public String getIs_relocate() {
            return is_relocate;
        }

        public String getAvailability() {
            return availability;
        }

        String id;
        String job_title;
        public String getLon() {
            return lon;
        }

        public String getStart_date() {
            return start_date;
        }

        public String getDescription() {
            return description;
        }

        public String getIndustry_type() {
            return industry_type;
        }

        public String getBudget() {
            return budget;
        }

        public String getDuration_project() {
            return duration_project;
        }

        public String getWhen_post() {
            return when_post;
        }

        public String getLat() {
            return lat;
        }

        String desired_location;
        String is_relocate;
        String availability;
        String budget;
        String duration_project;
        String industry_type;
        String description;
        String skill_set;

        public String getSkill_set() {
            return skill_set;
        }

        String start_date;
        String when_post;
        String lat;
        String lon;
        String status;
        String submit_id;
        String job_id;

        public String getJob_req_id() {
            return job_id;
        }

        public String getSubmit_id() {
            return submit_id;
        }

        public void setSubmit_id(String submit_id) {
            this.submit_id = submit_id;
        }

        public void setEnabled(String enabled) {
            this.status = enabled;
        }

        public String getEnabled() {

            return status;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(job_title);
            dest.writeString(desired_location);
            dest.writeString(availability);
            dest.writeString(budget);
            dest.writeString(duration_project);
            dest.writeString(start_date);
            dest.writeString(description);
            dest.writeString(industry_type);
            dest.writeString(submit_id);
            dest.writeString(skill_set);
        }
    }
}
