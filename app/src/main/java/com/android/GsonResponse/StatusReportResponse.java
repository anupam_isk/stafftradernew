package com.android.GsonResponse;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi on 7/12/2016.
 */
public class StatusReportResponse {
    boolean status;
    ArrayList<StatusReport> data;
    public ArrayList<StatusReport> getData() {
        return data;
    }
    public boolean isStatus() {
        return status;
    }
    public class StatusReport
    {

        String id;
        String job_title;
        String first_name;
        String image;
        String ranking;

        public String getPercentage() {
            return percentage;
        }

        public void setPercentage(String percentage) {
            this.percentage = percentage;
        }

        public String getRanking() {
            return ranking;
        }

        public void setRanking(String ranking) {
            this.ranking = ranking;
        }

        String percentage;
        String last_name;

        public boolean isInterviewed() {
            return interviewed;
        }

        public boolean isHired() {
            return hired;
        }

        boolean background,l9,reference,good_to_start,hired,interviewed;

        public String getId() {
            return id;
        }

        public String getJob_title() {
            return job_title;
        }

        public void setL9(boolean l9) {
            this.l9 = l9;
        }

        public void setBackground(boolean background) {
            this.background = background;
        }

        public void setHired(boolean hired) {
            this.hired = hired;
        }

        public void setGood_to_start(boolean good_to_start) {
            this.good_to_start = good_to_start;
        }

        public void setReference(boolean reference) {
            this.reference = reference;
        }

        public void setInterviewed(boolean interviewed) {
            this.interviewed = interviewed;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getImage() {
            return image;
        }

        public String getLast_name() {
            return last_name;
        }

        public boolean isBackground() {
            return background;
        }

        public boolean isL9() {
            return l9;
        }

        public boolean isGood_to_start() {
            return good_to_start;
        }

        public boolean isReference() {
            return reference;
        }
    }
}
