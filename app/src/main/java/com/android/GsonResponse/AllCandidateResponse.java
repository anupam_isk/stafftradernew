package com.android.GsonResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Anupam Tyagi  on 7/12/2016.
 */
public class AllCandidateResponse {
    boolean status;
    ArrayList<Candidate> data;
    ArrayList<Candidate> managers;
    ArrayList<Candidate> recruiters;
    public ArrayList<Candidate> getCandidates() {
        return candidates;
    }
    public ArrayList<Candidate> getRecruiters() {
        return recruiters;
    }
    public ArrayList<Candidate> getManagers() {
        return managers;
    }
    ArrayList<Candidate> candidates;
    public ArrayList<Candidate> getData() {
        return data;
    }
    public boolean isStatus() {
        return status;
    }
    public class Candidate implements Parcelable
    {
        public Candidate()
        {

        }
        protected Candidate(Parcel in) {
            id = in.readString();
            job_title = in.readString();
            image = in.readString();
            brif_bio = in.readString();
            qualification = in.readString();
            submit_id = in.readString();
            is_hired = in.readString();
            is_interested = in.readByte() != 0;
            duration_project = in.readString();
            budget = in.readString();
            desired_location = in.readString();
            first_name = in.readString();
            ranking = in.readString();
            comment = in.readString();
            last_name = in.readString();
            skill = in.readString();
            role_id=in.readString();
        }

        public  final Creator<Candidate> CREATOR = new Creator<Candidate>() {
            @Override
            public Candidate createFromParcel(Parcel in) {
                return new Candidate(in);
            }

            @Override
            public Candidate[] newArray(int size) {
                return new Candidate[size];
            }
        };

        public String getJob_title() {
            return job_title;
        }


        public String getId() {
            return id;
        }

        public String getImage() {
            return image;
        }
        String id;
        String job_title;
        String image;
        String brif_bio;
        String qualification;
        String submit_id;
        String is_hired;
        String is_accepted;
        boolean is_interested;

        public boolean is_hotbook() {
            return is_hotbook;
        }

        public void setIs_hotbook(boolean is_hotbook) {
            this.is_hotbook = is_hotbook;
        }

        boolean is_hotbook;
        String email;
        public String getUsername() {
            return email;
        }

        public void setUsername(String username) {
            this.email = username;
        }

        boolean is_job;

        public boolean is_job() {
            return is_job;
        }

        public void setIs_job(boolean is_job) {
            this.is_job = is_job;
        }

        String duration_project;
        String budget;
        String role_id;

        public String getJob_id() {
            return job_id;
        }

        public void setJob_id(String job_id) {
            this.job_id = job_id;
        }

        String job_id;

        public String getRole_id() {
            return role_id;
        }

        String desired_location;
        String first_name,ranking,comment;

        public void setBrif_bio(String brif_bio) {
            this.brif_bio = brif_bio;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public void setSubmit_id(String submit_id) {
            this.submit_id = submit_id;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public void setDuration_project(String duration_project) {
            this.duration_project = duration_project;
        }

        public void setDesired_location(String desired_location) {
            this.desired_location = desired_location;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public void setRanking(String ranking) {
            this.ranking = ranking;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public void setSkill(String skill) {
            this.skill = skill;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setJob_title(String job_title) {
            this.job_title = job_title;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getFirst_name() {
            return first_name;
        }

        String last_name;

        public String getComment() {
            return comment;
        }

        public String getRanking() {
            return ranking;
        }

        public void setIs_interested(boolean is_interested) {
            this.is_interested = is_interested;
        }

        public String getIs_accepted() {
            return is_accepted;
        }

        public void setIs_accepted(String is_accepted) {
            this.is_accepted = is_accepted;
        }

        public String getIs_hired() {
            return is_hired;
        }

        public void setIs_hired(String is_hired) {
            this.is_hired = is_hired;
        }

        public String getDuration_project() {

            return duration_project;
        }

        public String getBudget() {
            return budget;
        }
        public String getDesired_location() {
            return desired_location;
        }
        public boolean is_interested() {
            return is_interested;
        }
        public String getSubmit_id() {
            return submit_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getBrif_bio() {
            return brif_bio;
        }
        public String getQualification() {
            return qualification;
        }
        public String getSkill() {
            return skill;
        }
        String skill;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(id);
            parcel.writeString(job_title);
            parcel.writeString(image);
            parcel.writeString(brif_bio);
            parcel.writeString(qualification);
            parcel.writeString(submit_id);
            parcel.writeString(is_hired);
            parcel.writeByte((byte) (is_interested ? 1 : 0));
            parcel.writeString(duration_project);
            parcel.writeString(budget);
            parcel.writeString(desired_location);
            parcel.writeString(first_name);
            parcel.writeString(ranking);
            parcel.writeString(comment);
            parcel.writeString(last_name);
            parcel.writeString(skill);
            parcel.writeString(role_id);
        }
    }
}
