package com.android.GsonResponse;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi on 7/11/2016.
 */
public class SearchResponse {
    boolean status;
    ArrayList<SearchJob> data;

    public ArrayList<SearchJob> getData() {
        return data;
    }



    public boolean isStatus() {
        return status;
    }
    public class SearchJob
    {
        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getUsername() {
            return username;
        }

        public String getEmail() {
            return email;
        }

        public String getAssign_id() {
            return assign_id;
        }

        public String getHours() {
            return hours;
        }

        public String getEntered() {
            return entered;
        }

        public String getCompleted() {
            return completed;
        }

        public String getDate() {
            return date;
        }

        public String getStatus() {
            return status;
        }

        public String getPhone() {
            return phone;
        }

        public String getJob_title() {
            return job_title;
        }

        String id;

        public String getId() {
            return id;
        }

        String first_name;
        String last_name;
        String username;
        String email;
        String phone;
        String job_title;
        String assign_id;
        String hours;
        String entered;
        String completed;
        String date;
        String status;
    }
}
