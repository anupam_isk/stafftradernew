package com.android.GsonResponse;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Anupam tyagi on 7/21/2016.
 */
public class EventResponse  {
    boolean status;
    ArrayList<Event> data;

    public ArrayList<Event> getData() {
        return data;
    }



    public boolean isStatus() {
        return status;
    }

    public class Event implements Parcelable
    {
        protected Event(Parcel in) {
            id = in.readString();
            user_id = in.readString();
            phone = in.readString();
            subject = in.readString();
            email = in.readString();
            meeting = in.readString();
            link_recruiter = in.readString();
            link_candidate = in.readString();
            comments = in.readString();
            date = in.readString();
            link_manager = in.readString();
            outlook = in.readString();
            reminder = in.readString();
            link_recruiter_name=in.readString();
            link_manager_name=in.readString();
            link_candidate_name=in.readString();
        }
        public  final Creator<Event> CREATOR = new Creator<Event>() {
            @Override
            public Event createFromParcel(Parcel in) {
                return new Event(in);
            }

            @Override
            public Event[] newArray(int size) {
                return new Event[size];
            }
        };

        @Override
        public void writeToParcel(Parcel parcel, int i) {

            parcel.writeString(id);
            parcel.writeString(user_id);
            parcel.writeString(phone);
            parcel.writeString(subject);
            parcel.writeString(email);
            parcel.writeString(meeting);
            parcel.writeString(link_recruiter);
            parcel.writeString(link_candidate);
            parcel.writeString(comments);
            parcel.writeString(date);
            parcel.writeString(link_manager);
            parcel.writeString(outlook);
            parcel.writeString(reminder);
            parcel.writeString(link_recruiter_name);
            parcel.writeString(link_manager_name);
            parcel.writeString(link_candidate_name);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public String getId() {
            return id;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getPhone() {
            return phone;
        }

        public String getSubject() {
            return subject;
        }

        public String getEmail() {
            return email;
        }

        public String getMeeting() {
            return meeting;
        }

        public String getLink_candidate() {
            return link_candidate;
        }

        public String getLink_recruiter() {
            return link_recruiter;
        }

        public String getComments() {
            return comments;
        }

        public String getDate() {
            return date;
        }

        public String getLink_manager() {
            return link_manager;
        }

        public String getOutlook() {
            return outlook;
        }

        public String getReminder() {
            return reminder;
        }

        String id;
        String user_id;
        String phone;
        String subject;
        String email;
        String meeting;
        String link_recruiter;
        String link_candidate;
        String comments;
        String date;
        String link_manager;
        String outlook;
        String reminder;
        String link_recruiter_name;
        String comment_status;

        public String getComment_status() {
            return comment_status;
        }

        public String getLink_candidate_name() {
            return link_candidate_name;
        }

        String link_candidate_name;

        public String getLink_manager_name() {
            return link_manager_name;
        }

        public String getLink_recruiter_name() {
            return link_recruiter_name;
        }

        String link_manager_name;
    }
}
