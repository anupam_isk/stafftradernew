package com.android.GsonResponse;

/**
 * Created by Anupam tyagi on 7/1/2016.
 */
public class LoginUserResponse {
    boolean status;
     UserData data;

    public UserData getData() {
        return data;
    }

    public boolean isStatus() {
        return status;
    }

   public  class UserData
    {
        String id;
        String first_name;
        String last_name;
        String password;
        String email;
        String phone;
        String token;
        String lat;
        String role_id;
        String image;
        String job_title;

        public String getImage() {
            return image;
        }

        public String getLon() {
            return lon;
        }

        public String getLat() {
            return lat;
        }

        public String getToken() {
            return token;
        }

        public String getRole_id() {
            return role_id;
        }

        String lon;

        public String getId() {
            return id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getPassword() {
            return password;
        }

        public String getEmail() {
            return email;
        }

        public String getPhone() {
            return phone;
        }

        public String getJob_title() {
            return job_title;
        }
    }
}
