package com.android.GsonResponse;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Admin on 09-Nov-16.
 */

public class MilestoneResponse {
    boolean status;
    ArrayList<MilestoneClass> data;

    public boolean isStatus() {
        return status;
    }

    public ArrayList<MilestoneClass> getData() {
        return data;
    }

    public class MilestoneClass implements Parcelable {
        String id;
        String milestone_name;
        String result;
        String start_date;
        String due_date;
        String milestone_date;

        public String getMilestone_date() {
            return milestone_date;
        }

        String comments;
        String link_candidate;
        String link_manager;
        String link_recruiter;
        String candidate_name;
        String manager_name;

        public String getManager_name() {
            return manager_name;
        }

        String weightage;
        String task_name;
        String status;

        public String getStatus() {
            return status;
        }

        String milestone_id;

        public String getTask_name() {
            return task_name;
        }

        public String getMilestone_id() {
            return milestone_id;
        }

        public String getWeightage() {
            return weightage;
        }

        public String getCandidate_name() {
            return candidate_name;
        }

        public String getRecruiter_name() {
            return recruiter_name;
        }

        String recruiter_name;

        public String getLink_candidate() {
            return link_candidate;
        }

        public String getLink_manager() {
            return link_manager;
        }

        public String getLink_recruiter() {
            return link_recruiter;
        }

        protected MilestoneClass(Parcel in) {
            id = in.readString();
            milestone_name = in.readString();
            result = in.readString();
            start_date = in.readString();
            due_date = in.readString();
            comments = in.readString();
            link_candidate = in.readString();
            link_manager = in.readString();
            link_recruiter = in.readString();
            candidate_name = in.readString();
            recruiter_name = in.readString();
            weightage = in.readString();
            task_name = in.readString();
            milestone_id = in.readString();
            status = in.readString();
            milestone_date = in.readString();
            manager_name = in.readString();
        }

        public final Creator<MilestoneClass> CREATOR = new Creator<MilestoneClass>() {
            @Override
            public MilestoneClass createFromParcel(Parcel in) {
                return new MilestoneClass(in);
            }

            @Override
            public MilestoneClass[] newArray(int size) {
                return new MilestoneClass[size];
            }
        };

        public String getResult() {
            return result;
        }

        public String getId() {
            return id;
        }

        public String getMilestone_name() {
            return milestone_name;
        }

        public String getComments() {
            return comments;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        public String getStart_date() {
            return start_date;
        }

        public String getDue_date() {
            return due_date;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(id);
            parcel.writeString(milestone_name);
            parcel.writeString(result);
            parcel.writeString(start_date);
            parcel.writeString(due_date);
            parcel.writeString(comments);
            parcel.writeString(link_candidate);
            parcel.writeString(link_recruiter);
            parcel.writeString(milestone_date);
            parcel.writeString(link_manager);
            parcel.writeString(recruiter_name);
            parcel.writeString(candidate_name);
            parcel.writeString(weightage);
            parcel.writeString(milestone_id);
            parcel.writeString(task_name);
            parcel.writeString(status);
            parcel.writeString(manager_name);
        }
    }

}
